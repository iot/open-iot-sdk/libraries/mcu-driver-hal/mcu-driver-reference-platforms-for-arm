/* Copyright (c) 2022-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_CONF_MBED_TRACE_ENABLE 1
#define TRACE_GROUP                 "main"

#include "fpga_ci_test_shield/UARTTester.h"
#include "greentea-client/test_env.h"
#include "greentea-custom_io/custom_io.h"
#include "test_serial.h"
#include "test_utils/mbed_tester_platform_handlers_for_an552fpga.h"
#include "test_utils/utest_handlers_for_an552fpga.h"
#include "unity.h"
#include "utest/utest.h"

extern "C" {
#include "hal/serial_api.h"
#include "hal/us_ticker_api.h"
#include "gpio_objects.h"
#include "mbed_trace/mbed_trace.h"
#include "mps3_io.h"
#include "mps3_uart.h"
#include "ticker.h"
}

#include <inttypes.h>

using namespace utest::v1;

static void console_print(const char *const message);
static void test_run(uint32_t baudrate, uint8_t data_bits, mdh_serial_parity_t parity, uint8_t stop_bits);
static void test_9600_8_n_1(void);
static void test_19200_8_n_1(void);
static void test_38400_8_n_1(void);
static void test_115200_8_n_1(void);

static utest::v1::status_t greentea_test_setup(const size_t number_of_cases);

static mdh_serial_t *gs_serial = NULL;
static Case gs_test_cases[] = {Case("9600-8-N-1", test_9600_8_n_1),
                               Case("19200-8-N-1", test_19200_8_n_1),
                               Case("38400-8-N-1", test_38400_8_n_1),
                               Case("115200-8-N-1", test_115200_8_n_1)};

static Specification gs_specification(greentea_test_setup, gs_test_cases, greentea_test_teardown_handler);

static void console_print(const char *const message)
{
    const char *c = message;

    while (*c != '\0') {
        mdh_serial_put_data(gs_serial, *c);
        c++;
    }

    mdh_serial_put_data(gs_serial, '\r');
    mdh_serial_put_data(gs_serial, '\n');
}

#if defined(UART3_CMSDK_NS)
static void test_run(uint32_t baudrate, uint8_t data_bits, mdh_serial_parity_t parity, uint8_t stop_bits)
{
    mps3_io_t aux;
    PinName aux_control_pin = D0_5;
    TEST_ASSERT_TRUE(mps3_io_init(&aux, &GPIO0_CMSDK_DEV, aux_control_pin));

    mps3_io_t sdi;
    TEST_ASSERT_TRUE(mps3_io_init(&sdi, &GPIO0_CMSDK_DEV, (PinName)(aux_control_pin - 1)));

    mps3_io_t sdo;
    TEST_ASSERT_TRUE(mps3_io_init(&sdo, &GPIO0_CMSDK_DEV, (PinName)(aux_control_pin - 2)));

    mps3_io_t clk;
    TEST_ASSERT_TRUE(mps3_io_init(&clk, &GPIO0_CMSDK_DEV, (PinName)(aux_control_pin - 3)));

    MbedTester::ControlChannel control_channel = {
        .clk = {.dut_gpio = &clk.gpio, .tester_pin_index = (MbedTester::PhysicalIndex)GPIO_PIN_NUMBER(clk.pin)},
        .sdo = {.dut_gpio = &sdo.gpio, .tester_pin_index = (MbedTester::PhysicalIndex)GPIO_PIN_NUMBER(sdo.pin)},
        .sdi = {.dut_gpio = &sdi.gpio, .tester_pin_index = (MbedTester::PhysicalIndex)GPIO_PIN_NUMBER(sdi.pin)},
        .aux = {.dut_gpio = &aux.gpio, .tester_pin_index = (MbedTester::PhysicalIndex)GPIO_PIN_NUMBER(aux.pin)}};
    MbedTester::PlatformHandlers &an552fpga_handlers = get_mbed_tester_platform_handlers_for_an552fpga();
    UARTTester tester(control_channel, an552fpga_handlers);

    tester.reset();
    TEST_ASSERT_TRUE_MESSAGE(tester.verify_control_channel(),
                             "FPGA CI Test Shield not responding. Verify control channel config.");

    mdh_serial_t *serial_port = NULL;
    TEST_ASSERT_TRUE(mps3_uart_init(&serial_port, &UART3_CMSDK_DEV_NS));

    tester.pin_map_set(mps3_uart_get_tx_pin(&UART3_CMSDK_DEV_NS), MbedTester::LogicalPinUARTRx);
    tester.pin_map_set(mps3_uart_get_rx_pin(&UART3_CMSDK_DEV_NS), MbedTester::LogicalPinUARTTx);

    test_serial_transfers(
        tester, serial_port, baudrate, data_bits, parity, stop_bits, false, bsp_us_ticker_get_default_instance());

    tester.reset();

    TEST_ASSERT_TRUE(mps3_uart_deinit(&UART3_CMSDK_DEV_NS));
}
#else
#error The FPGA CI Test shield is required on the Non-Secure UART 3 device to build this test.
#endif // defined(UART3_CMSDK_NS)

static void test_9600_8_n_1(void)
{
    test_run(9600UL, 8U, MDH_SERIAL_PARITY_NONE, 1U);
}

static void test_19200_8_n_1(void)
{
    test_run(19200UL, 8U, MDH_SERIAL_PARITY_NONE, 1U);
}

static void test_38400_8_n_1(void)
{
    test_run(38400UL, 8U, MDH_SERIAL_PARITY_NONE, 1U);
}

static void test_115200_8_n_1(void)
{
    test_run(115200UL, 8U, MDH_SERIAL_PARITY_NONE, 1U);
}

static utest::v1::status_t greentea_test_setup(const size_t number_of_cases)
{
    GREENTEA_SETUP(240, "default_auto");
    return greentea_test_setup_handler_for_an552fpga_with_fpga_ci_test_shield(number_of_cases);
}

int main(void)
{
    mps3_uart_init(&gs_serial, &UART0_CMSDK_DEV_NS);
    mdh_serial_set_baud(gs_serial, 115200);
    greentea_init_custom_io(gs_serial);

    mbed_trace_init();
    mbed_trace_print_function_set(console_print);
    bsp_us_ticker_init(bsp_us_ticker_get_default_instance());

    Harness::run(gs_specification);
}
