/* Copyright (c) 2022-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_CONF_MBED_TRACE_ENABLE 1
#define TRACE_GROUP                 "main"

#include "PinNames.h"
#include "fpga_ci_test_shield/SPIControllerTester.h"
#include "greentea-client/test_env.h"
#include "greentea-custom_io/custom_io.h"
#include "test_spi.h"
#include "test_utils/mbed_tester_platform_handlers_for_an552fpga.h"
#include "test_utils/utest_handlers_for_an552fpga.h"
#include "unity.h"
#include "utest/utest.h"

extern "C" {
#include "hal/serial_api.h"
#include "hal/spi_api.h"
#include "hal/us_ticker_api.h"
#include "gpio_objects.h"
#include "mbed_trace/mbed_trace.h"
#include "mps3_io.h"
#include "mps3_spi.h"
#include "mps3_uart.h"
#include "ticker.h"
}

#include <inttypes.h>

using namespace utest::v1;

static void console_print(const char *const message);
static void test_run(mdh_test_spi_configuration_t &test_configuration);
static void test_mode_0(void);
static void test_mode_1(void);
static void test_data_word_size_8(void);
static void test_data_word_size_16(void);
static void test_frequency_200Khz(void);
static void test_frequency_2Mhz(void);
static void test_frequency_min(void);
static void test_frequency_max(void);
static void test_block_write(void);
static void test_block_one_data_word_transfer(void);
static utest::v1::status_t greentea_test_setup(const size_t number_of_cases);

static mdh_serial_t *gs_serial = NULL;
static Case gs_test_cases[] = {
    // This will be run for all peripherals
    Case("SPI - mode testing (MODE_0)", test_mode_0),
    Case("SPI - mode testing (MODE_1)", test_mode_1),

    Case("SPI - data word size (8)", test_data_word_size_8),
    Case("SPI - data word size (16)", test_data_word_size_16),

    Case("SPI - frequency testing (200 kHz)", test_frequency_200Khz),
    Case("SPI - frequency testing (2 MHz)", test_frequency_2Mhz),
    Case("SPI - Min capability frequency testing", test_frequency_min),
    Case("SPI - Max capability frequency testing", test_frequency_max),

    Case("SPI - block write", test_block_write),
    Case("SPI - block write(one sym)", test_block_one_data_word_transfer),
};

static Specification gs_specification(greentea_test_setup, gs_test_cases, greentea_test_teardown_handler);

static void console_print(const char *const message)
{
    const char *c = message;

    while (*c != '\0') {
        mdh_serial_put_data(gs_serial, *c);
        c++;
    }

    mdh_serial_put_data(gs_serial, '\r');
    mdh_serial_put_data(gs_serial, '\n');
}

#if defined(SPI_SHIELD0_PL022_NS)
static void test_run(mdh_test_spi_configuration_t &test_configuration)
{
    mps3_io_t aux;
    PinName aux_control_pin = D0_3;
    TEST_ASSERT_TRUE(mps3_io_init(&aux, &GPIO0_CMSDK_DEV, aux_control_pin));

    mps3_io_t sdi;
    TEST_ASSERT_TRUE(mps3_io_init(&sdi, &GPIO0_CMSDK_DEV, (PinName)(aux_control_pin - 1)));

    mps3_io_t sdo;
    TEST_ASSERT_TRUE(mps3_io_init(&sdo, &GPIO0_CMSDK_DEV, (PinName)(aux_control_pin - 2)));

    mps3_io_t clk;
    TEST_ASSERT_TRUE(mps3_io_init(&clk, &GPIO0_CMSDK_DEV, (PinName)(aux_control_pin - 3)));

    MbedTester::ControlChannel control_channel = {
        .clk = {.dut_gpio = &clk.gpio, .tester_pin_index = (MbedTester::PhysicalIndex)GPIO_PIN_NUMBER(clk.pin)},
        .sdo = {.dut_gpio = &sdo.gpio, .tester_pin_index = (MbedTester::PhysicalIndex)GPIO_PIN_NUMBER(sdo.pin)},
        .sdi = {.dut_gpio = &sdi.gpio, .tester_pin_index = (MbedTester::PhysicalIndex)GPIO_PIN_NUMBER(sdi.pin)},
        .aux = {.dut_gpio = &aux.gpio, .tester_pin_index = (MbedTester::PhysicalIndex)GPIO_PIN_NUMBER(aux.pin)}};
    MbedTester::PlatformHandlers &an552fpga_handlers = get_mbed_tester_platform_handlers_for_an552fpga();
    SPIControllerTester tester(control_channel, an552fpga_handlers);

    tester.reset();
    TEST_ASSERT_TRUE_MESSAGE(tester.verify_control_channel(),
                             "FPGA CI Test Shield not responding. Verify control channel config.");

    tester.pin_map_set(mps3_spi_get_sdo_pin(&SPI_SHIELD0_PL022_DEV_NS), MbedTester::LogicalPinSPISDO);
    tester.pin_map_set(mps3_spi_get_sdi_pin(&SPI_SHIELD0_PL022_DEV_NS), MbedTester::LogicalPinSPISDI);
    tester.pin_map_set(mps3_spi_get_sck_pin(&SPI_SHIELD0_PL022_DEV_NS), MbedTester::LogicalPinSPISCLK);

    mps3_io_t n_chip_select;
    TEST_ASSERT_TRUE(mps3_io_init(&n_chip_select, &GPIO0_CMSDK_DEV, mps3_spi_get_cs_pin(&SPI_SHIELD0_PL022_DEV_NS)));

    mps3_spi_t *spi_port = NULL;
    TEST_ASSERT_TRUE(mps3_spi_init(&spi_port, &SPI_SHIELD0_PL022_DEV));
    test_spi_transfer(tester, &(n_chip_select.gpio), &(spi_port->spi), test_configuration);

    tester.reset();

    TEST_ASSERT_TRUE(mps3_spi_deinit(&SPI_SHIELD0_PL022_DEV));
}
#else
#error The Non-secure ARM PrimeCell Synchronous Serial Port (PL022) device is required on SHIELD 0 to build this test.
#endif // defined(SPI_SHIELD0_PL022_NS)

static void test_mode_0(void)
{
    mdh_test_spi_configuration_t test_configuration = {
        .mode = MDH_SPI_MODE_0,
        .word_size = 8U,
        .transfer_type = MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_WRITE,
        .frequency = MDH_TEST_SPI_FREQ_1_MHZ,
        .buffer_sizes = MDH_TEST_SPI_BUF_SIZES_EQUAL,
        .use_hardware_chip_select = false,
    };
    test_run(test_configuration);
}

static void test_mode_1(void)
{
    mdh_test_spi_configuration_t test_configuration = {
        .mode = MDH_SPI_MODE_1,
        .word_size = 8U,
        .transfer_type = MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_WRITE,
        .frequency = MDH_TEST_SPI_FREQ_1_MHZ,
        .buffer_sizes = MDH_TEST_SPI_BUF_SIZES_EQUAL,
        .use_hardware_chip_select = false,
    };
    test_run(test_configuration);
}

static void test_data_word_size_8(void)
{
    mdh_test_spi_configuration_t test_configuration = {
        .mode = MDH_SPI_MODE_0,
        .word_size = 8U,
        .transfer_type = MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_WRITE,
        .frequency = MDH_TEST_SPI_FREQ_1_MHZ,
        .buffer_sizes = MDH_TEST_SPI_BUF_SIZES_EQUAL,
        .use_hardware_chip_select = false,
    };
    test_run(test_configuration);
}

static void test_data_word_size_16(void)
{
    mdh_test_spi_configuration_t test_configuration = {
        .mode = MDH_SPI_MODE_0,
        .word_size = 16U,
        .transfer_type = MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_WRITE,
        .frequency = MDH_TEST_SPI_FREQ_1_MHZ,
        .buffer_sizes = MDH_TEST_SPI_BUF_SIZES_EQUAL,
        .use_hardware_chip_select = false,
    };
    test_run(test_configuration);
}

static void test_frequency_200Khz(void)
{
    mdh_test_spi_configuration_t test_configuration = {
        .mode = MDH_SPI_MODE_0,
        .word_size = 8U,
        .transfer_type = MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_WRITE,
        .frequency = MDH_TEST_SPI_FREQ_200_KHZ,
        .buffer_sizes = MDH_TEST_SPI_BUF_SIZES_EQUAL,
        .use_hardware_chip_select = false,
    };
    test_run(test_configuration);
}

static void test_frequency_2Mhz(void)
{
    mdh_test_spi_configuration_t test_configuration = {
        .mode = MDH_SPI_MODE_0,
        .word_size = 8U,
        .transfer_type = MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_WRITE,
        .frequency = MDH_TEST_SPI_FREQ_2_MHZ,
        .buffer_sizes = MDH_TEST_SPI_BUF_SIZES_EQUAL,
        .use_hardware_chip_select = false,
    };
    test_run(test_configuration);
}

static void test_frequency_min(void)
{
    mdh_test_spi_configuration_t test_configuration = {
        .mode = MDH_SPI_MODE_0,
        .word_size = 8U,
        .transfer_type = MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_WRITE,
        .frequency = MDH_TEST_SPI_FREQ_MIN,
        .buffer_sizes = MDH_TEST_SPI_BUF_SIZES_EQUAL,
        .use_hardware_chip_select = false,
    };
    test_run(test_configuration);
}

static void test_frequency_max(void)
{
    mdh_test_spi_configuration_t test_configuration = {
        .mode = MDH_SPI_MODE_0,
        .word_size = 8U,
        .transfer_type = MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_WRITE,
        .frequency = MDH_TEST_SPI_FREQ_MAX,
        .buffer_sizes = MDH_TEST_SPI_BUF_SIZES_EQUAL,
        .use_hardware_chip_select = false,
    };
    test_run(test_configuration);
}

static void test_block_write(void)
{
    mdh_test_spi_configuration_t test_configuration = {
        .mode = MDH_SPI_MODE_0,
        .word_size = 8U,
        .transfer_type = MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_BLOCK_WRITE,
        .frequency = MDH_TEST_SPI_FREQ_1_MHZ,
        .buffer_sizes = MDH_TEST_SPI_BUF_SIZES_EQUAL,
        .use_hardware_chip_select = false,
    };
    test_run(test_configuration);
}

static void test_block_one_data_word_transfer(void)
{
    mdh_test_spi_configuration_t test_configuration = {
        .mode = MDH_SPI_MODE_0,
        .word_size = 8U,
        .transfer_type = MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_BLOCK_WRITE,
        .frequency = MDH_TEST_SPI_FREQ_1_MHZ,
        .buffer_sizes = MDH_TEST_SPI_BUF_SIZES_WRITE_ONE_WORD,
        .use_hardware_chip_select = false,
    };
    test_run(test_configuration);
}

static utest::v1::status_t greentea_test_setup(const size_t number_of_cases)
{
    GREENTEA_SETUP(60, "default_auto");
    return greentea_test_setup_handler_for_an552fpga_with_fpga_ci_test_shield(number_of_cases);
}

int main(void)
{
    mps3_uart_init(&gs_serial, &UART0_CMSDK_DEV_NS);
    mdh_serial_set_baud(gs_serial, 115200);
    greentea_init_custom_io(gs_serial);

    mbed_trace_init();
    mbed_trace_print_function_set(console_print);
    bsp_us_ticker_init(bsp_us_ticker_get_default_instance());

    Harness::run(gs_specification);
}
