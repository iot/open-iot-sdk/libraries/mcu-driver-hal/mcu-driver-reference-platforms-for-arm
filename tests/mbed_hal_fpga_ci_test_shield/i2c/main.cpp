/* Copyright (c) 2022-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_CONF_MBED_TRACE_ENABLE 1
#define TRACE_GROUP                 "main"

#include "fpga_ci_test_shield/I2CTester.h"
#include "greentea-client/test_env.h"
#include "greentea-custom_io/custom_io.h"
#include "test_i2c.h"
#include "test_utils/mbed_tester_platform_handlers_for_an552fpga.h"
#include "test_utils/utest_handlers_for_an552fpga.h"
#include "unity.h"
#include "utest/utest.h"

extern "C" {
#include "hal/serial_api.h"
#include "hal/us_ticker_api.h"
#include "gpio_objects.h"
#include "mbed_trace/mbed_trace.h"
#include "mps3_i2c.h"
#include "mps3_io.h"
#include "mps3_uart.h"
#include "ticker.h"
}

#include <inttypes.h>

using namespace utest::v1;

static void console_print(const char *const message);
static void test_write(void);
static void test_read(void);
static void test_write_byte(void);
static void test_read_byte(void);
static void run_test_case(void (*test_case_function)(I2CTester &tester, mdh_i2c_t *i2c));
static utest::v1::status_t greentea_test_setup(const size_t number_of_cases);

static mdh_serial_t *gs_serial = NULL;
static Case gs_test_cases[] = {Case("I2C - Multiple byte write", test_write),
                               Case("I2C - Multiple byte read", test_read),
                               Case("I2C - Single byte write", test_write_byte),
                               Case("I2C - Single byte read", test_read_byte)};
static Specification gs_specification(greentea_test_setup, gs_test_cases, greentea_test_teardown_handler);

static void console_print(const char *const message)
{
    const char *c = message;

    while (*c != '\0') {
        mdh_serial_put_data(gs_serial, *c);
        c++;
    }

    mdh_serial_put_data(gs_serial, '\r');
    mdh_serial_put_data(gs_serial, '\n');
}

int main(void)
{
    mps3_uart_init(&gs_serial, &UART0_CMSDK_DEV_NS);
    mdh_serial_set_baud(gs_serial, 115200);
    greentea_init_custom_io(gs_serial);

    mbed_trace_init();
    mbed_trace_print_function_set(console_print);
    bsp_us_ticker_init(bsp_us_ticker_get_default_instance());

    Harness::run(gs_specification);
}

static utest::v1::status_t greentea_test_setup(const size_t number_of_cases)
{
    GREENTEA_SETUP(30, "default_auto");
    return greentea_test_setup_handler_for_an552fpga_with_fpga_ci_test_shield(number_of_cases);
}

#if defined(I2C2_SBCON_NS)
static void run_test_case(void (*test_case_function)(I2CTester &tester, mdh_i2c_t *i2c))
{
    mps3_io_t aux;
    PinName aux_control_pin = D0_3;
    TEST_ASSERT_TRUE(mps3_io_init(&aux, &GPIO0_CMSDK_DEV, aux_control_pin));

    mps3_io_t sdi;
    TEST_ASSERT_TRUE(mps3_io_init(&sdi, &GPIO0_CMSDK_DEV, (PinName)(aux_control_pin - 1)));

    mps3_io_t sdo;
    TEST_ASSERT_TRUE(mps3_io_init(&sdo, &GPIO0_CMSDK_DEV, (PinName)(aux_control_pin - 2)));

    mps3_io_t clk;
    TEST_ASSERT_TRUE(mps3_io_init(&clk, &GPIO0_CMSDK_DEV, (PinName)(aux_control_pin - 3)));

    MbedTester::ControlChannel control_channel = {
        .clk = {.dut_gpio = &clk.gpio, .tester_pin_index = (MbedTester::PhysicalIndex)GPIO_PIN_NUMBER(clk.pin)},
        .sdo = {.dut_gpio = &sdo.gpio, .tester_pin_index = (MbedTester::PhysicalIndex)GPIO_PIN_NUMBER(sdo.pin)},
        .sdi = {.dut_gpio = &sdi.gpio, .tester_pin_index = (MbedTester::PhysicalIndex)GPIO_PIN_NUMBER(sdi.pin)},
        .aux = {.dut_gpio = &aux.gpio, .tester_pin_index = (MbedTester::PhysicalIndex)GPIO_PIN_NUMBER(aux.pin)}};
    MbedTester::PlatformHandlers &an552fpga_handlers = get_mbed_tester_platform_handlers_for_an552fpga();
    I2CTester tester(control_channel, an552fpga_handlers);

    tester.reset();
    TEST_ASSERT_TRUE_MESSAGE(tester.verify_control_channel(),
                             "FPGA CI Test Shield not responding. Verify control channel config.");

    PinName sda = mps3_i2c_get_sda_pin(&I2C2_SBCON_DEV);
    PinName scl = mps3_i2c_get_scl_pin(&I2C2_SBCON_DEV);
    tester.pin_map_set(sda, MbedTester::LogicalPinI2CSda);
    tester.pin_map_set(scl, MbedTester::LogicalPinI2CScl);

    tester.pin_set_pull(sda, MbedTester::PullUp);
    tester.pin_set_pull(scl, MbedTester::PullUp);

    mps3_i2c_t *i2c_port = NULL;
    TEST_ASSERT_TRUE(mps3_i2c_init(&i2c_port, &I2C2_SBCON_DEV));
    test_case_function(tester, &(i2c_port->i2c));

    tester.reset();
    tester.pin_set_pull(sda, MbedTester::PullNone);
    tester.pin_set_pull(scl, MbedTester::PullNone);
}
#else
#error The FPGA CI Test shield is required on the Non-Secure SBCon 2 device to build this
#endif // defined(I2C2_SBCON_NS)

static void test_write(void)
{
    run_test_case(test_i2c_write);
}

static void test_read(void)
{
    run_test_case(test_i2c_read);
}

static void test_write_byte(void)
{
    run_test_case(test_i2c_write_byte);
}

static void test_read_byte(void)
{
    run_test_case(test_i2c_read_byte);
}
