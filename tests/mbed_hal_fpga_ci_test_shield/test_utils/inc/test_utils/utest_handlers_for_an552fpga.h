/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef UTEST_HANDLERS_FOR_AN552FPGA_H
#define UTEST_HANDLERS_FOR_AN552FPGA_H

#include "utest/utest.h"
#include <cstddef>

/* Platform-specific greentea test suite setup handler
 *
 * NOTE: Specific to AN552, FPGA variant, being tested with FPGA CI Test Shield.
 *
 * Find a working control channel, use it to communicate with the FPGA CI Test
 * Shield, and bring the shield to a known state by performing a reset. Abort
 * test suite if unable to communicate with the shield.
 */
utest::v1::status_t greentea_test_setup_handler_for_an552fpga_with_fpga_ci_test_shield(const size_t number_of_cases);

#endif // UTEST_HANDLERS_FOR_AN552FPGA_H
