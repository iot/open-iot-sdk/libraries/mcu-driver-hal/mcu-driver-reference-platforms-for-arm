/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef MBED_TESTER_PLATFORM_HANDLERS_FOR_AN552_H
#define MBED_TESTER_PLATFORM_HANDLERS_FOR_AN552_H

#include "fpga_ci_test_shield/MbedTester.h"

MbedTester::PlatformHandlers &get_mbed_tester_platform_handlers_for_an552fpga(void);

#endif // MBED_TESTER_PLATFORM_HANDLERS_FOR_AN552_H
