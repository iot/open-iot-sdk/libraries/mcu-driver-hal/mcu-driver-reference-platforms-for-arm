/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "test_utils/mbed_tester_platform_handlers_for_an552fpga.h"

#include "fpga_ci_test_shield/MbedTester.h"
#include "platform/busy_wait.h"
#include "platform/error_handler.h"

MbedTester::PlatformHandlers &get_mbed_tester_platform_handlers_for_an552fpga(void)
{
    static MbedTester::PlatformHandlers an552fpga_handlers = {
        .error = halt_on_error, .wait_us = wait_us, .wait_ns = wait_ns};
    return an552fpga_handlers;
}
