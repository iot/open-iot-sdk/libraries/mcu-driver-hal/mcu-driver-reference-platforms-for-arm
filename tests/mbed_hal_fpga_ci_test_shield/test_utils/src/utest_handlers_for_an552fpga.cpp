/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "test_utils/utest_handlers_for_an552fpga.h"

#include "PinNames.h"
#include "fpga_ci_test_shield/MbedTester.h"
#include "test_utils/mbed_tester_platform_handlers_for_an552fpga.h"
#include "utest/utest.h"

extern "C" {
#include "mps3_io.h"
}

#include <cinttypes>
#include <cstddef>

static const PinName arduino_uno_pins[] = {
    D0_0, D0_1, D0_2, D0_3, D0_4, D0_5, D0_6, D0_7, D0_8, D0_9, D0_10, D0_11, D0_12, D0_13, D0_14, D0_15};
static const size_t num_arduino_uno_pins = sizeof arduino_uno_pins / sizeof arduino_uno_pins[0];

utest::v1::status_t greentea_test_setup_handler_for_an552fpga_with_fpga_ci_test_shield(const size_t number_of_cases)
{
    /* FPGA CI Test Shield does not have a dedicated set of control pins.
     * Instead, it is actively listening for SPI control communication on all the
     * pins. There are certain requirements for the control channel detection
     * mechanism to work:
     * - CLK has to be set on even pin (D0, D2, D(n*2) in case of Arduino Uno pinout),
     * - SDO has to be set on odd pin, immediately following CLK.
     * There are no requirements for SDI and AUX pins, however, for simplicity,
     * this function sets them to CLK+2 and CLK+3 respectively.
     *
     * FPGA CI Test Shield documentation:
     * https://github.com/ARMmbed/fpga-ci-test-shield/blob/master/docs/design_document.md#control-manager
     *
     * If the test shield was not reset during test teardown (e.g. when the test
     * was aborted because of a timeout) it may not detect a new control channel
     * on pins that have been previously mapped to peripheral-specific
     * functions, e.g. UART-RX. This is why we iterate over all the pins to
     * find a working set. After a reset, when all mappings are cleared, the
     * test may use any set of pins for communication channel again.
     */
    mps3_io_t clk, sdo, sdi, aux;
    for (MbedTester::PhysicalIndex i_clk = 0, i_sdo = 1, i_sdi = 2, i_aux = 3; i_aux < num_arduino_uno_pins;
         i_clk += 2, i_sdo += 2, i_sdi += 2, i_aux += 2) {
        mps3_io_init(&clk, &GPIO0_CMSDK_DEV, arduino_uno_pins[i_clk]);
        mps3_io_init(&sdo, &GPIO0_CMSDK_DEV, arduino_uno_pins[i_sdo]);
        mps3_io_init(&sdi, &GPIO0_CMSDK_DEV, arduino_uno_pins[i_sdi]);
        mps3_io_init(&aux, &GPIO0_CMSDK_DEV, arduino_uno_pins[i_aux]);
        MbedTester::ControlChannel control_channel = {.clk = {.dut_gpio = &clk.gpio, .tester_pin_index = i_clk},
                                                      .sdo = {.dut_gpio = &sdo.gpio, .tester_pin_index = i_sdo},
                                                      .sdi = {.dut_gpio = &sdi.gpio, .tester_pin_index = i_sdi},
                                                      .aux = {.dut_gpio = &aux.gpio, .tester_pin_index = i_aux}};
        MbedTester::PlatformHandlers &an552fpga_handlers = get_mbed_tester_platform_handlers_for_an552fpga();
        MbedTester tester(control_channel, an552fpga_handlers);
        tester.reset();
        if (tester.verify_control_channel()) {
            utest_printf("Successfully reset FPGA CI Test Shield using control pins CLK=%zu, SDO=%zu, SDI=%zu, "
                         "AUX=%zu. Firmware v%" PRIu32 ".\n",
                         i_clk,
                         i_sdo,
                         i_sdi,
                         i_aux,
                         tester.version());
            return utest::v1::greentea_test_setup_handler(number_of_cases);
        }
    }
    utest_printf("Unable to reset FPGA CI Test Shield. Aborting.");
    return utest::v1::STATUS_ABORT;
}
