# MCU-Driver-HAL Tests

This directory contains the build instructions for the tests defined in the _MCU-Driver-HAL_ repository.

Each test build instructions provides access to hardware specific source code and link with the example main algorithm provided as a library that is linked to the executable.

## Setup


1. Clone the repository **recursively** to fetch both `mcu-driver-hal` and `mcu-driver-hal/tools/greentea-client`:

    ```
    $ git clone --recursive <HostingLocation>/mcu-driver-reference-platforms-for-arm.git
    ```

## Building the tests

Each test can be built by doing the following:

1. Change the current working directory to `mcu-driver-reference-platforms-for-arm/`.
1. Generate the build system files:

    ```
    $ cmake -S ./ -B cmake_build/ --toolchain=components/mcu-driver-hal/tools/cmake/toolchain/toolchain-{armclang|arm-none-eabi-gcc}.cmake -DCMAKE_SYSTEM_PROCESSOR=cortex-m55 -DCMAKE_BUILD_TYPE=<build_profile> -DHTRUN_ARGUMENTS="--disk=<path_to_target_mount_point>;--port=<path_to_target_serial_port>;--baud-rate=<baudrate>;--enum-host-tests=<path_to_host_tests_dir>"
    ```

    Available toolchain files can be seen [here](../components/mcu-driver-hal/tools/cmake/toolchain).
    See the files [here](../components/mcu-driver-hal/tools/cmake/profiles) for possible `<build_profile>` values.


    The `HTRUN_ARGUMENTS` variable is a semicolon separated list of arguments to forward to Htrun. Execute `htrun --help` to see possible options. `--disk`, `--port` and `--baud-rate` have to be set by the user. The location of the host tests scripts is provided in CMake; you can extend the location to look for the scripts with additional `--enum-host-tests` arguments to the  `HTRUN_ARGUMENTS` CMake variable.

    If you are using a non-default build system, and you have not set the environment variable, then you can append `-G <build_system>` to the command above.

    See a complete configure command example below:

    ```
    $ cmake -S ./ -B cmake_build/ -G Ninja -DHTRUN_ARGUMENTS="--disk=/media/$USER/DIS_L4IOT;--port=/dev/ttyACM0;--baud-rate=115200"
    ```

1. Build:

    ```
    $ cmake --build cmake_build/ --target <test-cmake-executable-name>
    ```

    `<test-cmake-executable-name>` is the name of the CMake executable for the test you would like to build.

    1. Building all the tests at once

        Skip the `--target` argument. Run:

        ```
        $ cmake --build cmake_build/
        ```

Outcome: The tests compile, link and produce artefacts.

## Running the tests

1. Change the current working directory to `mcu-driver-reference-platforms-for-arm/`.

1. Use CTest to flash and run the test:

    ```
    $ ctest --test-dir cmake_build/ --tests-regex <test-cmake-executable-name> --verbose
    ```

    where `<test-cmake-executable-name>` is the name of the CMake executable for the test you would like to run.

    1. Running all the tests

        Skip the `--tests-regex` argument. Run:

        ```
        $ ctest --test-dir cmake_build/ --verbose
        ```

## Listing the tests

1. Change the current working directory to `mcu-driver-reference-platforms-for-arm/`.

1. Use CTest to list the tests:

    ```
    $ ctest --test-dir cmake_build/ --show-only
    ```

---

**Warning**

`htrun` uses DAPLink to program the binary image to the board by copying it to the mount point specified. It also uses DAPLink to reset the target before running the test. However, due to the incomplete DAPLink support for the Arm MPS3 FPGA board, programming and reseting the board is not possible.

Follow the steps [here][mdh_vscode] to load the image to the board using pyOCD.

After programming the board, run `htrun` as follows to run the test loaded on the board:

```
$ htrun --enum-host-tests=mcu-driver-hal/tests/host_tests/ --port=<path_to_target_serial_port> --baud-rate=115200 --skip-flashing
```

## EMAC Test Notes

When running EMAC Tests, please keep in mind that:

* `ctest` should be run with super user privileges because EMAC tests start the
   [Python CTP Server](../components/mcu-driver-hal/tests/host_tests/emac_ctp_server.py) which needs
   these permissions.
   ```
   $ sudo -E env "PATH=$PATH" ctest --test-dir cmake_build/ --tests-regex <test-cmake-executable-name>>
   ```
* Before starting EMAC tests, remember to setup your host environment properly and start up a TAP
  Network. The test assumes that there is a virtual (tap) interface named "ARMfmuser" which is used
  by the FVP and a bridge named "armbr0" to which the tap interface connects. The server runs on the
  "armbr0" bridge. If you run the test with a physical board, you don't need to setup the tap
  interface. Detailed instructions are available [here](https://gitlab.arm.com/iot/open-iot-sdk/mcu-driver-hal/mcu-driver-hal/-/blob/main/tests/host_tests/ctp_server/README.md).

[mdh_vscode]: https://gitlab.arm.com/iot/open-iot-sdk/mcu-driver-hal/mcu-driver-hal/-/blob/main/docs/user/debugging_with_visual_studio_code.md
