/* Copyright (c) 2022, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "greentea-client/test_env.h"
#include "greentea-custom_io/custom_io.h"
#include "test_flash_functional.h"
#include "utest/utest.h"

extern "C" {
#include "hal/flash_api.h"
#include "hal/serial_api.h"
#include "hal/us_ticker_api.h"
#include "flash_cs300.h"
#include "mps3_uart.h"
#include "ticker.h"
}

using namespace utest::v1;

static void test_flash_mapping_alignment(void);
static void test_flash_erase_sector(void);
static void test_flash_program_page(void);
static void test_flash_clock_and_cache(void);
static utest::v1::status_t setup(const size_t number_of_cases);
static void teardown(const size_t passed, const size_t failed, const failure_t failure);

static mdh_serial_t *gs_serial = NULL;
static Case gs_test_cases[] = {
    Case("Flash - mapping alignment", test_flash_mapping_alignment),
    Case("Flash - erase sector", test_flash_erase_sector),
    Case("Flash - program page", test_flash_program_page),
    Case("Flash - clock and cache test", test_flash_clock_and_cache),
};

static Specification gs_specification(setup, gs_test_cases, teardown);

static void test_flash_mapping_alignment(void)
{
    mdh_flash_test_mapping_alignment(get_ram_drive_instance(), bsp_us_ticker_get_default_instance());
}

static void test_flash_erase_sector(void)
{
    mdh_flash_test_erase_sector(get_ram_drive_instance());
}

static void test_flash_program_page(void)
{
    mdh_flash_test_program_page(get_ram_drive_instance());
}

static void test_flash_clock_and_cache(void)
{
    mdh_flash_test_clock_and_cache(bsp_us_ticker_get_default_instance());
}

static void teardown(const size_t passed, const size_t failed, const failure_t failure)
{
    greentea_test_teardown_handler(passed, failed, failure);
}

static utest::v1::status_t setup(const size_t number_of_cases)
{
    bsp_us_ticker_init(bsp_us_ticker_get_default_instance());

    GREENTEA_SETUP(20, "default_auto");
    return greentea_test_setup_handler(number_of_cases);
}

int main(void)
{
    mps3_uart_init(&gs_serial, &UART0_CMSDK_DEV_NS);
    mdh_serial_set_baud(gs_serial, 115200);
    greentea_init_custom_io(gs_serial);

    Harness::run(gs_specification);
}
