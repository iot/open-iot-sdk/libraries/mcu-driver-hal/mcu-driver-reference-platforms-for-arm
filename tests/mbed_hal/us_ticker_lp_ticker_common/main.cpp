/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "greentea-client/test_env.h"
#include "greentea-custom_io/custom_io.h"
#include "utest/utest.h"

extern "C" {
#include "hal/lp_ticker_api.h"
#include "hal/serial_api.h"
#include "hal/us_ticker_api.h"
#include "mps3_uart.h"
#include "ticker.h"
}

#include "test_us_ticker_lp_ticker_common.h"

using namespace utest::v1;

static mdh_serial_t *gs_serial = NULL;

static utest::v1::status_t us_ticker_setup(const Case *const source, const size_t index_of_case);
static utest::v1::status_t
us_ticker_teardown(const Case *const source, const size_t passed, const size_t failed, const failure_t reason);

static void us_ticker_info_test(void);
static void us_ticker_increment_test(void);
static void us_ticker_interrupt_test(void);
static void us_ticker_past_test(void);
static void us_ticker_repeat_reschedule_test(void);
static void us_ticker_fire_now_test(void);
static void us_ticker_speed_test(void);
static void us_ticker_overflow_test(void);

#if DEVICE_LPTICKER
static utest::v1::status_t lp_ticker_setup(const Case *const source, const size_t index_of_case);
static utest::v1::status_t
lp_ticker_teardown(const Case *const source, const size_t passed, const size_t failed, const failure_t reason);

static void lp_ticker_info_test(void);
static void lp_ticker_increment_test(void);
static void lp_ticker_interrupt_test(void);
static void lp_ticker_past_test(void);
static void lp_ticker_repeat_reschedule_test(void);
static void lp_ticker_fire_now_test(void);
static void lp_ticker_speed_test(void);
static void lp_ticker_overflow_test(void);
#endif

static Case s_test_cases[] = {
    Case("Microsecond ticker info test", us_ticker_setup, us_ticker_info_test, us_ticker_teardown),
    Case("Microsecond ticker interrupt test", us_ticker_setup, us_ticker_interrupt_test, us_ticker_teardown),
    Case("Microsecond ticker past interrupt test", us_ticker_setup, us_ticker_past_test, us_ticker_teardown),
    Case("Microsecond ticker reschedule test", us_ticker_setup, us_ticker_repeat_reschedule_test, us_ticker_teardown),
    Case("Microsecond ticker fire interrupt", us_ticker_setup, us_ticker_fire_now_test, us_ticker_teardown),
    Case("Microsecond ticker overflow test", us_ticker_setup, us_ticker_overflow_test, us_ticker_teardown),
    Case("Microsecond ticker increment test", us_ticker_setup, us_ticker_increment_test, us_ticker_teardown),
    Case("Microsecond ticker speed test", us_ticker_setup, us_ticker_speed_test, us_ticker_teardown),
#if DEVICE_LPTICKER
    Case("lp ticker info test", lp_ticker_setup, lp_ticker_info_test, lp_ticker_teardown),
    Case("lp ticker interrupt test", lp_ticker_setup, lp_ticker_interrupt_test, lp_ticker_teardown),
    Case("lp ticker past interrupt test", lp_ticker_setup, lp_ticker_past_test, lp_ticker_teardown),
    Case("lp ticker reschedule test", lp_ticker_setup, lp_ticker_repeat_reschedule_test, lp_ticker_teardown),
    Case("lp ticker fire interrupt", lp_ticker_setup, lp_ticker_fire_now_test, lp_ticker_teardown),
    Case("lp ticker overflow test", lp_ticker_setup, lp_ticker_overflow_test, lp_ticker_teardown),
    Case("lp ticker increment test", lp_ticker_setup, lp_ticker_increment_test, lp_ticker_teardown),
    Case("lp ticker speed test", lp_ticker_setup, lp_ticker_speed_test, lp_ticker_teardown),
#endif
};

static utest::v1::status_t us_ticker_setup(const Case *const source, const size_t index_of_case)
{
#ifdef MBED_CONF_RTOS_PRESENT
    /* OS, common ticker and low power ticker wrapper
     * may make use of us ticker so suspend them for this test */
    osKernelSuspend();
#endif

    bsp_us_ticker_init(bsp_us_ticker_get_default_instance());
    us_ticker_test_setup_handler(bsp_us_ticker_get_default_instance());

    return greentea_case_setup_handler(source, index_of_case);
}

static utest::v1::status_t
us_ticker_teardown(const Case *const source, const size_t passed, const size_t failed, const failure_t reason)
{
    us_ticker_test_teardown_handler(bsp_us_ticker_get_default_instance());

#ifdef MBED_CONF_RTOS_PRESENT
    osKernelResume(0);
#endif

    return greentea_case_teardown_handler(source, passed, failed, reason);
}

static void us_ticker_info_test(void)
{
    ticker_info_test(bsp_us_ticker_get_default_instance());
}

static void us_ticker_increment_test(void)
{
    ticker_increment_test(bsp_us_ticker_get_default_instance());
}

static void us_ticker_interrupt_test(void)
{
    ticker_interrupt_test(bsp_us_ticker_get_default_instance());
}

static void us_ticker_past_test(void)
{
    ticker_past_test(bsp_us_ticker_get_default_instance());
}

static void us_ticker_repeat_reschedule_test(void)
{
    ticker_repeat_reschedule_test(bsp_us_ticker_get_default_instance());
}

static void us_ticker_fire_now_test(void)
{
    ticker_fire_now_test(bsp_us_ticker_get_default_instance());
}

static void us_ticker_speed_test(void)
{
    ticker_speed_test(bsp_us_ticker_get_default_instance(), bsp_us_ticker_get_default_instance());
}

static void us_ticker_overflow_test(void)
{
    ticker_overflow_test(bsp_us_ticker_get_default_instance());
}

#if DEVICE_LPTICKER
static utest::v1::status_t lp_ticker_setup(const Case *const source, const size_t index_of_case)
{
#ifdef MBED_CONF_RTOS_PRESENT
    /* OS and common ticker may make use of lp ticker so suspend them for this test */
    osKernelSuspend();
#endif

    bsp_lp_ticker_init(bsp_lp_ticker_get_default_instance());
    lp_ticker_test_setup_handler(bsp_lp_ticker_get_default_instance());

    return greentea_case_setup_handler(source, index_of_case);
}

static utest::v1::status_t
lp_ticker_teardown(const Case *const source, const size_t passed, const size_t failed, const failure_t reason)
{
    lp_ticker_test_teardown_handler(bsp_lp_ticker_get_default_instance());

#ifdef MBED_CONF_RTOS_PRESENT
    osKernelResume(0);
#endif

    return greentea_case_teardown_handler(source, passed, failed, reason);
}

static void lp_ticker_info_test(void)
{
    ticker_info_test(bsp_lp_ticker_get_default_instance());
}

static void lp_ticker_increment_test(void)
{
    ticker_increment_test(bsp_lp_ticker_get_default_instance());
}

static void lp_ticker_interrupt_test(void)
{
    ticker_interrupt_test(bsp_lp_ticker_get_default_instance());
}

static void lp_ticker_past_test(void)
{
    ticker_past_test(bsp_lp_ticker_get_default_instance());
}

static void lp_ticker_repeat_reschedule_test(void)
{
    ticker_repeat_reschedule_test(bsp_lp_ticker_get_default_instance());
}

static void lp_ticker_fire_now_test(void)
{
    ticker_fire_now_test(bsp_lp_ticker_get_default_instance());
}

static void lp_ticker_speed_test(void)
{
    ticker_speed_test(bsp_lp_ticker_get_default_instance(), bsp_us_ticker_get_default_instance());
}

static void lp_ticker_overflow_test(void)
{
    ticker_overflow_test(bsp_lp_ticker_get_default_instance());
}
#endif

static utest::v1::status_t greentea_test_setup(const size_t number_of_cases)
{
    GREENTEA_SETUP(80, "default_auto");
    return verbose_test_setup_handler(number_of_cases);
}

Specification specification(greentea_test_setup, s_test_cases);

int main(void)
{
    mps3_uart_init(&gs_serial, &UART0_CMSDK_DEV_NS);
    mdh_serial_set_baud(gs_serial, 115200);
    greentea_init_custom_io(gs_serial);

    !Harness::run(specification);
}
