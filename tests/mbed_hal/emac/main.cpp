/* Copyright (c) 2021-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_CONF_MBED_TRACE_ENABLE 1
#define TRACE_GROUP                 "main"

#include "greentea-client/test_env.h"
#include "greentea-custom_io/custom_io.h"
#include "utest/utest.h"

extern "C" {
#include "hal/emac_api.h"
#include "hal/serial_api.h"
#include "hal/us_ticker_api.h"
#include "emac_cs300.h"
#include "mbed_trace/mbed_trace.h"
#include "mps3_uart.h"
#include "test_emac.h"
#include "ticker.h"
}

using namespace utest::v1;

static void console_print(const char *const message);
static void test_initialisation(void);
static void test_broadcast(void);
static void test_unicast(void);
static void test_unicast_frame_len(void);
static void test_unicast_burst(void);
static void test_unicast_long(void);
static void test_broadcast_handling(void);
#if (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)
static void test_multicast_filter(void);
#endif // (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)
static utest::v1::status_t greentea_test_setup(const size_t number_of_cases);

static mdh_serial_t *gs_serial = NULL;
static Case s_test_cases[] = {
    Case("EMAC initialize.", test_initialisation),
    Case("EMAC broadcast", test_broadcast),
    Case("EMAC unicast", test_unicast),
    Case("EMAC unicast frame length", test_unicast_frame_len),
    Case("EMAC unicast burst", test_unicast_burst),
    Case("EMAC broadcast handling", test_broadcast_handling),
#if (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)
    Case("EMAC multicast filter", test_multicast_filter),
#endif // (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)
    Case("EMAC unicast long", test_unicast_long),
};

Specification specification(greentea_test_setup, s_test_cases);

#define MSG_VALUE_DUMMY "0"
#define MSG_VALUE_LEN   24
#define MSG_KEY_LEN     24

static void console_print(const char *const message)
{
    const char *c = message;

    while (*c != '\0') {
        mdh_serial_put_data(gs_serial, *c);
        c++;
    }

    mdh_serial_put_data(gs_serial, '\r');
    mdh_serial_put_data(gs_serial, '\n');
}

static void test_initialisation(void)
{
    test_emac_initialise(cs300_emac_get_default_instance());
}

static void test_broadcast(void)
{
    test_emac_broadcast(cs300_emac_get_default_instance(), bsp_us_ticker_get_default_instance());
}

static void test_unicast(void)
{
    test_emac_unicast(cs300_emac_get_default_instance(), bsp_us_ticker_get_default_instance());
}

static void test_unicast_frame_len(void)
{
    test_emac_unicast_frame_len(cs300_emac_get_default_instance(), bsp_us_ticker_get_default_instance());
}

static void test_unicast_burst(void)
{
    test_emac_unicast_burst(cs300_emac_get_default_instance(), bsp_us_ticker_get_default_instance());
}

static void test_unicast_long(void)
{
    test_emac_unicast_long(cs300_emac_get_default_instance(), bsp_us_ticker_get_default_instance());
}

static void test_broadcast_handling(void)
{
    test_emac_broadcast_handling(cs300_emac_get_default_instance(), bsp_us_ticker_get_default_instance());
}

#if (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)
static void test_multicast_filter(void)
{
    test_emac_multicast_filter(cs300_emac_get_default_instance(), bsp_us_ticker_get_default_instance());
}
#endif // (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)

static utest::v1::status_t greentea_test_setup(const size_t number_of_cases)
{
#define CTP_SERVER_START   "start_ctp_server"
#define CTP_SERVER_STARTED "started_ctp_server"

    char key[MSG_KEY_LEN + 1] = {0};
    char value[MSG_VALUE_LEN + 1] = {0};

    GREENTEA_SETUP(1400, "emac_ctp_server");

    // start the ctp server and wait for ack.
    greentea_send_kv(CTP_SERVER_START, MSG_VALUE_DUMMY);
    greentea_parse_kv(key, value, MSG_KEY_LEN, MSG_VALUE_LEN);

    if (strcmp(key, CTP_SERVER_STARTED) != 0) {
        utest_printf("Failed to start emac ctp server.\n");
        return utest::v1::STATUS_ABORT;
    }

    return verbose_test_setup_handler(number_of_cases);
}

int main(void)
{
    mps3_uart_init(&gs_serial, &UART0_CMSDK_DEV_NS);
    mdh_serial_set_baud(gs_serial, 115200);
    greentea_init_custom_io(gs_serial);

    mbed_trace_init();
    mbed_trace_print_function_set(console_print);

    bsp_us_ticker_init(bsp_us_ticker_get_default_instance());

    Harness::run(specification);
}
