/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "greentea-client/test_env.h"
#include "greentea-custom_io/custom_io.h"
#include "utest/utest.h"

extern "C" {
#include "hal/serial_api.h"
#include "mps3_uart.h"
#include "ticker.h"
}

#include "test_us_ticker.h"

using namespace utest::v1;

static utest::v1::status_t greentea_test_setup(const size_t number_of_cases);
static void test_ticker_info(void);

static mdh_serial_t *gs_serial = NULL;
static Case s_test_cases[] = {
    Case("us ticker info test", test_ticker_info),
};

Specification specification(greentea_test_setup, s_test_cases);

static void test_ticker_info(void)
{
    us_ticker_info_test(bsp_us_ticker_get_default_instance());
}

static utest::v1::status_t greentea_test_setup(const size_t number_of_cases)
{
    GREENTEA_SETUP(20, "default_auto");
    return verbose_test_setup_handler(number_of_cases);
}

int main(void)
{
    mps3_uart_init(&gs_serial, &UART0_CMSDK_DEV_NS);
    mdh_serial_set_baud(gs_serial, 115200);
    greentea_init_custom_io(gs_serial);

    return !Harness::run(specification);
}
