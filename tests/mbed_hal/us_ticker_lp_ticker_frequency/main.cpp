/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "greentea-client/test_env.h"
#include "greentea-custom_io/custom_io.h"
#include "utest/utest.h"

extern "C" {
#include "hal/lp_ticker_api.h"
#include "hal/serial_api.h"
#include "hal/us_ticker_api.h"
#include "mps3_uart.h"
#include "ticker.h"
}

#include "test_us_ticker_lp_ticker_frequency.h"

using namespace utest::v1;

static mdh_serial_t *gs_serial = NULL;

static utest::v1::status_t greentea_test_setup(const size_t number_of_cases);
static void greentea_test_teardown(const size_t passed, const size_t failed, const failure_t failure);

static utest::v1::status_t us_ticker_case_setup_handler(const Case *const source, const size_t index_of_case);
static utest::v1::status_t us_ticker_case_teardown_handler(const Case *const source,
                                                           const size_t passed,
                                                           const size_t failed,
                                                           const failure_t reason);
static void us_ticker_frequency_test(void);

#if DEVICE_LPTICKER
static utest::v1::status_t lp_ticker_case_setup_handler(const Case *const source, const size_t index_of_case);
static utest::v1::status_t lp_ticker_case_teardown_handler(const Case *const source,
                                                           const size_t passed,
                                                           const size_t failed,
                                                           const failure_t reason);
static void lp_ticker_frequency_test(void);
#endif

// Test cases
Case cases[] = {
    Case("Microsecond ticker frequency test",
         us_ticker_case_setup_handler,
         us_ticker_frequency_test,
         us_ticker_case_teardown_handler),
#if DEVICE_LPTICKER
    Case("Low power ticker frequency test",
         lp_ticker_case_setup_handler,
         lp_ticker_frequency_test,
         lp_ticker_case_teardown_handler),
#endif
};

static utest::v1::status_t us_ticker_case_setup_handler(const Case *const source, const size_t index_of_case)
{
    bsp_us_ticker_init(bsp_us_ticker_get_default_instance());
    us_ticker_test_setup_handler(bsp_us_ticker_get_default_instance());

    return greentea_case_setup_handler(source, index_of_case);
}

static utest::v1::status_t us_ticker_case_teardown_handler(const Case *const source,
                                                           const size_t passed,
                                                           const size_t failed,
                                                           const failure_t reason)
{
    us_ticker_test_teardown_handler(bsp_us_ticker_get_default_instance());
    return greentea_case_teardown_handler(source, passed, failed, reason);
}

static void us_ticker_frequency_test(void)
{
    ticker_frequency_test(bsp_us_ticker_get_default_instance());
}

#if DEVICE_LPTICKER
static utest::v1::status_t lp_ticker_case_setup_handler(const Case *const source, const size_t index_of_case)
{
    bsp_lp_ticker_init(bsp_lp_ticker_get_default_instance());
    lp_ticker_test_setup_handler(bsp_lp_ticker_get_default_instance());

    return greentea_case_setup_handler(source, index_of_case);
}

static utest::v1::status_t lp_ticker_case_teardown_handler(const Case *const source,
                                                           const size_t passed,
                                                           const size_t failed,
                                                           const failure_t reason)
{
    lp_ticker_test_teardown_handler(bsp_lp_ticker_get_default_instance());
    return greentea_case_teardown_handler(source, passed, failed, reason);
}

static void lp_ticker_frequency_test(void)
{
    ticker_frequency_test(bsp_lp_ticker_get_default_instance());
}
#endif

static utest::v1::status_t greentea_test_setup(const size_t number_of_cases)
{
#ifdef MBED_CONF_RTOS_PRESENT
    /* Suspend RTOS Kernel so the timers are not in use. */
    osKernelSuspend();
#endif

    GREENTEA_SETUP(120, "timing_drift_auto");
    return greentea_test_setup_handler(number_of_cases);
}

static void greentea_test_teardown(const size_t passed, const size_t failed, const failure_t failure)
{
#ifdef MBED_CONF_RTOS_PRESENT
    osKernelResume(0);
#endif

    greentea_test_teardown_handler(passed, failed, failure);
}

Specification specification(greentea_test_setup, cases, greentea_test_teardown);

int main(void)
{
    mps3_uart_init(&gs_serial, &UART0_CMSDK_DEV_NS);
    mdh_serial_set_baud(gs_serial, 115200);
    greentea_init_custom_io(gs_serial);

    Harness::run(specification);
}
