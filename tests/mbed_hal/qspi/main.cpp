/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "greentea-client/test_env.h"
#include "greentea-custom_io/custom_io.h"
#include "objects.h"
#include "unity.h"
#include "utest/utest.h"

extern "C" {
#include "hal/qspi_api.h"
#include "hal/serial_api.h"
#include "hal/us_ticker_api.h"
#include "mps3_uart.h"
#include "ticker.h"
#include "xilinx_qspi.h"
}

#include "qspi_flash_config.h"
#include "test_qspi.h"
#include "test_qspi_utils.h"

// max write size is usually page size
#define DATA_SIZE_256  (QSPI_PAGE_SIZE)
#define DATA_SIZE_1024 (QSPI_PAGE_SIZE * 4)

typedef void (*case_write_read_handler_f)(
    mdh_qspi_t *qspi_handle, mdh_ticker_t *us_ticker, uint8_t *tx_buf, size_t tx_size, uint8_t *rx_buf, size_t rx_size);

using namespace utest::v1;

static void test_memory_id_test(void);
static void test_write_read_repeat(case_write_read_handler_f case_handler);
static void test_write_1_1_1_x1_read_1_1_1_x1_repeat_x1(void);
static void test_write_1_1_1_x4_read_1_1_1_x1_repeat_x1(void);
static void test_write_1_1_1_x1_read_1_1_1_x4_repeat_x1(void);
static void test_write_1_1_1_x1_read_1_1_1_x1_repeat_x4(void);

static void test_write_1_1_1_x1_read_1_1_2_x1_repeat_x1(void);
static void test_write_1_1_1_x4_read_1_1_2_x1_repeat_x1(void);
static void test_write_1_1_1_x1_read_1_1_2_x4_repeat_x1(void);
static void test_write_1_1_1_x1_read_1_1_2_x1_repeat_x4(void);

static void test_write_1_1_1_x1_read_1_2_2_x1_repeat_x1(void);
static void test_write_1_1_1_x4_read_1_2_2_x1_repeat_x1(void);
static void test_write_1_1_1_x1_read_1_2_2_x4_repeat_x1(void);
static void test_write_1_1_1_x1_read_1_2_2_x1_repeat_x4(void);

static void test_write_1_1_1_x1_read_1_1_4_x1_repeat_x1(void);
static void test_write_1_1_1_x4_read_1_1_4_x1_repeat_x1(void);
static void test_write_1_1_1_x1_read_1_1_4_x4_repeat_x1(void);
static void test_write_1_1_1_x1_read_1_1_4_x1_repeat_x4(void);

static void test_write_1_1_1_x1_read_1_4_4_x1_repeat_x1(void);
static void test_write_1_1_1_x4_read_1_4_4_x1_repeat_x1(void);
static void test_write_1_1_1_x1_read_1_4_4_x4_repeat_x1(void);
static void test_write_1_1_1_x1_read_1_4_4_x1_repeat_x4(void);

static utest::v1::status_t greentea_test_setup(const size_t number_of_cases);

static mdh_serial_t *gs_serial = NULL;
static uint8_t gs_tx_buf[DATA_SIZE_1024];
static uint8_t gs_rx_buf[DATA_SIZE_1024];

static Case s_test_cases[] = {
    Case("qspi memory id test", test_memory_id_test),
    //   read/x1 write/x1 - read/write block of data in single write/read
    //   operation read/x4 write/x4 - read/write block of data in adjacent
    //   locations in multiple write/read operations repeat/xN        - test
    //   repeat count (new data pattern each time) 1-1-1            - single
    //   channel SPI 1-1-2            - Dual data (extended SPI) 1-2-2 - Dual
    //   I/O  (extended SPI) 1-1-4            - Quad data (extended SPI) 1-4-4
    //   - Quad I/O  (extended SPI) 2-2-2            - DPI (multi-channel SPI)
    //   4-4-4            - QPI (multi-channel SPI)
    Case("qspi write(1-1-1)/x1  read(1-1-1)/x1  repeat/x1  test", test_write_1_1_1_x1_read_1_1_1_x1_repeat_x1),
    Case("qspi write(1-1-1)/x4  read(1-1-1)/x1  repeat/x1  test", test_write_1_1_1_x4_read_1_1_1_x1_repeat_x1),
    Case("qspi write(1-1-1)/x1  read(1-1-1)/x4  repeat/x1  test", test_write_1_1_1_x1_read_1_1_1_x4_repeat_x1),
    Case("qspi write(1-1-1)/x1  read(1-1-1)/x1  repeat/x4  test", test_write_1_1_1_x1_read_1_1_1_x1_repeat_x4),

    Case("qspi write(1-1-1)/x1  read(1-1-2)/x1  repeat/x1  test", test_write_1_1_1_x1_read_1_1_2_x1_repeat_x1),
    Case("qspi write(1-1-1)/x4  read(1-1-2)/x1  repeat/x1  test", test_write_1_1_1_x4_read_1_1_2_x1_repeat_x1),
    Case("qspi write(1-1-1)/x1  read(1-1-2)/x4  repeat/x1  test", test_write_1_1_1_x1_read_1_1_2_x4_repeat_x1),
    Case("qspi write(1-1-1)/x1  read(1-1-2)/x1  repeat/x4  test", test_write_1_1_1_x1_read_1_1_2_x1_repeat_x4),

    Case("qspi write(1-1-1)/x1  read(1-2-2)/x1  repeat/x1  test", test_write_1_1_1_x1_read_1_2_2_x1_repeat_x1),
    Case("qspi write(1-1-1)/x4  read(1-2-2)/x1  repeat/x1  test", test_write_1_1_1_x4_read_1_2_2_x1_repeat_x1),
    Case("qspi write(1-1-1)/x1  read(1-2-2)/x4  repeat/x1  test", test_write_1_1_1_x1_read_1_2_2_x4_repeat_x1),
    Case("qspi write(1-1-1)/x1  read(1-2-2)/x1  repeat/x4  test", test_write_1_1_1_x1_read_1_2_2_x1_repeat_x4),

    Case("qspi write(1-1-1)/x1  read(1-1-4)/x1  repeat/x1  test", test_write_1_1_1_x1_read_1_1_4_x1_repeat_x1),
    Case("qspi write(1-1-1)/x4  read(1-1-4)/x1  repeat/x1  test", test_write_1_1_1_x4_read_1_1_4_x1_repeat_x1),
    Case("qspi write(1-1-1)/x1  read(1-1-4)/x4  repeat/x1  test", test_write_1_1_1_x1_read_1_1_4_x4_repeat_x1),
    Case("qspi write(1-1-1)/x1  read(1-1-4)/x1  repeat/x4  test", test_write_1_1_1_x1_read_1_1_4_x1_repeat_x4),

    Case("qspi write(1-1-1)/x1  read(1-4-4)/x1  repeat/x1  test", test_write_1_1_1_x1_read_1_4_4_x1_repeat_x1),
    Case("qspi write(1-1-1)/x4  read(1-4-4)/x1  repeat/x1  test", test_write_1_1_1_x4_read_1_4_4_x1_repeat_x1),
    Case("qspi write(1-1-1)/x1  read(1-4-4)/x4  repeat/x1  test", test_write_1_1_1_x1_read_1_4_4_x4_repeat_x1),
    Case("qspi write(1-1-1)/x1  read(1-4-4)/x1  repeat/x4  test", test_write_1_1_1_x1_read_1_4_4_x1_repeat_x4)};

static Specification specification(greentea_test_setup, s_test_cases, greentea_test_teardown_handler);

int main(void)
{
    mps3_uart_init(&gs_serial, &UART0_CMSDK_DEV_NS);
    mdh_serial_set_baud(gs_serial, 115200);
    greentea_init_custom_io(gs_serial);

    bsp_us_ticker_init(bsp_us_ticker_get_default_instance());

    Harness::run(specification);
}

static utest::v1::status_t greentea_test_setup(const size_t number_of_cases)
{
    GREENTEA_SETUP(180, "default_auto");
    return greentea_test_setup_handler(number_of_cases);
}

static void test_memory_id_test(void)
{
    qspi_memory_id_test(QSPI_FLASH_CHIP_STRING);
}

static void test_write_read_repeat(case_write_read_handler_f case_handler)
{
    xilinx_qspi_t *qspi_port = NULL;

    TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, xilinx_qspi_init(&qspi_port, &QSPI_WRITE_DEV));

    case_handler(&(qspi_port->qspi),
                 bsp_us_ticker_get_default_instance(),
                 gs_tx_buf,
                 sizeof(gs_tx_buf),
                 gs_rx_buf,
                 sizeof(gs_rx_buf));

    TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, xilinx_qspi_deinit(&QSPI_WRITE_DEV));
}

static void test_write_1_1_1_x1_read_1_1_1_x1_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_1_1,
                                                QSPI_CMD_READ_1IO,
                                                QSPI_READ_1IO_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x4_read_1_1_1_x1_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_MULTIPLE,
                                                MODE_1_1_1,
                                                QSPI_CMD_READ_1IO,
                                                QSPI_READ_1IO_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_1024,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_1_1_x4_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_1_1,
                                                QSPI_CMD_READ_1IO,
                                                QSPI_READ_1IO_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_MULTIPLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_1_1_x1_repeat_x4(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_1_1,
                                                QSPI_CMD_READ_1IO,
                                                QSPI_READ_1IO_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_MULTIPLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_1_2_x1_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_1_2,
                                                QSPI_CMD_READ_1I2O,
                                                QSPI_READ_1I2O_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x4_read_1_1_2_x1_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_MULTIPLE,
                                                MODE_1_1_2,
                                                QSPI_CMD_READ_1I2O,
                                                QSPI_READ_1I2O_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_1024,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_1_2_x4_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_1_2,
                                                QSPI_CMD_READ_1I2O,
                                                QSPI_READ_1I2O_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_MULTIPLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_1_2_x1_repeat_x4(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_1_2,
                                                QSPI_CMD_READ_1I2O,
                                                QSPI_READ_1I2O_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_MULTIPLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_2_2_x1_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_2_2,
                                                QSPI_CMD_READ_2IO,
                                                QSPI_READ_2IO_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x4_read_1_2_2_x1_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_MULTIPLE,
                                                MODE_1_2_2,
                                                QSPI_CMD_READ_2IO,
                                                QSPI_READ_2IO_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_1024,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_2_2_x4_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_2_2,
                                                QSPI_CMD_READ_2IO,
                                                QSPI_READ_2IO_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_MULTIPLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_2_2_x1_repeat_x4(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_2_2,
                                                QSPI_CMD_READ_2IO,
                                                QSPI_READ_2IO_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_MULTIPLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_1_4_x1_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_1_4,
                                                QSPI_CMD_READ_1I4O,
                                                QSPI_READ_1I4O_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x4_read_1_1_4_x1_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_MULTIPLE,
                                                MODE_1_1_4,
                                                QSPI_CMD_READ_1I4O,
                                                QSPI_READ_1I4O_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_1024,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_1_4_x4_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_1_4,
                                                QSPI_CMD_READ_1I4O,
                                                QSPI_READ_1I4O_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_MULTIPLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_1_4_x1_repeat_x4(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_1_4,
                                                QSPI_CMD_READ_1I4O,
                                                QSPI_READ_1I4O_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_MULTIPLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_4_4_x1_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_4_4,
                                                QSPI_CMD_READ_4IO,
                                                QSPI_READ_4IO_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x4_read_1_4_4_x1_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_MULTIPLE,
                                                MODE_1_4_4,
                                                QSPI_CMD_READ_4IO,
                                                QSPI_READ_4IO_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_1024,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_4_4_x4_repeat_x1(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_4_4,
                                                QSPI_CMD_READ_4IO,
                                                QSPI_READ_4IO_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_MULTIPLE,
                                                TEST_REPEAT_SINGLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}

static void test_write_1_1_1_x1_read_1_4_4_x1_repeat_x4(void)
{
    test_write_read_repeat(qspi_write_read_test<MODE_1_1_1,
                                                QSPI_CMD_WRITE_1IO,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                WRITE_SINGLE,
                                                MODE_1_4_4,
                                                QSPI_CMD_READ_4IO,
                                                QSPI_READ_4IO_DUMMY_CYCLE,
                                                MDH_QSPI_ADDRESS_SIZE_24,
                                                MDH_QSPI_ALT_ADDRESS_SIZE_8,
                                                QSPI_FIXED_FREQUENCY_HZ,
                                                READ_SINGLE,
                                                TEST_REPEAT_MULTIPLE,
                                                DATA_SIZE_256,
                                                TEST_FLASH_ADDRESS,
                                                QSPI_SECTOR_COUNT,
                                                QSPI_SECTOR_SIZE,
                                                QSPI_CMD_ERASE_SECTOR,
                                                QSPI_ERASE_SECTOR_MAX_TIME,
                                                QSPI_PAGE_PROG_MAX_TIME,
                                                QSPI_STATUS_REG_SIZE,
                                                QSPI_MAX_REG_SIZE,
                                                QSPI_WRSR_MAX_TIME,
                                                QSPI_WAIT_MAX_TIME,
                                                QSPI_CMD_RDSR,
                                                QSPI_CMD_WRSR,
                                                QSPI_CMD_WREN,
                                                STATUS_BIT_WIP,
                                                STATUS_BIT_WEL,
                                                QSPI_CMD_WRDI>);
}
