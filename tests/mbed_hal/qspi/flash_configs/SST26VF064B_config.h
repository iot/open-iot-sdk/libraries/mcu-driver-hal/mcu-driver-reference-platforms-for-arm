/* Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef QSPI_FLASH_CONFIG_SST26VF064B_H
#define QSPI_FLASH_CONFIG_SST26VF064B_H

#define QSPI_FLASH_CHIP_STRING "Microchip SST26VF064B"

// Command for reading status register
#define QSPI_CMD_RDSR 0x05
// Command for reading configuration register
#define QSPI_CMD_RDCR0 0x35
// Command for writing status/configuration register
#define QSPI_CMD_WRSR 0x01

// Command for setting Reset Enable
// #define QSPI_CMD_RSTEN 0x66 // not supported by the controller
// Command for setting Reset
// #define QSPI_CMD_RST 0x99 // not supported by the controller

// Command for setting write enable
#define QSPI_CMD_WREN 0x06
// Command for setting write disable
#define QSPI_CMD_WRDI 0x04

// WRSR operations max time [us] (datasheet max time + 15%)
#define QSPI_WRSR_MAX_TIME 28750 // 25 ms (TWPEN)
// general wait max time [us]
#define QSPI_WAIT_MAX_TIME 100000 // 100ms

// Commands for writing (page programming)
#define QSPI_CMD_WRITE_1IO 0x02 // 1-1-1 mode
// FIXME Command 0x32 fails the test. Further investigation needed.
// #define QSPI_CMD_WRITE_4IO 0x32 // 1-4-4 mode

// write operations max time [us] (datasheet max time + 15%)
#define QSPI_PAGE_PROG_MAX_TIME 1725 // 1.5 ms

#define QSPI_PAGE_SIZE    256  // 256B
#define QSPI_SECTOR_SIZE  4096 // 4kB
#define QSPI_SECTOR_COUNT 2048 // 64 Mbit (8 MB) flash memory

// Commands for reading
#define QSPI_CMD_READ_1IO_FAST 0x0B // 1-1-1 mode
#define QSPI_CMD_READ_1IO      0x03 // 1-1-1 mode
#define QSPI_CMD_READ_2IO      0xBB // 1-2-2 mode
#define QSPI_CMD_READ_1I2O     0x3B // 1-1-2 mode
#define QSPI_CMD_READ_4IO      0xEB // 1-4-4 mode
#define QSPI_CMD_READ_1I4O     0x6B // 1-1-4 mode

#define QSPI_READ_1IO_DUMMY_CYCLE  0
#define QSPI_READ_FAST_DUMMY_CYCLE 3
#define QSPI_READ_2IO_DUMMY_CYCLE  1
#define QSPI_READ_1I2O_DUMMY_CYCLE 2
#define QSPI_READ_4IO_DUMMY_CYCLE  3
#define QSPI_READ_1I4O_DUMMY_CYCLE 4

// Commands for erasing
#define QSPI_CMD_ERASE_SECTOR   0xD8 // 0x20 (4kB Sector-Erase) not supported by the controller.
#define QSPI_CMD_ERASE_BLOCK_32 0xD8 // same command for all block sizes
#define QSPI_CMD_ERASE_BLOCK_64 0xD8 // 64kB
#define QSPI_CMD_ERASE_CHIP     0xC7

// erase operations max time [us] (datasheet max time + 15%)
#define QSPI_ERASE_SECTOR_MAX_TIME   28750 // 25 ms
#define QSPI_ERASE_BLOCK_32_MAX_TIME 28750 // 25 ms
#define QSPI_ERASE_BLOCK_64_MAX_TIME 28750 // 25 ms

// max frequency for basic rw operation (for fast mode)
#define QSPI_COMMON_MAX_FREQUENCY 104000000

#define QSPI_STATUS_REG_SIZE   1
#define QSPI_CONFIG_REG_0_SIZE 1
#define QSPI_MAX_REG_SIZE      1

// status register
#define STATUS_BIT_WIP (1 << 0) // write in progress bit
#define STATUS_BIT_WEL (1 << 1) // write enable latch

#endif // QSPI_FLASH_CONFIG_SST26VF064B_H
