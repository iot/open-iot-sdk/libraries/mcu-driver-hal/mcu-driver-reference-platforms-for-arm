/* Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef QSPI_FLASH_CONFIG_H
#define QSPI_FLASH_CONFIG_H

#include "SST26VF064B_config.h"

#endif // QSPI_FLASH_CONFIG_H
