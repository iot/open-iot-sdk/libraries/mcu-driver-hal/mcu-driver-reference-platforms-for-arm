/* Copyright (c) 2020-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_CONF_MBED_TRACE_ENABLE 1
#define TRACE_GROUP                 "main"

#include "PinNames.h"
#include "device.h"
#include "greentea-client/test_env.h"
#include "greentea-custom_io/custom_io.h"
#include "mbed_trace/mbed_trace.h"
#include "unity.h"
#include "utest/utest.h"

#include <stdint.h>
#include <stdio.h>
#include <string.h>

extern "C" {
#include "hal/i2c_api.h"
#include "hal/sai_api.h"
#include "hal/serial_api.h"
#include "hal/us_ticker_api.h"
#include "example_sai_loopback.h"
#include "fvp_sai.h"
#include "hal-toolbox/us_ticker_util.h"
#include "mps3_i2c.h"
#include "mps3_i2s.h"
#include "mps3_uart.h"
#include "ticker.h"
}

using namespace utest::v1;

enum test_phase_e { TEST_PHASE_LEFT, TEST_PHASE_RIGHT };

// Proviate defines
#define AAIC_I2C_ADDR 0x4B

#define AAIC_I2C_CRID      0x01
#define AAIC_I2C_CRPWRC1   0x02
#define AAIC_I2C_CRPWRC2   0x03
#define AAIC_I2C_CRPWRC3   0x04
#define AAIC_I2C_CLKCRTL   0x05
#define AAIC_I2C_INTFCRTL1 0x06
#define AAIC_I2C_INTFCRTL2 0x07
#define AAIC_I2C_INPUTASEL 0x08
#define AAIC_I2C_INPUTBSEL 0x09

// Private functions declarations
static mdh_i2c_t *aaic_i2c_get(void);
static uint8_t aaic_i2c_read(uint8_t reg_addr);
static void aaic_i2c_write(uint8_t reg_addr, uint8_t reg_value);

static void sai_get(mdh_sai_t **tx, mdh_sai_t **rx);
static void serial_print(const char *string);

// Private variables
static mdh_serial_t *gs_serial = NULL;

// Private functions definitions
static mdh_i2c_t *aaic_i2c_get(void)
{
    static mps3_i2c_t *s_aaic = NULL;
    static bool s_initialized = false;

    if (!s_initialized) {
        mps3_i2c_init(&s_aaic, &I2C1_SBCON_DEV);
        s_initialized = true;
    }
    return &(s_aaic->i2c);
}
static uint8_t aaic_i2c_read(uint8_t reg_addr)
{
    uint8_t reg_value = 0;
    mdh_i2c_t *i2c = aaic_i2c_get();
    mdh_i2c_write(i2c, AAIC_I2C_ADDR << 1, &reg_addr, 1, true);
    mdh_i2c_read(i2c, (AAIC_I2C_ADDR << 1) | 1, &reg_value, 1, true);
    return reg_value;
}
static void aaic_i2c_write(uint8_t reg_addr, uint8_t reg_value)
{
    uint8_t frame[2] = {reg_addr, reg_value};
    mdh_i2c_t *i2c = aaic_i2c_get();
    mdh_i2c_write(i2c, AAIC_I2C_ADDR << 1, frame, 2, true);
}

/// Return two 16bit 48KHz audio streams (respectively out and in).
static void sai_get(mdh_sai_t **tx, mdh_sai_t **rx)
{
    mps3_i2s_t *mps3_tx = NULL;
    mps3_i2s_t *mps3_rx = NULL;
    mps3_i2s_init(&mps3_tx, &mps3_rx, 48000);
    assert(mps3_tx != NULL);
    assert(mps3_rx != NULL);

    // magic settings sequence (as per cs42l52 doc) to undocumented registers.
    aaic_i2c_write(0x00, 0x99);
    aaic_i2c_write(0x3E, 0xBA);
    aaic_i2c_write(0x47, 0x80);
    // The self tests use these values.
    aaic_i2c_write(0x32, 0xBB);
    aaic_i2c_write(0x32, 0x3B);
    // But the datasheet only says to set/clear the MSB
    /*aaic_i2c_write(0x32, aaic_i2c_read(0x32) | 0x80);*/
    /*aaic_i2c_write(0x32, aaic_i2c_read(0x32) & 0x7F);*/
    aaic_i2c_write(0x00, 0x00);
    us_ticker_util_wait(bsp_us_ticker_get_default_instance(), 100 * 1000);

    // Enable MCLK and set frequency (LRCK=48KHz, MCLK=12.288MHz, /256)
    aaic_i2c_write(AAIC_I2C_CLKCRTL, 0xA0);

    // Power control 1 Codec powered up
    aaic_i2c_write(AAIC_I2C_CRPWRC1, 0x00);
    // Power control 2 MIC powered up (default bias 0.5*VA=0.9V)
    aaic_i2c_write(AAIC_I2C_CRPWRC2, 0x00);
    // Power control 3 Headphone channel always on, Speaker channel always on
    aaic_i2c_write(AAIC_I2C_CRPWRC3, 0xAA);

    // Input select AIN1A and AIN1B
    aaic_i2c_write(AAIC_I2C_INPUTASEL, 0x00);
    aaic_i2c_write(AAIC_I2C_INPUTBSEL, 0x00);

    // Audio setup complete
    us_ticker_util_wait(bsp_us_ticker_get_default_instance(), 10 * 1000);

    // Release I2S FIFO reset
    mps3_i2s_unlock();

    // Read the I2C chip ID and revision
    tr_info("AAIC id: 0x%" PRIx8, aaic_i2c_read(AAIC_I2C_CRID));

    assert(tx != NULL);
    assert(rx != NULL);
    *tx = &(mps3_tx->sai);
    *rx = &(mps3_rx->sai);
}

static void serial_print(const char *string)
{
    while (*string != 0) {
        mdh_serial_put_data(gs_serial, *string);
        string++;
    }
    mdh_serial_put_data(gs_serial, '\r');
    mdh_serial_put_data(gs_serial, '\n');
}

static void example_complete(int32_t left, int32_t right)
{
    static enum test_phase_e s_phase = TEST_PHASE_LEFT;

    switch (s_phase) {
        case TEST_PHASE_LEFT:
            // expect left to be close to the full range of int16_t +/- 15%
            // INT16_MAX - INT16_MIN = UINT16_MAX
            TEST_ASSERT_INT32_WITHIN(UINT16_MAX / (100 / 15), UINT16_MAX, left);
            TEST_ASSERT_INT32_WITHIN(200, 0, right);
            s_phase = TEST_PHASE_RIGHT;
            break;
        case TEST_PHASE_RIGHT:
            TEST_ASSERT_INT32_WITHIN(200, 0, left);
            TEST_ASSERT_INT32_WITHIN(UINT16_MAX / (100 / 15), UINT16_MAX, right);
            break;
    }
}

// Public function definition

/// This example emits a sinewave on a DAC and expects to read back that
/// sinewave from an ADC.
static void sai_loopback_test(void)
{
    mdh_sai_t *sai_in = NULL;
    mdh_sai_t *sai_out = NULL;

    sai_get(&sai_out, &sai_in);

    example_sai_loopback(sai_in, sai_out, example_complete, bsp_us_ticker_get_default_instance());
}

Case cases[] = {
    Case("Sound loopback.", sai_loopback_test),
};

utest::v1::status_t greentea_test_setup(const size_t number_of_cases)
{
    GREENTEA_SETUP(30, "default_auto");
    return greentea_test_setup_handler(number_of_cases);
}

Specification specification(greentea_test_setup, cases);

int main()
{
    mps3_uart_init(&gs_serial, &UART0_CMSDK_DEV_NS);
    mdh_serial_set_baud(gs_serial, 115200);
    greentea_init_custom_io(gs_serial);

    mbed_trace_init();
    mbed_trace_print_function_set(serial_print);
    bsp_us_ticker_init(bsp_us_ticker_get_default_instance());

    Harness::run(specification);
}
