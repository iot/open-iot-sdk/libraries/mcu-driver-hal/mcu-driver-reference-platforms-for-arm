# References used for development

* [Arm® MPS3 FPGA Prototyping Board Technical Reference Manual](https://developer.arm.com/documentation/100765/0000)
  Contains information about the hardware and how the chips are connected to each other.
* [Arm® Corstone™ SSE-300 with Cortex-M55 and Ethos-U55 : Example Subsystem for MPS3](https://developer.arm.com/documentation/dai0552/c)
  Contains information about the implementation of the SSE-300 example subsystem into the MPS3's FPGA.
* [Arm® Corstone™ SSE-300 Example Subsystem Technical Reference Manual](https://developer.arm.com/documentation/101773/0000)
  Contains information about the architecture of the subsystem example.
* [ARM® CoreLink™ SIE-200 System IP for Embedded Technical Reference Manual](https://developer.arm.com/documentation/ddi0571/f)
  Contains information about the Memory and Peripheral protection units implemented in the SSE-300
  subsystem example.
* [Arm® SSE-123 Example Subsystem Technical Reference Manual](https://developer.arm.com/documentation/101370/0000)
  Contains information about some of the peripheral implemented in the SSE-300 subsystem example such
  as the `System Counter` (Appendix B.2).
* [Arm® Cortex-M System Design Kit Technical Reference Manual](https://developer.arm.com/documentation/ddi0479/d)
  Contains information about some of the peripheral implemented in the SSE-300 subsystem example such
  as the `UART` (Chapter 4.3 APB UART).
* [ARM® PrimeCell™ Synchronous Serial Port](https://developer.arm.com/documentation/ddi0194/h)
  Contains information about some of the `SPI` peripheral implemented in the SSE-300 subsystem example.
