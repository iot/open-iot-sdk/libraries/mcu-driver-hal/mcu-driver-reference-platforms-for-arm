# FVP Corstone-300 networking

You need to explicitly enable networking by passing some parameters to the FVP binary. These parameters include:

- `smsc_91c111.enabled`: tells the FVP to enable the SMSC91C111 Ethernet IP emulation. If not enabled, the MCU-Driver-HAL Ethernet driver will not be
  able to read/write the IP registers successfully. This is a hard requirement for enabling Ethernet.

Once you have enabled the Ethernet IP emulation and want to talk to the external world, you will have two options:

- `User Networking`: In User Networking, the FVP creates a virtual network and acts as a router between the MCU-Driver-HAL and the external world. Consequently, it is the
     responsibility of the FVP to forward packets to and from the External World. In general, this is the easiest option as it does not require any set up or super user
     privileges. However, it has certain limitations including:
  - Generally slow as the FVP acts as a router between the external world and FVP software.
  - Blocks L2 (RAW Ethernet) and ICMP traffic.

     You can enable User Networking by adding the following parameter to the FVP command line:

     ```console
     cpu_core.mps3_board.hostbridge.userNetworking=1
     ```

- `TAP Networking` : This is more reliable but harder to set up as it requires creating virtual interfaces and binding them with a bridge so they can have
    connectivity to the external world. Creating a TAP interface also requires super user privileges, so it may not be ideal for all use cases. However, in this case, the
    FVP acts like a normal device on the network and there are no limitations whatsoever.

    You can enable TAP Networking by adding the following parameter to the FVP command line:

    ```sh
    mps3_board.hostbridge.interfaceName=${IFNAME}
    ```

More details about User Networking and the Steps required to set up TAP Networking are mentioned [here][7].

[7]: https://developer.arm.com/documentation/100964/1113/Introduction/Network-set-up/
