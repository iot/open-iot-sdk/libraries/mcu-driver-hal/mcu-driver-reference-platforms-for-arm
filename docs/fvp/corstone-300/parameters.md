# FVP Corstone-300 parameter value considerations

For the full list of restrictions on the processor models, see the [FVP Reference Guide][3].
For the full list of FVP command-line options, see the [FVP Reference Guide][4].

## Timing and the `--quantum N`

The FVP Reference Guide lists timing among model restrictions:

> Fast Models does not model accurate instruction timing. A processor issues a set of instructions (a "quantum") at a point in simulation time, and then waits before executing the next quantum. The processor averages one instruction per clock tick. Consequently:
>
> * (...)
> * **A program might be able to detect the quantized execution behavior of a processor, for example by polling a high-resolution timer.**

If your application polls a ticker (i.e. the microsecond ticker) in a loop, it is very likely it will see a step-wise increase of a ticker counter value. In other words, the ticker time will pass in huge steps from the application's perspective. You can counteract this behavior by decreasing the quantum number. The default value is `10000 ticks`. An example application that exposes this behavior is provided [here](../../../examples/us_ticker_fvp_quantum/main.c). Run it using different quantum values and observer the traces. For more info about this example, see the [README](../../../examples/us_ticker_fvp_quantum/README.md).

## Ethernet and the `--parameter SPEC`

The Ethernet is disabled by default and has to be explicitly enabled with `-C` / `--parameter` options as below.

```sh
FVP_Corstone_SSE-300_Ethos-U55 --application ${APP_ELF_PATH} --parameter cpu_core.mps3_board.smsc_91c111.enabled=1 --parameter cpu_core.mps3_board.hostbridge.userNetworking=1
```

[3]: https://developer.arm.com/documentation/dui0424/l/programmer-s-reference-for-the-eb-fvps/differences-between-the-eb-and-coretile-hardware-and-the-models/restrictions-on-the-processor-models?lang=en
[4]: https://developer.arm.com/documentation/100966/1115/Getting-Started-with-Fixed-Virtual-Platforms/FVP-command-line-options
