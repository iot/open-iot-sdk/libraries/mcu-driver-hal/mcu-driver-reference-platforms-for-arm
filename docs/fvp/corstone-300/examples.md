# FVP Corstone-300 examples

Note this guide is specific to the Corstone-300 FVP, for generic info about examples see [here][examples_readme].

## Building an example application

Follow build instructions from the [main README file][main_readme_building] and set FVP-specific options as below

- set the `CMAKE_SYSTEM_PROCESSOR` to `cortex-m55`,
- set the `MDH_PLATFORM` to `ARM_AN552_MPS3`,
- set the build `VARIANT` to `FVP`.

For example, these are the configure and build commands specific to the example serial application

```console
$ cmake -S ./ -B ./build/ --toolchain=components/mcu-driver-hal/tools/cmake/toolchain/toolchain-arm-none-eabi-gcc.cmake -D CMAKE_SYSTEM_PROCESSOR=cortex-m55 -D MDH_PLATFORM=ARM_AN552_MPS3 -D VARIANT=FVP
$ cmake --build ./build/ --target mdh-arm-hal-example-serial
```

## Running an example application

You can run any binary built for the Corstone-300 MPS3 platform on the FVP. The FVP allows you to toggle virtual buttons and switches, see virtual LEDs and view the serial output of the platform. Use the `--application` option to select the binary to run.

```console
$ FVP_Corstone_SSE-300_Ethos-U55 --application=${APP_ELF_PATH}
```

For example, this is the command specific to the example serial application

```console
$ FVP_Corstone_SSE-300_Ethos-U55 --application=./build/examples/serial/mdh-arm-hal-example-serial.elf
```

For more information about the FVP command line options, see the notes on [parameters][fvp_cs300_parameters] and [networking][fvp_cs300_networking].

[examples_readme]: ../../../examples/README.md
[fvp_cs300_networking]: ./networking.md
[fvp_cs300_parameters]: ./parameters.md
[main_readme_building]: ../../../README.md#building
