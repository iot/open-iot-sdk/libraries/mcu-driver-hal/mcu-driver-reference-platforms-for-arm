# Debugging code running on FVP with Arm Development Studio

Use this guide to export the project to _Arm Development Studio_, connect to a standalone _FVP_ and debug.

## Exporting from CMake

1. Go to the root of `mcu-driver-reference-platforms-for-arm` project, e.g.:

    ```bash
    cd ${GIT_PROJECTS_DIR}/mcu-driver-reference-platforms-for-arm/
    ```

1. Create a build directory in a location **outside of `mcu-driver-reference-platforms-for-arm` dir**.

    **NOTE**: As the CMake warning says _It is strongly recommended to use a build directory which is a sibling of the source directory._
    It does not have to be a direct sibling though, what matters is -- not a subdirectory of `mcu-driver-reference-platforms-for-arm`.

    ```bash
    DS_BUILD_DIR=${GIT_PROJECTS_DIR}/mcu-driver-reference-platforms-for-arm-DS_build-debug/
    mkdir -p ${DS_BUILD_DIR}
    ```

1. Generate build system files.

    ```bash
    cmake -D --toolchain=components/mcu-driver-hal/tools/cmake/toolchain/toolchain-{armclang|arm-none-eabi-gcc}.cmake -DCMAKE_SYSTEM_PROCESSOR=cortex-m55 -D VARIANT=FVP -D HTRUN_ARGUMENTS="--micro=FVP_CS300_U55;--fm=MPS3" -D CMAKE_BUILD_TYPE=Debug -G 'Eclipse CDT4 - Unix Makefiles' -S . -B ${DS_BUILD_DIR}
    ```

## Importing to Arm Development Studio

1. Open the _Arm DS IDE_.
1. `File` → `Import...`.
1. `General` → `Existing Projects into Workspace`, then `Next >`.
1. Set `Select root directory:` to same value as `${DS_BUILD_DIR}` above.
1. Make sure the project is selected in `Projects:`, then click `Finish`.

## Building a CMake target

1. If the project has just been imported, let the `C/C++ Indexer` finish its job (see the status bar at the bottom).
1. In `Project Explorer` right-click on `mcu-driver-reference-platforms-for-arm-Debug@mcu-driver-reference-platforms-for-arm-DS_build-debug` and select `Build Targets` → `Build...`.
1. Select one of the `[exe]` targets, e.g. `[exe] mdh-arm-test-us_ticker_lp_ticker_common`, then click `Build`.

**NOTE**: This build uses a toolchain set with the `cmake` command from above, not the one bundled with the _Arm DS IDE_.

## Adding an FVP connection

### Shell (adding an FVP connection)

1. Go to a location of your SSE-300 FVP, e.g.

    ```bash
    cd ${FVP_INSTALL_DIR}/models/Linux64_GCC-6.4
    ```

1. Start the FVP with `--iris-server` option (and leave it running).

    ```bash
    ./FVP_Corstone_SSE-300_Ethos-U55 --parameter mps3_board.DISABLE_GATING=1 --iris-server
    ```

    Note that the above command contains only the necessary flags.

    For convenience, below is the command extended with additional useful options. For more info about _quantum_, see [this document][fvp_cs300_parameters].

    ```bash
    ./FVP_Corstone_SSE-300_Ethos-U55 --quantum=10 --parameter mps3_board.DISABLE_GATING=1 --parameter mps3_board.visualisation.disable-visualisation=1 --parameter mps3_board.telnetterminal0.start_telnet=0 --iris-server
    ```

### Arm DS IDE (adding an FVP connection)

1. `File` → `New` → `Model Connection`.
1. Set a `Debug connection name`, e.g. `SSE-300-attach`, check `Associate debug connection with an existing project` and select `mcu-driver-reference-platforms-for-arm-Debug@mcu-driver-reference-platforms-for-arm-DS_build-debug`. Click `Next >`.
1. (optional) Click `Add a new model...`
    1. Set `Model Interface` to `Iris`. Click `Next >`.
    1. Select `Browse for model running on local host`. Click `Next >`.
    1. Select `FVP_MPS3_Corstone_SSE_300 (port=7100)` and click `Finish`.
1. Select `Imported` → `FVP_MPS3_Corstone_SSE_300` and click `Finish`.
1. Go to `Files` tab and in `Application on host to download:` click `Workspace...` and choose an ELF file you want to load and debug, and click `OK`.
    1. (example) `mcu-driver-reference-platforms-for-arm-Debug` → `mdh_arm` → `tests` → `mbed_hal` → `us_ticker_lp_ticker_common` → `mdh-arm-test-us_ticker_lp_ticker_common.elf`.
1. Select `Load symbols`.
1. Go to `Debugger` tab and choose `Run control` → `Debug from entry point`.
1. Click `Apply` and then `Debug` to start a debugging session.
1. Make use of the debugging session or in the `Debug Control` click `Disconnect from Target`.

## Debugging

### Shell (debugging)

1. Go to a location of your SSE-300 FVP, e.g.

    ```bash
    cd ${FVP_INSTALL_DIR}/models/Linux64_GCC-6.4
    ```

1. Start the FVP with `--iris-server` option (and leave it running).

    ```bash
    ./FVP_Corstone_SSE-300_Ethos-U55 --parameter mps3_board.DISABLE_GATING=1 --iris-server
    ```

    Note that the above command contains only the necessary flags.

    For convenience, below is the command extended with additional useful options. For more info about _quantum_, see [this document][fvp_cs300_parameters].

    ```bash
    ./FVP_Corstone_SSE-300_Ethos-U55 --quantum=10 --parameter mps3_board.DISABLE_GATING=1 --parameter mps3_board.visualisation.disable-visualisation=1 --parameter mps3_board.telnetterminal0.start_telnet=0 --iris-server
    ```

### Arm DS IDE (debugging)

1. In the `Debug Control` tab right-click on a connection added in the previous step, i.e. `SSE-300-attach` and choose
    - `Debug Configurations...` to change settings, e.g. choose a different ELF file in the `Files` tab.
    - `Connect to Target` to start a new debugging session.

### Troubleshooting

#### No source file view

Symptoms:

- no source files show up when debugging,
- (in the `Disassembly` tab) right-click and `Show in Source` results in _No source available for address (...)_,
- (in the `Commands` tab) "_WARNING(IMG53): <your-elf-file>.elf has no source level debug information_".

Solution:

1. Open `<your-elf-file>.elf` in the built-in _ELF content editor_ and verify the presence of debug sections.
    1. In the `Project Explorer`, double click `<your-elf-file>.elf`.
    1. (example) `mcu-driver-reference-platforms-for-arm-Debug` → `mdh_arm` → `tests` → `mbed_hal` → `us_ticker_lp_ticker_common` → `mdh-arm-test-us_ticker_lp_ticker_common.elf`.
    1. Go to `Sections` tab. If `.debug_*` (i.e. `.debug_line`) sections are missing, you have to fix the compiler (and linker) flags in your project.
1. Fix the [_armclang_ compiler flags][1] to include:
    - `-gdwarf-4`.
1. (optional) To further improve debugging experience you might consider these _armlink_ linker options:
    - `--no_remove`,
    - `--debug`,
    - `--bestdebug`.
1. With the flags fixed,
    1. export the CMake project again,
    1. rebuild the target,
    1. start the debugger.

[1]: https://gitlab.arm.com/iot/open-iot-sdk/mcu-driver-hal/mcu-driver-hal/-/blob/release-0.2.0/tools/cmake/profiles/debug.cmake#L59-92
[fvp_cs300_parameters]: ./parameters.md
