# Getting started, a step-by-step guide

This guide gathers all the steps necessary to set up a basic development environment, build the examples and tests, and run them on the FVP. More details in the relevant docs.

Completing this guide produces an environment with the following components:

- host OS: `Ubuntu 20.04.4 LTS`,
- Python interpreter: `Python 3.8.10`,
- build system: `cmake version 3.22.4` & `GNU Make 4.2.1`,
- toolchain: `arm-none-eabi-gcc (GNU Arm Embedded Toolchain 10.3-2021.10) 10.3.1 20210824 (release)`,
- FVP_Corstone_SSE-300_Ethos-U55: `Fast Models [11.16.26 (Dec  9 2021)]`,
- testing tools:
  - `greentea-host 2.0.2`,
  - `mbed-fastmodel-agent 2.0.1`,
  - `mbed-greentea 1.8.13`,
  - `mbed-host-tests 1.8.13`.

## Prerequisites

This guide builds on top of a fresh installation of the Ubuntu 20.04 OS.

## Install system packages

```console
$ sudo apt-get --yes update
$ sudo apt-get --yes install git python3 python3-pip wget xterm
$ sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.8 1
```

## Install Python modules

```console
$ pip3 install --upgrade cmake greentea-host mbed-greentea mbed-host-tests
```

## Install GNU Arm Embedded Toolchain

```console
$ wget --quiet https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.10/gcc-arm-none-eabi-10.3-2021.10-x86_64-linux.tar.bz2 --output-document=- | sudo tar --extract --bzip2 --directory=/opt
$ export PATH="/opt/gcc-arm-none-eabi-10.3-2021.10/bin:${PATH}"
```

## Fetch the project source files

Note that `$GIT_PROJECTS_DIR` variable is used only to simplify the guide.

```console
$ GIT_PROJECTS_DIR=${HOME}
$ cd ${GIT_PROJECTS_DIR}
$ git clone --recursive <HostingLocation>/mcu-driver-hal/mcu-driver-reference-platforms-for-arm
```

## Build all the examples and tests

Note you may need to log out and log back in, to have the `$PATH` updated. As an alternative reload the files you set the `$PATH` in, e.g.

```console
$ source ~/.profile
```

```console
$ cd ${GIT_PROJECTS_DIR}/mcu-driver-reference-platforms-for-arm/
$ cmake -S ./ -B ./build/ --toolchain=components/mcu-driver-hal/tools/cmake/toolchain/toolchain-arm-none-eabi-gcc.cmake -D CMAKE_SYSTEM_PROCESSOR=cortex-m55 -D MDH_PLATFORM=ARM_AN552_MPS3 -D VARIANT=FVP -D HTRUN_ARGUMENTS="--micro=FVP_CS300_U55;--fm=MPS3"
$ cmake --build ./build/ --parallel
```

## Install FVP Corstone-300, AN552

```console
$ FVP_TEMP_DIR=$(mktemp --directory)
$ pushd ${FVP_TEMP_DIR}
$ wget https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Corstone-300/FVP_Corstone_SSE-300_11.16_26.tgz
$ tar --extract --gzip --file=FVP_Corstone_SSE-300_11.16_26.tgz
$ sudo ./FVP_Corstone_SSE-300.sh --i-agree-to-the-contained-eula --force --destination /opt/FVP_Corstone_SSE-300 --no-interactive
$ popd
$ rm --recursive ${FVP_TEMP_DIR}
$ export PATH="/opt/FVP_Corstone_SSE-300/models/Linux64_GCC-6.4:${PATH}"
```

## Run the serial example on FVP

```console
$ FVP_Corstone_SSE-300_Ethos-U55 --application=${GIT_PROJECTS_DIR}/mcu-driver-reference-platforms-for-arm/build/examples/serial/mdh-arm-hal-example-serial.elf
```

The FVP supports many handy options. For example you can disable the visualisation window and redirect the serial output to STDOUT as below.

```console
$ FVP_Corstone_SSE-300_Ethos-U55 -C mps3_board.visualisation.disable-visualisation=1 -C mps3_board.telnetterminal0.start_telnet=0 -C mps3_board.uart0.out_file=- -C mps3_board.uart0.unbuffered_output=1 --application=${GIT_PROJECTS_DIR}/mcu-driver-reference-platforms-for-arm/build/examples/serial/mdh-arm-hal-example-serial.elf
(...)

    Ethos-U rev 136b7d75 --- Nov 25 2021 12:05:57
    (C) COPYRIGHT 2019-2021 Arm Limited
    ALL RIGHTS RESERVED

Hello world!
<NUL>^C
Stopping simulation...
```

## Install mbed-fastmodel-agent

This is a component that enables `ctest` to run tests on the FVP.

```console
$ cd ${GIT_PROJECTS_DIR}
$ git clone https://github.com/ARMmbed/mbed-fastmodel-agent.git
$ cd mbed-fastmodel-agent
$ git checkout v2.0.1
$ cat <<EOF >fm_agent/settings.json
{
    "COMMON": {
        "IRIS_path": "/opt/FVP_Corstone_SSE-300/Iris/Python/"
    },
    "FVP_CS300_U55": {
        "model_binary": "/opt/FVP_Corstone_SSE-300/models/Linux64_GCC-6.4/FVP_Corstone_SSE-300_Ethos-U55",
        "model_options": [
            "--quantum=10"
        ],
        "terminal_component": "component.FVP_MPS3_Corstone_SSE_300.mps3_board.telnetterminal0",
        "configs": {
            "MPS3": "MPS3.conf"
        }
    }
}
EOF
$ sudo python3 setup.py install
```

## Run all the tests on the FVP

Run all the tests except the EMAC test which requires additional setup.

```console
$ cd ${GIT_PROJECTS_DIR}/mcu-driver-reference-platforms-for-arm/
$ ctest --test-dir build --show-only
(...)
  Test #1: mdh-arm-test-echo
  Test #2: mdh-arm-test-emac
  Test #3: mdh-arm-test-flash-functional_tests
  Test #4: mdh-arm-test-sleep
  Test #5: mdh-arm-test-us_ticker_lp_ticker_common
  Test #6: mdh-arm-test-us_ticker
  Test #7: mdh-arm-test-lp_ticker

$ ctest --test-dir build --verbose --no-tests=error --exclude-regex "mdh-arm-test-emac"
(...)
100% tests passed, 0 tests failed out of 6

Total Test time (real) =  58.66 sec
```
