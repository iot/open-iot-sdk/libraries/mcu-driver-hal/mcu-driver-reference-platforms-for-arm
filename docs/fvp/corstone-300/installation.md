# FVP Corstone-300 installation

## Prerequisites

- The most recent release of one of the supported toolchains. For more information about toolchains, see [here][sdk_toolchain].
  - `Arm Compiler for Embedded`,
  - `GNU Arm Embedded Toolchain`.
- For Linux hosts
  - `telnet`,
  - `xterm`.
- For Windows hosts
  - `telnet` (`Microsoft Telnet Client` enabled).

## Setup

1. Obtain and install the `FVP model for the Corstone-300 MPS3 based platform`. Installers for both, Linux and Windows, are available [here][fvp_downloads]. `FVP_Corstone_SSE-300_11.16_26`, which simulates AN552, is recommended.

1. It is recommended to add the FVP executable to your `PATH` environment variable. The binaries are located

    - in `<installation_directory>/models/Linux64_GCC-6.4/`, for Linux,
    - in `<installation_directory>\models\Win64_VC2017\`, for Windows.

    You can verify that the FVP is correctly added to `PATH` by running `FVP_Corstone_SSE-300_Ethos-U55 --version`.

1. To enable `ctest` to run tests on the FVP, install `mbed-fastmodel-agent` Python module. Note that it is required to set the `<path_to>` placeholders with correct value before executing the final install command.

    ```console
    $ git clone https://github.com/ARMmbed/mbed-fastmodel-agent.git
    $ cd mbed-fastmodel-agent
    $ git checkout v2.0.3
    $ cat <<EOF >fm_agent/settings.json
    {
        "COMMON": {
            "IRIS_path": "<path_to>/FVP_Corstone_SSE-300/Iris/Python/"
        },
        "FVP_CS300_U55": {
            "model_binary": "<path_to>/FVP_Corstone_SSE-300/models/Linux64_GCC-6.4/FVP_Corstone_SSE-300_Ethos-U55",
            "model_options": [
                "--quantum=10"
            ],
            "terminal_component": "component.FVP_MPS3_Corstone_SSE_300.mps3_board.telnetterminal0",
            "configs": {
                "MPS3": "MPS3.conf",
                "MPS3-tap-network": "MPS3-tap-network.conf"
            }
        }
    }
    EOF
    $ sudo python3 setup.py install
    ```

[fvp_downloads]: https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps
[sdk_toolchain]: https://gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/Prerequisites.md#toolchains
