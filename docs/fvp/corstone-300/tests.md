# FVP Corstone-300 tests

Note this guide is specific to the Corstone-300 FVP, for generic info about tests see [here][tests_readme].

## Building a test

Follow the build instructions from the [main README file][main_readme_building] and set the FVP-specific options as below

- set the `CMAKE_SYSTEM_PROCESSOR` to `cortex-m55`,
- set the `MDH_PLATFORM` to `ARM_AN552_MPS3`,
- set the build `VARIANT` to `FVP`,
- set the `HTRUN_ARGUMENTS` to `"--micro=FVP_CS300_U55;--fm=MPS3"` to enable `ctest` to run tests on the FVP.
- set the `HTRUN_ARGUMENTS` to `"--micro=FVP_CS300_U55;--fm=MPS3-tap-network"` to enable `ctest` to run EMAC tests on the FVP.

For example, these are the configure and build commands specific to the echo test

```console
$ cmake -S ./ -B ./build/ --toolchain=components/mcu-driver-hal/tools/cmake/toolchain/toolchain-arm-none-eabi-gcc.cmake -D CMAKE_SYSTEM_PROCESSOR=cortex-m55 -D MDH_PLATFORM=ARM_AN552_MPS3 -D VARIANT=FVP -D HTRUN_ARGUMENTS="--micro=FVP_CS300_U55;--fm=MPS3"
$ cmake --build ./build/ --target mdh-arm-test-echo
```

## Running a test

Use the `ctest` to run all the tests, or only the selected ones.

For example, this is the command specific to the echo test

```console
$ ctest --test-dir ./build/ --verbose --no-tests=error --tests-regex 'mdh-arm-test-echo'
(...)
The following tests passed:
	mdh-arm-test-echo
(...)
```

## Running the EMAC test

As noted [here][tests_readme_emac_notes], it is mandatory to set up a bridge named `armbr0`, and a `ARMfmuser` TAP interface, for example

```console
$ cat /etc/network/interfaces
source-directory /etc/network/interfaces.d

auto armbr0
iface armbr0 inet static
    address 192.168.1.11
    netmask 255.255.255.0
    gateway 192.168.1.10
    pre-up ip tuntap add dev ARMfmuser mode tap user root
    pre-up ifconfig ARMfmuser 0.0.0.0 promisc
    post-down ip tuntap del dev ARMfmuser mode tap
    bridge_ports ARMfmuser
$ ifup armbr0
```

With `armbr0` up, and `HTRUN_ARGUMENTS` set as mentioned above, use the `ctest` to run the EMAC test.

```console
$ ctest --test-dir ./build/ --verbose --no-tests=error --tests-regex 'mdh-arm-test-emac'
(...)
The following tests passed:
	mdh-arm-test-emac
(...)
```

[main_readme_building]: ../../../README.md#building
[tests_readme]: ../../../tests/README.md
[tests_readme_emac_notes]: ../../../tests/README.md#emac-test-notes
