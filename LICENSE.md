Unless specifically indicated otherwise in a file, files are licensed under the Apache 2.0 license,
as can be found in: LICENSE-apache-2.0.txt

## Components

Folders containing external components are listed below. Each component should contain its own README file with license specified for its files. The original license text is included in those source files.

```json:table
{
    "fields": [
        "Component",
        "Path",
        "License",
        "Origin",
        "Category",
        "Version",
        "Security risk"
    ],
    "items": [
        {
            "Component": "Audio I2S driver",
            "Path": "components/audio/arm_mps3_audio",
            "License": "BSD-3-Clause",
            "Origin": "",
            "Category": "1",
            "Version": "v3.1.1",
            "Security risk": "low"
        },
        {
            "Component": "CMSIS_5",
            "Path": "components/CMSIS/CMSIS_5",
            "License": "Apache 2.0",
            "Origin": "https://github.com/ARM-software/CMSIS_5.git",
            "Category": "1",
            "Version": "5.9.0",
            "Security risk": "low"
        },
        {
            "Component": "ethernet driver (smsc9220)",
            "Path": "components/ethernet/smsc9220",
            "License": "Apache 2.0",
            "Origin": "",
            "Category": "1",
            "Version": "",
            "Security risk": "low"
        },
        {
            "Component": "i2c driver",
            "Path": "components/i2c/sbcon",
            "License": "Apache 2.0",
            "Origin": "",
            "Category": "1",
            "Version": "",
            "Security risk": "low"
        },
        {
            "Component": "io driver (arm_mps_io)",
            "Path": "components/io/arm_mps_io",
            "License": "Apache 2.0",
            "Origin": "",
            "Category": "1",
            "Version": "",
            "Security risk": "low"
        },
        {
            "Component": "io driver (cmsdk_gpio)",
            "Path": "components/io/cmsdk_gpio",
            "License": "Apache 2.0",
            "Origin": "",
            "Category": "1",
            "Version": "",
            "Security risk": "low"
        },
        {
            "Component": "spi driver",
            "Path": "components/spi/pl022",
            "License": "Apache 2.0",
            "Origin": "",
            "Category": "1",
            "Version": "",
            "Security risk": "low"
        },
        {
            "Component": "timer driver (cmsdk_dualtimer)",
            "Path": "components/timer/cmsdk_dualtimer",
            "License": "Apache 2.0",
            "Origin": "",
            "Category": "1",
            "Version": "",
            "Security risk": "low"
        },
        {
            "Component": "timer driver (cmsdk_timer)",
            "Path": "components/timer/cmsdk_timer",
            "License": "Apache 2.0",
            "Origin": "",
            "Category": "1",
            "Version": "",
            "Security risk": "low"
        },
        {
            "Component": "timer driver (syscounter)",
            "Path": "components/timer/syscounter_armv8-m",
            "License": "Apache 2.0",
            "Origin": "",
            "Category": "1",
            "Version": "",
            "Security risk": "low"
        },
        {
            "Component": "timer driver (systimer)",
            "Path": "components/timer/systimer_armv8-m",
            "License": "Apache 2.0",
            "Origin": "",
            "Category": "1",
            "Version": "",
            "Security risk": "low"
        },
        {
            "Component": "uart",
            "Path": "components/uart",
            "License": "Apache 2.0",
            "Origin": "",
            "Category": "1",
            "Version": "",
            "Security risk": "low"
        },
        {
            "Component": "platforms/cs300, platforms/cs310, platforms/mps2",
            "Path": "platforms",
            "License": "Apache 2.0",
            "Origin": "",
            "Category": "1",
            "Version": "",
            "Security risk": "low"
        }
    ]
}
```
