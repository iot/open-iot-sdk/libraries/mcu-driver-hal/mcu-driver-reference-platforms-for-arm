# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

add_library(i2c-sbcon i2c_sbcon_drv.c)

target_include_directories(i2c-sbcon PUBLIC .)
