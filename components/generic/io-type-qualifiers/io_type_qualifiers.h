/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef IO_TYPE_QUALIFIERS_M
#define IO_TYPE_QUALIFIERS_M

// This header file provides following definitions:
// - Register access:
//   __I
//   __O
//   __IO
// - Struct member access:
//   __IM
//   __OM
//   __IOM

// Read only access to a register.
#ifndef __I
#ifdef __cplusplus
#define __I volatile
#else // __cplusplus
#define __I volatile const
#endif // __cplusplus
#endif // __I

// Write only access to a register.
#ifndef __O
#define __O volatile
#endif // __O

// Read & write access to a register.
#ifndef __IO
#define __IO volatile
#endif // __IO

// Read only access to a struct member.
#ifndef __IM
#define __IM volatile const
#endif // __IM

// Write only access to a struct member.
#ifndef __OM
#define __OM volatile
#endif // __OM

// Read & write access to a struct member.
#ifndef __IOM
#define __IOM volatile
#endif // __IOM

#endif // IO_TYPE_QUALIFIERS_M
