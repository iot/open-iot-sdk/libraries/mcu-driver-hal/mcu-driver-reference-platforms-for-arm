# Xilinx AXI Quad SPI LogiCORE IP controller native driver

Implementation of low-level operations for the Xilinx AXI QSPI controller.

This non-Arm IP is currently used on [Arm MPS3 FPGA Prototyping Board](https://developer.arm.com/documentation/100765/0000).

## Terminology

The _Master/Slave_ terminology for SPI is considered obsolete. _Master_ is now _Controller_ and _Slave_ is now _Peripheral_.
