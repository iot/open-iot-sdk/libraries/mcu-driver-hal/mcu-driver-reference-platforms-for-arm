/* Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __QSPI_XILINX_DRV_H__
#define __QSPI_XILINX_DRV_H__

#include <stddef.h>
#include <stdint.h>

#define QSPI_XILINX_FIXED_FREQUENCY_HZ 32000000

typedef enum qspi_xilinx_error_e {
    QSPI_XILINX_ERR_NONE = 0,     /*!< No error */
    QSPI_XILINX_ERR_INVALID_ARGS, /*!< Invalid input arguments */
    QSPI_XILINX_ERR_TRANSACTION,  /*!< Serial Quad I/O (SQI) transaction error */
    QSPI_XILINX_ERR_COMMAND,      /*!< Serial Quad I/O (SQI) transaction opcode error */
} qspi_xilinx_error_t;

typedef struct qspi_xilinx_dev_cfg_s {
    const uint32_t base;     /*!< QSPI base address */
    const uint32_t scc_base; /*!< FPGA Serial Configuration Controller base address */
} qspi_xilinx_dev_cfg_t;

typedef enum qspi_xilinx_clock_phase_e {
    QSPI_XILINX_CLOCK_PHASE_0 = 0,
    QSPI_XILINX_CLOCK_PHASE_1 = 1
} qspi_xilinx_clock_phase_t;

typedef enum qspi_xilinx_clock_polarity_e {
    QSPI_XILINX_CLOCK_POLARITY_0 = 0,
    QSPI_XILINX_CLOCK_POLARITY_1 = 1
} qspi_xilinx_clock_polarity_t;

typedef struct qspi_xilinx_ctrl_cfg_s {
    qspi_xilinx_clock_phase_t cpha;
    qspi_xilinx_clock_polarity_t cpol;
} qspi_xilinx_ctrl_cfg_t;

typedef struct qspi_xilinx_dev_data_s {
    qspi_xilinx_ctrl_cfg_t ctrl_cfg;
} qspi_xilinx_dev_data_t;

typedef struct qspi_xilinx_dev_s {
    const qspi_xilinx_dev_cfg_t *const cfg; /*!< QSPI configuration */
    qspi_xilinx_dev_data_t *const data;     /*!< QSPI data */
} qspi_xilinx_dev_t;

typedef enum qspi_xilinx_clock_mode_e {
    QSPI_XILINX_CLOCK_MODE_0 = 0,
    QSPI_XILINX_CLOCK_MODE_1 = 1,
    QSPI_XILINX_CLOCK_MODE_2 = 2,
    QSPI_XILINX_CLOCK_MODE_3 = 3,
} qspi_xilinx_clock_mode_t;

typedef struct qspi_xilinx_transaction_s {
    struct {
        const void *tx_buff;
        size_t tx_buff_size;
        void *rx_buff;
        size_t rx_buff_size;
    } data;
    uint32_t address;
    uint8_t opcode;
    uint8_t num_address_bytes;
    uint8_t num_dummy_bytes; // 1 dummy byte equals 2 clock periods in Serial Quad I/O (SQI) mode.
} qspi_xilinx_transaction_t;

/** Initialize Xilinx AXI QSPI controller.
 *
 * @param dev QSPI controller.
 * @return QSPI_XILINX_ERR_NONE if successfully initialized.
 */
qspi_xilinx_error_t qspi_xilinx_init(qspi_xilinx_dev_t *dev);

/** Deinitialize Xilinx AXI QSPI controller.
 *
 * @param dev QSPI controller.
 * @return QSPI_XILINX_ERR_NONE if successfully uninitialized.
 */
qspi_xilinx_error_t qspi_xilinx_deinit(qspi_xilinx_dev_t *dev);

/** Set SPI clock polarity and phase mode.
 *
 * @note
 * The only clock modes allowed in dual and quad SPI modes are:
 * - `QSPI_XILINX_CLOCK_MODE_0`,
 * - `QSPI_XILINX_CLOCK_MODE_3`.
 *
 * @param dev QSPI controller.
 * @param mode A SPI clock mode.
 * @return QSPI_XILINX_ERR_NONE if successfully set.
 *         QSPI_XILINX_ERR_INVALID_ARGS if invalid parameter found.
 */
qspi_xilinx_error_t qspi_xilinx_set_clock_mode(qspi_xilinx_dev_t *dev, qspi_xilinx_clock_mode_t mode);

/** Execute SPI transaction.
 *
 * @param dev QSPI controller.
 * @param transaction A SPI transaction struct.
 * @return QSPI_XILINX_ERR_NONE if transaction completed successfully.
 *         QSPI_XILINX_ERR_INVALID_ARGS if invalid parameter found.
 *         QSPI_XILINX_ERR_TRANSACTION if transaction error occurred.
 *         QSPI_XILINX_ERR_COMMAND if the controller rejected command opcode.
 */
qspi_xilinx_error_t qspi_xilinx_execute_transaction(qspi_xilinx_dev_t *dev,
                                                    const qspi_xilinx_transaction_t *const transaction);

#endif /* __QSPI_XILINX_DRV_H__ */
