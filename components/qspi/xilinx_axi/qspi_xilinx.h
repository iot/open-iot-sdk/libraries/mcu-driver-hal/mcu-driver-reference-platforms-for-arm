/* Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __QSPI_XILINX_H__
#define __QSPI_XILINX_H__

#include "io_type_qualifiers.h"

#include <stdint.h>

typedef struct fpga_scc_reg_map_s {
    __IOM uint32_t cfg_reg0; // offset: 0x00
    __IOM uint32_t cfg_reg1; // offset: 0x04
    __IOM uint32_t cfg_reg2; // offset: 0x08, Bits [0]: QSPI Select signal
    // The remaining SCC registers are not relevant to the QSPI implementation and are therefore not listed.
} fpga_scc_reg_map_t;

enum fpga_scc_reg_cfg_reg2_bits_e {
    FPGA_SCC_REG_CFG_REG2_QSPISEL_WRITE_NO_XIP = 0 // QSPI Select signal, 0 - XIP read only, 1 - Write enable
};

typedef struct qspi_xilinx_dev_reg_map_s {
    // Reserved
    __IM uint32_t reserved_0_6[7]; // offset: 0x00

    // Interrupt Control Grouping
    __IOM uint32_t dgier;     // offset: 0x1c, Device global interrupt enable register
    __IOM uint32_t ipisr;     // offset: 0x20, IP interrupt status register, TOW: toggle on write
    __IM uint32_t reserved_9; // offset: 0x24
    __IOM uint32_t ipier;     // offset: 0x28, IP interrupt enable register

    // Reserved
    __IM uint32_t reserved_11_15[5]; // offset: 0x2c

    // Core Grouping
    __OM uint32_t srr;               // offset: 0x40, Software reset register
    __IM uint32_t reserved_17_23[7]; // offset: 0x44
    __IOM uint32_t spicr;            // offset: 0x60, SPI control register
    __IM uint32_t spisr;             // offset: 0x64, SPI status register
    __OM uint32_t spidtr;            // offset: 0x68, SPI data transmit register
    __IM uint32_t spidrr;            // offset: 0x6c, SPI data receive register
    __IOM uint32_t spipsr;           // offset: 0x70, SPI peripheral select register
    __IM uint32_t spitfifo;          // offset: 0x74, SPI transmit FIFO occupancy register
    __IM uint32_t spirfifo;          // offset: 0x78, SPI receive FIFO occupancy register
} qspi_xilinx_dev_reg_map_t;

// Device Global Interrupt Enable Register
enum qspi_xilinx_reg_dgier_bits_e {
    QSPI_XILINX_REG_DGIER_GIE = 31, // GIE (Global Interrupt Enable)
};

// IP Interrupt Status Register
enum qspi_xilinx_reg_ipisr_bits_e {
    QSPI_XILINX_REG_IPISR_MODFERR = 0,       // MODF Error (Mode-fault Error)
    QSPI_XILINX_REG_IPISR_PERIPHMODFERR = 1, // Peripheral MODF Error (Peripheral Mode-fault Error)
    QSPI_XILINX_REG_IPISR_DTRE = 2,          // DTR Empty
    QSPI_XILINX_REG_IPISR_DTRUNDRN = 3,      // DTR Underrun
    QSPI_XILINX_REG_IPISR_DRRFULL = 4,       // DRR Full
    QSPI_XILINX_REG_IPISR_DRROVRN = 5,       // DRR Overrun
    QSPI_XILINX_REG_IPISR_TXFIFOHE = 6,      // TX FIFO Half Empty
    QSPI_XILINX_REG_IPISR_PERIPHSELMOD = 7,  // Peripheral Select Mode
    QSPI_XILINX_REG_IPISR_DRRNE = 8,         // DRR Not Empty
    QSPI_XILINX_REG_IPISR_CPOLCPHAERR = 9,   // CPOL CPHA Error
    QSPI_XILINX_REG_IPISR_PERIPHMODERR = 10, // Peripheral Mode Error
    QSPI_XILINX_REG_IPISR_MSBERR = 11,       // MSB Error
    QSPI_XILINX_REG_IPISR_LOOPERR = 12,      // Loopback Error
    QSPI_XILINX_REG_IPISR_CMDERR = 13,       // Command Error
};

// IP Interrupt Enable Register
enum qspi_xilinx_reg_ipier_bits_e {
    QSPI_XILINX_REG_IPIER_MODFERR = 0,       // MODF Error (Mode-fault Error)
    QSPI_XILINX_REG_IPIER_PERIPHMODFERR = 1, // Peripheral MODF Error (Peripheral Mode-fault Error)
    QSPI_XILINX_REG_IPIER_DTRE = 2,          // DTR Empty
    QSPI_XILINX_REG_IPIER_DTRUNDRN = 3,      // DTR Underrun
    QSPI_XILINX_REG_IPIER_DRRFULL = 4,       // DRR Full
    QSPI_XILINX_REG_IPIER_DRROVRN = 5,       // DRR Overrun
    QSPI_XILINX_REG_IPIER_TXFIFOHE = 6,      // TX FIFO Half Empty
    QSPI_XILINX_REG_IPIER_PERIPHSELMOD = 7,  // Peripheral Select Mode
    QSPI_XILINX_REG_IPIER_DRRNE = 8,         // DRR Not Empty
    QSPI_XILINX_REG_IPIER_CPOLCPHAERR = 9,   // CPOL CPHA Error
    QSPI_XILINX_REG_IPIER_PERIPHMODERR = 10, // Peripheral Mode Error
    QSPI_XILINX_REG_IPIER_MSBERR = 11,       // MSB Error
    QSPI_XILINX_REG_IPIER_LOOPERR = 12,      // Loopback Error
    QSPI_XILINX_REG_IPIER_CMDERR = 13,       // Command Error
};

// SPI Control register
enum qspi_xilinx_reg_spicr_bits_e {
    QSPI_XILINX_REG_SPICR_LOOP = 0,           // LOOP (Local loopback mode)
    QSPI_XILINX_REG_SPICR_SPE = 1,            // SPE (SPI system enable)
    QSPI_XILINX_REG_SPICR_CTRL = 2,           // Controller (SPI controller mode)
    QSPI_XILINX_REG_SPICR_CPOL = 3,           // CPOL (Clock polarity)
    QSPI_XILINX_REG_SPICR_CPHA = 4,           // CPHA (Clock phase)
    QSPI_XILINX_REG_SPICR_TXFIFORST = 5,      // TX FIFO Reset
    QSPI_XILINX_REG_SPICR_RXFIFORST = 6,      // RX FIFO Reset
    QSPI_XILINX_REG_SPICR_MANPERIPHSELAE = 7, // Manual Peripheral Select Assertion Enable
    QSPI_XILINX_REG_SPICR_CTRLTRINH = 8,      // Controller Transaction Inhibit
    QSPI_XILINX_REG_SPICR_LSBFST = 9,         // LSB First
};

// SPI Status Register
enum qspi_xilinx_reg_spisr_bits_e {
    QSPI_XILINX_REG_SPISR_RXE = 0,          // RX Empty
    QSPI_XILINX_REG_SPISR_RXFULL = 1,       // RX Full
    QSPI_XILINX_REG_SPISR_TXE = 2,          // TX Empty
    QSPI_XILINX_REG_SPISR_TXFULL = 3,       // TX Full
    QSPI_XILINX_REG_SPISR_MODFERR = 4,      // MODF Error (Mode-fault Error)
    QSPI_XILINX_REG_SPISR_PERIPHSELMOD = 5, // Peripheral Mode Select
    QSPI_XILINX_REG_SPISR_CPOLCPHAERR = 6,  // CPOL CPHA Error
    QSPI_XILINX_REG_SPISR_PERIPHMODERR = 7, // Peripheral Mode Error
    QSPI_XILINX_REG_SPISR_MSBERR = 8,       // MSB Error
    QSPI_XILINX_REG_SPISR_LOOPERR = 9,      // Loopback Error
    QSPI_XILINX_REG_SPISR_CMDERR = 10,      // Command Error
};

// Software Reset Register
// Writing 0x0000_000a to the SRR resets the core register for four AXI clock cycles.
// Any other write access generates undefined results and results in an error.
#define QSPI_XILINX_SRR_RST_VAL_RESET 0x0000000AUL

// SPI Data Transmit Register
enum qspi_xilinx_reg_spidtr_bits_e {
    QSPI_XILINX_REG_SPIDTR_TXDATA_LSB = 0, // LSB of N-bit SPI transmit data. N can be 8, 16 or 32.
};

// SPI Data Receive Register
enum qspi_xilinx_reg_spidrr_bits_e {
    QSPI_XILINX_REG_SPIDRR_RXDATA_LSB = 0, // LSB of N-bit SPI receive data. N can be 8, 16 or 32.
};

// SPI Peripheral Select Register
// The peripherals are numbered right to left; active-low.
enum qspi_xilinx_reg_spipsr_bits_e {
    QSPI_XILINX_REG_SPIPSR_SELPERIPH_0 = 0, // Selected Peripheral 0 (the only one available)
};

#endif /* __QSPI_XILINX_H__ */
