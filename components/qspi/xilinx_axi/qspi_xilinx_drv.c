/* Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "qspi_xilinx_drv.h"
#include "qspi_xilinx.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

// Xilinx AXI QSPI IP core config
#define QSPI_XILINX_FIFO_DEPTH 256
#define QSPI_XILINX_FIFO_WIDTH 8

#define GET_BIT(WORD, BIT_INDEX)  ((bool)((WORD) & (1UL << (BIT_INDEX))))
#define CLR_BIT(WORD, BIT_INDEX)  ((WORD) &= ~(1UL << (BIT_INDEX)))
#define SET_BIT(WORD, BIT_INDEX)  ((WORD) |= (1UL << (BIT_INDEX)))
#define GET_REG_8BIT(WORD)        (0xFFUL & (WORD))
#define SET_REG_8BIT(WORD, VALUE) ((WORD) = (0xFFUL & (VALUE)))

typedef struct {
    enum { OPCODE_ADDRESS_DUMMY_AND_DATA, DATA_ONLY } phase;
    const qspi_xilinx_transaction_t *const transaction;
    const void *current_tx_buff_addr;
    size_t remaining_tx_data_size;
    const void *current_rx_buff_addr;
    size_t remaining_rx_data_size;
} transaction_transfer_t;

static void write_tx_fifo(qspi_xilinx_dev_reg_map_t *const qspi_regs, transaction_transfer_t *const transfer);
static qspi_xilinx_error_t wait_until_tx_fifo_empty(qspi_xilinx_dev_reg_map_t *const qspi_regs);
static void read_rx_fifo(qspi_xilinx_dev_reg_map_t *const qspi_regs, transaction_transfer_t *const transfer);

qspi_xilinx_error_t qspi_xilinx_init(qspi_xilinx_dev_t *dev)
{
    // Set the Serial Configuration Controller QSPI Select to write mode.
    fpga_scc_reg_map_t *fpga_scc_regs = (fpga_scc_reg_map_t *)dev->cfg->scc_base;
    SET_BIT(fpga_scc_regs->cfg_reg2, FPGA_SCC_REG_CFG_REG2_QSPISEL_WRITE_NO_XIP);

    // Reset the QSPI controller
    qspi_xilinx_dev_reg_map_t *qspi_regs = (qspi_xilinx_dev_reg_map_t *)dev->cfg->base;
    qspi_regs->srr = QSPI_XILINX_SRR_RST_VAL_RESET;

    return QSPI_XILINX_ERR_NONE;
}

qspi_xilinx_error_t qspi_xilinx_deinit(qspi_xilinx_dev_t *dev)
{
    qspi_xilinx_dev_reg_map_t *qspi_regs = (qspi_xilinx_dev_reg_map_t *)dev->cfg->base;
    // Put the core into controller mode and set the controller transaction inhibit bit.
    __IO uint32_t spicr = 0x0UL;
    SET_BIT(spicr, QSPI_XILINX_REG_SPICR_CTRL);
    SET_BIT(spicr, QSPI_XILINX_REG_SPICR_CTRLTRINH);
    qspi_regs->spicr = spicr;

    return QSPI_XILINX_ERR_NONE;
}

qspi_xilinx_error_t qspi_xilinx_set_clock_mode(qspi_xilinx_dev_t *dev, qspi_xilinx_clock_mode_t mode)
{
    switch (mode) {
        case QSPI_XILINX_CLOCK_MODE_0:
            dev->data->ctrl_cfg.cpha = QSPI_XILINX_CLOCK_PHASE_0;
            dev->data->ctrl_cfg.cpol = QSPI_XILINX_CLOCK_POLARITY_0;
            break;
        case QSPI_XILINX_CLOCK_MODE_3:
            dev->data->ctrl_cfg.cpha = QSPI_XILINX_CLOCK_PHASE_1;
            dev->data->ctrl_cfg.cpol = QSPI_XILINX_CLOCK_POLARITY_1;
            break;
        case QSPI_XILINX_CLOCK_MODE_1:
        case QSPI_XILINX_CLOCK_MODE_2:
        default:
            // Only mode 00 and 11 are allowed in dual and quad SPI modes.
            return QSPI_XILINX_ERR_INVALID_ARGS;
    }
    return QSPI_XILINX_ERR_NONE;
}

qspi_xilinx_error_t qspi_xilinx_execute_transaction(qspi_xilinx_dev_t *dev,
                                                    const qspi_xilinx_transaction_t *const transaction)
{
    // Only 3-byte (24-bit) addresses are valid, 0 is used to skip the address command phase.
    // The buffer size can be 0 only if the buffer pointer is NULL.
    if ((transaction->num_address_bytes != 0U && transaction->num_address_bytes != 3U)
        || (transaction->data.tx_buff_size != 0U && transaction->data.tx_buff == NULL)
        || (transaction->data.rx_buff_size != 0U && transaction->data.rx_buff == NULL)) {
        return QSPI_XILINX_ERR_INVALID_ARGS;
    }

    qspi_xilinx_dev_reg_map_t *qspi_regs = (qspi_xilinx_dev_reg_map_t *)dev->cfg->base;

    // Reset the core. This reset is active for 16 AXI cycles, during which each FIFO register is in the reset state.
    qspi_regs->srr = QSPI_XILINX_SRR_RST_VAL_RESET;

    __IO uint32_t spicr = 0x0UL;
    // Put the core in controller mode.
    SET_BIT(spicr, QSPI_XILINX_REG_SPICR_CTRL);
    // Set the CPOL, CPHA values.
    if (dev->data->ctrl_cfg.cpha) {
        SET_BIT(spicr, QSPI_XILINX_REG_SPICR_CPHA);
    }
    if (dev->data->ctrl_cfg.cpol) {
        SET_BIT(spicr, QSPI_XILINX_REG_SPICR_CPOL);
    }
    // Make sure that the controller transaction inhibit bit is set.
    SET_BIT(spicr, QSPI_XILINX_REG_SPICR_CTRLTRINH);
    // Enable SPI system.
    SET_BIT(spicr, QSPI_XILINX_REG_SPICR_SPE);
    // Reset the DRR and DTR FIFOs.
    SET_BIT(spicr, QSPI_XILINX_REG_SPICR_RXFIFORST);
    SET_BIT(spicr, QSPI_XILINX_REG_SPICR_TXFIFORST);
    qspi_regs->spicr = spicr;

    transaction_transfer_t transfer = {.phase = OPCODE_ADDRESS_DUMMY_AND_DATA,
                                       .transaction = transaction,
                                       .current_tx_buff_addr = transaction->data.tx_buff,
                                       .remaining_tx_data_size = transaction->data.tx_buff_size,
                                       .current_rx_buff_addr = transaction->data.rx_buff,
                                       .remaining_rx_data_size = transaction->data.rx_buff_size};

    if (transaction->data.rx_buff_size > transaction->data.tx_buff_size) {
        // When the tx_buff is exhausted, dummy bytes will be sent to get the remaining RX data.
        transfer.remaining_tx_data_size = transfer.remaining_rx_data_size;
    }

    do {
        write_tx_fifo(qspi_regs, &transfer);

        if (transfer.phase == OPCODE_ADDRESS_DUMMY_AND_DATA) {
            // Assert the chip select signal from the core.
            CLR_BIT(qspi_regs->spipsr, QSPI_XILINX_REG_SPIPSR_SELPERIPH_0);
            // Enable the controller transaction inhibit bit, so that the core starts the SPI clock.
            CLR_BIT(qspi_regs->spicr, QSPI_XILINX_REG_SPICR_CTRLTRINH);
        }

        // Wait until the DTR empty interrupt is generated.
        qspi_xilinx_error_t status = wait_until_tx_fifo_empty(qspi_regs);
        if (status != QSPI_XILINX_ERR_NONE) {
            return status;
        }

        read_rx_fifo(qspi_regs, &transfer);

        // Clear the DTR Empty interrupt. Toggle on write.
        SET_BIT(qspi_regs->ipisr, QSPI_XILINX_REG_IPISR_DTRE);

        if (transfer.remaining_tx_data_size > 0U) {
            transfer.phase = DATA_ONLY;
        }
    } while (transfer.remaining_tx_data_size > 0U);

    // Disable the selected peripheral and stop the SPI clock.
    SET_BIT(qspi_regs->spipsr, QSPI_XILINX_REG_SPIPSR_SELPERIPH_0);

    return QSPI_XILINX_ERR_NONE;
}

static void write_tx_fifo(qspi_xilinx_dev_reg_map_t *const qspi_regs, transaction_transfer_t *const transfer)
{
    size_t remaining_tx_fifo_size = QSPI_XILINX_FIFO_DEPTH;

    if (transfer->phase == OPCODE_ADDRESS_DUMMY_AND_DATA) {
        // Write command, aka 8-bit opcode.
        SET_REG_8BIT(qspi_regs->spidtr, transfer->transaction->opcode);
        remaining_tx_fifo_size--;

        // Write address.
        if (transfer->transaction->num_address_bytes == 3U) {
            SET_REG_8BIT(qspi_regs->spidtr, transfer->transaction->address >> 16);
            SET_REG_8BIT(qspi_regs->spidtr, transfer->transaction->address >> 8);
            SET_REG_8BIT(qspi_regs->spidtr, transfer->transaction->address >> 0);
            remaining_tx_fifo_size -= 3U;
        }

        // Write dummy bytes.
        uint8_t remaining_tx_dummy_bytes = transfer->transaction->num_dummy_bytes;
        while (remaining_tx_dummy_bytes > 0U) {
            SET_REG_8BIT(qspi_regs->spidtr, 0x0UL);
            remaining_tx_dummy_bytes--;
            remaining_tx_fifo_size--;
        }
    }

    // Write data bytes.
    while (transfer->remaining_tx_data_size > 0U && remaining_tx_fifo_size > 0U) {
        if (transfer->current_tx_buff_addr
            < (transfer->transaction->data.tx_buff + transfer->transaction->data.tx_buff_size)) {
            // Write actual data from the TX buff.
            SET_REG_8BIT(qspi_regs->spidtr, *(const uint8_t *)transfer->current_tx_buff_addr);
            transfer->current_tx_buff_addr++;
        } else {
            // Write dummy data to drive the RX.
            SET_REG_8BIT(qspi_regs->spidtr, 0x0UL);
        }
        transfer->remaining_tx_data_size--;
        remaining_tx_fifo_size--;
    }
}

static qspi_xilinx_error_t wait_until_tx_fifo_empty(qspi_xilinx_dev_reg_map_t *const qspi_regs)
{
    __IO uint32_t ipisr;
    uint32_t ipisr_transaction_error_mask = 0x0UL;
    SET_BIT(ipisr_transaction_error_mask, QSPI_XILINX_REG_IPISR_MSBERR);
    SET_BIT(ipisr_transaction_error_mask, QSPI_XILINX_REG_IPISR_CPOLCPHAERR);
    SET_BIT(ipisr_transaction_error_mask, QSPI_XILINX_REG_IPISR_DRROVRN);
    SET_BIT(ipisr_transaction_error_mask, QSPI_XILINX_REG_IPISR_DTRUNDRN);
    SET_BIT(ipisr_transaction_error_mask, QSPI_XILINX_REG_IPISR_MODFERR);

    do {
        ipisr = qspi_regs->ipisr;
        // Check for errors.
        if (ipisr & ipisr_transaction_error_mask) {
            return QSPI_XILINX_ERR_TRANSACTION;
        }
        if (GET_BIT(ipisr, QSPI_XILINX_REG_IPISR_CMDERR)) {
            return QSPI_XILINX_ERR_COMMAND;
        }
    } while (!GET_BIT(ipisr, QSPI_XILINX_REG_IPISR_DTRE));

    return QSPI_XILINX_ERR_NONE;
}

static void read_rx_fifo(qspi_xilinx_dev_reg_map_t *const qspi_regs, transaction_transfer_t *const transfer)
{
    // Read and discard dummy data.
    if (transfer->phase == OPCODE_ADDRESS_DUMMY_AND_DATA) {
        uint8_t remaining_rx_dummy_bytes = 1U; // command opcode
        remaining_rx_dummy_bytes += transfer->transaction->num_address_bytes;
        remaining_rx_dummy_bytes += transfer->transaction->num_dummy_bytes;
        /* The only reliable way to determine that the receive FIFO is empty/full
        is by reading the Rx_Empty/Rx_Full status bit in the SPI status register. */
        while (remaining_rx_dummy_bytes && !GET_BIT(qspi_regs->spisr, QSPI_XILINX_REG_SPISR_RXE)) {
            __IO uint32_t spidrr_data_to_discard = qspi_regs->spidrr;
            (void)spidrr_data_to_discard;
            remaining_rx_dummy_bytes--;
        }
    }

    // Read data bytes.
    __IO uint32_t spidrr;
    while (!GET_BIT(qspi_regs->spisr, QSPI_XILINX_REG_SPISR_RXE)) {
        spidrr = qspi_regs->spidrr;
        if (transfer->remaining_rx_data_size > 0U) {
            *(uint8_t *)transfer->current_rx_buff_addr = (uint8_t)GET_REG_8BIT(spidrr);
            transfer->current_rx_buff_addr++;
            transfer->remaining_rx_data_size--;
        }
    }
}
