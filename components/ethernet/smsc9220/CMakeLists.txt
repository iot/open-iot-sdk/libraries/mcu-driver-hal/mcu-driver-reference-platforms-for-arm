# Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

add_library(ethernet-smsc9220 smsc9220_eth_drv.c)

target_include_directories(ethernet-smsc9220 PUBLIC .)

target_link_libraries(ethernet-smsc9220 PRIVATE io-type-qualifiers)
