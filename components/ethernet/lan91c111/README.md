# Microchip LAN91C111 Ethernet controller native driver

Implementation of low-level operations for the Microchip LAN91C111 Ethernet controller.

This non-ARM IP external chip is currently used on [Arm Fixed Virtual Platforms (FVPs)](https://developer.arm.com/documentation/100966/1115/Getting-Started-with-Fixed-Virtual-Platforms/Ethernet-with-VE-FVPs).

The functionalities provided are generic and not tied to the FVPs.

---
**Note**

The model IP used in certain versions of some FVPs do not accurately implement the frame format in the Receive areas. It omits to set bit fields of the Receive Frame Status (RFS), precisely the `MULTCAST` bit as well as the `HASH VALUE` bit field. These fields are used by the receive routine to speed up the group address search.
However, since the bit fields are missing from the RFS, the present native driver implements a workaround by performing multicast frame detection and hash value generation from received frames in software. This slows down the receive routine but provides a way to filter multicast frames.
The workaround can be removed during compilation as the code sections related to it are guarded by conditional pre-processor directives. If the multicast related bit fields are present in the RFS, include the define `LAN91C111_RFS_MULTICAST_SUPPORT` to exclude the workaround.

---
