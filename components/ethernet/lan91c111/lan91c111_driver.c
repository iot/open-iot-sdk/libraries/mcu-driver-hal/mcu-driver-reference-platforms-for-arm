/* Copyright (c) 2021 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>
#include "lan91c111_driver.h"

#define SET_BIT(WORD, BIT_INDEX) ((WORD) |= (1U << (BIT_INDEX)))
#define CLR_BIT(WORD, BIT_INDEX) ((WORD) &= ~(1U << (BIT_INDEX)))
#define GET_BIT(WORD, BIT_INDEX) (bool)(((WORD) & (1U << (BIT_INDEX))))
#define SET_BIT_FIELD(WORD, BIT_MASK, BIT_OFFSET, VALUE) \
            (WORD |= ((VALUE & BIT_MASK) << BIT_OFFSET))
#define CLR_BIT_FIELD(WORD, BIT_MASK, BIT_OFFSET, VALUE) \
            (WORD &= ~((VALUE & BIT_MASK) << BIT_OFFSET))
#define GET_BIT_FIELD(WORD, BIT_MASK, BIT_OFFSET) \
            ((WORD >> BIT_OFFSET) & BIT_MASK)


#define ETHERNET_MAC_ADDR_LEN 6U

uint16_t isr_bank_select_reg = MAC_REG_BANK_SELECT_DEFAULT_VALUE;
uint16_t isr_ptr_reg = MAC_REG_POINTER_DEFAULT_VALUE;

static enum mac_reg_bank_e get_selected_registers_bank(
    const lan91c111_dev_t* dev
);
static void select_registers_bank(
    const lan91c111_dev_t* dev, enum mac_reg_bank_e bank
);
static void write_mdo_pin(const lan91c111_dev_t* dev, uint8_t value);
static uint8_t read_mdi_pin(const lan91c111_dev_t* dev);
static void issue_mmu_command(
    const lan91c111_dev_t* dev, enum mmucr_cmd_e command
);
static bool complete_memory_allocation(const lan91c111_dev_t* dev);
static void get_multicast_table_position(
    uint32_t (*crc32) (const uint8_t *src, size_t len),
    const uint8_t* address,
    uint8_t* mt_reg_index,
    uint8_t* bit_index
);


static enum mac_reg_bank_e get_selected_registers_bank(
    const lan91c111_dev_t* dev
) {
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    return (enum mac_reg_bank_e)GET_BIT_FIELD(
        regs->bank_0.bank, MAC_REG_BANK_BANK_MASK, MAC_REG_BANK_BANK_POS
    );
}

static void select_registers_bank(
    const lan91c111_dev_t* dev, enum mac_reg_bank_e bank
) {
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    if (bank == get_selected_registers_bank(dev)) {
        return;
    }

    uint16_t reg_value = 0x0000U;
    SET_BIT_FIELD(
        reg_value,
        MAC_REG_BANK_BANK_MASK,
        MAC_REG_BANK_BANK_POS,
        bank
    );
    regs->bank_0.bank = reg_value;
}

static void write_mdo_pin(const lan91c111_dev_t* dev, uint8_t value) {
    select_registers_bank(dev, MAC_REG_BANK_3);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    uint16_t reg_value = regs->bank_3.mgmt;
    SET_BIT(reg_value, MAC_REG_MGMT_MDOE);

    if (value) {
        SET_BIT(reg_value, MAC_REG_MGMT_MDO);
    } else {
        CLR_BIT(reg_value, MAC_REG_MGMT_MDO);
    }

    regs->bank_3.mgmt = reg_value;

    SET_BIT(regs->bank_3.mgmt, MAC_REG_MGMT_MCLK);
    CLR_BIT(regs->bank_3.mgmt, MAC_REG_MGMT_MCLK);
}

static uint8_t read_mdi_pin(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_3);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    CLR_BIT(regs->bank_3.mgmt, MAC_REG_MGMT_MDOE);

    SET_BIT(regs->bank_3.mgmt, MAC_REG_MGMT_MCLK);

    uint8_t value = GET_BIT(regs->bank_3.mgmt, MAC_REG_MGMT_MDI);

    CLR_BIT(regs->bank_3.mgmt, MAC_REG_MGMT_MCLK);

    return value;
}

static void issue_mmu_command(
    const lan91c111_dev_t* dev, enum mmucr_cmd_e command
) {
    select_registers_bank(dev, MAC_REG_BANK_2);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    uint16_t reg_value = 0x0000U;

    SET_BIT_FIELD(
        reg_value,
        MAC_REG_MMUCR_OP_CODE_MASK,
        MAC_REG_MMUCR_OP_CODE_POS,
        command
    );

    regs->bank_2.mmucr = reg_value;

    while (GET_BIT(regs->bank_2.mmucr, MAC_REG_MMUCR_BUSY)) {}
}

static bool complete_memory_allocation(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_2);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    // Memory allocation sequence
    issue_mmu_command(dev, MMUCR_CMD_ALLOC_TX);

    select_registers_bank(dev, MAC_REG_BANK_2);
    while (!(GET_BIT(regs->bank_2.ist, MAC_REG_IST_ALLOC_INT))) {}

    return (bool)!GET_BIT(regs->bank_2.arr, MAC_REG_ARR_FAILED);
}

static void get_multicast_table_position(
    uint32_t (*crc32) (const uint8_t *src, size_t len),
    const uint8_t* address,
    uint8_t* mt_reg_index,
    uint8_t* bit_index
) {
    uint32_t crc = crc32(address, ETHERNET_MAC_ADDR_LEN);
    uint8_t hash = GET_BIT_FIELD(
        crc, CRC_MULTICAST_HASH_VALUE_MASK, CRC_MULTICAST_HASH_VALUE_POS
    );

    // The hash value is defined as the six most significant bits
    // of the CRC of the destination addresses. The three msb's determine the
    // register to be used (MT0-MT7), while the other three determine the bit
    // within the register.

    *mt_reg_index = GET_BIT_FIELD(
        hash, MAC_REG_MT_HASH_REG_MASK, MAC_REG_MT_HASH_REG_POS
    );

    *bit_index = GET_BIT_FIELD(
        hash, MAC_REG_MT_HASH_BIT_MASK, MAC_REG_MT_HASH_BIT_POS
    );
}

lan91c111_error_t lan91c111_init(const lan91c111_dev_t* dev) {
    if ((dev->cfg == NULL) || (dev->data == NULL)) {
        return LAN91C111_ERROR_ARG;
    }

    lan91c111_disable_all_interrupts(dev);
    lan91c111_clear_all_interrupts(dev);

    lan91c111_soft_reset(dev);

    /* Clear control registers */
    select_registers_bank(dev, MAC_REG_BANK_0);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;
    regs->bank_0.rcr = 0U;
    regs->bank_0.tcr = 0U;

    /* Power up the EPH */
    select_registers_bank(dev, MAC_REG_BANK_1);
    SET_BIT(regs->bank_1.cr, MAC_REG_CR_EPH_POW_EN);

    lan91c111_error_t error = lan91c111_reset_phy(dev);
    if (LAN91C111_ERROR_NONE != error) {
        return error;
    };

    // Read the interrupt bits to deassert them
    lan91c111_read_phy_mii_reg(dev, PHY_MII_REG_STATUS_OUT);

    lan91c111_advertise_cap(dev);
    lan91c111_set_phy_speed(dev);

    /* Set the Control Register */
    uint16_t reg_value = regs->bank_1.ctr;
    SET_BIT(reg_value, MAC_REG_CTR_AUTO_REL);
    SET_BIT(reg_value, MAC_REG_CTR_LE_ENABLE);
    SET_BIT(reg_value, MAC_REG_CTR_CR_ENABLE);
    SET_BIT(reg_value, MAC_REG_CTR_TE_ENABLE);
    select_registers_bank(dev, MAC_REG_BANK_1);
    regs->bank_1.ctr = reg_value;

    lan91c111_enable_transmit(dev);

    issue_mmu_command(dev, MMUCR_CMD_RESET);

    return LAN91C111_ERROR_NONE;
}

bool lan91c111_has_valid_link(const lan91c111_dev_t* dev) {
    return (bool)(
        GET_BIT(
            lan91c111_read_phy_mii_reg(dev, PHY_MII_REG_STATUS),
            PHY_MII_REG_STATUS_LINK
        )
    );
}

void lan91c111_read_mac_address(const lan91c111_dev_t* dev, uint8_t *addr) {
    select_registers_bank(dev, MAC_REG_BANK_1);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    uint16_t eeprom_addr_part = regs->bank_1.ia0_1;
    addr[0] = (uint8_t)eeprom_addr_part;
    addr[1] = (uint8_t)(eeprom_addr_part >> 8U);

    eeprom_addr_part = regs->bank_1.ia2_3;
    addr[2] = (uint8_t)eeprom_addr_part;
    addr[3] = (uint8_t)(eeprom_addr_part >> 8U);

    eeprom_addr_part = regs->bank_1.ia4_5;
    addr[4] = (uint8_t)eeprom_addr_part;
    addr[5] = (uint8_t)(eeprom_addr_part >> 8U);
}

lan91c111_error_t lan91c111_send_frame(
    const lan91c111_dev_t* dev, const uint8_t* const data, size_t data_size
) {
    if ((data == NULL) || (data_size == 0U)) {
        return LAN91C111_ERROR_ARG;
    }

    if (complete_memory_allocation(dev) == false) {
        issue_mmu_command(dev, MMUCR_CMD_RESET);
        return LAN91C111_ERROR_INTERNAL;
    }

    select_registers_bank(dev, MAC_REG_BANK_2);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    regs->bank_2.pnr = (
        regs->bank_2.arr & MAC_REG_PNR_PACKET_NUMBER_AT_TX_AREA
    );

    uint16_t reg_value = 0x0000U;
    SET_BIT(reg_value, MAC_REG_PTR_AUTO_INCR);
    regs->bank_2.ptr = reg_value;

    // Start copying the frame in buffer memory...

    // The first word is reserved for the status word. The next word is used to
    // specify the total number of bytes, and it is followed by the data area.
    // The data area holds the frame itself. By default, the last byte in the
    // receive frame format is followed by the CRC, and the Control byte
    // follows the CRC.

    const uint16_t status_word = 0x0000U;
    regs->bank_2.data.u16 = status_word;

    uint8_t ctrl_byte = 0x00U;

    // Total = STATUS WORD (2) + BYTE COUNT (2) + DATA AREA (data_size)
    // + CONTROL BYTE WORD (2)
    const uint16_t byte_count = (uint16_t)(
        sizeof(status_word)
        + sizeof(byte_count)
        + data_size
        + (sizeof(ctrl_byte) + 1U)
    );
    regs->bank_2.data.u16 = byte_count;

    /* Copy data to buffer memory */
    uint16_t* data_word = (uint16_t *)data;
    uint16_t number_data_byte = data_size;
    while (number_data_byte > 1U) {
        regs->bank_2.data.u16 = *data_word;
        data_word++;
        number_data_byte -= sizeof(*data_word);
    }

    // If data is odd, add the last data byte in addition to the control byte
    uint16_t last_word = 0x0000U;
    if (number_data_byte != 0U) {
        SET_BIT(ctrl_byte, FRAME_FORMAT_CTRL_BYTE_ODD);

        SET_BIT_FIELD(
            last_word,
            FRAME_FORMAT_CTRL_WORD_LAST_DATA_BYTE_MASK,
            FRAME_FORMAT_CTRL_WORD_LAST_DATA_BYTE_POS,
            *data_word
        );
    }

    SET_BIT_FIELD(
        last_word,
        FRAME_FORMAT_CTRL_WORD_CTRL_BYTE_MASK,
        FRAME_FORMAT_CTRL_WORD_CTRL_BYTE_POS,
        ctrl_byte
    );

    regs->bank_2.data.u16 = last_word;

    // Data frame copied to buffer memory, ready to send!

    lan91c111_enable_transmit(dev);

    issue_mmu_command(dev, MMUCR_CMD_ENQ_TX);

    return LAN91C111_ERROR_NONE;
}

size_t lan91c111_receive_frame(
    const lan91c111_dev_t* dev, const uint8_t* data, size_t data_size
) {
    if ((data == NULL) || (data_size == 0U)) {
        return 0U;
    }

    select_registers_bank(dev, MAC_REG_BANK_2);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    if (GET_BIT(regs->bank_2.fifo, MAC_REG_FIFO_REMPTY)) {
        return 0U;
    }

    uint16_t reg_value = 0x0000U;
    SET_BIT(reg_value, MAC_REG_PTR_RCV);
    SET_BIT(reg_value, MAC_REG_PTR_AUTO_INCR);
    SET_BIT(reg_value, MAC_REG_PTR_READ);
    regs->bank_2.ptr = reg_value;

    // Start reading the frame from buffer memory...

    /* Read the Receive Frame Status and the Byte Count */
    const uint16_t rfs = regs->bank_2.data.u16;

    if (
        GET_BIT(rfs, FRAME_FORMAT_RFS_WORD_TOOSHORT)
        || GET_BIT(rfs, FRAME_FORMAT_RFS_WORD_TOOLNG)
        || GET_BIT(rfs, FRAME_FORMAT_RFS_WORD_BADCRC)
        || GET_BIT(rfs, FRAME_FORMAT_RFS_WORD_ALGNERR)
    ) {
        issue_mmu_command(dev, MMUCR_CMD_REMV_REL_RX);
        return 0U;
    }

#ifdef LAN91C111_RFS_MULTICAST_SUPPORT
    // When receive frame was multicast; if hash value corresponds to a
    // multicast table bit that is set, and the address
    // was a multicast, the packet will pass address filtering regardless of
    // other filtering criteria
    if (GET_BIT(rfs, FRAME_FORMAT_RFS_WORD_MULTCAST)) {
        uint8_t hash = GET_BIT_FIELD(
            rfs, FRAME_FORMAT_RFS_WORD_HASH_VALUE_MASK, FRAME_FORMAT_RFS_WORD_HASH_VALUE_POS
        );

        uint8_t mt_reg_index = GET_BIT_FIELD(
            hash, MAC_REG_MT_HASH_REG_MASK, MAC_REG_MT_HASH_REG_POS
        );

        uint8_t bit_index = GET_BIT_FIELD(
            hash, MAC_REG_MT_HASH_BIT_MASK, MAC_REG_MT_HASH_BIT_POS
        );

        select_registers_bank(dev, MAC_REG_BANK_3);
        lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

        if (!GET_BIT(regs->bank_3.mt[mt_reg_index], bit_index)) {
            issue_mmu_command(dev, MMUCR_CMD_REMV_REL_RX);
            return 0U;
        }
    }
#endif // LAN91C111_RFS_MULTICAST_SUPPORT

    const uint16_t byte_count = regs->bank_2.data.u16;

    // DATA AREA size = Total - STATUS WORD (2) - BYTE COUNT (2)
    // - CONTROL BYTE WORD (2)
    uint16_t number_data_byte = (
        byte_count
        - sizeof(rfs)
        - sizeof(byte_count)
        - 2U
    );

    if (GET_BIT(rfs, FRAME_FORMAT_RFS_WORD_ODDFRM)) {
        number_data_byte++;
    }

    if (number_data_byte > data_size) {
        issue_mmu_command(dev, MMUCR_CMD_REMV_REL_RX);
        return 0U;
    }

#ifndef LAN91C111_RFS_MULTICAST_SUPPORT
    uint8_t data_byte_read = 0;
    bool multicast_check_completed = false;
#endif // LAN91C111_RFS_MULTICAST_SUPPORT
    // Copy data from buffer memory
    uint32_t* data_dword = (uint32_t*)data;
    select_registers_bank(dev, MAC_REG_BANK_2);
    for (
        uint32_t number_data_to_read = (number_data_byte + 3) >> 2;
        number_data_to_read;
        number_data_to_read--
    ) {
        *data_dword++ = regs->bank_2.data.u32;
#ifndef LAN91C111_RFS_MULTICAST_SUPPORT
        // Manually check if the frame received is multicast then check if it
        // should be rejected or accepted based on the state of the multicast
        // table and if all multicast frames should be received regardless of
        // of table.

        data_byte_read += sizeof(uint32_t);

        if (
            multicast_check_completed == false
            && (data_byte_read >= ETHERNET_MAC_ADDR_LEN)
        ) {
            multicast_check_completed = true;
            const uint8_t* destination = data;

            // Ethernet frames with a value of 1 in the least-significant bit of the first
            // octet of the destination MAC address are treated as multicast frames
            // Filter multicast frames only if requested
            select_registers_bank(dev, MAC_REG_BANK_0);
            static const uint8_t s_broadcast_address[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
            if ((0 == memcmp(destination, s_broadcast_address, ETHERNET_MAC_ADDR_LEN))
                || !GET_BIT(destination[0U], 0U)
                || GET_BIT(regs->bank_0.rcr, MAC_REG_RCR_ALMUL)
            ) {
                goto multicast_check_done;
            }

            uint8_t mt_reg_index = 0;
            uint8_t bit_index = 0;
            get_multicast_table_position(
                dev->data->crc32_cbk, destination, &mt_reg_index, &bit_index
            );

            select_registers_bank(dev, MAC_REG_BANK_3);
            if (!GET_BIT(regs->bank_3.mt[mt_reg_index], bit_index)) {
                issue_mmu_command(dev, MMUCR_CMD_REMV_REL_RX);
                return 0U;
            }

multicast_check_done:
            // Re-select the MAC register bank to carry on reading data
            select_registers_bank(dev, MAC_REG_BANK_2);
        }
#endif // LAN91C111_RFS_MULTICAST_SUPPORT
    }

    // Processing of present receive frame completed!

    issue_mmu_command(dev, MMUCR_CMD_REMV_REL_RX);

    return (size_t)number_data_byte;
}

bool lan91c111_is_rx_fifo_empty(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_2);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    return (bool)(GET_BIT(regs->bank_2.ist, MAC_REG_IST_RCV_INT) == 0U);
}

bool lan91c111_is_tx_fifo_empty(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_2);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    return (bool)(GET_BIT(regs->bank_2.ist, MAC_REG_IST_TX_INT) == 0U);
}

void lan91c111_enable_interrupt(
    const lan91c111_dev_t* dev, enum mac_reg_msk_bits_e source
) {
    select_registers_bank(dev, MAC_REG_BANK_2);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    SET_BIT(regs->bank_2.msk, source);
}

void lan91c111_disable_interrupt(
    const lan91c111_dev_t* dev, enum mac_reg_msk_bits_e source
) {
    select_registers_bank(dev, MAC_REG_BANK_2);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    CLR_BIT(regs->bank_2.msk, source);
}

void lan91c111_disable_all_interrupts(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_2);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    regs->bank_2.msk = 0U;
}

uint8_t lan91c111_get_interrupt_status(
    const lan91c111_dev_t* dev, enum mac_reg_msk_bits_e source
) {
    select_registers_bank(dev, MAC_REG_BANK_2);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    return GET_BIT(regs->bank_2.ist, source);
}

void lan91c111_clear_interrupt(
    const lan91c111_dev_t* dev, enum mac_reg_msk_bits_e source
) {
    select_registers_bank(dev, MAC_REG_BANK_2);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    SET_BIT(regs->bank_2.ist, source);
}

void lan91c111_clear_all_interrupts(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_2);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    regs->bank_2.ist = UINT8_MAX;
}

void lan91c111_soft_reset(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_0);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    // Initiate reset
    SET_BIT(regs->bank_0.rcr, MAC_REG_RCR_SOFT_RST);

    // Terminate reset
    CLR_BIT(regs->bank_0.rcr, MAC_REG_RCR_SOFT_RST);
}

lan91c111_error_t lan91c111_reset_phy(const lan91c111_dev_t* dev) {
    uint16_t reg_value = 0x0000U;
    SET_BIT(reg_value, PHY_MII_REG_CTRL_RST);

    lan91c111_write_phy_mii_reg(dev, PHY_MII_REG_CTRL, reg_value);

    dev->data->wait_ms_cbk(PHY_MII_RESET_TIME_OUT_MS);

    if (
        GET_BIT(
            lan91c111_read_phy_mii_reg(dev, PHY_MII_REG_CTRL),
            PHY_MII_REG_CTRL_RST
        )
    ) {
        return LAN91C111_ERROR_TIMEOUT;
    }

    return LAN91C111_ERROR_NONE;
}

uint16_t lan91c111_read_revision(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_3);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    return regs->bank_3.revision;
}

bool lan91c111_is_chip_id_correct(const lan91c111_dev_t* dev) {
    const uint16_t revision = lan91c111_read_revision(dev);

    return (
        (
            MAC_REG_REV_CHIP_ID
            == GET_BIT_FIELD(
                revision, MAC_REG_REV_CHIP_MASK, MAC_REG_REV_CHIP_POS
            )
        )
        && (
            MAC_REG_REV_REV_ID
            == GET_BIT_FIELD(
                revision, MAC_REG_REV_REV_MASK, MAC_REG_REV_REV_POS
            )
        )
    );
}

void lan91c111_enable_transmit(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_0);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    uint16_t reg_value = regs->bank_0.tcr;

    SET_BIT(reg_value, MAC_REG_TCR_TXENA);
    SET_BIT(reg_value, MAC_REG_TCR_PAD_EN);

    if (
        GET_BIT(
            lan91c111_read_phy_mii_reg(dev, PHY_MII_REG_STATUS_OUT),
            PHY_MII_REG_STATUS_OUTPUT_DPLXDET
        )
    ) {
        SET_BIT(reg_value, MAC_REG_TCR_FDUPLX);
    }

    select_registers_bank(dev, MAC_REG_BANK_0);
    regs->bank_0.tcr = reg_value;
}

void lan91c111_disable_transmit(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_0);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    uint16_t reg_value = regs->bank_0.tcr;

    CLR_BIT(reg_value, MAC_REG_TCR_TXENA);
    CLR_BIT(reg_value, MAC_REG_TCR_PAD_EN);
    CLR_BIT(reg_value, MAC_REG_TCR_FDUPLX);

    regs->bank_0.tcr = reg_value;
}

void lan91c111_enable_reception(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_0);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    SET_BIT(regs->bank_0.rcr, MAC_REG_RCR_RXEN);
}

void lan91c111_disable_reception(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_0);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    CLR_BIT(regs->bank_0.rcr, MAC_REG_RCR_RXEN);
}

void lan91c111_advertise_cap(const lan91c111_dev_t* dev) {
    uint16_t reg_value = lan91c111_read_phy_mii_reg(dev, PHY_MII_REG_ANEG_ADV);

    SET_BIT(reg_value, PHY_MII_REG_ANEG_10_FDX);
    SET_BIT(reg_value, PHY_MII_REG_ANEG_TX_FDX);

    lan91c111_write_phy_mii_reg(dev, PHY_MII_REG_ANEG_ADV, reg_value);
}

uint16_t lan91c111_read_phy_mii_reg(
    const lan91c111_dev_t* dev, enum phy_mii_regs_e reg_addr
) {
    // Idle Pattern
    for (uint8_t i = 0U; i < 32U; i++) {
        write_mdo_pin(dev, 1U);
    }

    // Start Bits
    write_mdo_pin(dev, 0U);
    write_mdo_pin(dev, 1U);

    // Read Select
    write_mdo_pin(dev, 1U);

    // Write Select
    write_mdo_pin(dev, 0U);

    // Physical Device Address
    // The LAN91C111 internal PHY address is 00000
    for (uint8_t i = 0U; i < 5U; i++) {
        write_mdo_pin(dev, 0U);
    }

    // Register Address
    const uint8_t reg_addr_bit_len = 5U;
    for (uint8_t i = 0U; i < reg_addr_bit_len; i++) {
        write_mdo_pin(dev, GET_BIT(reg_addr, reg_addr_bit_len - (i + 1U)));
    }

    // Turnaround Time
    read_mdi_pin(dev);
    write_mdo_pin(dev, 0U);

    // Data
    uint16_t data = 0U;
    const uint8_t data_bit_len = 16U;
    for (uint8_t i = 0U; i < data_bit_len; i++) {
        if (0U != read_mdi_pin(dev)) {
            SET_BIT(data, data_bit_len - (i + 1U));
        }
    }

    return data;
}

void lan91c111_write_phy_mii_reg(
    const lan91c111_dev_t* dev, enum phy_mii_regs_e reg_addr, uint16_t value
) {
    // Idle Pattern
    for (uint8_t i = 0U; i < 32U; i++) {
        write_mdo_pin(dev, 1U);
    }

    // Start Bits
    write_mdo_pin(dev, 0U);
    write_mdo_pin(dev, 1U);

    // Read Select
    write_mdo_pin(dev, 0U);

    // Write Select
    write_mdo_pin(dev, 1U);

    // Physical Device Address
    // The LAN91C111 internal PHY address is 00000
    for (uint8_t i = 0U; i < 5U; i++) {
        write_mdo_pin(dev, 0U);
    }

    // Register Address
    const uint8_t reg_addr_bit_len = 5U;
    for (uint8_t i = 0U; i < reg_addr_bit_len; i++) {
        write_mdo_pin(dev, GET_BIT(reg_addr, reg_addr_bit_len - (i + 1U)));
    }

    // Turnaround Time
    write_mdo_pin(dev, 1U);
    write_mdo_pin(dev, 0U);

    // Data
    const uint8_t data_bit_len = 16U;
    for (uint8_t i = 0U; i < data_bit_len; i++) {
        write_mdo_pin(dev, GET_BIT(value, data_bit_len - (i + 1U)));
    }
}

void lan91c111_set_phy_speed(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_0);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    uint16_t reg_value = 0x0000U;
    SET_BIT(reg_value, MAC_REG_RPCR_SPEED);
    SET_BIT_FIELD(
        reg_value,
        MAC_REG_RPCR_LEDA_MASK,
        MAC_REG_RPCR_LEDA_POS,
        LED_SELECT_SIGNAL_NPLED3PLUS_NPLED0
    );
    SET_BIT_FIELD(
        reg_value,
        MAC_REG_RPCR_LEDB_MASK,
        MAC_REG_RPCR_LEDB_POS,
        LED_SELECT_SIGNAL_NPLED2
    );

    regs->bank_0.rpcr = reg_value;
}

void lan91c111_accept_all_multicast_enable(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_0);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    SET_BIT(regs->bank_0.rcr, MAC_REG_RCR_ALMUL);
}

void lan91c111_accept_all_multicast_disable(const lan91c111_dev_t* dev) {
    select_registers_bank(dev, MAC_REG_BANK_0);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    CLR_BIT(regs->bank_0.rcr, MAC_REG_RCR_ALMUL);
}

lan91c111_error_t lan91c111_add_multicast_group(
    const lan91c111_dev_t* dev,
    const uint8_t* destination
) {
    if (NULL == dev->data->crc32_cbk) {
        return LAN91C111_ERROR_ARG;
    }

    uint8_t mt_reg_index = 0;
    uint8_t bit_index = 0;
    get_multicast_table_position(
        dev->data->crc32_cbk, destination, &mt_reg_index, &bit_index
    );

    select_registers_bank(dev, MAC_REG_BANK_3);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    SET_BIT(regs->bank_3.mt[mt_reg_index], bit_index);

    return LAN91C111_ERROR_NONE;
}

lan91c111_error_t lan91c111_remove_multicast_group(
    const lan91c111_dev_t* dev,
    const uint8_t* destination
) {
    if (NULL == dev->data->crc32_cbk) {
        return LAN91C111_ERROR_ARG;
    }

    uint8_t mt_reg_index = 0;
    uint8_t bit_index = 0;
    get_multicast_table_position(
        dev->data->crc32_cbk, destination, &mt_reg_index, &bit_index
    );

    select_registers_bank(dev, MAC_REG_BANK_3);
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;

    CLR_BIT(regs->bank_3.mt[mt_reg_index], bit_index);

    return LAN91C111_ERROR_NONE;
}

void lan91c111_isr_save_context(const lan91c111_dev_t* dev) {
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;
    isr_bank_select_reg = regs->bank_0.bank;
    isr_ptr_reg = regs->bank_2.ptr;
}

void lan91c111_isr_restore_context(const lan91c111_dev_t* dev) {
    lan91c111_registers_t* regs = (lan91c111_registers_t*)dev->cfg->base;
    regs->bank_0.bank = isr_bank_select_reg;
    regs->bank_2.ptr = isr_ptr_reg;
}
