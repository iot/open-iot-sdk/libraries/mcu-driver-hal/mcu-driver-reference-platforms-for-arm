/* Copyright (c) 2021 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __LAN91C111_DRIVER_H
#define __LAN91C111_DRIVER_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "lan91c111.h"

/** Initialises the Lan91C111 ethernet controller.
 *          - device ID is checked
 *          - all interrupts are disabled except for RCV and RX_OVRN interrupts
 *          - all capabilities are advertised
 *              - 10Base-T full duplex
 *              - 100Base-Tx full duplex
 *          - set PHY speed to 10/100 Mbps
 *          - Rx enabled
 *          - Tx enabled
 *          Init should be called prior to any other process and it is the
 *          caller's responsibility to ensure correct call order.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * 
 * @returns
 *  - LAN91C111_ERROR_NONE if successfully initialised;
 *  - LAN91C111_ERROR_TIMEOUT an operation timedout;
 *  - LAN91C111_ERROR_ARG if invalid argument passed.
 *  - LAN91C111_ERROR_INTERNAL if the Chip ID is incorrect
 */
lan91c111_error_t lan91c111_init(const lan91c111_dev_t* dev);

/** Checks if a valid link was established
 * 
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * @returns
 *  - true if there is a valid Link;
 *  - false otherwise.
 */
bool lan91c111_has_valid_link(const lan91c111_dev_t* dev);

/** Reads the MAC address stored in the EEPROM
 * 
 * @note
 * @param addr must be able to store at least 6 bytes
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * @param[out] addr Pointer to memory location to write the address
 */
void lan91c111_read_mac_address(const lan91c111_dev_t* dev, uint8_t *addr);

/** Sends a frame
 * 
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * @param[in] data Frame data to send
 * @param[in] data_size The frame data length (in bytes)
 * 
 * @returns
 *  - LAN91C111_ERROR_NONE if successfully sent;
 *  - LAN91C111_ERROR_ARG if arguments provided are incorrect;
 *  - LAN91C111_ERROR_INTERNAL if unable to allocate buffer memory.
 */
lan91c111_error_t lan91c111_send_frame(
    const lan91c111_dev_t* dev, const uint8_t* const data, size_t data_size
);

/** Receive a frame
 * 
 * @note
 * Returns 0 if data_size is smaller than the received frame's byte count
 * 
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * @param[in] data Memory allocated to store frame
 * @param[in] data_size Memory allocated length (in bytes)
 * 
 * @returns The number of bytes read from the buffer memory
 */
size_t lan91c111_receive_frame(
    const lan91c111_dev_t* dev, const uint8_t* data, size_t data_size
);

/** Checks if the RX FIFO is empty
 * 
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * 
 * @returns
*  - true if the RX FIFO is empty
 *  - false otherwise.
 */
bool lan91c111_is_rx_fifo_empty(const lan91c111_dev_t* dev);

/** Checks if the TX FIFO is empty
 * 
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * 
 * @returns
 *  - true if the TX FIFO is empty
 *  - false otherwise.
 */
bool lan91c111_is_tx_fifo_empty(const lan91c111_dev_t* dev);

/** Disables the given interrupt source.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * @param[in] source Enum of the interrupt source.
 */
void lan91c111_disable_interrupt(
    const lan91c111_dev_t* dev, enum mac_reg_msk_bits_e source
);

/** Enables the given interrupt source.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * @param[in] source Enum of the interrupt source.
 */
void lan91c111_enable_interrupt(
    const lan91c111_dev_t* dev, enum mac_reg_msk_bits_e source
);

/** Disables all interrupt sources
 * 
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 */
void lan91c111_disable_all_interrupts(const lan91c111_dev_t* dev);

/** Gets the status of the given interrupt source.
 * 
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * @param[in] source Enum of the interrupt source.
 * 
 * @returns
 *  - non-zero if the given interrupt source is triggered
 *  - zero otherwise.
 */
uint8_t lan91c111_get_interrupt_status(
    const lan91c111_dev_t* dev, enum mac_reg_msk_bits_e source
);

/** Clears the given interrupt source.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * @param[in] source Enum of the interrupt source.
 */
void lan91c111_clear_interrupt(
    const lan91c111_dev_t* dev, enum mac_reg_msk_bits_e source
);

/** Clears all interrupt sources.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 */
void lan91c111_clear_all_interrupts(const lan91c111_dev_t* dev);

/** Initiates a soft reset
 * 
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 */
void lan91c111_soft_reset(const lan91c111_dev_t* dev);

/** Resets the PHY
 * 
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 *
 * @returns
 *  - LAN91C111_ERROR_NONE if successfully reset;
 *  - LAN91C111_ERROR_TIMEOUT failed to reset within 50mS.
 */
lan91c111_error_t lan91c111_reset_phy(const lan91c111_dev_t* dev);

/** Reads the Ethernet Controller's revision
 *
 * @note
 * The revision includes both the Chip ID and the Revision ID
 * 
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 *
 * @returns ID number
 */
uint16_t lan91c111_read_revision(const lan91c111_dev_t* dev);

/** Checks device ID
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * @returns
 * - true if Chip ID is \ref MAC_REG_REV_CHIP_ID and Revision is \ref MAC_REG_REV_REV_ID
 * - false otherwise
 */
bool lan91c111_is_chip_id_correct(const lan91c111_dev_t* dev);

/** Enables transmission.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 */
void lan91c111_enable_transmit(const lan91c111_dev_t* dev);

/** Disables transmission.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 */
void lan91c111_disable_transmit(const lan91c111_dev_t* dev);

/** Enables reception.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 */
void lan91c111_enable_reception(const lan91c111_dev_t* dev);

/** Disables reception.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 */
void lan91c111_disable_reception(const lan91c111_dev_t* dev);

/** Advertises all speeds capabilities.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 */
void lan91c111_advertise_cap(const lan91c111_dev_t* dev);

/** Reads the PHY register.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * @param[in] reg_addr Register address
 *
 * @returns the PHY register value read
 */
uint16_t lan91c111_read_phy_mii_reg(
    const lan91c111_dev_t* dev, enum phy_mii_regs_e reg_addr
);

/** Writes the PHY register.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * @param[in] reg_addr Register offset
 * @param[in] value Register value to write
 */
void lan91c111_write_phy_mii_reg(
    const lan91c111_dev_t* dev, enum phy_mii_regs_e reg_addr, uint16_t value
);

/** Sets the speed mode to 100 Mbps.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 */
void lan91c111_set_phy_speed(const lan91c111_dev_t* dev);

/** Accepts all multicast frames.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 */
void lan91c111_accept_all_multicast_enable(const lan91c111_dev_t* dev);

/** Accepts only the multicast frames that match the muticast table setting.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 */
void lan91c111_accept_all_multicast_disable(const lan91c111_dev_t* dev);

/** Add the specified destination address to the hardware multicast address filtering
 * 
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * @param[in] destination MAC address of the multicast group
 *
 * @returns
 *  - LAN91C111_ERROR_NONE if successfully added;
 *  - LAN91C111_ERROR_ARG No CRC32 function available.
 */
lan91c111_error_t lan91c111_add_multicast_group(
    const lan91c111_dev_t* dev,
    const uint8_t* destination
);

/** Remove the specified destination address to the hardware multicast address filtering
 * 
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 * @param[in] destination MAC address of the multicast group
 *
 * @returns
 *  - LAN91C111_ERROR_NONE if successfully removed;
 *  - LAN91C111_ERROR_ARG No CRC32 function available.
 */
lan91c111_error_t lan91c111_remove_multicast_group(
    const lan91c111_dev_t* dev,
    const uint8_t* destination
);

/** Saves the bank select register and the pointer register in ISR
 * 
 * @note This function must be called only at the begining of a ISR.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 */
void lan91c111_isr_save_context(const lan91c111_dev_t* dev);

/** Restores the bank select register and the pointer register in ISR
 * 
 * @note This function must be called only at the end of a ISR.
 *
 * @param[in] dev Ethernet device structure \ref lan91c111_dev_t
 */
void lan91c111_isr_restore_context(const lan91c111_dev_t* dev);

#endif // __LAN91C111_DRIVER_H
