/* Copyright (c) 2021-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __LAN91C111_H
#define __LAN91C111_H

#include <stdint.h>

#include "io_type_qualifiers.h"

typedef struct lan91c111_dev_cfg_s {
    const uint32_t base;
} lan91c111_dev_cfg_t;

typedef void (*lan91c111_wait_ms_f) (uint32_t);
typedef uint32_t (*lan91c111_crc32_f) (const uint8_t *src, size_t len);
typedef struct lan91c111_dev_data_s {
    lan91c111_wait_ms_f wait_ms_cbk;
    lan91c111_crc32_f crc32_cbk;
} lan91c111_dev_data_t;

typedef struct lan91c111_dev_s {
    const lan91c111_dev_cfg_t* const cfg;
    lan91c111_dev_data_t* const data;
} lan91c111_dev_t;

typedef enum lan91c111_error_e {
    LAN91C111_ERROR_NONE     = 0U, /*!< no error */
    LAN91C111_ERROR_TIMEOUT  = 1U, /*!< timeout */
    LAN91C111_ERROR_BUSY     = 2U, /*!< no error */
    LAN91C111_ERROR_ARG      = 3U, /*!< invalid parameter */
    LAN91C111_ERROR_INTERNAL = 4U  /*!< internal error */
} lan91c111_error_t;


enum mac_reg_ist_bits_e {
    MAC_REG_IST_RCV_INT      = 0U,   /* Rx Complete intererupt */
    MAC_REG_IST_TX_INT       = 1U,   /* Tx Complete interrupt */
    MAC_REG_IST_TX_EMPTY_INT = 2U,   /* Tx FIFO empty interrupt */
    MAC_REG_IST_ALLOC_INT    = 3U,   /* Tx ram Allocation interrupt */
    MAC_REG_IST_RX_OVRN_INT  = 4U,   /* Receive Overrun interrupt */
    MAC_REG_IST_EPH_INT      = 5U,   /* EPH Type interrupt */
    MAC_REG_IST_MDINT        = 7U    /* PHY MI Register 18 change status interrupt*/
};

enum mac_reg_msk_bits_e {
    MAC_REG_MSK_RCV         = 0U,    /* Rx Complete int. mask */
    MAC_REG_MSK_TX_INT      = 1U,    /* Tx Complete int. mask */
    MAC_REG_MSK_TX_EMPTY    = 2U,    /* Tx FIFO empty int. mask */
    MAC_REG_MSK_ALLOC_INT   = 3U,    /* Tx ram Allocation int. mask */
    MAC_REG_MSK_RX_OVRN     = 4U,    /* Receive Overrun int. mask */
    MAC_REG_MSK_EPH_INT     = 5U,    /* EPH Type int. mask */
    MAC_REG_MSK_ERCV_INT    = 6U,    /* Early Receive int. mask */
    MAC_REG_MSK_MDINT       = 7U     /* PHY MI Register 18 change int. mask */
};

#define MAC_REG_MT_HASH_REG_MASK 0x07U
#define MAC_REG_MT_HASH_REG_POS 3U
#define MAC_REG_MT_HASH_BIT_MASK 0x07U
#define MAC_REG_MT_HASH_BIT_POS 0U

#define CRC_MULTICAST_HASH_VALUE_MASK 0x3FU
#define CRC_MULTICAST_HASH_VALUE_POS 26U

#define MAC_REG_REV_REV_MASK 0x0FU
#define MAC_REG_REV_REV_POS 0U
#define MAC_REG_REV_CHIP_MASK 0x0FU
#define MAC_REG_REV_CHIP_POS 4U
#define MAC_REG_REV_REV_ID  2U
#define MAC_REG_REV_CHIP_ID 9U

enum phy_mii_regs_e {
    PHY_MII_REG_CTRL       =  0U,    /* Control */
    PHY_MII_REG_STATUS     =  1U,    /* Status */
    PHY_MII_REG_PHY_ID_1   =  2U,    /* PHY ID#1 */
    PHY_MII_REG_PHY_ID_2   =  3U,    /* PHY ID#2 */
    PHY_MII_REG_ANEG_ADV   =  4U,    /* Auto-Negotiation Advertisement */
    PHY_MII_REG_ANEG_REC   =  5U,    /* Auto-Negotiation Remote End Capability */
    PHY_MII_REG_CONFIG_1   = 16U,    /* Configuration 1 */
    PHY_MII_REG_CONFIG_2   = 17U,    /* Configuration 2 */
    PHY_MII_REG_STATUS_OUT = 18U,    /* Status Output */
    PHY_MII_REG_MASK       = 19U     /* Mask */
};

enum phy_mii_reg_status_bits_e {
    PHY_MII_REG_STATUS_EXREG    =  0U,     /* EXTENDED CAPABILITY REGISTER */
    PHY_MII_REG_STATUS_JAB      =  1U,     /* JABBER DETECT */
    PHY_MII_REG_STATUS_LINK     =  2U,     /* LINK STATUS */
    PHY_MII_REG_STATUS_CAP_ANEG =  3U,     /* AUTO-NEGOTIATION CAPABLE */
    PHY_MII_REG_STATUS_REM_FLT  =  4U,     /* REMOTE FAULT DETECT */
    PHY_MII_REG_STATUS_ANEG_ACK =  5U,     /* AUTO-NEGOTIATION ACKNOWLEDGMENT */
    PHY_MII_REG_STATUS_CAP_SUPR =  6U,     /* MI PREAMBLE SUPPRESSION CAPABLE */
    PHY_MII_REG_STATUS_CAP_TH   = 11U,     /* 10BASE-T HALF DUPLEX CAPABLE */
    PHY_MII_REG_STATUS_CAP_TF   = 12U,     /* 10BASE-T FULL DUPLEX CAPABLE */
    PHY_MII_REG_STATUS_CAP_TXH  = 13U,     /* 100BASE-TX HALF DUPLEX CAPABLE */
    PHY_MII_REG_STATUS_CAP_TXF  = 14U,     /* 100BASE-TX FULL DUPLEX CAPABLE */
    PHY_MII_REG_STATUS_CAP_T4   = 15U      /* 100BASE-T4 CAPABLE */
};

enum phy_mii_reg_ctrl_bits_e {
    PHY_MII_REG_CTRL_COLST     =  7U, /* COLLISION TEST */
    PHY_MII_REG_CTRL_DPLX      =  8U, /* DUPLEX MODE */
    PHY_MII_REG_CTRL_ANEG_RST  =  9U, /* AUTO-NEGOTIATION RESET */
    PHY_MII_REG_CTRL_MII_DIS   = 10U, /* MII DISABLE */
    PHY_MII_REG_CTRL_PDN       = 11U, /* POWER DOWN */
    PHY_MII_REG_CTRL_ANEG_EN   = 12U, /* AUTO-NEGOTIATION ENABLE */
    PHY_MII_REG_CTRL_SPEED     = 13U, /* SPEED SELECTION */
    PHY_MII_REG_CTRL_LPBK      = 14U, /* Loopback */
    PHY_MII_REG_CTRL_RST       = 15U  /* RESET */
};
#define PHY_MII_RESET_TIME_OUT_MS    50U

enum phy_mii_reg_status_output_bits_e {
    PHY_MII_REG_STATUS_OUTPUT_DPLXDET  =  6U,   /* Duplex Detect */
    PHY_MII_REG_STATUS_OUTPUT_SPDDET   =  7U,   /* 100/10 Speed Detect */
    PHY_MII_REG_STATUS_OUTPUT_JAB      =  8U,   /* Jabber Detect */
    PHY_MII_REG_STATUS_OUTPUT_RPOL     =  9U,   /* Reverse Polarity Detect */
    PHY_MII_REG_STATUS_OUTPUT_ESD      = 10U,   /* End Of Stream Error */
    PHY_MII_REG_STATUS_OUTPUT_SSD      = 11U,   /* Start Of Stream Error */
    PHY_MII_REG_STATUS_OUTPUT_CWRD     = 12U,   /* Synchronization Detect Codeword Error */
    PHY_MII_REG_STATUS_OUTPUT_LOSSSYNC = 13U,   /* Descrambler Loss of */
    PHY_MII_REG_STATUS_OUTPUT_LNKFAIL  = 14U,   /* Link Fail Detect */
    PHY_MII_REG_STATUS_OUTPUT_INT      = 15U    /* Interrupt Detect */
};

enum phy_mii_reg_aneg_bits_e {
    PHY_MII_REG_ANEG_CSMA   =  0U, /* 802.3 CSMA */
    PHY_MII_REG_ANEG_10_HDX =  5U, /* 10BASE-T HALF DUPLEX CAPABLE */
    PHY_MII_REG_ANEG_10_FDX =  6U, /* 10BASE-T FULL DUPLEX CAPABLE */
    PHY_MII_REG_ANEG_TX_HDX =  7U, /* 100BASE-TX HALF DUPLEX CAPABLE */
    PHY_MII_REG_ANEG_TX_FDX =  8U, /* 100BASE-TX FULL DUPLEX CAPABLE */
    PHY_MII_REG_ANEG_T4     =  9U, /* 100BASE-T4 */
    PHY_MII_REG_ANEG_RF     = 13U, /* REMOTE FAULT */
    PHY_MII_REG_ANEG_ACK    = 14U, /* ACKNOWLEDGE */
    PHY_MII_REG_ANEG_NP     = 15U  /* NEXT PAGE */
};

enum mac_reg_bank_bits_e {
    MAC_REG_BANK_BS0 = 0U,   /* Bank Register Select 0 */
    MAC_REG_BANK_BS1 = 1U,   /* Bank Register Select 1 */
    MAC_REG_BANK_BS2 = 2U    /* Bank Register Select 2 */
};
#define MAC_REG_BANK_BANK_MASK 0x0007U
#define MAC_REG_BANK_BANK_POS 0U
#define MAC_REG_BANK_SELECT_DEFAULT_VALUE 0x3300U

enum mac_reg_bank_e {
    MAC_REG_BANK_0 = 0U,
    MAC_REG_BANK_1 = 1U,
    MAC_REG_BANK_2 = 2U,
    MAC_REG_BANK_3 = 3U
};

enum mac_reg_tcr_bits_e {
    MAC_REG_TCR_TXENA       =  0U,  /* Enable transmitter */
    MAC_REG_TCR_LOOP        =  1U,  /* PHY Local loopback */
    MAC_REG_TCR_FORCOL      =  2U,  /* Force collision */
    MAC_REG_TCR_PAD_EN      =  7U,  /* Pad short frames with 0 if len < 64 bytes */
    MAC_REG_TCR_NOCRC       =  8U,  /* Don't append CRC to tx frames */
    MAC_REG_TCR_MON_CSN     = 10U,  /* Monitor carrier while transmitting */
    MAC_REG_TCR_FDUPLX      = 11U,  /* Full duplex mode (receive own frames) */
    MAC_REG_TCR_STP_SQET    = 12U,  /* Stop transmit on SQET error */
    MAC_REG_TCR_EPH_LOOP    = 13U,  /* Internal Loopback at the EPH block */
    MAC_REG_TCR_SWFDUP      = 15U   /* Switched Full Duplex Mode */
};

enum mac_reg_ephsr_bits_e {
    MAC_REG_EPHSR_TX_SUC     =  0U,  /* Last transmit was successful */
    MAC_REG_EPHSR_SNGLCOL    =  1U,  /* Single collision detected for last tx */
    MAC_REG_EPHSR_MULCOL     =  2U,  /* Multiple collision detected for last tx */
    MAC_REG_EPHSR_LTX_MULT   =  3U,  /* Last transmit frame was a multicast */
    MAC_REG_EPHSR_16COL      =  4U,  /* 16 collisions reached */
    MAC_REG_EPHSR_SQET       =  5U,  /* Signal Quality Error Test */
    MAC_REG_EPHSR_LTX_BRD    =  6U,  /* Last Tx Frame was a broadcast */
    MAC_REG_EPHSR_TX_DEFR    =  7U,  /* Transmit Deferred */
    MAC_REG_EPHSR_LATCOL     =  9U,  /* Late Collision Detected */
    MAC_REG_EPHSR_LOST_CARR  = 10U,  /* Lost Carrier Sense */
    MAC_REG_EPHSR_EXC_DEF    = 11U,  /* Excessive Deferral */
    MAC_REG_EPHSR_CTR_ROL    = 12U,  /* Counter Roll Over */
    MAC_REG_EPHSR_LINK_OK    = 14U   /* General purpose input driven by nLNK pin */
};

enum mac_reg_rcr_bits_e {
    MAC_REG_RCR_RX_ABORT    = 0U,   /* Receive frame aborted (too long) */
    MAC_REG_RCR_PRMS        = 1U,   /* Promiscuous mode */
    MAC_REG_RCR_ALMUL       = 2U,   /* Accept all multicast (no filtering) */
    MAC_REG_RCR_RXEN        = 8U,   /* Enable Receiver */
    MAC_REG_RCR_STRIP_CRC   = 9U,   /* Strip CRC of received frames */
    MAC_REG_RCR_ABORT_ENB   = 13U,  /* Enable Abort of Rx when collision */
    MAC_REG_RCR_FILT_CAR    = 14U,  /* Filter Carrier */
    MAC_REG_RCR_SOFT_RST    = 15U   /* Software-Activated Reset */
};

enum mac_reg_rpcr_bits_e {
    MAC_REG_RPCR_LS0B       =  2U,  /* LEDB select Signal Enable 0 */
    MAC_REG_RPCR_LS1B       =  3U,  /* LEDB select Signal Enable 1 */
    MAC_REG_RPCR_LS2B       =  4U,  /* LEDB select Signal Enable 2 */
    MAC_REG_RPCR_LS0A       =  5U,  /* LEDA select Signal Enable 0 */
    MAC_REG_RPCR_LS1A       =  6U,  /* LEDA select Signal Enable 1 */
    MAC_REG_RPCR_LS2A       =  7U,  /* LEDA select Signal Enable 2 */
    MAC_REG_RPCR_ANEG       = 11U,  /* Auto-Negotiation mode select */
    MAC_REG_RPCR_DPLX       = 12U,  /* Duplex Select (Full/Half Duplex) */
    MAC_REG_RPCR_SPEED      = 13U   /* Speed select input (10/100 MBps) */
};
#define MAC_REG_RPCR_LEDB_MASK 0x07U
#define MAC_REG_RPCR_LEDB_POS 2U
#define MAC_REG_RPCR_LEDA_MASK 0x07U
#define MAC_REG_RPCR_LEDA_POS 5U
// RPCR REG LEDA and LEDB Select Signals
#define LED_SELECT_SIGNAL_NPLED3PLUS_NPLED0  0U  /* Logical OR of 100Mbps Link detected 10Mbps Link detected (default) */
#define LED_SELECT_SIGNAL_NPLED0             2U  /* 10Mbps Link detected */
#define LED_SELECT_SIGNAL_NPLED1             3U  /* Full Duplex Mode enabled */
#define LED_SELECT_SIGNAL_NPLED2             4U  /* Transmit or Receive packet occurred */
#define LED_SELECT_SIGNAL_NPLED3             5U  /* 100Mbps Link detected */
#define LED_SELECT_SIGNAL_NPLED4             6U  /* Receive packet occurred */
#define LED_SELECT_SIGNAL_NPLED5             7U  /* Transmit packet occurred */

enum mac_reg_cr_bits_e {
    MAC_REG_CR_EXT_PHY      = 9U,   /* External PHY enabled (0= internal PHY) */
    MAC_REG_CR_GPCNTRL      = 10U,  /* General purpose Output drives nCNTRL pin */
    MAC_REG_CR_NO_WAIT      = 12U,  /* No wait states */
    MAC_REG_CR_EPH_POW_EN   = 15U   /* EPH Power Enable  (0= power down PHY) */
};

enum mac_reg_ctr_bits_e {
    MAC_REG_CTR_STORE      =  0U,  /* Store to EEPROM */
    MAC_REG_CTR_RELOAD     =  1U,  /* Reload from EEPROM */
    MAC_REG_CTR_EEPROM_SEL =  2U,  /* EEPROM select */
    MAC_REG_CTR_TE_ENABLE  =  5U,  /* Transmit error enable (mux into EPH int) */
    MAC_REG_CTR_CR_ENABLE  =  6U,  /* Counter rollover enable (mux into EPH int) */
    MAC_REG_CTR_LE_ENABLE  =  7U,  /* Link error enable (mux into EPH int) */
    MAC_REG_CTR_AUTO_REL   = 11U,  /* Auto-release Tx memory */
    MAC_REG_CTR_RCV_BAD    = 14U   /* Bad CRC packet received */
};

enum mac_reg_mmucr_bits_e {
    MAC_REG_MMUCR_BUSY = 0U /* MMU processing a release command */
};
#define MAC_REG_MMUCR_OP_CODE_MASK 0x07U
#define MAC_REG_MMUCR_OP_CODE_POS  5U
// MMUCR REG Operation Code Command set
enum mmucr_cmd_e {
    MMUCR_CMD_NOOP         = 0U,  /* No operation */
    MMUCR_CMD_ALLOC_TX     = 1U,  /* Allocate memory for Tx */
    MMUCR_CMD_RESET        = 2U,  /* Reset MMU to initial state */
    MMUCR_CMD_REMV_RX      = 3U,  /* Remove frame from top of Rx FIFO */
    MMUCR_CMD_REMV_REL_RX  = 4U,  /* Remove and Release top of Rx FIFO */
    MMUCR_CMD_REL_PKT      = 5U,  /* Release specific packet */
    MMUCR_CMD_ENQ_TX       = 6U,  /* Enqueue packet number into Tx FIFO */
    MMUCR_CMD_RESET_TX     = 7U   /* Reset Tx FIFO */
};

#define MAC_REG_PNR_PACKET_NUMBER_AT_TX_AREA 0x3FU

enum mac_reg_arr_bits_E {
    MAC_REG_ARR_FAILED = 7U   /* Allocation failure */
};
#define MAC_REG_ARR_ALLOCATED_PACKET_NUMBER_MASK 0x3FU
#define MAC_REG_ARR_ALLOCATED_PACKET_NUMBER_POS 0U

enum mac_reg_fifo_bits_e {
    MAC_REG_FIFO_TEMPTY =  7U,  /* No receive packets queued in Rx FIFO      */
    MAC_REG_FIFO_REMPTY = 15U   /* No transmit packets in completion queue   */
};
#define MAC_REG_FIFO_TX_FIFO_PACKET_NUMBER_MASK 0x3FU
#define MAC_REG_FIFO_TX_FIFO_PACKET_NUMBER_POS 0U
#define MAC_REG_FIFO_RX_FIFO_PACKET_NUMBER_MASK 0x3FU
#define MAC_REG_FIFO_RX_FIFO_PACKET_NUMBER_POS 8U

enum mac_reg_ptr_bits_e {
    MAC_REG_PTR_NOT_EMPTY   = 11U,  /* Data FIFO not empty yet (read only bit) */
    MAC_REG_PTR_READ        = 13U,  /* Read access (0= write access) */
    MAC_REG_PTR_AUTO_INCR   = 14U,  /* Auto increment on access */
    MAC_REG_PTR_RCV         = 15U   /* Address refers to Rx area (0= Tx area) */
};
#define MAC_REG_PTR_POINTER_HIGH_MASK 0x07U
#define MAC_REG_PTR_POINTER_HIGH_POS 8U
#define MAC_REG_PTR_POINTER_LOW_MASK  0xFFU
#define MAC_REG_PTR_POINTER_LOW_POS  0U
#define MAC_REG_POINTER_DEFAULT_VALUE     0x0000U

enum mac_reg_mgmt_bits_e {
    MAC_REG_MGMT_MDO        =  0U,  /* MII - Value drives MDO pin */
    MAC_REG_MGMT_MDI        =  1U,  /* MII - Value of MDI pin when read */
    MAC_REG_MGMT_MCLK       =  2U,  /* MII - Value drives MDCLK pin */
    MAC_REG_MGMT_MDOE       =  3U,  /* MII - 1= MDO pin output, 0= MDO tristated */
    MAC_REG_MGMT_MSK_CRS100 = 14U   /* Disables CRS100 detection in Tx Half Dupl.*/
};

#define FRAME_FORMAT_BYTE_COUNT_MASK 0x07FFU
#define FRAME_FORMAT_BYTE_COUNT_POS 0U

enum frame_format_rfs_word_bits_e {
    FRAME_FORMAT_RFS_WORD_MULTCAST    =  0U,  /* Multicast frame received */
    FRAME_FORMAT_RFS_WORD_TOOSHORT    = 10U,  /* Too short frame received (min. 64 bytes) */
    FRAME_FORMAT_RFS_WORD_TOOLNG      = 11U,  /* Too long frame received (max. 1518 bytes) */
    FRAME_FORMAT_RFS_WORD_ODDFRM      = 12U,  /* Frame with Odd number of bytes received */
    FRAME_FORMAT_RFS_WORD_BADCRC      = 13U,  /* Bad CRC error */
    FRAME_FORMAT_RFS_WORD_BROADCAST   = 14U,  /* Received broadcast frame */
    FRAME_FORMAT_RFS_WORD_ALGNERR     = 15U   /* Frame alignment error */
};
#define FRAME_FORMAT_RFS_WORD_HASH_VALUE_MASK CRC_MULTICAST_HASH_VALUE_MASK
#define FRAME_FORMAT_RFS_WORD_HASH_VALUE_POS 1U

enum frame_format_ctrl_byte_bits_e {
    FRAME_FORMAT_CTRL_BYTE_CRC = 4U, /* Append CRC (valid when TCR_NOCRC = 1) */
    FRAME_FORMAT_CTRL_BYTE_ODD = 5U  /* Odd number of bytes in frame */
};
#define FRAME_FORMAT_CTRL_WORD_CTRL_BYTE_MASK 0xFFU
#define FRAME_FORMAT_CTRL_WORD_CTRL_BYTE_POS 8U
#define FRAME_FORMAT_CTRL_WORD_LAST_DATA_BYTE_MASK 0xFFU
#define FRAME_FORMAT_CTRL_WORD_LAST_DATA_BYTE_POS 0U


typedef union lan91c111_registers_u {
    struct {
        __IO uint16_t tcr;            /* Transmit Control */
        __I  uint16_t eph_status;     /* EPH Status */
        __IO uint16_t rcr;            /* Receive Control */
        __I  uint16_t counter;        /* Counter */
        __I  uint16_t mir;            /* Memory Information */
        __IO uint16_t rpcr;           /* Receive/Phy Control */
             uint16_t reserved;       /* Reserved */
        __IO uint16_t bank;           /* Bank */
    } bank_0;

    struct {
        __IO uint16_t cr;                 /* Configuration */
        __IO uint16_t base;               /* Base Address */
        __IO uint16_t ia0_1;              /* Individual Address 0 to 1 */
        __IO uint16_t ia2_3;              /* Individual Address 2 to 3 */
        __IO uint16_t ia4_5;              /* Individual Address 4 to 5 */
        __IO uint16_t general_purpose;    /* General Purpose */
        __IO uint16_t ctr;                /* Control */
        __IO uint16_t bank;               /* Bank */
    } bank_1;

    struct {
        __IO uint16_t mmucr;              /* MMU Command */
        __IO uint8_t pnr;                 /* Packet Number */
        __I  uint8_t arr;                 /* Allocation Result */
        __I  uint16_t fifo;               /* Receive FIFO and the Transmit completion FIFO */
        __IO uint16_t ptr;                /* Pointer */
        __IO union reg_map_bank2_data_u { /* Data */
            __IO uint32_t u32;
            __IO uint16_t u16;
            __IO uint8_t u8;
        } data;
        __IO  uint8_t ist;                /* Interrupt Status */
        __IO uint8_t msk;                 /* Interrupt Mask */
        __IO uint16_t bank;               /* Bank */
    } bank_2;

    struct {
        __IO uint8_t mt[8];            /* Multicast Hash Tables 0 to 7 */
        __IO uint16_t mgmt;            /* Management Interface */
        __IO uint16_t revision;        /* Revision */
        __IO uint16_t rcv;             /* Early Receive */
        __IO uint16_t bank;            /* Bank */
    } bank_3;
} lan91c111_registers_t;

#endif // __LAN91C111_H
