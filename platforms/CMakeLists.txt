# Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

add_subdirectory(common/api)

# Note: TARGET_OBJECTS is not needed when an executable links a startup object
# directly. But when examples and tests link startup objects transitively via
# mdh-platform-startup-linker, CMake does not automatically pass objects to the
# linker and we need to extract them explicitly.
add_library(mdh-platform-startup-linker INTERFACE)
if(MDH_PLATFORM STREQUAL "ARM_AN386_MPS2")
    add_subdirectory(an386)
    add_library(mdh-reference-platforms-for-arm ALIAS mdh-arm-hal-impl-an386)
    target_link_libraries(mdh-platform-startup-linker
        INTERFACE
            $<TARGET_OBJECTS:mdh-arm-startup-an386>
            mdh-arm-startup-an386
            mdh-arm-linker-script-an386
    )
elseif(MDH_PLATFORM STREQUAL "ARM_AN552_MPS3")
    add_subdirectory(an552)
    add_library(mdh-reference-platforms-for-arm ALIAS mdh-arm-hal-impl-an552)
    target_link_libraries(mdh-platform-startup-linker
        INTERFACE
            $<TARGET_OBJECTS:mdh-arm-startup-an552>
            mdh-arm-startup-an552
            mdh-arm-linker-script-an552
    )
elseif(MDH_PLATFORM STREQUAL "ARM_AN555_MPS3")
    add_subdirectory(an555)
    add_library(mdh-reference-platforms-for-arm ALIAS mdh-arm-hal-impl-an555)
    target_link_libraries(mdh-platform-startup-linker
        INTERFACE
            $<TARGET_OBJECTS:mdh-arm-startup-an555>
            mdh-arm-startup-an555
            mdh-arm-linker-script-an555
    )
endif()
