/* Copyright (c) 2019-2023, Arm Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __MBED_OBJECTS_H__
#define __MBED_OBJECTS_H__

#include "PeripheralNames.h"
#include "PinNames.h"
#include "an552.h"
#include "arm_mps3_io_drv.h"
#include "device_definition.h"
#include "gpio_cmsdk_drv.h"
#include "gpio_objects.h"
#include "i2c_sbcon_drv.h"
#include "qspi_xilinx_drv.h"
#include "spi_pl022_drv.h"

#define QSPI_FIXED_FREQUENCY_HZ (QSPI_XILINX_FIXED_FREQUENCY_HZ)

#ifdef __cplusplus
extern "C" {
#endif

typedef struct gpio_s {
    struct gpio_cmsdk_dev_t *gpio_dev;
    struct arm_mps3_io_dev_t *mps3_io_dev;
    void (*arm_mps3_io_write)(struct arm_mps3_io_dev_t *dev,
                              enum arm_mps3_io_access_t access,
                              uint8_t pin_num,
                              uint32_t value);
    uint32_t pin_number;
    PinDirection direction;
    enum gpio_cmsdk_altfunc_t function;
} gpio_t;

typedef struct gpio_irq_s {
    struct gpio_cmsdk_dev_t *gpio_dev;
    uint32_t pin_number;     /* Pin number inside the GPIO */
    uint32_t exp_pin_number; /* Pin number on the expension port */
    IRQn_Type irq_number;    /* IRQ number of the GPIO interrupt of
                                this pin */
} gpio_irq_t;

struct serial_s {
    struct uart_cmsdk_dev_t *uart_dev;
    UARTName uart_index;     /* UART device number */
    IRQn_Type rx_irq_number; /* IRQ number of the RX interrupt for
                                 this UART device */
};

struct spi_s {
    struct spi_pl022_dev_t *spi;
};

#if DEVICE_TRNG
struct trng_s {
    /*  nothing to be stored for now */
    void *dummy;
};
#endif // DEVICE_TRNG

#if DEVICE_I2C || DEVICE_I2C_PERIPHERAL
enum byte_transfer_states {
    BYTE_TRANSFER_STATE_NONE = 0,
    BYTE_TRANSFER_STATE_START,
    BYTE_TRANSFER_STATE_ADDRESS,
    BYTE_TRANSFER_STATE_DATA,
};

struct i2c_s {
    struct i2c_sbcon_dev_t *dev;
};
#endif

struct qspi_s {
    qspi_xilinx_dev_t *dev;
};

#ifdef __cplusplus
}
#endif

#endif /* __MBED_OBJECTS_H__ */
