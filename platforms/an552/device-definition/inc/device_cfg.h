/*
 * Copyright (c) 2017-2023 Arm Limited
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing software
 * distributed under the License is distributed on an "AS IS" BASIS
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __MDH_ARM_DEVICE_CFG_H__
#define __MDH_ARM_DEVICE_CFG_H__

#include "device_definition.h"

/**
 * \file device_cfg.h
 * \brief Configuration file native driver re-targeting
 *
 * \details This file can be used to add native driver specific macro
 *          definitions to select which peripherals are available in the build.
 *
 * This is a default device configuration file with all peripherals enabled.
 */

/* ARM UART CMSDK Controller */
#define UART0_CMSDK_NS
#define UART0_CMSDK_DEV UART0_CMSDK_DEV_NS

#define UART1_CMSDK_NS
#define UART1_CMSDK_DEV UART1_CMSDK_DEV_NS

#define UART2_CMSDK_NS
#define UART2_CMSDK_DEV UART2_CMSDK_DEV_NS

#define UART3_CMSDK_NS
#define UART3_CMSDK_DEV UART3_CMSDK_DEV_NS

#define UART4_CMSDK_NS
#define UART4_CMSDK_DEV UART4_CMSDK_DEV_NS

#define UART5_CMSDK_NS
#define UART5_CMSDK_DEV UART5_CMSDK_DEV_NS

/* ARM SPI PL022 */
#define SPI_ADC_PL022_NS
#define SPI_ADC_PL022_DEV SPI_ADC_PL022_DEV_NS
#define SPI_SHIELD0_PL022_NS
#define SPI_SHIELD0_PL022_DEV SPI_SHIELD0_PL022_DEV_NS
#define SPI_SHIELD1_PL022_NS
#define SPI_SHIELD1_PL022_DEV SPI_SHIELD1_PL022_DEV_NS
#define DEFAULT_SPI_SPEED_HZ  400000U /* 400KHz */

/* QSPI */
#define QSPI_WRITE_NS
#define QSPI_WRITE_DEV QSPI_WRITE_DEV_NS

/* System Counter Armv8-M */
#define SYSCOUNTER_CNTRL_ARMV8_M_S
#define SYSCOUNTER_CNTRL_ARMV8_M_DEV SYSCOUNTER_CNTRL_ARMV8_M_DEV_S
/**
 * Arbitrary scaling values
 */
#define SYSCOUNTER_ARMV8_M_DEFAULT_SCALE0_INT   1u
#define SYSCOUNTER_ARMV8_M_DEFAULT_SCALE0_FRACT 0u
#define SYSCOUNTER_ARMV8_M_DEFAULT_SCALE1_INT   1u
#define SYSCOUNTER_ARMV8_M_DEFAULT_SCALE1_FRACT 0u

/* Systimer0 used for us ticker */
#define SYSTIMER0_ARMV8_M_NS
#define SYSTIMER0_DEV                    SYSTIMER0_ARMV8_M_DEV_NS
#define SYSTIMER0_ARMV8M_DEFAULT_FREQ_HZ (32000000ul)

#define SYSTIMER0_ALARM_IRQ   TIMER0_IRQn
#define SYSTIMER0_IRQ_HANDLER TIMER0_Handler
#define SYSTIMER0_FREQ_HZ     SYSTIMER0_ARMV8M_DEFAULT_FREQ_HZ /* System Clock */
#define SYSTIMER0_BIT_WIDTH   32U
/**
 * mbed usec high-resolution ticker configuration
 */
#define USEC_TIMER_DEV            SYSTIMER0_DEV
#define usec_interval_irq_handler TIMER0_Handler
#define USEC_INTERVAL_IRQ         TIMER0_IRQn
#define USEC_TIMER_FREQ_HZ        SYSTIMER0_ARMV8M_DEFAULT_FREQ_HZ
#define USEC_REPORTED_BITS        SYSTIMER0_BIT_WIDTH

/* Systimer1 */
#define SYSTIMER1_ARMV8_M_NS
#define SYSTIMER1_DEV                    SYSTIMER1_ARMV8_M_DEV_NS
#define SYSTIMER1_ARMV8M_DEFAULT_FREQ_HZ (32000000ul)

#define SYSTIMER1_ALARM_IRQ   TIMER1_IRQn
#define SYSTIMER1_IRQ_HANDLER TIMER1_Handler
#define SYSTIMER1_FREQ_HZ     SYSTIMER1_ARMV8M_DEFAULT_FREQ_HZ /* System Clock */
#define SYSTIMER1_BIT_WIDTH   32U
/**
 *mbed low power ticker configuration
 */
#define LP_TIMER_DEV            SYSTIMER1_DEV
#define lp_interval_irq_handler TIMER1_Handler
#define LP_INTERVAL_IRQ         TIMER1_IRQn
#define LP_TIMER_BIT_WIDTH      SYSTIMER1_BIT_WIDTH
#define LP_REPORTED_FREQ_HZ     SYSTIMER1_FREQ_HZ
#define LP_REPORTED_BITS        LP_TIMER_BIT_WIDTH

/* GPIO */
#define GPIO0_CMSDK_NS
#define GPIO0_CMSDK_DEV GPIO0_CMSDK_DEV_NS

#define GPIO1_CMSDK_NS
#define GPIO1_CMSDK_DEV GPIO1_CMSDK_DEV_NS

#define GPIO2_CMSDK_NS
#define GPIO2_CMSDK_DEV GPIO2_CMSDK_DEV_NS

/* ARM MPS3 IO SCC */
#define MPS3_IO_NS
#define MPS3_IO_DEV MPS3_IO_DEV_NS

/* I2C SBCON */
#define I2C0_SBCON_NS
#define I2C0_SBCON_DEV I2C0_SBCON_DEV_NS
#define I2C1_SBCON_NS
#define I2C1_SBCON_DEV I2C1_SBCON_DEV_NS
#define I2C2_SBCON_NS
#define I2C2_SBCON_DEV I2C2_SBCON_DEV_NS
#define I2C3_SBCON_NS
#define I2C3_SBCON_DEV I2C3_SBCON_DEV_NS

#define DEFAULT_UART_BAUDRATE 9600U

#endif /* __MDH_ARM_DEVICE_CFG_H__ */
