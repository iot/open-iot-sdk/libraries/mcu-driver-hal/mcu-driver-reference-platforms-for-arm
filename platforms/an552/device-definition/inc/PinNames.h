/* Copyright (c) 2020-2023, Arm Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MBED_PINNAMES_H
#define MBED_PINNAMES_H

/* Pins used by mbed OS to identify STDIO UART pins */
#define STDIO_UART_TX USBTX
#define STDIO_UART_RX USBRX

#ifdef __cplusplus
extern "C" {
#endif

typedef enum { PIN_INPUT, PIN_OUTPUT } PinDirection;

typedef enum {
    /* MPS3 EXP Pin Names */
    EXP0 = 0,
    EXP1 = 1,
    EXP2 = 2,
    EXP3 = 3,
    EXP4 = 4,
    EXP5 = 5,
    EXP6 = 6,
    EXP7 = 7,
    EXP8 = 8,
    EXP9 = 9,
    EXP10 = 10,
    EXP11 = 11,
    EXP12 = 12,
    EXP13 = 13,
    EXP14 = 14,
    EXP15 = 15,
    EXP16 = 16,
    EXP17 = 17,
    EXP18 = 18,
    EXP19 = 19,
    EXP20 = 20,
    EXP21 = 21,
    EXP22 = 22,
    EXP23 = 23,
    EXP24 = 24,
    EXP25 = 25,
    EXP26 = 26,
    EXP27 = 27,
    EXP28 = 28,
    EXP29 = 29,
    EXP30 = 30,
    EXP31 = 31,
    EXP32 = 32,
    EXP33 = 33,
    EXP34 = 34,
    EXP35 = 35,
    EXP36 = 36,
    EXP37 = 37,
    EXP38 = 38,
    EXP39 = 39,
    EXP40 = 40,
    EXP41 = 41,
    EXP42 = 42,
    EXP43 = 43,
    EXP44 = 44,
    EXP45 = 45,
    EXP46 = 46,
    EXP47 = 47,
    EXP48 = 48,
    EXP49 = 49,
    EXP50 = 50,
    EXP51 = 51,

    // Other mbed Pin Names
    SW1 = 100,
    SW2 = 101,
    SW3 = 102,
    SW4 = 103,
    SW5 = 104,
    SW6 = 105,
    SW7 = 106,
    SW8 = 107,

    LED1 = 200,
    LED2 = 201,
    LED3 = 202,
    LED4 = 203,
    LED5 = 204,
    LED6 = 205,
    LED7 = 206,
    LED8 = 207,
    USERLED1 = 208,
    USERLED2 = 209,

    USERPB1 = 300,
    USERPB2 = 301,

    CONSOLE_RX = 400,
    CONSOLE_TX = 401,
    U1_RX = 410,
    U1_TX = 411,
    U2_RX = 420,
    U2_TX = 421,
    U5_RX = 450,
    U5_TX = 451,

    SPI_ADC_SDO = 500,
    SPI_ADC_SDI = 501,
    SPI_ADC_SCK = 502,
    SPI_ADC_CS = 503,

    I2C_TOUCH_SDA = 510,
    I2C_TOUCH_SCL = 511,
    I2C_AUDIO_SDA = 512,
    I2C_AUDIO_SCL = 513,

    QSPI_FLASH1_IO0 = 520,
    QSPI_FLASH1_IO1 = 521,
    QSPI_FLASH1_IO2 = 522,
    QSPI_FLASH1_IO3 = 523,
    QSPI_FLASH1_SCK = 524,
    QSPI_FLASH1_CSN = 525,

    /* MPS3 shield Analog pins */
    A0_0 = 600,
    A0_1 = 601,
    A0_2 = 602,
    A0_3 = 603,
    A0_4 = 604,
    A0_5 = 605,
    A1_0 = 606,
    A1_1 = 607,
    A1_2 = 608,
    A1_3 = 609,
    A1_4 = 610,
    A1_5 = 611,
    /* MPS3 Shield Digital pins */
    D0_0 = EXP0,
    D0_1 = EXP1,
    D0_2 = EXP2,
    D0_3 = EXP3,
    D0_4 = EXP4,
    D0_5 = EXP5,
    D0_6 = EXP6,
    D0_7 = EXP7,
    D0_8 = EXP8,
    D0_9 = EXP9,
    D0_10 = EXP10,
    D0_11 = EXP11,
    D0_12 = EXP12,
    D0_13 = EXP13,
    D0_14 = EXP14,
    D0_15 = EXP15,
    D0_16 = EXP32,
    D0_17 = EXP33,

    D1_0 = EXP16,
    D1_1 = EXP17,
    D1_2 = EXP18,
    D1_3 = EXP19,
    D1_4 = EXP20,
    D1_5 = EXP21,
    D1_6 = EXP22,
    D1_7 = EXP23,
    D1_8 = EXP24,
    D1_9 = EXP25,
    D1_10 = EXP26,
    D1_11 = EXP27,
    D1_12 = EXP28,
    D1_13 = EXP29,
    D1_14 = EXP30,
    D1_15 = EXP31,
    D1_16 = EXP34,
    D1_17 = EXP35,

    ARDUINO_UNO_SPI_MOSI = D0_11,
    ARDUINO_UNO_SPI_MISO = D0_12,
    ARDUINO_UNO_SPI_SCK = D0_13,
    ARDUINO_UNO_SPI_CS = D0_10,

    /* Not connected */
    NC = (int)0xFFFFFFFF,
} PinName;

typedef enum {
    ALTERNATE_FUNC = 0, /* The pin is used for alternative function */
    GPIO_FUNC = 1,      /* The pin is used for GPIO function */
    NOT_A_GPIO = 2      /* The pin is not a GPIO and function is not relevant */
} PinFunction;

typedef enum { PullUp = 2, PullDown = 1, PullNone = 0, Repeater = 3, OpenDrain = 4, PullDefault = PullDown } PinMode;

#ifdef __cplusplus
}
#endif

#endif
