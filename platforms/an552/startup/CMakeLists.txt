# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

add_library(mdh-arm-startup-an552 OBJECT
    $<$<STREQUAL:${CMAKE_C_COMPILER_ID},ARMClang>:src/startup_cmsdk_cs300.c>
    $<$<STREQUAL:${CMAKE_C_COMPILER_ID},GNU>:src/startup_cmsdk_cs300.S>
)

target_link_options(mdh-arm-startup-an552 INTERFACE --entry=Reset_Handler)

target_link_libraries(mdh-arm-startup-an552 PRIVATE mdh-arm-cmsis-core-device-an552)
