/* Copyright (c) 2023, Arm  Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "platform/busy_wait.h"

#include "system_core_init.h"
#include <stdint.h>

/* Function in assembly code to guarantee predictable delay depending on the
 * architecture. */
static void delay_loop(uint32_t count) __attribute__((naked, noinline));

void wait_us(uint32_t us)
{
    while (us > 1024) {
        us -= 1024;
        wait_ns(1024000);
    }
    if (us > 0) {
        wait_ns(us * 1000);
    }
}

static void delay_loop(uint32_t count)
{
    asm volatile("1: SUBS R0, R0, #1\n"
                 "NOP\n"
                 "NOP\n"
                 "BCS 1b\n"
                 "BX LR");
}

void wait_ns(uint32_t ns)
{
    uint32_t cycles_per_us = SystemCoreClock / 1000000U;

    /* Cortex-M33 and M55 can dual issue for 3 cycles per iteration
     * (SUB,NOP) = 1,
     * (NOP,BCS) = 2. */
    uint32_t delay_loop_scaler = 3000U;

    /* Note that this very calculation, plus call overhead, will take multiple
     * cycles. Could well be 100ns on its own... So round down here, startup is
     * worth at least one loop iteration. */
    uint32_t count = (cycles_per_us * ns) / delay_loop_scaler;

    delay_loop(count);
}
