/*
 * Copyright (c) 2022 Arm Limited. All rights reserved.
 *
 * Licensed under the Apache License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing software
 * distributed under the License is distributed on an "AS IS" BASIS
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __PLATFORM_IRQ_H__
#define __PLATFORM_IRQ_H__

typedef enum _IRQn_Type {
    /* -------------------  Cortex-M4 Processor Exceptions Numbers  ------------------- */
    NonMaskableInt_IRQn = -14,   /*  2 Non Maskable Interrupt          */
    HardFault_IRQn = -13,        /*  3 HardFault Interrupt             */
    MemoryManagement_IRQn = -12, /*  4 Memory Management Interrupt     */
    BusFault_IRQn = -11,         /*  5 Bus Fault Interrupt             */
    UsageFault_IRQn = -10,       /*  6 Usage Fault Interrupt           */
    SVCall_IRQn = -5,            /* 11 SV Call Interrupt               */
    DebugMonitor_IRQn = -4,      /* 12 Debug Monitor Interrupt         */
    PendSV_IRQn = -2,            /* 14 Pend SV Interrupt               */
    SysTick_IRQn = -1,           /* 15 System Tick Interrupt           */

    /******  CMSDK Specific Interrupt Numbers *********************************************************/
    UARTRX0_IRQn = 0,    /*!< UART 0 RX Interrupt                               */
    UARTTX0_IRQn = 1,    /*!< UART 0 TX Interrupt                               */
    UARTRX1_IRQn = 2,    /*!< UART 1 RX Interrupt                               */
    UARTTX1_IRQn = 3,    /*!< UART 1 TX Interrupt                               */
    UARTRX2_IRQn = 4,    /*!< UART 2 RX Interrupt                               */
    UARTTX2_IRQn = 5,    /*!< UART 2 TX Interrupt                               */
    PORT0_ALL_IRQn = 6,  /*!< Port 0 combined Interrupt                         */
    PORT1_ALL_IRQn = 7,  /*!< Port 1 combined Interrupt                         */
    TIMER0_IRQn = 8,     /*!< TIMER 0 Interrupt                                 */
    TIMER1_IRQn = 9,     /*!< TIMER 1 Interrupt                                 */
    DUALTIMER_IRQn = 10, /*!< Dual Timer Interrupt                              */
    SPI_IRQn = 11,       /*!< SPI Interrupt                                     */
    UARTOVF_IRQn = 12,   /*!< UART 0,1,2 Overflow Interrupt                     */
    ETHERNET_IRQn = 13,  /*!< Ethernet Interrupt                                */
    I2S_IRQn = 14,       /*!< I2S Interrupt                                     */
    TSC_IRQn = 15,       /*!< Touch Screen Interrupt                            */
    PORT2_ALL_IRQn = 16, /*!< Port 2 combined Interrupt                         */
    PORT3_ALL_IRQn = 17, /*!< Port 3 combined Interrupt                         */
    UARTRX3_IRQn = 18,   /*!< UART 3 RX Interrupt                               */
    UARTTX3_IRQn = 19,   /*!< UART 3 TX Interrupt                               */
    UARTRX4_IRQn = 20,   /*!< UART 4 RX Interrupt                               */
    UARTTX4_IRQn = 21,   /*!< UART 4 TX Interrupt                               */
    ADCSPI_IRQn = 22,    /*!< SHIELD ADC SPI Interrupt                          */
    SHIELDSPI_IRQn = 23, /*!< SHIELD SPI Combined Interrupt                     */
    PORT0_0_IRQn = 24,   /*!<  GPIO Port 0 pin 0 Interrupt                      */
    PORT0_1_IRQn = 25,   /*!<  GPIO Port 0 pin 1 Interrupt                      */
    PORT0_2_IRQn = 26,   /*!<  GPIO Port 0 pin 2 Interrupt                      */
    PORT0_3_IRQn = 27,   /*!<  GPIO Port 0 pin 3 Interrupt                      */
    PORT0_4_IRQn = 28,   /*!<  GPIO Port 0 pin 4 Interrupt                      */
    PORT0_5_IRQn = 29,   /*!<  GPIO Port 0 pin 5 Interrupt                      */
    PORT0_6_IRQn = 30,   /*!<  GPIO Port 0 pin 6 Interrupt                      */
    PORT0_7_IRQn = 31,   /*!<  GPIO Port 0 pin 7 Interrupt                      */

    /* TODO: The following drivers are not currently supported on MPS2,
        but they cannot be removed from the list below. */
    UARTRX5_IRQn = -1, /* UART 4 RX Interrupt */
    UARTTX5_IRQn = -1, /* UART 4 TX Interrupt */

} IRQn_Type;

#endif /* __PLATFORM_IRQ_H__ */
