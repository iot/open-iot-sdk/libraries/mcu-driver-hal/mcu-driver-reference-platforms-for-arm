/*
 * Copyright (c) 2022 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include CMSIS_device_header

/*----------------------------------------------------------------------------
  Exception / Interrupt Handler Function Prototype
 *----------------------------------------------------------------------------*/
typedef void (*pFunc)(void);

/*----------------------------------------------------------------------------
  External References
 *----------------------------------------------------------------------------*/
extern uint32_t __INITIAL_SP;
extern uint32_t __STACK_LIMIT;

extern void __PROGRAM_START(void) __NO_RETURN;

/*----------------------------------------------------------------------------
  Internal References
 *----------------------------------------------------------------------------*/
void Reset_Handler(void) __NO_RETURN;

/*----------------------------------------------------------------------------
  Exception / Interrupt Handler
 *----------------------------------------------------------------------------*/
#define DEFAULT_IRQ_HANDLER(handler_name) \
    void __WEAK handler_name(void);       \
    void handler_name(void)               \
    {                                     \
        while (1)                         \
            ;                             \
    }

/* Exceptions */
DEFAULT_IRQ_HANDLER(NMI_Handler)
DEFAULT_IRQ_HANDLER(HardFault_Handler)
DEFAULT_IRQ_HANDLER(MemManage_Handler)
DEFAULT_IRQ_HANDLER(BusFault_Handler)
DEFAULT_IRQ_HANDLER(UsageFault_Handler)
DEFAULT_IRQ_HANDLER(SVC_Handler)
DEFAULT_IRQ_HANDLER(DebugMon_Handler)
DEFAULT_IRQ_HANDLER(PendSV_Handler)
DEFAULT_IRQ_HANDLER(SysTick_Handler)

DEFAULT_IRQ_HANDLER(UARTRX0_Handler)
DEFAULT_IRQ_HANDLER(UARTTX0_Handler)
DEFAULT_IRQ_HANDLER(UARTRX1_Handler)
DEFAULT_IRQ_HANDLER(UARTTX1_Handler)
DEFAULT_IRQ_HANDLER(UARTRX2_Handler)
DEFAULT_IRQ_HANDLER(UARTTX2_Handler)
DEFAULT_IRQ_HANDLER(PORT0_COMB_Handler)
DEFAULT_IRQ_HANDLER(PORT1_COMB_Handler)
DEFAULT_IRQ_HANDLER(TIMER0_Handler)
DEFAULT_IRQ_HANDLER(TIMER1_Handler)
DEFAULT_IRQ_HANDLER(DUALTIMER_HANDLER)
DEFAULT_IRQ_HANDLER(SPI_Handler)
DEFAULT_IRQ_HANDLER(UARTOVF_Handler)
DEFAULT_IRQ_HANDLER(ETHERNET_Handler)
DEFAULT_IRQ_HANDLER(I2S_Handler)
DEFAULT_IRQ_HANDLER(TSC_Handler)
DEFAULT_IRQ_HANDLER(PORT2_COMB_Handler)
DEFAULT_IRQ_HANDLER(PORT3_COMB_Handler)
DEFAULT_IRQ_HANDLER(UARTRX3_Handler)
DEFAULT_IRQ_HANDLER(UARTTX3_Handler)
DEFAULT_IRQ_HANDLER(UARTRX4_Handler)
DEFAULT_IRQ_HANDLER(UARTTX4_Handler)
DEFAULT_IRQ_HANDLER(ADCSPI_Handler)
DEFAULT_IRQ_HANDLER(SHIELDSPI_Handler)
DEFAULT_IRQ_HANDLER(PORT0_0_Handler)
DEFAULT_IRQ_HANDLER(PORT0_1_Handler)
DEFAULT_IRQ_HANDLER(PORT0_2_Handler)
DEFAULT_IRQ_HANDLER(PORT0_3_Handler)
DEFAULT_IRQ_HANDLER(PORT0_4_Handler)
DEFAULT_IRQ_HANDLER(PORT0_5_Handler)
DEFAULT_IRQ_HANDLER(PORT0_6_Handler)
DEFAULT_IRQ_HANDLER(PORT0_7_Handler)

/*----------------------------------------------------------------------------
  Exception / Interrupt Vector table
 *----------------------------------------------------------------------------*/

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#endif

extern const pFunc __VECTOR_TABLE[496];
const pFunc __VECTOR_TABLE[496] __VECTOR_TABLE_ATTRIBUTE = {
    (pFunc)(&__INITIAL_SP), /*      Initial Stack Pointer */
    Reset_Handler,          /*      Reset Handler */
    NMI_Handler,            /* -14: NMI Handler */
    HardFault_Handler,      /* -13: Hard Fault Handler */
    MemManage_Handler,      /* -12: MPU Fault Handler */
    BusFault_Handler,       /* -11: Bus Fault Handler */
    UsageFault_Handler,     /* -10: Usage Fault Handler */
    0,                      /*      Reserved */
    0,                      /*      Reserved */
    0,                      /*      Reserved */
    0,                      /*      Reserved */
    SVC_Handler,            /*  -5: SVCall Handler */
    DebugMon_Handler,       /*  -4: Debug Monitor Handler */
    0,                      /*      Reserved */
    PendSV_Handler,         /*  -2: PendSV Handler */
    SysTick_Handler,        /*  -1: SysTick Handler */

    UARTRX0_Handler,    /* UART 0 RX Handler */
    UARTTX0_Handler,    /* UART 0 TX Handler */
    UARTRX1_Handler,    /* UART 1 RX Handler */
    UARTTX1_Handler,    /* UART 1 TX Handler */
    UARTRX2_Handler,    /* UART 2 RX Handler */
    UARTTX2_Handler,    /* UART 2 TX Handler */
    PORT0_COMB_Handler, /* GPIO Port 0 Combined Handler */
    PORT1_COMB_Handler, /* GPIO Port 1 Combined Handler */
    TIMER0_Handler,     /* TIMER 0 handler */
    TIMER1_Handler,     /* TIMER 1 handler */
    DUALTIMER_HANDLER,  /* Dual timer handler */
    SPI_Handler,        /* SPI exceptions Handler */
    UARTOVF_Handler,    /* UART 0,1,2 Overflow Handler */
    ETHERNET_Handler,   /* Ethernet Overflow Handler */
    I2S_Handler,        /* I2S Handler */
    TSC_Handler,        /* Touch Screen handler */
    PORT2_COMB_Handler, /* GPIO Port 2 Combined Handler */
    PORT3_COMB_Handler, /* GPIO Port 3 Combined Handler */
    UARTRX3_Handler,    /* UART 3 RX Handler */
    UARTTX3_Handler,    /* UART 3 TX Handler */
    UARTRX4_Handler,    /* UART 4 RX Handler */
    UARTTX4_Handler,    /* UART 4 TX Handler */
    ADCSPI_Handler,     /* SHIELD ADC SPI exceptions Handler */
    SHIELDSPI_Handler,  /* SHIELD SPI exceptions Handler */
    PORT0_0_Handler,    /* GPIO Port 0 pin 0 Handler */
    PORT0_1_Handler,    /* GPIO Port 0 pin 1 Handler */
    PORT0_2_Handler,    /* GPIO Port 0 pin 2 Handler */
    PORT0_3_Handler,    /* GPIO Port 0 pin 3 Handler */
    PORT0_4_Handler,    /* GPIO Port 0 pin 4 Handler */
    PORT0_5_Handler,    /* GPIO Port 0 pin 5 Handler */
    PORT0_6_Handler,    /* GPIO Port 0 pin 6 Handler */
    PORT0_7_Handler,    /* GPIO Port 0 pin 7 Handler */
};

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

/*----------------------------------------------------------------------------
  Reset Handler called on controller reset
 *----------------------------------------------------------------------------*/
void Reset_Handler(void)
{
    SystemInit();      /* CMSIS System Initialization */
    __PROGRAM_START(); /* Enter PreMain (C library entry point) */
}
