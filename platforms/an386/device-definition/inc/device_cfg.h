/*
 * Copyright (c) 2017-2023 Arm Limited
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing software
 * distributed under the License is distributed on an "AS IS" BASIS
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __MDH_ARM_DEVICE_CFG_H__
#define __MDH_ARM_DEVICE_CFG_H__

#include "device_definition.h"

/**
 * \file device_cfg.h
 * \brief Configuration file native driver re-targeting
 *
 * \details This file can be used to add native driver specific macro
 *          definitions to select which peripherals are available in the build.
 *
 * This is a default device configuration file with all peripherals enabled.
 */

/* ARM UART CMSDK Controller */
#define UART0_CMSDK_NS
#define UART0_CMSDK_DEV UART0_CMSDK_DEV_NS

#define UART1_CMSDK_NS
#define UART1_CMSDK_DEV UART1_CMSDK_DEV_NS

#define UART2_CMSDK_NS
#define UART2_CMSDK_DEV UART2_CMSDK_DEV_NS

#define UART3_CMSDK_NS
#define UART3_CMSDK_DEV UART3_CMSDK_DEV_NS

#define UART4_CMSDK_NS
#define UART4_CMSDK_DEV UART4_CMSDK_DEV_NS

#define UART5_CMSDK_NS
#define UART5_CMSDK_DEV UART5_CMSDK_DEV_NS

#define USEC_REPORTED_SHIFT       0
#define USEC_REPORTED_FREQ_HZ     1562500
#define USEC_REPORTED_BITS        32
#define USEC_INTERVAL_IRQ         DUALTIMER_IRQn
#define TIMER_CMSDK_MAX_RELOAD    0xFFFFFFFF
#define usec_interval_irq_handler DUALTIMER_HANDLER

/* GPIO */
#define GPIO0_CMSDK_NS
#define GPIO0_CMSDK_DEV GPIO0_CMSDK_DEV_NS

#define GPIO1_CMSDK_NS
#define GPIO1_CMSDK_DEV GPIO1_CMSDK_DEV_NS

#define GPIO2_CMSDK_NS
#define GPIO2_CMSDK_DEV GPIO2_CMSDK_DEV_NS

/* ARM MPS3 IO SCC */
#define MPS3_IO_NS
#define MPS3_IO_DEV MPS3_IO_DEV_NS

#define DEFAULT_UART_BAUDRATE 9600U

#endif /* __MDH_ARM_DEVICE_CFG_H__ */
