/* Copyright (c) 2019-2022, Arm Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MBED_PERIPHERALNAMES_H
#define MBED_PERIPHERALNAMES_H

#include CMSIS_device_header

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    UART_0 = 0, // FPGA_UART0
    UART_1,     // FPGA_UART1
    UART_2,     // FPGA_UART2
    UART_3,     // Shield 0
    UART_4,     // Shield 1
    UART_5,     // FPGA_UART3

    UART_MAX
} UARTName;

typedef enum {
    ADC0_0 = 0,
    ADC0_1,
    ADC0_2,
    ADC0_3,
    ADC0_4,
    ADC0_5,
    ADC0_6,
    ADC0_7,
    ADC0_8,
    ADC0_9,
    ADC0_10,
    ADC0_11
} ADCName;

typedef enum {
    SPI_ADC = 0,
    SPI_SHIELD0,
    SPI_SHIELD1,

    SPI_NC,
    SPI_COUNT = SPI_NC
} SPIName;

typedef enum { I2C_AUDIO, I2C_TOUCH, I2C_SHIELD0, I2C_SHIELD1 } I2CName;

#define DEVICE_SPI_COUNT SPI_COUNT

// #define STDIO_UART_TX  UART0_TX
// #define STDIO_UART_RX  UART0_RX
// #define STDIO_UART     UART_0

// #define USBTX          STDIO_UART_TX
// #define USBRX          STDIO_UART_RX

typedef enum { QSPI_FLASH } QSPIName;

#ifdef __cplusplus
}
#endif

#endif
