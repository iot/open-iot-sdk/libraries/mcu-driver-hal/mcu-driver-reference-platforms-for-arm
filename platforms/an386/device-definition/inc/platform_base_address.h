/*
 * Copyright (c) 2022 Arm Limited
 *
 * Licensed under the Apache License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing software
 * distributed under the License is distributed on an "AS IS" BASIS
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * \file platform_base_address.h
 * \brief This file defines all the peripheral base addresses for MPS2
 */

#ifndef __PLATFORM_BASE_ADDRESS_H__
#define __PLATFORM_BASE_ADDRESS_H__

/* ======= Defines peripherals memory map addresses ======= */

/* Non-secure memory map addresses */
#define UART0_BASE_NS 0x40004000 /* UART 0 Non-Secure base address */
#define UART1_BASE_NS 0x40005000 /* UART 1 Non-Secure base address */
#define UART2_BASE_NS 0x40006000 /* UART 2 Non-Secure base address */

#define GPIO0_CMSDK_BASE_NS 0x40010000 /* GPIO 0 Non-Secure base address */
#define GPIO1_CMSDK_BASE_NS 0x40011000 /* GPIO 1 Non-Secure base address */
#define GPIO2_CMSDK_BASE_NS 0x40012000 /* GPIO 2 Non-Secure base address */
#define GPIO3_CMSDK_BASE_NS 0x40013000 /* GPIO 3 Non-Secure base address */

#define FPGA_IO_BASE_NS  0x40028000 /* FPGA - IO (System Ctrl + I/O) Non-Secure base address */
#define ETHERNET_BASE_NS 0x40200000 /* Ethernet Non-Secure base address */

#define CMSDK_DUALTIMER_BASE 0x40002000 /* CMSDK Dual Timer Non-Secure base address */
#define CMSDK_TIMER0_BASE    0x40000000 /* CMSDK Timer Non-Secure base address */
#define CMSDK_TIMER1_BASE    0x40001000 /* CMSDK Timer Non-Secure base address */

/* TODO: The devices that use the following macros are unsupported by mps2-m4. However,
   removing them causes build errors. To be reworked. */
/* Non-Secure Subsystem peripheral region */
#define QSPI_CONFIG_BASE_NS          0x41800000 /* QSPI Config Non-Secure base address */
#define QSPI_WRITE_BASE_NS           0x41801000 /* QSPI Write Non-Secure base address */
#define SYSTIMER0_ARMV8_M_BASE_NS    0x48000000 /* System Timer 0 Non-Secure base address */
#define SYSTIMER1_ARMV8_M_BASE_NS    0x48001000 /* System Timer 1 Non-Secure base address */
#define SYSCNTR_READ_BASE_NS         0x48101000 /* System Counter Read Secure base address */
#define ETHOS_U55_APB_BASE_NS        0x48102000 /* Ethos-U55 APB Non-Secure base address */
#define FPGA_SBCon_I2C_TOUCH_BASE_NS 0x49200000 /* FPGA - SBCon I2C (Touch) Non-Secure base address */
#define FPGA_SBCon_I2C_AUDIO_BASE_NS 0x49201000 /* FPGA - SBCon I2C (Audio Conf) Non-Secure base address */
#define FPGA_SPI_ADC_BASE_NS         0x49202000 /* FPGA - PL022 (SPI ADC) Non-Secure base address */
#define FPGA_SPI_SHIELD0_BASE_NS     0x49203000 /* FPGA - PL022 (SPI Shield0) Non-Secure base address */
#define FPGA_SPI_SHIELD1_BASE_NS     0x49204000 /* FPGA - PL022 (SPI Shield1) Non-Secure base address */
#define SBCon_I2C_SHIELD0_BASE_NS    0x49205000 /* SBCon (I2C - Shield0) Non-Secure base address */
#define SBCon_I2C_SHIELD1_BASE_NS    0x49206000 /* SBCon (I2C – Shield1) Non-Secure base address */
#define USER_APB_BASE_NS             0x49207000 /* USER APB Non-Secure base address */
#define FPGA_SCC_BASE_NS             0x49300000 /* FPGA - SCC registers Non-Secure base address */
#define FPGA_I2S_BASE_NS             0x49301000 /* FPGA - I2S (Audio) Non-Secure base address */
#define UART3_BASE_NS                0x49306000 /* UART 3 Non-Secure base address */
#define UART4_BASE_NS                0x49307000 /* UART 4 Non-Secure base address */
#define UART5_BASE_NS                0x49308000 /* UART 5 Non-Secure base address */

/* Secure memory map addresses */
#define SRAM_BASE_S          0x11000000 /* CODE SRAM Secure base address */
#define SYSCNTR_CNTRL_BASE_S 0x58100000 /* System Counter Control Secure base address */
#define SYSCNTR_READ_BASE_S  0x58101000 /* System Counter Read Secure base address */

/* Memory size definitions */
#define SRAM_SIZE (0x00200000) /* 2 MB */

#endif /* __PLATFORM_BASE_ADDRESS_H__ */
