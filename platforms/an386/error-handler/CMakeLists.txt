# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

add_library(mdh-arm-error-handler-an386 src/error_handler.c)
target_link_libraries(mdh-arm-error-handler-an386 PUBLIC mdh-arm-platform-common-api)
