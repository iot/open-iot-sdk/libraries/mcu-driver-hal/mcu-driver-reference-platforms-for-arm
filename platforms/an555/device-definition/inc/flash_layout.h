/*
 * Copyright (c) 2019-2022 Arm Limited. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FLASH_LAYOUT_H__
#define __FLASH_LAYOUT_H__

#include "platform_base_address.h"

#define FLASH_BASE_ADDRESS           (SRAM_BASE_S) /* Same as FLASH0_BASE_S */
#define FLASH_TOTAL_SIZE             (SRAM_SIZE)   /* 2 MB, Same as FLASH0_SIZE */
#define FLASH_AREA_IMAGE_SECTOR_SIZE (0x1000)      /* 4 KB */

#endif /* __FLASH_LAYOUT_H__ */
