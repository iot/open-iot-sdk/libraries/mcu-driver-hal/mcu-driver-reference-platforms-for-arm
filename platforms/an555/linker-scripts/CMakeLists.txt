# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

add_library(mdh-arm-linker-script-an555 INTERFACE)

if(CMAKE_C_COMPILER_ID STREQUAL "ARMClang")
    set(LINKER_FILE ${CMAKE_CURRENT_LIST_DIR}/src/cs310_ns.sct)
elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")
    set(LINKER_FILE ${CMAKE_CURRENT_LIST_DIR}/src/cs310_ns.ld)
endif()

set(link_definitions_file ${CMAKE_CURRENT_BINARY_DIR}/link_definitions.txt)
mdh_generate_definitions_for_linker(mdh-arm-linker-script-an555 ${link_definitions_file})
mdh_set_linker_script(mdh-arm-linker-script-an555 ${LINKER_FILE} ${link_definitions_file})
