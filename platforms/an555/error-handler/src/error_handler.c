/* Copyright (c) 2023, Arm  Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "platform/error_handler.h"

void halt_on_error(const char *format, ...)
{
    (void)format;

    while (1) {
        // no-op infinite loop
    }
}
