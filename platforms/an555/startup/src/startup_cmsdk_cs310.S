;/*
; * Copyright (c) 2019-2022 Arm Limited
; *
; * SPDX-License-Identifier: Apache-2.0
; *
; * Licensed under the Apache License, Version 2.0 (the "License");
; * you may not use this file except in compliance with the License.
; * You may obtain a copy of the License at
; *
; *     http://www.apache.org/licenses/LICENSE-2.0
; *
; * Unless required by applicable law or agreed to in writing, software
; * distributed under the License is distributed on an "AS IS" BASIS,
; * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; * See the License for the specific language governing permissions and
; * limitations under the License.
; *
; *
; * This file is derivative of CMSIS V5.00 startup_ARMCM33.S
; */

    .syntax    unified
    .arch    armv8-m.main

    .section .vectors
    .align 2
    .globl    __Vectors
__Vectors:
    .long    __StackTop            /* Top of Stack                                      */
    .long    Reset_Handler         /* Reset Handler */
    .long    NMI_Handler           /* -14: NMI Handler */
    .long    HardFault_Handler     /* -13: Hard Fault Handler */
    .long    MemManage_Handler     /* -12: MPU Fault Handler */
    .long    BusFault_Handler      /* -11: Bus Fault Handler */
    .long    UsageFault_Handler    /* -10: Usage Fault Handler */
    .long    SecureFault_Handler   /* -9: Secure Fault Handler */
    .long    0                     /* Reserved */
    .long    0                     /* Reserved */
    .long    0                     /* Reserved */
    .long    SVC_Handler           /* SVCall Handler */
    .long    DebugMon_Handler      /* Debug Monitor Handler */
    .long    0                     /* Reserved */
    .long    PendSV_Handler        /* PendSV Handler */
    .long    SysTick_Handler       /* SysTick Handler */

    /* Core interrupts */
    .long    NONSEC_WATCHDOG_RESET_Handler    /* 0: Non-Secure Watchdog Reset Request Interrupt */
    .long    NONSEC_WATCHDOG_Handler          /* 1: Non-Secure Watchdog Interrupt */
    .long    SLOWCLK_Timer_Handler           /* 2: SLOWCLK Timer Handler */
    .long    TIMER0_Handler                  /* 3: TIMER 0 Handler */
    .long    TIMER1_Handler                  /* 4: TIMER 1 Handler */
    .long    TIMER2_Handler                  /* 5: TIMER 2 Handler */
    .long    0                               /* 6: Reserved */
    .long    0                               /* 7: Reserved */
    .long    0                               /* 8: Reserved */
    .long    MPC_Handler                     /* 9: MPC Combined (Secure) Handler */
    .long    PPC_Handler                     /* 10: PPC Combined (Secure) Handler */
    .long    MSC_Handler                     /* 11: MSC Combined (Secure) Handler */
    .long    BRIDGE_ERROR_Handler            /* 12: Bridge Error (Secure) Handler */
    .long    0                               /* 13: Reserved */
    .long    MGMT_PPU_Handler                /* 14: MGMT PPU Handler */
    .long    SYS_PPU_Handler                 /* 15: System PPU Interrupt */
    .long    CPU0_PPU_Handler                /* 16: CPU0 PPU Handler */
    .long    0                               /* 17: Reserved */
    .long    0                               /* 18: Reserved */
    .long    0                               /* 19: Reserved */
    .long    0                               /* 20: Reserved */
    .long    0                               /* 21: Reserved */
    .long    0                               /* 22: Reserved */
    .long    0                               /* 23: Reserved */
    .long    0                               /* 24: Reserved */
    .long    0                               /* 25: Reserved */
    .long    DEBUG_PPU_Handler               /* 26: Debug PPU Interrupt */
    .long    TIMER3_Handler                  /* 27: TIMER 3 Handler */
    .long    CTI_REQ0_IRQHandler             /* 28: CTI request 0 IRQ Handler */
    .long    CTI_REQ1_IRQHandler             /* 29: CTI request 1 IRQ Handler */
    .long    0                               /* 30: Reserved */
    .long    0                               /* 31: Reserved */

    /* External interrupts */
    .long    System_Timestamp_Counter_Handler   /* 32: System timestamp counter Handler */
    .long    UARTRX0_Handler                    /* 33: UART 0 RX Handler */
    .long    UARTTX0_Handler                    /* 34: UART 0 TX Handler */
    .long    UARTRX1_Handler                    /* 35: UART 1 RX Handler */
    .long    UARTTX1_Handler                    /* 36: UART 1 TX Handler */
    .long    UARTRX2_Handler                    /* 37: UART 2 RX Handler */
    .long    UARTTX2_Handler                    /* 38: UART 2 TX Handler */
    .long    UARTRX3_Handler                    /* 39: UART 3 RX Handler */
    .long    UARTTX3_Handler                    /* 40: UART 3 TX Handler */
    .long    UARTRX4_Handler                    /* 41: UART 4 RX Handler */
    .long    UARTTX4_Handler                    /* 42: UART 4 TX Handler */
    .long    UART0_Combined_Handler             /* 43: UART 0 Combined Handler */
    .long    UART1_Combined_Handler             /* 44: UART 1 Combined Handler */
    .long    UART2_Combined_Handler             /* 45: UART 2 Combined Handler */
    .long    UART3_Combined_Handler             /* 46: UART 3 Combined Handler */
    .long    UART4_Combined_Handler             /* 47: UART 4 Combined Handler */
    .long    UARTOVF_Handler                    /* 48: UART 0, 1, 2, 3, 4 & 5 Overflow Handler */
    .long    ETHERNET_Handler                   /* 49: Ethernet Handler */
    .long    I2S_Handler                        /* 50: Audio I2S Handler */
    .long    TOUCH_SCREEN_Handler               /* 51: Touch Screen Handler */
    .long    USB_Handler                        /* 52: USB Handler */
    .long    SPI_ADC_Handler                    /* 53: SPI ADC Handler */
    .long    SPI_SHIELD0_Handler                /* 54: SPI (Shield 0) Handler */
    .long    SPI_SHIELD1_Handler                /* 55: SPI (Shield 1) Handler */
    .long    ETHOS_U55_Handler                  /* 56: Ethos-U55 Handler */
    .long    0                                  /* 57: Reserved */
    .long    0                                  /* 58: Reserved */
    .long    0                                  /* 59: Reserved */
    .long    0                                  /* 60: Reserved */
    .long    0                                  /* 61: Reserved */
    .long    0                                  /* 62: Reserved */
    .long    0                                  /* 63: Reserved */
    .long    0                                  /* 64: Reserved */
    .long    0                                  /* 65: Reserved */
    .long    0                                  /* 66: Reserved */
    .long    0                                  /* 67: Reserved */
    .long    0                                  /* 68: Reserved */
    .long    GPIO0_Combined_Handler             /* 69: GPIO 0 Combined Handler */
    .long    GPIO1_Combined_Handler             /* 70: GPIO 1 Combined Handler */
    .long    GPIO2_Combined_Handler             /* 71: GPIO 2 Combined Handler */
    .long    GPIO3_Combined_Handler             /* 72: GPIO 3 Combined Handler */
    .long    GPIO0_0_Handler                    /* 73: GPIO0 Pin 0 Handler */
    .long    GPIO0_1_Handler                    /* 74: GPIO0 Pin 1 Handler */
    .long    GPIO0_2_Handler                    /* 75: GPIO0 Pin 2 Handler */
    .long    GPIO0_3_Handler                    /* 76: GPIO0 Pin 3 Handler */
    .long    GPIO0_4_Handler                    /* 77: GPIO0 Pin 4 Handler */
    .long    GPIO0_5_Handler                    /* 78: GPIO0 Pin 5 Handler */
    .long    GPIO0_6_Handler                    /* 79: GPIO0 Pin 6 Handler */
    .long    GPIO0_7_Handler                    /* 80: GPIO0 Pin 7 Handler */
    .long    GPIO0_8_Handler                    /* 81: GPIO0 Pin 8 Handler */
    .long    GPIO0_9_Handler                    /* 82: GPIO0 Pin 9 Handler */
    .long    GPIO0_10_Handler                   /* 83: GPIO0 Pin 10 Handler */
    .long    GPIO0_11_Handler                   /* 84: GPIO0 Pin 11 Handler */
    .long    GPIO0_12_Handler                   /* 85: GPIO0 Pin 12 Handler */
    .long    GPIO0_13_Handler                   /* 86: GPIO0 Pin 13 Handler */
    .long    GPIO0_14_Handler                   /* 87: GPIO0 Pin 14 Handler */
    .long    GPIO0_15_Handler                   /* 88: GPIO0 Pin 15 Handler */
    .long    GPIO1_0_Handler                    /* 89: GPIO1 Pin 0 Handler */
    .long    GPIO1_1_Handler                    /* 90: GPIO1 Pin 1 Handler */
    .long    GPIO1_2_Handler                    /* 91: GPIO1 Pin 2 Handler */
    .long    GPIO1_3_Handler                    /* 92: GPIO1 Pin 3 Handler */
    .long    GPIO1_4_Handler                    /* 93: GPIO1 Pin 4 Handler */
    .long    GPIO1_5_Handler                    /* 94: GPIO1 Pin 5 Handler */
    .long    GPIO1_6_Handler                    /* 95: GPIO1 Pin 6 Handler */
    .long    GPIO1_7_Handler                    /* 96: GPIO1 Pin 7 Handler */
    .long    GPIO1_8_Handler                    /* 97: GPIO1 Pin 8 Handler */
    .long    GPIO1_9_Handler                    /* 98: GPIO1 Pin 9 Handler */
    .long    GPIO1_10_Handler                    /* 99: GPIO1 Pin 10 Handler */
    .long    GPIO1_11_Handler                    /* 100: GPIO1 Pin 11 Handler */
    .long    GPIO1_12_Handler                    /* 101: GPIO1 Pin 12 Handler */
    .long    GPIO1_13_Handler                    /* 102: GPIO1 Pin 13 Handler */
    .long    GPIO1_14_Handler                    /* 103: GPIO1 Pin 14 Handler */
    .long    GPIO1_15_Handler                    /* 104: GPIO1 Pin 15 Handler */
    .long    GPIO2_0_Handler                    /* 105: GPIO2 Pin 0 Handler */
    .long    GPIO2_1_Handler                    /* 106: GPIO2 Pin 1 Handler */
    .long    GPIO2_2_Handler                    /* 107: GPIO2 Pin 2 Handler */
    .long    GPIO2_3_Handler                    /* 108: GPIO2 Pin 3 Handler */
    .long    GPIO2_4_Handler                    /* 109: GPIO2 Pin 4 Handler */
    .long    GPIO2_5_Handler                    /* 110: GPIO2 Pin 5 Handler */
    .long    GPIO2_6_Handler                    /* 111: GPIO2 Pin 6 Handler */
    .long    GPIO2_7_Handler                    /* 112: GPIO2 Pin 7 Handler */
    .long    GPIO2_8_Handler                    /* 113: GPIO2 Pin 8 Handler */
    .long    GPIO2_9_Handler                    /* 114: GPIO2 Pin 9 Handler */
    .long    GPIO2_10_Handler                    /* 115: GPIO2 Pin 10 Handler */
    .long    GPIO2_11_Handler                    /* 116: GPIO2 Pin 11 Handler */
    .long    GPIO2_12_Handler                    /* 117: GPIO2 Pin 12 Handler */
    .long    GPIO2_13_Handler                    /* 118: GPIO2 Pin 13 Handler */
    .long    GPIO2_14_Handler                    /* 119: GPIO2 Pin 14 Handler */
    .long    GPIO2_15_Handler                    /* 120: GPIO2 Pin 15 Handler */
    .long    GPIO3_0_Handler                    /* 121: GPIO3 Pin 0 Handler */
    .long    GPIO3_1_Handler                    /* 122: GPIO3 Pin 1 Handler */
    .long    GPIO3_2_Handler                    /* 123: GPIO3 Pin 2 Handler */
    .long    GPIO3_3_Handler                    /* 124: GPIO3 Pin 3 Handler */
    .long    UARTRX5_Handler                    /* 125: UART 5 RX Interrupt */
    .long    UARTTX5_Handler                    /* 126: UART 5 TX Interrupt */
    .long    UART5_Handler                      /* 127: UART 5 combined Interrupt */
    .long    0                                  /* 128: Reserved */
    .long    0                                  /* 129: Reserved */
    .long    0                                  /* 130: Reserved */
    .long    0                                  /* 131: Reserved */
    .long    0                                  /* 132: Reserved */
    .long    0                                  /* 133: Reserved */
    .long    0                                  /* 134: Reserved */
    .long    0                                  /* 135: Reserved */
    .long    0                                  /* 136: Reserved */
    .long    0                                  /* 137: Reserved */
    .long    0                                  /* 138: Reserved */
    .long    0                                  /* 139: Reserved */
    .long    0                                  /* 140: Reserved */
    .long    0                                  /* 141: Reserved */
    .long    0                                  /* 142: Reserved */
    .long    0                                  /* 143: Reserved */
    .long    0                                  /* 144: Reserved */
    .long    0                                  /* 145: Reserved */
    .long    0                                  /* 146: Reserved */
    .long    0                                  /* 147: Reserved */
    .long    0                                  /* 148: Reserved */
    .long    0                                  /* 149: Reserved */
    .long    0                                  /* 150: Reserved */
    .long    0                                  /* 151: Reserved */
    .long    0                                  /* 152: Reserved */
    .long    0                                  /* 153: Reserved */
    .long    0                                  /* 154: Reserved */
    .long    0                                  /* 155: Reserved */
    .long    0                                  /* 156: Reserved */
    .long    0                                  /* 157: Reserved */
    .long    0                                  /* 158: Reserved */
    .long    0                                  /* 159: Reserved */
    .long    0                                  /* 160: Reserved */
    .long    0                                  /* 161: Reserved */
    .long    0                                  /* 162: Reserved */
    .long    0                                  /* 163: Reserved */
    .long    0                                  /* 164: Reserved */
    .long    0                                  /* 165: Reserved */
    .long    0                                  /* 166: Reserved */
    .long    0                                  /* 167: Reserved */
    .long    0                                  /* 168: Reserved */
    .long    0                                  /* 169: Reserved */
    .long    0                                  /* 170: Reserved */
    .long    0                                  /* 171: Reserved */
    .long    0                                  /* 172: Reserved */
    .long    0                                  /* 173: Reserved */
    .long    0                                  /* 174: Reserved */
    .long    0                                  /* 175: Reserved */
    .long    0                                  /* 176: Reserved */
    .long    0                                  /* 177: Reserved */
    .long    0                                  /* 178: Reserved */
    .long    0                                  /* 179: Reserved */
    .long    0                                  /* 180: Reserved */
    .long    0                                  /* 181: Reserved */
    .long    0                                  /* 182: Reserved */
    .long    0                                  /* 183: Reserved */
    .long    0                                  /* 184: Reserved */
    .long    0                                  /* 185: Reserved */
    .long    0                                  /* 186: Reserved */
    .long    0                                  /* 187: Reserved */
    .long    0                                  /* 188: Reserved */
    .long    0                                  /* 189: Reserved */
    .long    0                                  /* 190: Reserved */
    .long    0                                  /* 191: Reserved */
    .long    0                                  /* 192: Reserved */
    .long    0                                  /* 193: Reserved */
    .long    0                                  /* 194: Reserved */
    .long    0                                  /* 195: Reserved */
    .long    0                                  /* 196: Reserved */
    .long    0                                  /* 197: Reserved */
    .long    0                                  /* 198: Reserved */
    .long    0                                  /* 199: Reserved */
    .long    0                                  /* 200: Reserved */
    .long    0                                  /* 201: Reserved */
    .long    0                                  /* 202: Reserved */
    .long    0                                  /* 203: Reserved */
    .long    0                                  /* 204: Reserved */
    .long    0                                  /* 205: Reserved */
    .long    0                                  /* 206: Reserved */
    .long    0                                  /* 207: Reserved */
    .long    0                                  /* 208: Reserved */
    .long    0                                  /* 209: Reserved */
    .long    0                                  /* 210: Reserved */
    .long    0                                  /* 211: Reserved */
    .long    0                                  /* 212: Reserved */
    .long    0                                  /* 213: Reserved */
    .long    0                                  /* 214: Reserved */
    .long    0                                  /* 215: Reserved */
    .long    0                                  /* 216: Reserved */
    .long    0                                  /* 217: Reserved */
    .long    0                                  /* 218: Reserved */
    .long    0                                  /* 219: Reserved */
    .long    0                                  /* 220: Reserved */
    .long    0                                  /* 221: Reserved */
    .long    0                                  /* 222: Reserved */
    .long    0                                  /* 223: Reserved */
    .long    ARM_VSI0_Handler                   /* 224: FVP Virtual Stream Interface: Audio */
    .long    ARM_VSI1_Handler                   /* 224: FVP Virtual Stream Interface: Audio */
    .long    ARM_VSI2_Handler                   /* 224: FVP Virtual Stream Interface: Audio */
    .long    ARM_VSI3_Handler                   /* 224: FVP Virtual Stream Interface: Audio */
    .long    ARM_VSI4_Handler                   /* 224: FVP Virtual Stream Interface: Audio */
    .long    ARM_VSI5_Handler                   /* 224: FVP Virtual Stream Interface: Audio */
    .long    ARM_VSI6_Handler                   /* 224: FVP Virtual Stream Interface: Audio */
    .long    ARM_VSI7_Handler                   /* 224: FVP Virtual Stream Interface: Audio */


    .size    __Vectors, . - __Vectors

    .text
    .thumb
    .thumb_func
    .align    2
    .globl    Reset_Handler
    .type    Reset_Handler, %function
Reset_Handler:
/*  Firstly it copies data from read only memory to RAM. There are two schemes
 *  to copy. One can copy more than one sections. Another can only copy
 *  one section.  The former scheme needs more instructions and read-only
 *  data to implement than the latter.
 *  Macro __STARTUP_COPY_MULTIPLE is used to choose between two schemes.  */

#ifdef __STARTUP_COPY_MULTIPLE
/*  Multiple sections scheme.
 *
 *  Between symbol address __copy_table_start__ and __copy_table_end__,
 *  there are array of triplets, each of which specify:
 *    offset 0: LMA of start of a section to copy from
 *    offset 4: VMA of start of a section to copy to
 *    offset 8: size of the section to copy. Must be multiply of 4
 *
 *  All addresses must be aligned to 4 bytes boundary.
 */
    ldr    r4, =__copy_table_start__
    ldr    r5, =__copy_table_end__

.L_loop0:
    cmp    r4, r5
    bge    .L_loop0_done
    ldr    r1, [r4]
    ldr    r2, [r4, #4]
    ldr    r3, [r4, #8]

.L_loop0_0:
    subs    r3, #4
    ittt    ge
    ldrge    r0, [r1, r3]
    strge    r0, [r2, r3]
    bge    .L_loop0_0

    adds    r4, #12
    b    .L_loop0

.L_loop0_done:
#else
/*  Single section scheme.
 *
 *  The ranges of copy from/to are specified by following symbols
 *    __etext: LMA of start of the section to copy from. Usually end of text
 *    __data_start__: VMA of start of the section to copy to
 *    __data_end__: VMA of end of the section to copy to
 *
 *  All addresses must be aligned to 4 bytes boundary.
 */
    ldr    r1, =__etext
    ldr    r2, =__data_start__
    ldr    r3, =__data_end__

.L_loop1:
    cmp    r2, r3
    ittt    lt
    ldrlt    r0, [r1], #4
    strlt    r0, [r2], #4
    blt    .L_loop1
#endif /*__STARTUP_COPY_MULTIPLE */

/*  This part of work usually is done in C library startup code. Otherwise,
 *  define this macro to enable it in this startup.
 *
 *  There are two schemes too. One can clear multiple BSS sections. Another
 *  can only clear one section. The former is more size expensive than the
 *  latter.
 *
 *  Define macro __STARTUP_CLEAR_BSS_MULTIPLE to choose the former.
 *  Otherwise efine macro __STARTUP_CLEAR_BSS to choose the later.
 */
#ifdef __STARTUP_CLEAR_BSS_MULTIPLE
/*  Multiple sections scheme.
 *
 *  Between symbol address __copy_table_start__ and __copy_table_end__,
 *  there are array of tuples specifying:
 *    offset 0: Start of a BSS section
 *    offset 4: Size of this BSS section. Must be multiply of 4
 */
    ldr    r3, =__zero_table_start__
    ldr    r4, =__zero_table_end__

.L_loop2:
    cmp    r3, r4
    bge    .L_loop2_done
    ldr    r1, [r3]
    ldr    r2, [r3, #4]
    movs    r0, 0

.L_loop2_0:
    subs    r2, #4
    itt    ge
    strge    r0, [r1, r2]
    bge    .L_loop2_0

    adds    r3, #8
    b    .L_loop2
.L_loop2_done:
#elif defined (__STARTUP_CLEAR_BSS)
/*  Single BSS section scheme.
 *
 *  The BSS section is specified by following symbols
 *    __bss_start__: start of the BSS section.
 *    __bss_end__: end of the BSS section.
 *
 *  Both addresses must be aligned to 4 bytes boundary.
 */
    ldr    r1, =__bss_start__
    ldr    r2, =__bss_end__

    movs    r0, 0
.L_loop3:
    cmp    r1, r2
    itt    lt
    strlt    r0, [r1], #4
    blt    .L_loop3
#endif /* __STARTUP_CLEAR_BSS_MULTIPLE || __STARTUP_CLEAR_BSS */

#ifndef __NO_SYSTEM_INIT
    bl    SystemInit
#endif

#ifndef __START
#define __START _start
#endif
    bl    __START

    .pool
    .size    Reset_Handler, . - Reset_Handler


/*  Macro to define default handlers. */
    .macro    def_irq_handler    handler_name
    .align    1
    .thumb_func
    .weak    \handler_name
    \handler_name:
    b        \handler_name
    .endm

    def_irq_handler             NMI_Handler
    def_irq_handler             HardFault_Handler
    def_irq_handler             MemManage_Handler
    def_irq_handler             BusFault_Handler
    def_irq_handler             UsageFault_Handler
    def_irq_handler             SecureFault_Handler
    def_irq_handler             SVC_Handler
    def_irq_handler             DebugMon_Handler
    def_irq_handler             PendSV_Handler
    def_irq_handler             SysTick_Handler

    /* Core interrupts */
    def_irq_handler     NONSEC_WATCHDOG_RESET_Handler       /*   0: Non-Secure Watchdog Reset Handler */
    def_irq_handler     NONSEC_WATCHDOG_Handler             /*   1: Non-Secure Watchdog Handler */
    def_irq_handler     SLOWCLK_Timer_Handler               /*   2: SLOWCLK Timer Handler */
    def_irq_handler     TIMER0_Handler                      /*   3: TIMER 0 Handler */
    def_irq_handler     TIMER1_Handler                      /*   4: TIMER 1 Handler */
    def_irq_handler     TIMER2_Handler                      /*   5: TIMER 2 Handler */
    def_irq_handler     MPC_Handler                         /*   9: MPC Combined (Secure) Handler */
    def_irq_handler     PPC_Handler                         /*  10: PPC Combined (Secure) Handler */
    def_irq_handler     MSC_Handler                         /*  11: MSC Combined (Secure) Handler */
    def_irq_handler     BRIDGE_ERROR_Handler                /*  12: Bridge Error (Secure) Handler */
    def_irq_handler     MGMT_PPU_Handler                    /*  14: MGMT PPU Handler */
    def_irq_handler     SYS_PPU_Handler                     /*  15: SYS PPU Handler */
    def_irq_handler     CPU0_PPU_Handler                    /*  16: CPU0 PPU Handler */
    def_irq_handler     DEBUG_PPU_Handler                   /*  26: DEBUG PPU Handler */
    def_irq_handler     TIMER3_Handler                      /*  27: TIMER 3 Handler */
    def_irq_handler     CTI_REQ0_IRQHandler                 /*  28: CTI request 0 IRQ Handler */
    def_irq_handler     CTI_REQ1_IRQHandler                 /*  29: CTI request 1 IRQ Handler */

    /* External interrupts */
    def_irq_handler     System_Timestamp_Counter_Handler   /* 32: System timestamp counter Handler */
    def_irq_handler     UARTRX0_Handler                    /* 33: UART 0 RX Handler */
    def_irq_handler     UARTTX0_Handler                    /* 34: UART 0 TX Handler */
    def_irq_handler     UARTRX1_Handler                    /* 35: UART 1 RX Handler */
    def_irq_handler     UARTTX1_Handler                    /* 36: UART 1 TX Handler */
    def_irq_handler     UARTRX2_Handler                    /* 37: UART 2 RX Handler */
    def_irq_handler     UARTTX2_Handler                    /* 38: UART 2 TX Handler */
    def_irq_handler     UARTRX3_Handler                    /* 39: UART 3 RX Handler */
    def_irq_handler     UARTTX3_Handler                    /* 40: UART 3 TX Handler */
    def_irq_handler     UARTRX4_Handler                    /* 41: UART 4 RX Handler */
    def_irq_handler     UARTTX4_Handler                    /* 42: UART 4 TX Handler */
    def_irq_handler     UART0_Combined_Handler             /* 43: UART 0 Combined Handler */
    def_irq_handler     UART1_Combined_Handler             /* 44: UART 1 Combined Handler */
    def_irq_handler     UART2_Combined_Handler             /* 45: UART 2 Combined Handler */
    def_irq_handler     UART3_Combined_Handler             /* 46: UART 3 Combined Handler */
    def_irq_handler     UART4_Combined_Handler             /* 47: UART 4 Combined Handler */
    def_irq_handler     UARTOVF_Handler                    /* 48: UART 0, 1, 2, 3, 4 & 5 Overflow Handler */
    def_irq_handler     ETHERNET_Handler                   /* 49: Ethernet Handler */
    def_irq_handler     I2S_Handler                        /* 50: Audio I2S Handler */
    def_irq_handler     TOUCH_SCREEN_Handler               /* 51: Touch Screen Handler */
    def_irq_handler     USB_Handler                        /* 52: USB Handler */
    def_irq_handler     SPI_ADC_Handler                    /* 53: SPI ADC Handler */
    def_irq_handler     SPI_SHIELD0_Handler                /* 54: SPI (Shield 0) Handler */
    def_irq_handler     SPI_SHIELD1_Handler                /* 55: SPI (Shield 1) Handler */
    def_irq_handler     ETHOS_U55_Handler                  /* 56: Ethos-U55 Handler */
    def_irq_handler     GPIO0_Combined_Handler             /* 69: GPIO 0 Combined Handler */
    def_irq_handler     GPIO1_Combined_Handler             /* 70: GPIO 1 Combined Handler */
    def_irq_handler     GPIO2_Combined_Handler             /* 71: GPIO 2 Combined Handler */
    def_irq_handler     GPIO3_Combined_Handler             /* 72: GPIO 3 Combined Handler */
    def_irq_handler     GPIO0_0_Handler                    /* 73: GPIO0 Pin 0 Handler */
    def_irq_handler     GPIO0_1_Handler                    /* 74: GPIO0 Pin 1 Handler */
    def_irq_handler     GPIO0_2_Handler                    /* 75: GPIO0 Pin 2 Handler */
    def_irq_handler     GPIO0_3_Handler                    /* 76: GPIO0 Pin 3 Handler */
    def_irq_handler     GPIO0_4_Handler                    /* 77: GPIO0 Pin 4 Handler */
    def_irq_handler     GPIO0_5_Handler                    /* 78: GPIO0 Pin 5 Handler */
    def_irq_handler     GPIO0_6_Handler                    /* 79: GPIO0 Pin 6 Handler */
    def_irq_handler     GPIO0_7_Handler                    /* 80: GPIO0 Pin 7 Handler */
    def_irq_handler     GPIO0_8_Handler                    /* 81: GPIO0 Pin 8 Handler */
    def_irq_handler     GPIO0_9_Handler                    /* 82: GPIO0 Pin 9 Handler */
    def_irq_handler     GPIO0_10_Handler                   /* 83: GPIO0 Pin 10 Handler */
    def_irq_handler     GPIO0_11_Handler                   /* 84: GPIO0 Pin 11 Handler */
    def_irq_handler     GPIO0_12_Handler                   /* 85: GPIO0 Pin 12 Handler */
    def_irq_handler     GPIO0_13_Handler                   /* 86: GPIO0 Pin 13 Handler */
    def_irq_handler     GPIO0_14_Handler                   /* 87: GPIO0 Pin 14 Handler */
    def_irq_handler     GPIO0_15_Handler                   /* 88: GPIO0 Pin 15 Handler */
    def_irq_handler     GPIO1_0_Handler                    /* 89: GPIO1 Pin 0 Handler */
    def_irq_handler     GPIO1_1_Handler                    /* 90: GPIO1 Pin 1 Handler */
    def_irq_handler     GPIO1_2_Handler                    /* 91: GPIO1 Pin 2 Handler */
    def_irq_handler     GPIO1_3_Handler                    /* 92: GPIO1 Pin 3 Handler */
    def_irq_handler     GPIO1_4_Handler                    /* 93: GPIO1 Pin 4 Handler */
    def_irq_handler     GPIO1_5_Handler                    /* 94: GPIO1 Pin 5 Handler */
    def_irq_handler     GPIO1_6_Handler                    /* 95: GPIO1 Pin 6 Handler */
    def_irq_handler     GPIO1_7_Handler                    /* 96: GPIO1 Pin 7 Handler */
    def_irq_handler     GPIO1_8_Handler                    /* 97: GPIO1 Pin 8 Handler */
    def_irq_handler     GPIO1_9_Handler                    /* 98: GPIO1 Pin 9 Handler */
    def_irq_handler     GPIO1_10_Handler                    /* 99: GPIO1 Pin 10 Handler */
    def_irq_handler     GPIO1_11_Handler                    /* 100: GPIO1 Pin 11 Handler */
    def_irq_handler     GPIO1_12_Handler                    /* 101: GPIO1 Pin 12 Handler */
    def_irq_handler     GPIO1_13_Handler                    /* 102: GPIO1 Pin 13 Handler */
    def_irq_handler     GPIO1_14_Handler                    /* 103: GPIO1 Pin 14 Handler */
    def_irq_handler     GPIO1_15_Handler                    /* 104: GPIO1 Pin 15 Handler */
    def_irq_handler     GPIO2_0_Handler                    /* 105: GPIO2 Pin 0 Handler */
    def_irq_handler     GPIO2_1_Handler                    /* 106: GPIO2 Pin 1 Handler */
    def_irq_handler     GPIO2_2_Handler                    /* 107: GPIO2 Pin 2 Handler */
    def_irq_handler     GPIO2_3_Handler                    /* 108: GPIO2 Pin 3 Handler */
    def_irq_handler     GPIO2_4_Handler                    /* 109: GPIO2 Pin 4 Handler */
    def_irq_handler     GPIO2_5_Handler                    /* 110: GPIO2 Pin 5 Handler */
    def_irq_handler     GPIO2_6_Handler                    /* 111: GPIO2 Pin 6 Handler */
    def_irq_handler     GPIO2_7_Handler                    /* 112: GPIO2 Pin 7 Handler */
    def_irq_handler     GPIO2_8_Handler                    /* 113: GPIO2 Pin 8 Handler */
    def_irq_handler     GPIO2_9_Handler                    /* 114: GPIO2 Pin 9 Handler */
    def_irq_handler     GPIO2_10_Handler                    /* 115: GPIO2 Pin 10 Handler */
    def_irq_handler     GPIO2_11_Handler                    /* 116: GPIO2 Pin 11 Handler */
    def_irq_handler     GPIO2_12_Handler                    /* 117: GPIO2 Pin 12 Handler */
    def_irq_handler     GPIO2_13_Handler                    /* 118: GPIO2 Pin 13 Handler */
    def_irq_handler     GPIO2_14_Handler                    /* 119: GPIO2 Pin 14 Handler */
    def_irq_handler     GPIO2_15_Handler                    /* 120: GPIO2 Pin 15 Handler */
    def_irq_handler     GPIO3_0_Handler                    /* 121: GPIO3 Pin 0 Handler */
    def_irq_handler     GPIO3_1_Handler                    /* 122: GPIO3 Pin 1 Handler */
    def_irq_handler     GPIO3_2_Handler                    /* 123: GPIO3 Pin 2 Handler */
    def_irq_handler     GPIO3_3_Handler                    /* 124: GPIO3 Pin 3 Handler */
    def_irq_handler     UARTRX5_Handler                    /* 125: UART 5 RX Interrupt */
    def_irq_handler     UARTTX5_Handler                    /* 126: UART 5 TX Interrupt */
    def_irq_handler     UART5_Handler                      /* 127: UART 5 combined Interrupt */
    def_irq_handler     ARM_VSI0_Handler                   /* 224: FVP Virtual Stream Interface: Audio */
    def_irq_handler     ARM_VSI1_Handler                   /* 225: FVP Virtual Stream Interface: Audio */
    def_irq_handler     ARM_VSI2_Handler                   /* 226: FVP Virtual Stream Interface: Audio */
    def_irq_handler     ARM_VSI3_Handler                   /* 227: FVP Virtual Stream Interface: Audio */
    def_irq_handler     ARM_VSI4_Handler                   /* 228: FVP Virtual Stream Interface: Audio */
    def_irq_handler     ARM_VSI5_Handler                   /* 229: FVP Virtual Stream Interface: Audio */
    def_irq_handler     ARM_VSI6_Handler                   /* 230: FVP Virtual Stream Interface: Audio */
    def_irq_handler     ARM_VSI7_Handler                   /* 231: FVP Virtual Stream Interface: Audio */


    .end
