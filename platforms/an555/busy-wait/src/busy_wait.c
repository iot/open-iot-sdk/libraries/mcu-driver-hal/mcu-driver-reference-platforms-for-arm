/* Copyright (c) 2023, Arm  Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "platform/busy_wait.h"

#include "an555.h"
#include <stdint.h>

/* Function in assembly code to guarantee predictable delay depending on the
 * architecture. */
static void delay_loop(uint32_t count) __attribute__((naked, noinline));

void wait_us(uint32_t us)
{
    while (us > 1024) {
        us -= 1024;
        wait_ns(1024000);
    }
    if (us > 0) {
        wait_ns(us * 1000);
    }
}

static void delay_loop(uint32_t count)
{
    asm volatile("1: SUBS R0, R0, #1\n"
                 "NOP\n"
                 "NOP\n"
                 "BCS 1b\n"
                 "BX LR");
}

void wait_ns(uint32_t ns)
{
    uint32_t cycles_per_us = SystemCoreClock / 1000000U;

    /* Cortex-M85 takes 1.(3) cycles per iteration when Branch Prediction is enabled. */
    uint32_t loop_scaler_bp_on = 1333U;
    /* Cortex-M85 takes 16 cycles per iteration when Branch Prediction is disabled. */
    uint32_t loop_scaler_bp_off = 16000U;
    uint32_t delay_loop_scaler = SCB->CCR & SCB_CCR_BP_Msk ? loop_scaler_bp_on : loop_scaler_bp_off;

    /* Note that this very calculation, plus call overhead, will take multiple
     * cycles. Could well be 100ns on its own... So round down here, startup is
     * worth at least one loop iteration. */
    uint32_t count = (cycles_per_us * ns) / delay_loop_scaler;

    delay_loop(count);
}
