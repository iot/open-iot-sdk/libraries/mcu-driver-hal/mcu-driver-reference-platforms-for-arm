/* Copyright (c) 2023, Arm  Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef PLATFORM_ERROR_HANDLER_H
#define PLATFORM_ERROR_HANDLER_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Stop execution by entering an infinite loop
 */
void halt_on_error(const char *format, ...);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // PLATFORM_ERROR_HANDLER_H
