/* Copyright (c) 2023, Arm  Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef PLATFORM_BUSY_WAIT_H
#define PLATFORM_BUSY_WAIT_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Wait a number of microseconds.
 *
 *  @param us the whole number of microseconds to wait
 *
 *  @note
 *    This function always spins to get the exact number of microseconds.
 *    This will affect power and multithread performance. Therefore, spinning for
 *    millisecond wait is not recommended.
 *
 *  @note You may call this function from ISR context, but large delays may
 *    impact system stability - interrupt handlers should take less than
 *    50us.
 */
void wait_us(uint32_t us);

/** Wait a number of nanoseconds.
 *
 * This function spins the CPU to produce a small delay. It should normally
 * only be used for delays of 10us (10000ns) or less. As it is calculated
 * based on the expected execution time of a software loop, it may well run
 * slower than requested based on activity from other threads and interrupts.
 * If greater precision is required, this can be called from inside a critical
 * section.
 *
 *  @param ns the number of nanoseconds to wait
 *
 *  @note
 *    Any delay larger than a millisecond (1000000ns) is liable to cause
 *    overflow in the internal loop calculation. You shouldn't normally be
 *    using this for such large delays anyway in real code, but be aware if
 *    calibrating. Make repeated calls for longer test runs.
 *
 *  @note You may call this function from ISR context.
 *
 */
void wait_ns(uint32_t ns);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // PLATFORM_BUSY_WAIT_H
