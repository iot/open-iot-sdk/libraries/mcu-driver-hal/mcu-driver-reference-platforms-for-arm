/* Copyright (c) 2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CMSDK_UART_H
#define CMSDK_UART_H

#include "hal/serial_api.h"

#include "uart_cmsdk_drv.h"

typedef struct cmsdk_uart_s {
    mdh_serial_t serial;
    struct uart_cmsdk_dev_t *device;
    mdh_serial_callback_f on_event;
    void *context;
} cmsdk_uart_t;

extern const mdh_serial_vtable_t cmsdk_uart_vtable;

#endif // MPS3_UART_H
