/* Copyright (c) 2017-2023, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stddef.h>

#include "hal/serial_api.h"
#include "platform/error_handler.h"

#include "cmsdk_uart.h"

#ifndef container_of
// Returns a pointer to the `type` containing the `member` pointed by `ptr`;
#define container_of(ptr, type, member)                    \
    ({                                                     \
        const typeof(((type *)0)->member) *__mptr = (ptr); \
        (type *)((char *)__mptr - offsetof(type, member)); \
    })
#endif

static void set_baud(mdh_serial_t *self, uint32_t baudrate);
static void set_format(mdh_serial_t *self, uint8_t data_bits, mdh_serial_parity_t parity, uint8_t stop_bits);
static void set_irq_callback(mdh_serial_t *self, mdh_serial_callback_f cbk, void *context);
static void set_irq_availability(mdh_serial_t *self, mdh_serial_irq_type_t irq, bool enable);
static uint32_t get_data(mdh_serial_t *self);
static void put_data(mdh_serial_t *self, uint32_t data);
static bool is_readable(mdh_serial_t *self);
static bool is_writable(mdh_serial_t *self);
static void clear(mdh_serial_t *self);
static void set_break(mdh_serial_t *self);
static void clear_break(mdh_serial_t *self);

const mdh_serial_vtable_t cmsdk_uart_vtable = {.set_baud = set_baud,
                                               .set_format = set_format,
                                               .set_irq_callback = set_irq_callback,
                                               .set_irq_availability = set_irq_availability,
                                               .get_data = get_data,
                                               .put_data = put_data,
                                               .is_readable = is_readable,
                                               .is_writable = is_writable,
                                               .clear = clear,
                                               .set_break = set_break,
                                               .clear_break = clear_break};

static void set_baud(mdh_serial_t *self, uint32_t baudrate)
{
    cmsdk_uart_t *that = container_of(self, cmsdk_uart_t, serial);

    if (UART_CMSDK_ERR_INVALID_BAUD == uart_cmsdk_set_baudrate(that->device, baudrate)) {
        halt_on_error("baudrate is invalid");
    }
}

// The APB UART is a simple design that supports 8-bit communication
// without parity, and is fixed at one stop bit per configuration.
static void set_format(mdh_serial_t *self, uint8_t data_bits, mdh_serial_parity_t parity, uint8_t stop_bits)
{
}

static void set_irq_callback(mdh_serial_t *self, mdh_serial_callback_f cbk, void *context)
{
    cmsdk_uart_t *that = container_of(self, cmsdk_uart_t, serial);
    that->on_event = cbk;
    that->context = context;
}

static void set_irq_availability(mdh_serial_t *self, mdh_serial_irq_type_t irq, bool enable)
{
    cmsdk_uart_t *that = container_of(self, cmsdk_uart_t, serial);

    if (irq == MDH_SERIAL_IRQ_TYPE_RX) {
        if (enable) {
            (void)uart_cmsdk_irq_rx_enable(that->device);
        } else {
            uart_cmsdk_irq_rx_disable(that->device);
        }
    } else { // MDH_SERIAL_IRQ_TYPE_TX
        if (enable) {
            (void)uart_cmsdk_irq_tx_enable(that->device);
            // Ensures the on_event is called when enabled as
            // required by this API.
            that->on_event(that->context, MDH_SERIAL_IRQ_TYPE_TX);
        } else {
            uart_cmsdk_irq_tx_disable(that->device);
        }
    }
}

static bool is_readable(mdh_serial_t *self)
{
    cmsdk_uart_t *that = container_of(self, cmsdk_uart_t, serial);
    return (bool)uart_cmsdk_rx_ready(that->device);
}

static bool is_writable(mdh_serial_t *self)
{
    cmsdk_uart_t *that = container_of(self, cmsdk_uart_t, serial);
    return (bool)uart_cmsdk_tx_ready(that->device);
}

static uint32_t get_data(mdh_serial_t *self)
{
    while (!is_readable(self))
        ;

    uint8_t data = 0;
    cmsdk_uart_t *that = container_of(self, cmsdk_uart_t, serial);
    (void)uart_cmsdk_read(that->device, &data);

    return (uint32_t)data;
}

static void put_data(mdh_serial_t *self, uint32_t data)
{
    while (!is_writable(self))
        ;
    cmsdk_uart_t *that = container_of(self, cmsdk_uart_t, serial);
    (void)uart_cmsdk_write(that->device, (uint8_t)data);
}

static void clear(mdh_serial_t *self)
{
    cmsdk_uart_t *that = container_of(self, cmsdk_uart_t, serial);
    (void)uart_cmsdk_write(that->device, 0x00U);
}

// The APB UART is a simple design that does not support setting break
static void set_break(mdh_serial_t *self)
{
}

// The APB UART is a simple design that does not support clearing break
static void clear_break(mdh_serial_t *self)
{
}
