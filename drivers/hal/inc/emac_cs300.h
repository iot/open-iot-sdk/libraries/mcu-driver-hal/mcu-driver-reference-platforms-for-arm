/* Copyright (c) 2021 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EMAC_CS300_H
#define EMAC_CS300_H

#include "hal/emac_api.h"

typedef struct cs300_emac_s {
    mdh_emac_t emac;

    mdh_emac_callbacks_t *cbks;
    void *context;

    mdh_network_stack_memory_manager_t *memory_manager;
} cs300_emac_t;

mdh_emac_t *cs300_emac_get_default_instance(void);

#endif // EMAC_CS300_H
