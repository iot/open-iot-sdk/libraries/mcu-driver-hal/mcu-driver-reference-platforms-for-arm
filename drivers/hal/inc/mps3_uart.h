/* Copyright (c) 2022-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MPS3_UART_H
#define MPS3_UART_H

#include "hal/serial_api.h"
#include "PinNames.h"
#include "device_definition.h"
#include "uart_cmsdk_drv.h"

#include <stdbool.h>

bool mps3_uart_init(mdh_serial_t **serial, struct uart_cmsdk_dev_t *cmsdk_instance);
bool mps3_uart_deinit(struct uart_cmsdk_dev_t *cmsdk_instance);
PinName mps3_uart_get_tx_pin(const struct uart_cmsdk_dev_t *cmsdk_instance);
PinName mps3_uart_get_rx_pin(const struct uart_cmsdk_dev_t *cmsdk_instance);
#endif // MPS3_UART_H
