/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TICKER_H
#define TICKER_H

#if defined(TARGET_M4)
#include "ticker_mps2.h"
#else
#include "ticker_cs300.h"
#endif /* defined(TARGET_M4) */

#if DEVICE_USTICKER
#if defined(TARGET_M4)
#define bsp_us_ticker_get_default_instance() mps2_us_ticker_get_default_instance()
#define bsp_us_ticker_init                   mps2_us_ticker_init
#define bsp_us_ticker_deinit                 mps2_us_ticker_deinit
#else
#define bsp_us_ticker_get_default_instance() cs300_us_ticker_get_default_instance()
#define bsp_us_ticker_init                   cs300_us_ticker_init
#define bsp_us_ticker_deinit                 cs300_us_ticker_deinit
#endif /* defined(TARGET_M4) */
#endif /* DEVICE_USTICKER */

#if DEVICE_LPTICKER
#if defined(CORTEX_M4)
#error "LP Ticker unsupported"
#else
#define bsp_lp_ticker_get_default_instance() cs300_lp_ticker_get_default_instance()
#define bsp_lp_ticker_init                   cs300_lp_ticker_init
#define bsp_lp_ticker_deinit                 cs300_lp_ticker_deinit
#endif /* defined(TARGET_M4) */
#endif /* DEVICE_LPTICKER */

#endif // TICKER_H
