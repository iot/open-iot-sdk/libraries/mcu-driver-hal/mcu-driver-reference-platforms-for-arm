/* Copyright (c) 2021 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FVP_SAI_H
#define FVP_SAI_H

#include "hal/sai_api.h"

/*
 * FVP SAI driver is using non-secure mapped peripheral - ARM_VSI0_NS.
 * This peripheral is defined in arm_vsi.h header file.
 */

typedef struct fvp_sai_s {
    // FVP's VSI implements the sai interface.
    mdh_sai_t sai;
} fvp_sai_t;

/**
 * Initialize returns the configured instance of the requested interface.
 */
fvp_sai_t *fvp_sai_init(uint32_t channels, uint32_t sample_bits, uint32_t sample_rate, uint32_t blk_size);

/**
 * Release & de-initialize the referenced peripherals.
 * @param self This VSI instance.
 */
void fvp_sai_uninit(fvp_sai_t *self);

/**@}*/

#endif // FVP_SAI_H
