/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TICKER_MPS2_H
#define TICKER_MPS2_H

#include "hal/ticker_types.h"

typedef struct mps2_ticker_s {
    mdh_ticker_t ticker;
    const struct dualtimer_cmsdk_dev_t *dev;
} mps2_us_ticker_t;

mdh_ticker_t *mps2_us_ticker_get_default_instance(void);

void mps2_us_ticker_init(mdh_ticker_t *self);
void mps2_us_ticker_deinit(mdh_ticker_t *self);

#endif // TICKER_MPS2_H
