/* Copyright (c) 2022-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MPS3_SPI_H
#define MPS3_SPI_H

#include "hal/spi_api.h"
#include "PinNames.h"
#include "device_definition.h"
#include "spi_pl022_drv.h"

#include <stdbool.h>

typedef struct mps3_spi_s {
    mdh_spi_t spi;
} mps3_spi_t;

bool mps3_spi_init(mps3_spi_t **spi, struct spi_pl022_dev_t *spi_pl022_instance);
bool mps3_spi_deinit(struct spi_pl022_dev_t *spi_pl022_instance);
PinName mps3_spi_get_cs_pin(const struct spi_pl022_dev_t *spi_pl022_instance);
PinName mps3_spi_get_sdo_pin(const struct spi_pl022_dev_t *spi_pl022_instance);
PinName mps3_spi_get_sdi_pin(const struct spi_pl022_dev_t *spi_pl022_instance);
PinName mps3_spi_get_sck_pin(const struct spi_pl022_dev_t *spi_pl022_instance);
#endif // MPS3_SPI_H
