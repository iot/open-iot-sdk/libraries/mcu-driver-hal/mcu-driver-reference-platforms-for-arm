/* Copyright (c) 2021 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MPS3_I2S_H
#define MPS3_I2S_H

#include "hal/sai_api.h"

typedef struct mps3_i2s_s {
    // cmsdk i2s implements the sai interface.
    mdh_sai_t sai;
} mps3_i2s_t;

typedef enum mps3_i2s_dir_e { MPS3_I2S_DIR_In, MPS3_I2S_DIR_Out } mps3_i2s_dir_t;

/**
 * Initialize returns the configured instance of the requested interface.
 *
 * @param[out] tx   If successful returns an output stream.
 * @param[out] rx   If successful returns an input stream.
 * @param[in]  sample_rate Audio sample rate (typically 48KHz).
 */
void mps3_i2s_init(mps3_i2s_t **tx, mps3_i2s_t **rx, uint32_t sample_rate);

/**
 * Unlock the fifos and allow for emittion/reception.
 */
void mps3_i2s_unlock(void);

/**
 * Release & de-initialize both the input and output streams.
 */
void mps3_i2s_uninit(void);

/**@}*/

#endif // MPS3_I2S_H
