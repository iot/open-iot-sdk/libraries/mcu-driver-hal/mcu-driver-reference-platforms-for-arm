/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TICKER_CS300_H
#define TICKER_CS300_H

#include "hal/ticker_types.h"

typedef struct cs300_ticker_s {
    mdh_ticker_t ticker;
    struct systimer_armv8_m_dev_t *inst;
} cs300_us_ticker_t, cs300_lp_ticker_t;

mdh_ticker_t *cs300_us_ticker_get_default_instance(void);
void cs300_us_ticker_init(mdh_ticker_t *self);
void cs300_us_ticker_deinit(mdh_ticker_t *self);

mdh_ticker_t *cs300_lp_ticker_get_default_instance(void);
void cs300_lp_ticker_init(mdh_ticker_t *self);
void cs300_lp_ticker_deinit(mdh_ticker_t *self);

#endif // TICKER_CS300_H
