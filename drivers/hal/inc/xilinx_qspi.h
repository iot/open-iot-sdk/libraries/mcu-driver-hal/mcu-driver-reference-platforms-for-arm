/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef XILINX_QSPI_H
#define XILINX_QSPI_H

#include "hal/qspi_api.h"
#include "device_definition.h"
#include "qspi_xilinx_drv.h"

typedef struct xilinx_qspi_s {
    mdh_qspi_t qspi;
} xilinx_qspi_t;

mdh_qspi_status_t xilinx_qspi_init(xilinx_qspi_t **qspi, qspi_xilinx_dev_t *qspi_instance);
mdh_qspi_status_t xilinx_qspi_deinit(qspi_xilinx_dev_t *qspi_instance);

#endif // XILINX_QSPI_H
