/* Copyright (c) 2022-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MPS3_I2C_H
#define MPS3_I2C_H

#include "hal/i2c_api.h"
#include "PinNames.h"
#include "device_definition.h"
#include "i2c_sbcon_drv.h"

#include <stdbool.h>

typedef struct mps3_i2c_s {
    mdh_i2c_t i2c;
} mps3_i2c_t;

bool mps3_i2c_init(mps3_i2c_t **i2c, struct i2c_sbcon_dev_t *i2c_sbcon_instance);
bool mps3_i2c_deinit(struct i2c_sbcon_dev_t *i2c_sbcon_instance);
PinName mps3_i2c_get_sda_pin(const struct i2c_sbcon_dev_t *i2c_sbcon_instance);
PinName mps3_i2c_get_scl_pin(const struct i2c_sbcon_dev_t *i2c_sbcon_instance);

#endif // MPS3_I2C_H
