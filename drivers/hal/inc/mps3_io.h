/* Copyright (c) 2022-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MPS3_IO_H
#define MPS3_IO_H

#include "hal/gpio_api.h"
#include "PinNames.h"
#include "device_definition.h"

#include <stdbool.h>
#include <stdint.h>

typedef struct mps3_io_s {
    mdh_gpio_t gpio;
    PinName pin;
} mps3_io_t;

bool mps3_io_init(mps3_io_t *io, void *io_device, PinName pin);
/* Return the correct mask of the given PIN */
uint32_t mps3_io_set_pin_as_io(PinName pin);
#endif // MPS3_IO_H
