/* Copyright (c) 2021 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * TODO: implements double buffering and overrun events
 */

#include "fvp_sai.h"

#include "hal/sai_api.h"
#include "arm_vsi.h"
#include CMSIS_device_header

static mdh_sai_status_t fvp_vsi_mdh_sai_set_transfer_complete_callback(mdh_sai_t *self,
                                                                       mdh_sai_transfer_complete_f cbk);
static mdh_sai_status_t fvp_vsi_mdh_sai_set_event_callback(mdh_sai_t *self, mdh_sai_event_f cbk);
static mdh_sai_status_t fvp_vsi_mdh_sai_transfer(mdh_sai_t *self, uint8_t *samples, uint32_t len, void *ctx);
static mdh_sai_status_t fvp_vsi_mdh_sai_cancel_transfer(mdh_sai_t *self);

struct private_fvp_sai_s {
    fvp_sai_t vsi;

    mdh_sai_transfer_complete_f on_xfer_complete;
    /*mdh_sai_event_f on_event;*/
    void *ctx;

    uint32_t blk_size;
};
// ================================================================================================
// Private variables & constants
static const uint32_t REGS_CONTROL = 0;
static const uint32_t REGS_CHANNELS = 1;
static const uint32_t REGS_SAMPLE_BITS = 2;
static const uint32_t REGS_SAMPLE_RATE = 3;

static const mdh_sai_vtable_t gcs_mdh_sai_vtable = {.set_transfer_complete_callback =
                                                        fvp_vsi_mdh_sai_set_transfer_complete_callback,
                                                    .set_event_callback = fvp_vsi_mdh_sai_set_event_callback,
                                                    .transfer = fvp_vsi_mdh_sai_transfer,
                                                    .cancel_transfer = fvp_vsi_mdh_sai_cancel_transfer};

static struct private_fvp_sai_s gs_vsi_rx = {.vsi.sai.vtable = &gcs_mdh_sai_vtable,
                                             .on_xfer_complete = NULL,
                                             /*.on_event = NULL,*/
                                             .ctx = NULL,
                                             .blk_size = 0};

// ================================================================================================
// Private methods
static mdh_sai_status_t fvp_vsi_mdh_sai_set_transfer_complete_callback(mdh_sai_t *self, mdh_sai_transfer_complete_f cbk)
{
    if (ARM_VSI0_NS->Regs[REGS_CONTROL] == 1u) {
        return MDH_SAI_STATUS_BUSY;
    }

    gs_vsi_rx.on_xfer_complete = cbk;
    return MDH_SAI_STATUS_NO_ERROR;
}

static mdh_sai_status_t fvp_vsi_mdh_sai_set_event_callback(mdh_sai_t *self, mdh_sai_event_f cbk)
{
    if (ARM_VSI0_NS->Regs[REGS_CONTROL] == 1u) {
        return MDH_SAI_STATUS_BUSY;
    }

    /*gs_vsi_rx.on_event = cbk;*/
    return MDH_SAI_STATUS_NO_ERROR;
}

static mdh_sai_status_t fvp_vsi_mdh_sai_transfer(mdh_sai_t *self, uint8_t *samples, uint32_t len, void *ctx)
{
    if (ARM_VSI0_NS->Regs[REGS_CONTROL] == 1) {
        return MDH_SAI_STATUS_BUSY;
    }

    if (len > 0) {
        ARM_VSI0_NS->DMA.Address = (uint32_t)samples;
        ARM_VSI0_NS->DMA.BlockNum = len;
        ARM_VSI0_NS->DMA.BlockSize = gs_vsi_rx.blk_size;

        uint32_t sample_rate = ARM_VSI0_NS->Regs[REGS_SAMPLE_RATE];
        uint32_t sample_size = ARM_VSI0_NS->Regs[REGS_CHANNELS] * ((ARM_VSI0_NS->Regs[REGS_SAMPLE_BITS] + 7U) / 8U);

        ARM_VSI0_NS->Timer.Interval = (1000000U * (gs_vsi_rx.blk_size / sample_size)) / sample_rate;

        ARM_VSI0_NS->DMA.Control = ARM_VSI_DMA_Direction_P2M | ARM_VSI_DMA_Enable_Msk;
        ARM_VSI0_NS->Regs[REGS_CONTROL] = 1; // enable
        ARM_VSI0_NS->Timer.Control = ARM_VSI_Timer_Trig_DMA_Msk | ARM_VSI_Timer_Trig_IRQ_Msk
                                     | ARM_VSI_Timer_Periodic_Msk | ARM_VSI_Timer_Run_Msk;
    } else {
        gs_vsi_rx.on_xfer_complete(self, ctx, MDH_SAI_TRANSFER_COMPLETE_DONE);
    }
    return MDH_SAI_STATUS_NO_ERROR;
}

static mdh_sai_status_t fvp_vsi_mdh_sai_cancel_transfer(mdh_sai_t *self)
{
    // transfers cannot be cancelled
    // failure do provide a read buffer will lead to an overrun event
    return MDH_SAI_STATUS_BUSY;
}

/* Interrupt Handler */
void ARM_VSI0_Handler(void)
{
    ARM_VSI0_NS->IRQ.Clear = 0x00000001U;

    __ISB();
    __DSB();

    // the ref impl has a 200iterations long __NOP loop here.
    mdh_sai_transfer_complete_f f = gs_vsi_rx.on_xfer_complete;
    if (NULL != f) {
        f(&gs_vsi_rx.vsi.sai, gs_vsi_rx.ctx, MDH_SAI_TRANSFER_COMPLETE_DONE);
    }
}

// ================================================================================================
// Public methods

/**
 * Initialize returns the configured instance of the requested interface.
 */
fvp_sai_t *fvp_sai_init(uint32_t channels, uint32_t sample_bits, uint32_t sample_rate, uint32_t block_size)
{
    if ((channels < 1U) || (channels > 32U) || (sample_bits < 8U) || (sample_bits > 32U) || (sample_rate == 0U)) {
        return NULL;
    }

    ARM_VSI0_NS->Timer.Control = 0U;
    ARM_VSI0_NS->DMA.Control = 0U;
    ARM_VSI0_NS->IRQ.Clear = 0x00000001U;
    ARM_VSI0_NS->IRQ.Enable = 0x00000001U;

    NVIC_EnableIRQ(ARM_VSI0_IRQn);
    NVIC_EnableIRQ(ARM_VSI1_IRQn);
    NVIC_EnableIRQ(ARM_VSI2_IRQn);
    NVIC_EnableIRQ(ARM_VSI3_IRQn);
    NVIC_EnableIRQ(ARM_VSI4_IRQn);
    NVIC_EnableIRQ(ARM_VSI5_IRQn);
    NVIC_EnableIRQ(ARM_VSI6_IRQn);
    NVIC_EnableIRQ(ARM_VSI7_IRQn);

    ARM_VSI0_NS->Regs[REGS_CHANNELS] = channels;
    ARM_VSI0_NS->Regs[REGS_SAMPLE_RATE] = sample_rate;
    ARM_VSI0_NS->Regs[REGS_SAMPLE_BITS] = sample_bits;

    gs_vsi_rx.blk_size = block_size;

    return &gs_vsi_rx.vsi;
}

/**
 * Release & de-initialize the referenced peripherals.
 * @param self This VSI instance.
 */
void fvp_sai_uninit(fvp_sai_t *self)
{
    NVIC_DisableIRQ(ARM_VSI0_IRQn);

    ARM_VSI0_NS->Timer.Control = 0;
    ARM_VSI0_NS->DMA.Control = 0;
    ARM_VSI0_NS->IRQ.Clear = 0x00000001U;
    ARM_VSI0_NS->IRQ.Enable = 0x00000000U;
    ARM_VSI0_NS->Regs[REGS_CONTROL] = 0;

    gs_vsi_rx.on_xfer_complete = NULL;
    gs_vsi_rx.ctx = NULL;
}
