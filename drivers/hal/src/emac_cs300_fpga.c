/* Copyright (c) 2021 - 2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal/emac_api.h"
#include "hal/network_stack_memory_manager.h"
#include "hal/us_ticker_api.h"
#include CMSIS_device_header
#include "emac_cs300.h"
#include "hal-toolbox/us_ticker_util.h"
#include "platform_base_address.h"
#include "smsc9220_eth_drv.h"
#include "ticker.h"

#include <string.h>

#define EMAC_LAN9220_INTERFACE_NAME   "lan9220"
#define EMAC_LAN9220_MAC_ADDRESS_SIZE 6U
#define EMAC_LAN9220_MEMORY_ALIGNMENT 4U
#define EMAC_LAN9220_MTU              1500U

static void wait_ms(uint32_t ms);
static uint32_t microchip_crc32_compute(const uint8_t *src, size_t len);
static size_t get_lan9220_mtu(const mdh_emac_t *const self);
static size_t get_lan9220_data_alignment_size(const mdh_emac_t *const self);
static size_t get_lan9220_interface_name(const mdh_emac_t *const self, char *name, size_t name_size);
static size_t get_lan9220_mac_addr_size(const mdh_emac_t *const self);
static mdh_emac_status_t get_lan9220_mac_addr(const mdh_emac_t *const self, uint8_t *addr);
static mdh_emac_status_t set_lan9220_mac_addr(mdh_emac_t *const self, const uint8_t *addr);
static mdh_emac_status_t power_up_lan9220(const mdh_emac_t *self,
                                          mdh_emac_callbacks_t *cbks,
                                          mdh_network_stack_memory_manager_t *memory_manager,
                                          void *ctx);
static mdh_emac_status_t power_down(mdh_emac_t *const self);
static mdh_emac_status_t transmit_lan9220(mdh_emac_t *const self, const mdh_network_stack_buffer_t *buf);
static mdh_emac_status_t receive_lan9220(mdh_emac_t *const self, const mdh_network_stack_buffer_t *buf);
static mdh_emac_status_t add_to_multicast_group_smsc9220(mdh_emac_t *const self, const uint8_t *addr);
static mdh_emac_status_t remove_from_multicast_group_smsc9220(mdh_emac_t *const self, const uint8_t *addr);
static mdh_emac_status_t config_multicast_reception_smsc9220(mdh_emac_t *const self, bool receive_all);

static const mdh_emac_vtable_t gsc_vtable = {.power_up = power_up_lan9220,
                                             .power_down = power_down,

                                             .get_mtu = get_lan9220_mtu,
                                             .get_align = get_lan9220_data_alignment_size,
                                             .get_interface_name = get_lan9220_interface_name,
                                             .get_mac_addr_size = get_lan9220_mac_addr_size,
                                             .get_mac_addr = get_lan9220_mac_addr,
                                             .set_mac_addr = set_lan9220_mac_addr,

                                             .add_to_multicast_group = add_to_multicast_group_smsc9220,
                                             .remove_from_multicast_group = remove_from_multicast_group_smsc9220,
                                             .config_multicast_reception = config_multicast_reception_smsc9220,

                                             .transmit = transmit_lan9220,
                                             .receive = receive_lan9220};
static const struct smsc9220_eth_dev_cfg_t gsc_eth_device_config = {.base = ETHERNET_BASE_NS, .wait_ms = wait_ms};
static struct smsc9220_eth_dev_data_t gsc_eth_device_data = {.crc32 = microchip_crc32_compute};
static struct smsc9220_eth_dev_t gs_eth_device = {.cfg = &gsc_eth_device_config, .data = &gsc_eth_device_data};
static cs300_emac_t gs_emac_instance = {.emac.vtable = &gsc_vtable, .cbks = NULL, .memory_manager = NULL};

// Blocking wait
static void wait_ms(uint32_t ms)
{
    us_ticker_util_wait(bsp_us_ticker_get_default_instance(), ms * 1000U);
}

// This is not a standard way of calculating CRC32 but appears to be what is
// implemented on Microchip Ethernet controller devices
static uint32_t microchip_crc32_compute(const uint8_t *src, size_t len)
{
    uint32_t crc = 0xFFFFFFFFUL;

    for (size_t byte_index = 0; byte_index < len; byte_index++) {
        uint8_t temp = *src++;
        for (size_t bit_index = 0; bit_index < 8; bit_index++) {
            uint8_t carry = ((crc & 0x80000000) ? 1 : 0) ^ (temp & 0x01);
            crc <<= 1;
            if (carry) {
                crc = (crc ^ 0x04c11db6) | carry;
            }

            temp >>= 1;
        }
    }

    return crc;
}

static mdh_emac_status_t power_up_lan9220(const mdh_emac_t *self,
                                          mdh_emac_callbacks_t *cbks,
                                          mdh_network_stack_memory_manager_t *memory_manager,
                                          void *ctx)
{
    if ((NULL == self) || (NULL == cbks) || (NULL == cbks->rx) || (NULL == cbks->tx) || (NULL == memory_manager)) {
        return MDH_EMAC_STATUS_INVALID_ARGS;
    }

    gs_emac_instance.cbks = cbks;
    gs_emac_instance.memory_manager = memory_manager;
    gs_emac_instance.context = ctx;

    bsp_us_ticker_init(bsp_us_ticker_get_default_instance());
    mdh_emac_status_t status = (SMSC9220_ERROR_NONE == smsc9220_init(&gs_eth_device)) ? MDH_EMAC_STATUS_NO_ERROR
                                                                                      : MDH_EMAC_STATUS_INTERNAL_ERROR;

    if (status == MDH_EMAC_STATUS_NO_ERROR) {
        // Init FIFO level interrupts: use Rx status level irq to trigger
        // interrupts for any non-processed packets, while Tx is not irq driven
        smsc9220_set_fifo_level_irq(
            &gs_eth_device, SMSC9220_FIFO_LEVEL_IRQ_RX_STATUS_POS, SMSC9220_FIFO_LEVEL_IRQ_LEVEL_MIN);
        smsc9220_set_fifo_level_irq(
            &gs_eth_device, SMSC9220_FIFO_LEVEL_IRQ_TX_STATUS_POS, SMSC9220_FIFO_LEVEL_IRQ_LEVEL_MIN);
        smsc9220_set_fifo_level_irq(
            &gs_eth_device, SMSC9220_FIFO_LEVEL_IRQ_TX_DATA_POS, SMSC9220_FIFO_LEVEL_IRQ_LEVEL_MAX);

        NVIC_ClearPendingIRQ(ETHERNET_IRQn);
        NVIC_EnableIRQ(ETHERNET_IRQn);

        smsc9220_enable_interrupt(&gs_eth_device, SMSC9220_INTERRUPT_RX_STATUS_FIFO_LEVEL);
    }

    return status;
}

static mdh_emac_status_t power_down(mdh_emac_t *const self)
{
    NVIC_DisableIRQ(ETHERNET_IRQn);

    return MDH_EMAC_STATUS_NO_ERROR;
}

static size_t get_lan9220_mtu(const mdh_emac_t *const self)
{
    return EMAC_LAN9220_MTU;
}

static size_t get_lan9220_data_alignment_size(const mdh_emac_t *const self)
{
    return EMAC_LAN9220_MEMORY_ALIGNMENT;
}

static size_t get_lan9220_interface_name(const mdh_emac_t *const self, char *name, size_t name_size)
{
    size_t retrieved_size = 0U;

    if (name_size < 2U) {
        return retrieved_size;
    }

    if (sizeof(EMAC_LAN9220_INTERFACE_NAME) > name_size) {
        // Ensures cropped name is null terminated
        memcpy(name, EMAC_LAN9220_INTERFACE_NAME, name_size - 2U);
        name[name_size - 1U] = '\0';

        retrieved_size = name_size;
    } else {
        memcpy(name, EMAC_LAN9220_INTERFACE_NAME, sizeof(EMAC_LAN9220_INTERFACE_NAME));

        retrieved_size = sizeof(EMAC_LAN9220_INTERFACE_NAME);
    }

    return retrieved_size;
}

static size_t get_lan9220_mac_addr_size(const mdh_emac_t *const self)
{
    return EMAC_LAN9220_MAC_ADDRESS_SIZE;
}

static mdh_emac_status_t get_lan9220_mac_addr(const mdh_emac_t *const self, uint8_t *addr)
{
    if (SMSC9220_ERROR_NONE == smsc9220_read_mac_address(&gs_eth_device, (char *)addr)) {
        return MDH_EMAC_STATUS_NO_ERROR;
    } else {
        return MDH_EMAC_STATUS_INTERNAL_ERROR;
    }
}

static mdh_emac_status_t set_lan9220_mac_addr(mdh_emac_t *const self, const uint8_t *addr)
{
    if (!addr) {
        return MDH_EMAC_STATUS_INTERNAL_ERROR;
    }

    uint32_t mac_low = 0U;
    uint32_t mac_high = 0U;

    // Using local variables to make sure the right alignment is used
    memcpy((void *)&mac_low, (void *)addr, 4U);
    memcpy((void *)&mac_high, (void *)(addr + 4U), 2U);

    if ((SMSC9220_ERROR_NONE != smsc9220_mac_regwrite(&gs_eth_device, SMSC9220_MAC_REG_OFFSET_ADDRL, mac_low))
        || (SMSC9220_ERROR_NONE != smsc9220_mac_regwrite(&gs_eth_device, SMSC9220_MAC_REG_OFFSET_ADDRH, mac_high))) {
        return MDH_EMAC_STATUS_INTERNAL_ERROR;
    }

    return MDH_EMAC_STATUS_NO_ERROR;
}

static mdh_emac_status_t transmit_lan9220(mdh_emac_t *const self, const mdh_network_stack_buffer_t *buf)
{
    uint8_t *buffer_data =
        (uint8_t *)mdh_network_stack_memory_manager_get_payload(gs_emac_instance.memory_manager, buf);

    size_t buf_payload_len = mdh_network_stack_memory_manager_get_payload_len(gs_emac_instance.memory_manager, buf);

    mdh_emac_status_t status =
        (SMSC9220_ERROR_NONE
         == smsc9220_send_by_chunks(&gs_eth_device, buf_payload_len, true, (const char *)buffer_data, buf_payload_len))
            ? MDH_EMAC_STATUS_NO_ERROR
            : MDH_EMAC_STATUS_INTERNAL_ERROR;

    gs_emac_instance.cbks->tx(self,
                              gs_emac_instance.context,
                              (status == MDH_EMAC_STATUS_NO_ERROR) ? MDH_EMAC_TRANSFER_DONE : MDH_EMAC_TRANSFER_ERROR,
                              buf);

    return status;
}

static mdh_emac_status_t receive_lan9220(mdh_emac_t *const self, const mdh_network_stack_buffer_t *buf)
{
    if (0U == smsc9220_peek_next_packet_size(&gs_eth_device)) {
        return MDH_EMAC_STATUS_INTERNAL_ERROR;
    }

    uint8_t *buffer_data =
        (uint8_t *)mdh_network_stack_memory_manager_get_payload(gs_emac_instance.memory_manager, buf);

    size_t buf_payload_len = mdh_network_stack_memory_manager_get_payload_len(gs_emac_instance.memory_manager, buf);
    size_t frame_size = smsc9220_peek_next_packet_size(&gs_eth_device);

    if (frame_size > buf_payload_len) {
        return MDH_EMAC_STATUS_INTERNAL_ERROR;
    }

    mdh_emac_status_t status =
        (SMSC9220_ERROR_NONE == smsc9220_get_received_packet(&gs_eth_device, (char *)buffer_data, frame_size))
            ? MDH_EMAC_STATUS_NO_ERROR
            : MDH_EMAC_STATUS_INTERNAL_ERROR;

    mdh_network_stack_memory_manager_set_payload_len(gs_emac_instance.memory_manager, buf, frame_size);

    smsc9220_clear_interrupt(&gs_eth_device, SMSC9220_INTERRUPT_RX_STATUS_FIFO_LEVEL);
    smsc9220_enable_interrupt(&gs_eth_device, SMSC9220_INTERRUPT_RX_STATUS_FIFO_LEVEL);

    return status;
}

static mdh_emac_status_t add_to_multicast_group_smsc9220(mdh_emac_t *const self, const uint8_t *addr)
{
    mdh_emac_status_t status = (SMSC9220_ERROR_NONE == smsc9220_add_multicast_group(&gs_eth_device, addr))
                                   ? MDH_EMAC_STATUS_NO_ERROR
                                   : MDH_EMAC_STATUS_INTERNAL_ERROR;

    return status;
}

static mdh_emac_status_t remove_from_multicast_group_smsc9220(mdh_emac_t *const self, const uint8_t *addr)
{
    mdh_emac_status_t status = (SMSC9220_ERROR_NONE == smsc9220_remove_multicast_group(&gs_eth_device, addr))
                                   ? MDH_EMAC_STATUS_NO_ERROR
                                   : MDH_EMAC_STATUS_INTERNAL_ERROR;

    return status;
}

static mdh_emac_status_t config_multicast_reception_smsc9220(mdh_emac_t *const self, bool receive_all)
{
    if (receive_all) {
        smsc9220_accept_all_multicast_enable(&gs_eth_device);
    } else {
        smsc9220_accept_all_multicast_disable(&gs_eth_device);
    }

    return MDH_EMAC_STATUS_NO_ERROR;
}

mdh_emac_t *cs300_emac_get_default_instance(void)
{
    return &(gs_emac_instance.emac);
}

void ETHERNET_Handler(void)
{
    NVIC_ClearPendingIRQ(ETHERNET_IRQn);

    if (smsc9220_get_interrupt(&gs_eth_device, SMSC9220_INTERRUPT_RX_STATUS_FIFO_LEVEL)) {
        smsc9220_clear_interrupt(&gs_eth_device, SMSC9220_INTERRUPT_RX_STATUS_FIFO_LEVEL);
        smsc9220_disable_interrupt(&gs_eth_device, SMSC9220_INTERRUPT_RX_STATUS_FIFO_LEVEL);

        gs_emac_instance.cbks->rx(&(gs_emac_instance.emac), gs_emac_instance.context, MDH_EMAC_RECEIVE_DONE);
    }
}
