/* Copyright (c) 2021-2023, Arm Limited. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mps3_spi.h"

#include "hal/spi_api.h"
#include "PeripheralNames.h"
#include "PinNames.h"
#include "pinmap.h"
#include "platform/error_handler.h"
#include "spi_pl022_drv.h"

#ifndef container_of
// Returns a pointer to the `type` containing the `member` pointed by `ptr`;
#define container_of(ptr, type, member)                    \
    ({                                                     \
        const typeof(((type *)0)->member) *__mptr = (ptr); \
        (type *)((char *)__mptr - offsetof(type, member)); \
    })
#endif

// Expressed in number of bits
#define MIN_WORD_SIZE 4U
#define MAX_WORD_SIZE 16U

// If the bit is set then the phase is high as it indicates that it is
// either mode 1 (0x0000_0001) or 3 (0x0000_0011)
#define SPI_MODE_PHASE_BIT     0U
#define SPI_MODE_PHASE_BIT_MSK (0x1UL << SPI_MODE_PHASE_BIT)

// If the bit is set then the polarity is high as it indicates that it is
// either mode 2 (0x0000_0010) or 3 (0x0000_0011) which are not supported
#define SPI_MODE_POLARITY_BIT     1
#define SPI_MODE_POLARITY_BIT_MSK (0x1UL << SPI_MODE_POLARITY_BIT)

#define SPI_MODE_MAX_VALUE_MSK ((0x1UL << (SPI_MODE_POLARITY_BIT + 1U)) - 1U)

typedef struct priv_mps3_spi_t {
    mps3_spi_t mps3_spi;
    struct spi_pl022_dev_t *device;
    PinName cs;
    PinName sdo;
    PinName sdi;
    PinName sck;
} priv_mps3_spi_t;

static void get_capabilities(mdh_spi_t *self, bool is_peripheral, mdh_spi_capabilities_t *capabilities);
static void set_format(mdh_spi_t *self, uint8_t word_size, mdh_spi_mode_t mode, bool is_peripheral);
static void set_frequency(mdh_spi_t *self, uint32_t hz);
static uint32_t controller_write(mdh_spi_t *self, uint32_t write_data);
static uint32_t controller_block_read_write(mdh_spi_t *self,
                                            const uint8_t *write_buffer,
                                            size_t write_size,
                                            uint8_t *read_buffer,
                                            size_t read_size,
                                            uint8_t write_fill);
static bool is_peripheral_data_available(mdh_spi_t *self);
static uint32_t peripheral_read(mdh_spi_t *self);
static void peripheral_write(mdh_spi_t *self, uint32_t data);
static bool is_busy(mdh_spi_t *self);
static priv_mps3_spi_t *get_priv_mps3_spi_instance(const struct spi_pl022_dev_t *spi_pl022_instance);

static const mdh_spi_vtable_t gsc_vtable = {.get_capabilities = get_capabilities,
                                            .set_format = set_format,
                                            .set_frequency = set_frequency,
                                            .controller_write = controller_write,
                                            .controller_block_read_write = controller_block_read_write,
                                            .is_peripheral_data_available = is_peripheral_data_available,
                                            .peripheral_read = peripheral_read,
                                            .peripheral_write = peripheral_write,
                                            .is_busy = is_busy};

#ifdef SPI_ADC_PL022_NS
static priv_mps3_spi_t gs_priv_spi_adc = {.mps3_spi.spi.vfptr = &gsc_vtable,
                                          .device = &SPI_ADC_PL022_DEV,
                                          .cs = SPI_ADC_CS,
                                          .sdo = SPI_ADC_SDO,
                                          .sdi = SPI_ADC_SDI,
                                          .sck = SPI_ADC_SCK};
#endif
#ifdef SPI_SHIELD0_PL022_NS
static priv_mps3_spi_t gs_priv_spi_shield0 = {.mps3_spi.spi.vfptr = &gsc_vtable,
                                              .device = &SPI_SHIELD0_PL022_DEV,
                                              .cs = D0_10,
                                              .sdo = D0_11,
                                              .sdi = D0_12,
                                              .sck = D0_13};
#endif
#ifdef SPI_SHIELD1_PL022_NS
static priv_mps3_spi_t gs_priv_spi_shield1 = {.mps3_spi.spi.vfptr = &gsc_vtable,
                                              .device = &SPI_SHIELD1_PL022_DEV,
                                              .cs = D1_10,
                                              .sdo = D1_11,
                                              .sdi = D1_12,
                                              .sck = D1_13};
#endif

bool mps3_spi_init(mps3_spi_t **spi, struct spi_pl022_dev_t *spi_pl022_instance)
{
    priv_mps3_spi_t *priv_spi = get_priv_mps3_spi_instance(spi_pl022_instance);

    if (NULL == priv_spi) {
        return false;
    }

    *spi = &(priv_spi->mps3_spi);

    // CS, SDO and SDI pins can be unconnected, therefore set the function only
    // if they are connected.
    pin_set_function(priv_spi->cs, ALTERNATE_FUNC);
    pin_set_function(priv_spi->sdo, ALTERNATE_FUNC);
    pin_set_function(priv_spi->sdi, ALTERNATE_FUNC);

    // SCK must always be connected.
    pin_set_function(priv_spi->sck, ALTERNATE_FUNC);

    return (SPI_PL022_ERR_NONE == spi_pl022_init(priv_spi->device, SystemCoreClock));
}

bool mps3_spi_deinit(struct spi_pl022_dev_t *spi_pl022_instance)
{
    return true;
}

PinName mps3_spi_get_cs_pin(const struct spi_pl022_dev_t *spi_pl022_instance)
{
#ifdef SPI_ADC_PL022_NS
    if (spi_pl022_instance == &SPI_ADC_PL022_DEV) {
        return gs_priv_spi_adc.cs;
    }
#endif
#ifdef SPI_SHIELD0_PL022_NS
    if (spi_pl022_instance == &SPI_SHIELD0_PL022_DEV) {
        return gs_priv_spi_shield0.cs;
    }
#endif
#ifdef SPI_SHIELD1_PL022_NS
    if (spi_pl022_instance == &SPI_SHIELD1_PL022_DEV) {
        return gs_priv_spi_shield1.cs;
    }
#endif

    return NC;
}

PinName mps3_spi_get_sdo_pin(const struct spi_pl022_dev_t *spi_pl022_instance)
{
#ifdef SPI_ADC_PL022_NS
    if (spi_pl022_instance == &SPI_ADC_PL022_DEV) {
        return gs_priv_spi_adc.sdo;
    }
#endif
#ifdef SPI_SHIELD0_PL022_NS
    if (spi_pl022_instance == &SPI_SHIELD0_PL022_DEV) {
        return gs_priv_spi_shield0.sdo;
    }
#endif
#ifdef SPI_SHIELD1_PL022_NS
    if (spi_pl022_instance == &SPI_SHIELD1_PL022_DEV) {
        return gs_priv_spi_shield1.sdo;
    }
#endif

    return NC;
}

PinName mps3_spi_get_sdi_pin(const struct spi_pl022_dev_t *spi_pl022_instance)
{
#ifdef SPI_ADC_PL022_NS
    if (spi_pl022_instance == &SPI_ADC_PL022_DEV) {
        return gs_priv_spi_adc.sdi;
    }
#endif
#ifdef SPI_SHIELD0_PL022_NS
    if (spi_pl022_instance == &SPI_SHIELD0_PL022_DEV) {
        return gs_priv_spi_shield0.sdi;
    }
#endif
#ifdef SPI_SHIELD1_PL022_NS
    if (spi_pl022_instance == &SPI_SHIELD1_PL022_DEV) {
        return gs_priv_spi_shield1.sdi;
    }
#endif

    return NC;
}

PinName mps3_spi_get_sck_pin(const struct spi_pl022_dev_t *spi_pl022_instance)
{
#ifdef SPI_ADC_PL022_NS
    if (spi_pl022_instance == &SPI_ADC_PL022_DEV) {
        return gs_priv_spi_adc.sck;
    }
#endif
#ifdef SPI_SHIELD0_PL022_NS
    if (spi_pl022_instance == &SPI_SHIELD0_PL022_DEV) {
        return gs_priv_spi_shield0.sck;
    }
#endif
#ifdef SPI_SHIELD1_PL022_NS
    if (spi_pl022_instance == &SPI_SHIELD1_PL022_DEV) {
        return gs_priv_spi_shield1.sck;
    }
#endif

    return NC;
}

static void set_format(mdh_spi_t *self, uint8_t word_size, mdh_spi_mode_t mode, bool is_peripheral)
{
    if (!((MIN_WORD_SIZE <= word_size) && (word_size <= MAX_WORD_SIZE)) || (mode & ~SPI_MODE_MAX_VALUE_MSK)) {
        halt_on_error("SPI format error");
        return;
    }

    priv_mps3_spi_t *that = container_of(self, priv_mps3_spi_t, mps3_spi.spi);

    spi_pl022_dev_disable(that->device);

    struct spi_pl022_ctrl_cfg_t ctrl_cfg;
    if (SPI_PL022_ERR_NONE != spi_pl022_get_ctrl_cfg(that->device, &ctrl_cfg)) {
        halt_on_error("SPI not initialized");
        return;
    };

    uint32_t polarity = ((uint8_t)mode & SPI_MODE_POLARITY_BIT_MSK) ? 1U : 0U;
    uint32_t phase = ((uint8_t)mode & SPI_MODE_PHASE_BIT_MSK) ? 1U : 0U;
    uint32_t frame_format = ((SPI_PL022_CFG_FRF_MOT << 0U) | (polarity << 6U) | (phase << 7U));

    ctrl_cfg.frame_format = (uint8_t)frame_format;
    ctrl_cfg.word_size = word_size;
    ctrl_cfg.spi_mode = is_peripheral ? SPI_PL022_SLAVE_SELECT : SPI_PL022_MASTER_SELECT;

    if (SPI_PL022_ERR_NONE != spi_pl022_set_ctrl_cfg(that->device, &ctrl_cfg)) {
        halt_on_error("SPI configuration failed");
    }

    spi_pl022_dev_enable(that->device);
}

static void set_frequency(mdh_spi_t *self, uint32_t hz)
{
    priv_mps3_spi_t *that = container_of(self, priv_mps3_spi_t, mps3_spi.spi);

    spi_pl022_dev_disable(that->device);

    that->device->data->ctrl_cfg.bit_rate = hz;

    if (SPI_PL022_ERR_NONE != spi_pl022_set_sys_clk(that->device, PERIPHERAL_CLOCK_HZ)) {
        halt_on_error("SPI frequency config failed");
    }

    spi_pl022_dev_enable(that->device);
}

static uint32_t controller_write(mdh_spi_t *self, uint32_t write_data)
{
    uint32_t write_size = 1U;
    priv_mps3_spi_t *that = container_of(self, priv_mps3_spi_t, mps3_spi.spi);
    if (that->device->data->ctrl_cfg.word_size > 8U) {
        write_size = 2U;
    }

    uint32_t read_data = 0U;
    uint32_t read_size = write_size;

    if (SPI_PL022_ERR_NONE != spi_pl022_txrx_blocking(that->device, &write_data, &write_size, &read_data, &read_size)) {
        return 0U;
    }

    return read_data;
}

static uint32_t controller_block_read_write(mdh_spi_t *self,
                                            const uint8_t *write_buffer,
                                            size_t write_size,
                                            uint8_t *read_buffer,
                                            size_t read_size,
                                            uint8_t write_fill)
{
    priv_mps3_spi_t *that = container_of(self, priv_mps3_spi_t, mps3_spi.spi);
    if (SPI_PL022_ERR_NONE
        != spi_pl022_txrx_blocking(
            that->device, write_buffer, (uint32_t *)&write_size, read_buffer, (uint32_t *)&read_size)) {
        return 0U;
    }

    return ((write_size > read_size) ? write_size : read_size);
}

static bool is_peripheral_data_available(mdh_spi_t *self)
{
    priv_mps3_spi_t *that = container_of(self, priv_mps3_spi_t, mps3_spi.spi);
    uint32_t status = spi_pl022_get_status(that->device);

    return (bool)((status & SPI_PL022_SSPSR_RNE_MSK) && !(status & SPI_PL022_SSPSR_BSY_MSK));
}

static uint32_t peripheral_read(mdh_spi_t *self)
{
    while (!is_peripheral_data_available(self)) {
    }

    priv_mps3_spi_t *that = container_of(self, priv_mps3_spi_t, mps3_spi.spi);
    return spi_pl022_slave_read(that->device);
}

static void peripheral_write(mdh_spi_t *self, uint32_t data)
{
    priv_mps3_spi_t *that = container_of(self, priv_mps3_spi_t, mps3_spi.spi);
    (void)spi_pl022_write(that->device, SPI_PL022_SLAVE_SELECT, &data);
}

static bool is_busy(mdh_spi_t *self)
{
    priv_mps3_spi_t *that = container_of(self, priv_mps3_spi_t, mps3_spi.spi);
    return (bool)(spi_pl022_get_status(that->device) & SPI_PL022_SSPSR_BSY_MSK);
}

static void get_capabilities(mdh_spi_t *self, bool is_peripheral, mdh_spi_capabilities_t *capabilities)
{
    capabilities->minimum_frequency = 200000U;  // 200 kHz
    capabilities->maximum_frequency = 2000000U; // 2 MHz
    capabilities->word_lengths = 0x00008080U;   // 8 and 16 bit symbols
    capabilities->clk_modes = 0x0FU;            // All clock modes
    capabilities->hw_cs_handle = false;
    capabilities->buffers_equal_length = true;
#if DEVICE_SPI_ASYNCH
    capabilities->async_mode = true;
#else
    capabilities->async_mode = false;
#endif // DEVICE_SPI_ASYNCH
#if DEVICE_SPISLAVE
    capabilities->supports_peripheral_mode = true;
#else
    capabilities->supports_peripheral_mode = false;
#endif // DEVICE_SPISLAVE
    if (is_peripheral) {
        capabilities->peripheral_delay_between_symbols_ns = 2500U; // 2.5 us
    } else {
        capabilities->peripheral_delay_between_symbols_ns = 0U; // Irrelevant in contoller mode
    }
}

static priv_mps3_spi_t *get_priv_mps3_spi_instance(const struct spi_pl022_dev_t *spi_pl022_instance)
{
#ifdef SPI_ADC_PL022_NS
    if (spi_pl022_instance == &SPI_ADC_PL022_DEV) {
        return &gs_priv_spi_adc;
    }
#endif
#ifdef SPI_SHIELD0_PL022_NS
    if (spi_pl022_instance == &SPI_SHIELD0_PL022_DEV) {
        return &gs_priv_spi_shield0;
    }
#endif
#ifdef SPI_SHIELD1_PL022_NS
    if (spi_pl022_instance == &SPI_SHIELD1_PL022_DEV) {
        return &gs_priv_spi_shield1;
    }
#endif

    return NULL;
}
