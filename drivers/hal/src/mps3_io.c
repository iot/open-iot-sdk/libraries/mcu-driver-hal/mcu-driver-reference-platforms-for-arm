/* Copyright (c) 2006-2023 ARM Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define TRACE_GROUP "gpio"

#include "mps3_io.h"

#include "hal/gpio_api.h"
#include "arm_mps3_io_drv.h"
#include "gpio_cmsdk_drv.h"
#include "mbed_trace/mbed_trace.h"
#include "objects.h"
#include "pinmap.h"
#include "platform/error_handler.h"

#include <inttypes.h>
#include <stddef.h>

#define BITS_PER_GPIO 16U
#ifdef MPS3_IO_DEV
#define BITS_PER_MPS3_IO       20U
#define NUMBER_OF_PUSH_BUTTONS 2U
#define NUMBER_OF_LEDS         10U
#endif // MPS3_IO_DEV

#ifndef container_of
// Returns a pointer to the `type` containing the `member` pointed by `ptr`;
#define container_of(ptr, type, member)                    \
    ({                                                     \
        const typeof(((type *)0)->member) *__mptr = (ptr); \
        (type *)((char *)__mptr - offsetof(type, member)); \
    })
#endif

#ifdef MPS3_IO_DEV
typedef struct priv_mps3_io_s {
    struct arm_mps3_io_dev_t *device;
    mdh_gpio_direction_t direction[BITS_PER_MPS3_IO];
} priv_mps3_io_t;
#endif // MPS3_IO_DEV

typedef struct priv_ahb_gpio_s {
    struct gpio_cmsdk_dev_t *device;
    mdh_gpio_direction_t direction[BITS_PER_GPIO];
} priv_ahb_gpio_t;

static void set_mode(mdh_gpio_t *self, mdh_gpio_mode_t mode);
static void set_direction(mdh_gpio_t *self, mdh_gpio_direction_t direction);
static bool is_connected(const mdh_gpio_t *self);
static void _write(mdh_gpio_t *self, uint32_t value);
static uint32_t _read(mdh_gpio_t *self);
static void get_capabilities(mdh_gpio_t *self, mdh_gpio_capabilities_t *capabilities);
static bool is_gpio_pin(PinName pin);
static priv_ahb_gpio_t *get_gpio_instance(PinName pin);
#ifdef MPS3_IO_DEV
static bool is_user_led(PinName pin);
static bool is_user_push_button(PinName pin);
static bool is_mcc_led(PinName pin);
static bool is_mcc_switch(PinName pin);
static bool is_mps3_io_pin(PinName pin);
static int8_t get_mps3_io_pin_direction_index(PinName pin);
static int8_t get_mps3_io_pin_number(PinName pin);
#endif // MPS3_IO_DEV

static const mdh_gpio_vtable_t gsc_vtable = {.is_connected = is_connected,
                                             .set_mode = set_mode,
                                             .set_direction = set_direction,
                                             .write = _write,
                                             .read = _read,
                                             .get_capabilities = get_capabilities};

#ifdef GPIO0_CMSDK_DEV
static priv_ahb_gpio_t gs_priv_gpio_0 = {.device = &GPIO0_CMSDK_DEV,
                                         .direction = {MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN}};
#endif // GPIO0_CMSDK_DEV
#ifdef GPIO1_CMSDK_DEV
static priv_ahb_gpio_t gs_priv_gpio_1 = {.device = &GPIO1_CMSDK_DEV,
                                         .direction = {MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN}};
#endif // GPIO1_CMSDK_DEV
#ifdef GPIO2_CMSDK_DEV
static priv_ahb_gpio_t gs_priv_gpio_2 = {.device = &GPIO2_CMSDK_DEV,
                                         .direction = {MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN}};
#endif // GPIO2_CMSDK_DEV
#ifdef GPIO3_CMSDK_DEV
static priv_ahb_gpio_t gs_priv_gpio_3 = {.device = &GPIO3_CMSDK_DEV,
                                         .direction = {MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN}};
#endif // GPIO3_CMSDK_DEV
#ifdef MPS3_IO_DEV
static priv_mps3_io_t gs_priv_mps3_io = {.device = &MPS3_IO_DEV,
                                         .direction = {MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN,
                                                       MDH_GPIO_DIRECTION_IN}};

#endif // MPS3_IO_DEV

uint32_t mps3_io_set_pin_as_io(PinName pin)
{
    pin_set_function(pin, GPIO_FUNC);

    if (is_gpio_pin(pin)) {
        return (1UL << GPIO_PIN_NUMBER(pin));
    }
#ifdef MPS3_IO_DEV
    else if (is_mps3_io_pin(pin)) {
        int8_t mps3_io_pin_number = get_mps3_io_pin_number(pin);
        if (mps3_io_pin_number != -1) {
            return (1UL << mps3_io_pin_number);
        }
    }
#endif // MPS3_IO_DEV

    return 0UL;
}

bool mps3_io_init(mps3_io_t *io, void *io_device, PinName pin)
{
    io->pin = pin;
    io->gpio.vfptr = &gsc_vtable;

    if (pin == NC) {
        return true;
    }

    if (is_gpio_pin(pin)) {
        priv_ahb_gpio_t *priv_gpio = get_gpio_instance(pin);

        if (priv_gpio) {
            priv_gpio->device = (struct gpio_cmsdk_dev_t *)io_device;
            gpio_cmsdk_init(priv_gpio->device);
            return true;
        }

        halt_on_error("GPIO %d, associated with expansion pin %d, is disabled", GPIO_DEV_NUMBER(pin), pin);
        return false;
    }
#ifdef MPS3_IO_DEV
    else if (is_mps3_io_pin(pin)) {
        gs_priv_mps3_io.device = (struct arm_mps3_io_dev_t *)io_device;
        int8_t pin_direction_index = get_mps3_io_pin_direction_index(pin);

        if (pin_direction_index != -1) {
            if (is_user_led(pin) || is_mcc_led(pin)) {
                gs_priv_mps3_io.direction[pin_direction_index] = MDH_GPIO_DIRECTION_OUT;
                return true;
            } else if (is_user_push_button(pin) || is_mcc_switch(pin)) {
                gs_priv_mps3_io.direction[pin_direction_index] = MDH_GPIO_DIRECTION_IN;
                return true;
            }
        }
    }
#endif /* MPS3_IO_DEV */

    halt_on_error("pin %d is not a GPIO", pin);
    return false;
}

static void set_mode(mdh_gpio_t *self, mdh_gpio_mode_t mode)
{
    if (mode == MDH_GPIO_MODE_PULL_NONE) {
        return;
    }

    tr_warn("Setting the mode on pin %" PRIu32 " is not supported", container_of(self, mps3_io_t, gpio)->pin);
}

static void set_direction(mdh_gpio_t *self, mdh_gpio_direction_t direction)
{
    mps3_io_t *that = container_of(self, mps3_io_t, gpio);

    if (is_gpio_pin(that->pin)) {
        priv_ahb_gpio_t *priv_gpio = get_gpio_instance(that->pin);

        if (priv_gpio && priv_gpio->device) {
            enum gpio_cmsdk_direction_t dir = (enum gpio_cmsdk_direction_t)0;
            if (direction == MDH_GPIO_DIRECTION_IN) {
                dir |= GPIO_CMSDK_INPUT;
            } else { // direction == MDH_GPIO_DIRECTION_OUT
                dir |= GPIO_CMSDK_OUTPUT;
            }

            uint8_t pin_number = GPIO_PIN_NUMBER(that->pin);
            (void)gpio_cmsdk_pin_config(priv_gpio->device, pin_number, dir, GPIO_CMSDK_MAIN_FUNC);
            priv_gpio->direction[pin_number] = direction;

            return;
        }
    }
#ifdef MPS3_IO_DEV
    else if (is_mps3_io_pin(that->pin)) {
        /* Do nothing as MPS3 IO direction can not be changed */
        return;
    }
#endif // MPS3_IO_DEV

    halt_on_error("can not change the direction of pin");
}

static bool is_connected(const mdh_gpio_t *self)
{
    mps3_io_t *that = container_of(self, mps3_io_t, gpio);

    return (that->pin == NC) ? false : true;
}

static void _write(mdh_gpio_t *self, uint32_t value)
{
    mps3_io_t *that = container_of(self, mps3_io_t, gpio);

    if (is_gpio_pin(that->pin)) {
        priv_ahb_gpio_t *priv_gpio = get_gpio_instance(that->pin);

        if (priv_gpio && priv_gpio->device) {
            (void)gpio_cmsdk_pin_write(priv_gpio->device, GPIO_PIN_NUMBER(that->pin), value);
            return;
        }
    }
#ifdef MPS3_IO_DEV
    else if (is_mps3_io_pin(that->pin)) {
        int8_t pin_direction_index = get_mps3_io_pin_direction_index(that->pin);
        if (pin_direction_index != -1) {
            if (gs_priv_mps3_io.direction[pin_direction_index] == MDH_GPIO_DIRECTION_OUT) {
                arm_mps3_io_write_leds(
                    gs_priv_mps3_io.device, ARM_MPS3_IO_ACCESS_PIN, get_mps3_io_pin_number(that->pin), value);
            }

            return;
        }
    }
#endif // MPS3_IO_DEV

    halt_on_error("cannot write to pin");
}

static uint32_t _read(mdh_gpio_t *self)
{
    mps3_io_t *that = container_of(self, mps3_io_t, gpio);

    if (is_gpio_pin(that->pin)) {
        priv_ahb_gpio_t *priv_gpio = get_gpio_instance(that->pin);

        if (priv_gpio && priv_gpio->device) {
            uint32_t data = 0UL;
            (void)gpio_cmsdk_pin_read(priv_gpio->device, GPIO_PIN_NUMBER(that->pin), &data);
            return data;
        }
    }
#ifdef MPS3_IO_DEV
    else if (is_mps3_io_pin(that->pin)) {
        int8_t pin_direction_index = get_mps3_io_pin_direction_index(that->pin);
        if (pin_direction_index != -1) {
            if (gs_priv_mps3_io.direction[pin_direction_index] == MDH_GPIO_DIRECTION_IN) {
                if (is_user_push_button(that->pin)) {
                    return arm_mps3_io_read_buttons(
                        gs_priv_mps3_io.device, ARM_MPS3_IO_ACCESS_PIN, get_mps3_io_pin_number(that->pin));
                } else if (is_mcc_switch(that->pin)) {
                    return arm_mps3_io_read_switches(
                        gs_priv_mps3_io.device, ARM_MPS3_IO_ACCESS_PIN, get_mps3_io_pin_number(that->pin));
                }
            } else if (gs_priv_mps3_io.direction[pin_direction_index] == MDH_GPIO_DIRECTION_OUT) {
                return arm_mps3_io_read_leds(
                    gs_priv_mps3_io.device, ARM_MPS3_IO_ACCESS_PIN, get_mps3_io_pin_number(that->pin));
            }
        }
    }
#endif // MPS3_IO_DEV

    halt_on_error("cannot read pin");
    return 0UL;
}

static void get_capabilities(mdh_gpio_t *self, mdh_gpio_capabilities_t *capabilities)
{
    (void)self;

    capabilities->pull_none = 1U;
    capabilities->pull_down = 0U;
    capabilities->pull_up = 0U;
}

static bool is_gpio_pin(PinName pin)
{
    return pin >= EXP0 && pin <= EXP51;
}

static priv_ahb_gpio_t *get_gpio_instance(PinName pin)
{
    uint8_t gpio_peripheral_number = GPIO_DEV_NUMBER(pin);
    switch (gpio_peripheral_number) {
#ifdef GPIO0_CMSDK_DEV
        case GPIO0_NUMBER:
            return &gs_priv_gpio_0;
#endif // GPIO0_CMSDK_DEV
#ifdef GPIO1_CMSDK_DEV
        case GPIO1_NUMBER:
            return &gs_priv_gpio_1;
#endif // GPIO1_CMSDK_DEV
#ifdef GPIO2_CMSDK_DEV
        case GPIO2_NUMBER:
            return &gs_priv_gpio_2;
#endif // GPIO2_CMSDK_DEV
#ifdef GPIO3_CMSDK_DEV
        case GPIO3_NUMBER:
            return &gs_priv_gpio_3;
#endif // GPIO3_CMSDK_DEV
        default:
            halt_on_error("GPIO %d, associated with expansion pin %d, is disabled", gpio_peripheral_number, pin);
            return NULL;
    }
}

#ifdef MPS3_IO_DEV
static bool is_user_led(PinName pin)
{
    return pin == USERLED1 || pin == USERLED2;
}

static bool is_user_push_button(PinName pin)
{
    return pin == USERPB1 || pin == USERPB2;
}

static bool is_mcc_led(PinName pin)
{
    return pin >= LED1 && pin <= LED8;
}

static bool is_mcc_switch(PinName pin)
{
    return pin >= SW1 && pin <= SW8;
}

static bool is_mps3_io_pin(PinName pin)
{
    return (is_user_led(pin) || is_mcc_led(pin) || is_user_push_button(pin) || is_mcc_switch(pin));
}

static int8_t get_mps3_io_pin_direction_index(PinName pin)
{
    if (is_user_push_button(pin)) {
        return pin - USERPB1;
    } else if (is_user_led(pin) || is_mcc_led(pin)) {
        return NUMBER_OF_PUSH_BUTTONS + pin - LED1;
    } else if (is_mcc_switch(pin)) {
        return (NUMBER_OF_PUSH_BUTTONS + NUMBER_OF_LEDS) + pin - SW1;
    } else {
        return -1;
    }
}

static int8_t get_mps3_io_pin_number(PinName pin)
{
    if (is_user_push_button(pin)) {
        return get_mps3_io_pin_direction_index(pin);
    } else if (is_user_led(pin) || is_mcc_led(pin)) {
        return get_mps3_io_pin_direction_index(pin) - NUMBER_OF_PUSH_BUTTONS;
    } else if (is_mcc_switch(pin)) {
        return get_mps3_io_pin_direction_index(pin) - NUMBER_OF_LEDS - NUMBER_OF_PUSH_BUTTONS;
    } else {
        return -1;
    }
}
#endif // MPS3_IO_DEV
