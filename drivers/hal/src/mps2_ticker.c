/* Copyright (c) 2022-2023 Arm Limited
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include CMSIS_device_header
#include "device.h"
#include "device_cfg.h"

#include "hal/us_ticker_api.h"
#include "dualtimer_cmsdk_drv.h"
#include "hal-toolbox/container_of.h"
#include "platform_base_address.h"
#include "ticker_mps2.h"

static uint32_t us_ticker_read(mdh_ticker_t *self);
static void us_ticker_set_interrupt(mdh_ticker_t *self, timestamp_t timestamp);
static void us_ticker_disable_interrupt(mdh_ticker_t *self);
static void us_ticker_clear_interrupt(mdh_ticker_t *self);
static void us_ticker_fire_interrupt(mdh_ticker_t *self);
static ticker_irq_callback_f us_ticker_set_irq_callback(mdh_ticker_t *self, ticker_irq_callback_f callback);
static const ticker_info_t *us_ticker_get_info(mdh_ticker_t *self);

static const mdh_ticker_vtable_t gsc_vtable_us_ticker = {
    .read = us_ticker_read,
    .disable_interrupt = us_ticker_disable_interrupt,
    .clear_interrupt = us_ticker_clear_interrupt,
    .set_interrupt = us_ticker_set_interrupt,
    .fire_interrupt = us_ticker_fire_interrupt,
    .set_irq_handler = us_ticker_set_irq_callback,
    .get_info = us_ticker_get_info,
};

static struct dualtimer_cmsdk_dev_data_t timer_data;
static const struct dualtimer_cmsdk_dev_cfg_t timer_cfg = {
    .base = CMSDK_DUALTIMER_BASE,
};

static const struct dualtimer_cmsdk_dev_t timer_dev = {
    .cfg = &timer_cfg,
    .data = &timer_data,
};

static mps2_us_ticker_t gs_us_ticker_instance = {
    .ticker.vtable = &gsc_vtable_us_ticker,
    .dev = &timer_dev,
};

static ticker_irq_callback_f us_ticker_callback;

static uint32_t us_ticker_read(mdh_ticker_t *self)
{
    mps2_us_ticker_t *ticker = container_of(self, mps2_us_ticker_t, ticker);

    return dualtimer_cmsdk_get_elapsed_value_timer1(ticker->dev);
}

static void us_ticker_set_interrupt(mdh_ticker_t *self, timestamp_t timestamp)
{
    mps2_us_ticker_t *ticker = container_of(self, mps2_us_ticker_t, ticker);
    uint32_t reload = timestamp - us_ticker_read(self);

    dualtimer_cmsdk_disable_timer2(ticker->dev);
    dualtimer_cmsdk_set_reload_timer2(ticker->dev, reload);
    dualtimer_cmsdk_reset_timer2(ticker->dev);
    dualtimer_cmsdk_enable_interrupt_timer2(ticker->dev);
    NVIC_EnableIRQ(USEC_INTERVAL_IRQ);
    dualtimer_cmsdk_enable_timer2(ticker->dev);
}

static void us_ticker_disable_interrupt(mdh_ticker_t *self)
{
    mps2_us_ticker_t *ticker = container_of(self, mps2_us_ticker_t, ticker);

    dualtimer_cmsdk_disable_interrupt_timer2(ticker->dev);
    NVIC_DisableIRQ(USEC_INTERVAL_IRQ);
}

static void us_ticker_clear_interrupt(mdh_ticker_t *self)
{
    mps2_us_ticker_t *ticker = container_of(self, mps2_us_ticker_t, ticker);

    dualtimer_cmsdk_clear_interrupt_timer2(ticker->dev);
}

static void us_ticker_fire_interrupt(mdh_ticker_t *self)
{
    NVIC_EnableIRQ(USEC_INTERVAL_IRQ);
    NVIC_SetPendingIRQ(USEC_INTERVAL_IRQ);
}

static const ticker_info_t *us_ticker_get_info(mdh_ticker_t *self)
{
    static const ticker_info_t info = {.frequency = USEC_REPORTED_FREQ_HZ, .bits = USEC_REPORTED_BITS};
    return &info;
}

static ticker_irq_callback_f us_ticker_set_irq_callback(mdh_ticker_t *self, ticker_irq_callback_f callback)
{
    ticker_irq_callback_f tmp = us_ticker_callback;
    us_ticker_callback = callback;
    return tmp;
}

mdh_ticker_t *mps2_us_ticker_get_default_instance(void)
{
    return &(gs_us_ticker_instance.ticker);
}

#ifndef usec_interval_irq_handler
#error "usec_interval_irq_handler should be defined, check device_cfg.h!"
#endif
void usec_interval_irq_handler(void)
{
    us_ticker_clear_interrupt(mps2_us_ticker_get_default_instance());
    us_ticker_disable_interrupt(mps2_us_ticker_get_default_instance());

    if (us_ticker_callback)
        us_ticker_callback(mps2_us_ticker_get_default_instance());
}

void mps2_us_ticker_init(mdh_ticker_t *self)
{
    mps2_us_ticker_t *ticker = container_of(self, mps2_us_ticker_t, ticker);

    if (timer_data.is_initialized) {
        us_ticker_disable_interrupt(self);
        return;
    }

    dualtimer_cmsdk_init(ticker->dev);
    dualtimer_cmsdk_set_size_both_timers(ticker->dev, DUALTIMER_CMSDK_SIZE_32BIT);
    dualtimer_cmsdk_set_prescale_both_timers(ticker->dev, DUALTIMER_CMSDK_CLOCK_DIV16);
    dualtimer_cmsdk_disable_both_timers(ticker->dev);
    dualtimer_cmsdk_set_reload_both_timers(ticker->dev, TIMER_CMSDK_MAX_RELOAD);
    dualtimer_cmsdk_reset_both_timers(ticker->dev);
    NVIC_EnableIRQ(USEC_INTERVAL_IRQ);
    dualtimer_cmsdk_enable_both_timers(ticker->dev);
}

void mps2_us_ticker_deinit(mdh_ticker_t *self)
{
    mps2_us_ticker_t *ticker = container_of(self, mps2_us_ticker_t, ticker);

    dualtimer_cmsdk_free(ticker->dev);
}
