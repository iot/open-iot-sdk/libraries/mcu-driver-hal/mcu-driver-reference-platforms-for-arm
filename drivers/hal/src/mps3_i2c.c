/* Copyright (c) 2017-2022 Arm Limited. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mps3_i2c.h"

#include "hal/i2c_api.h"
#include "PeripheralNames.h"
#include "PinNames.h"
#include "pinmap.h"

#ifndef container_of
// Returns a pointer to the `type` containing the `member` pointed by `ptr`;
#define container_of(ptr, type, member)                    \
    ({                                                     \
        const typeof(((type *)0)->member) *__mptr = (ptr); \
        (type *)((char *)__mptr - offsetof(type, member)); \
    })
#endif

typedef struct priv_mps3_i2c_s {
    mps3_i2c_t mps3_i2c;
    struct i2c_sbcon_dev_t *device;
    PinName sda;
    PinName scl;
    PinFunction function;
} priv_mps3_i2c_t;

static mdh_i2c_error_t set_frequency(mdh_i2c_t *self, uint32_t hz);
static void reset(mdh_i2c_t *self);
static int send_start(mdh_i2c_t *self);
static int send_stop(mdh_i2c_t *self);
static int read_byte(mdh_i2c_t *self, uint8_t ack_bit);
static int write_byte(mdh_i2c_t *self, uint8_t data);
static int _read(mdh_i2c_t *self, uint16_t address, uint8_t *buffer, size_t length, bool add_stop);
static int _write(mdh_i2c_t *self, uint16_t address, const uint8_t *buffer, size_t length, bool add_stop);
static priv_mps3_i2c_t *get_priv_mps3_i2c_instance(const struct i2c_sbcon_dev_t *i2c_sbcon_instance);

static const mdh_i2c_vtable_t gsc_vtable = {
    .set_frequency = set_frequency,
    .send_start = send_start,
    .send_stop = send_stop,
    .read = _read,
    .write = _write,
    .reset = reset,
    .read_byte = read_byte,
    .write_byte = write_byte,
};

#ifdef I2C0_SBCON_NS
static priv_mps3_i2c_t gs_priv_i2c_touch = {.mps3_i2c.i2c.vfptr = &gsc_vtable,
                                            .device = &I2C0_SBCON_DEV,
                                            .sda = I2C_TOUCH_SDA,
                                            .scl = I2C_TOUCH_SCL,
                                            .function = NOT_A_GPIO};
#endif
#ifdef I2C1_SBCON_NS
static priv_mps3_i2c_t gs_priv_i2c_audio = {.mps3_i2c.i2c.vfptr = &gsc_vtable,
                                            .device = &I2C1_SBCON_DEV,
                                            .sda = I2C_AUDIO_SDA,
                                            .scl = I2C_AUDIO_SCL,
                                            .function = NOT_A_GPIO};
#endif
#ifdef I2C2_SBCON_NS
static priv_mps3_i2c_t gs_priv_i2c_shield0 = {.mps3_i2c.i2c.vfptr = &gsc_vtable,
                                              .device = &I2C2_SBCON_DEV,
                                              .sda = D0_14,
                                              .scl = D0_15,
                                              .function = ALTERNATE_FUNC};
#endif
#ifdef I2C3_SBCON_NS
static priv_mps3_i2c_t gs_priv_i2c_shield1 = {.mps3_i2c.i2c.vfptr = &gsc_vtable,
                                              .device = &I2C3_SBCON_DEV,
                                              .sda = D1_14,
                                              .scl = D1_15,
                                              .function = ALTERNATE_FUNC};
#endif

bool mps3_i2c_init(mps3_i2c_t **i2c, struct i2c_sbcon_dev_t *i2c_sbcon_instance)
{
    priv_mps3_i2c_t *priv_i2c = get_priv_mps3_i2c_instance(i2c_sbcon_instance);

    if (NULL == priv_i2c) {
        return false;
    }

    *i2c = &(priv_i2c->mps3_i2c);

    pin_set_function(priv_i2c->scl, priv_i2c->function);
    pin_set_function(priv_i2c->sda, priv_i2c->function);

    return (I2C_ERR_NONE == i2c_sbcon_init(priv_i2c->device, PERIPHERAL_CLOCK_HZ))
           && (I2C_ERR_NONE == i2c_sbcon_reset(priv_i2c->device));
}

bool mps3_i2c_deinit(struct i2c_sbcon_dev_t *i2c_sbcon_instance)
{
    return (I2C_ERR_NONE == i2c_sbcon_reset(i2c_sbcon_instance)) ? true : false;
}

static mdh_i2c_error_t set_frequency(mdh_i2c_t *self, uint32_t hz)
{
    priv_mps3_i2c_t *that = container_of(self, priv_mps3_i2c_t, mps3_i2c.i2c);
    switch (i2c_sbcon_set_freq(that->device, hz)) {
        case I2C_ERR_NONE:
            return MDH_I2C_ERROR_NO_ERROR;
        case I2C_ERR_INVALID_ARG:
            return MDH_I2C_ERROR_INVALID_ARG;
        default:
            return MDH_I2C_ERROR_INTERNAL_ERROR;
    }
}

static void reset(mdh_i2c_t *self)
{
    priv_mps3_i2c_t *that = container_of(self, priv_mps3_i2c_t, mps3_i2c.i2c);
    i2c_sbcon_reset(that->device);
}

static int send_start(mdh_i2c_t *self)
{
    priv_mps3_i2c_t *that = container_of(self, priv_mps3_i2c_t, mps3_i2c.i2c);
    i2c_sbcon_tx_start(that->device);
    return 0;
}

static int send_stop(mdh_i2c_t *self)
{
    priv_mps3_i2c_t *that = container_of(self, priv_mps3_i2c_t, mps3_i2c.i2c);
    i2c_sbcon_tx_stop(that->device);
    return 0;
}

static int read_byte(mdh_i2c_t *self, uint8_t ack_bit)
{
    priv_mps3_i2c_t *that = container_of(self, priv_mps3_i2c_t, mps3_i2c.i2c);
    uint8_t read = i2c_sbcon_rx_byte(that->device);
    i2c_sbcon_tx_ack(that->device, ack_bit);
    return read;
}

static int write_byte(mdh_i2c_t *self, uint8_t data)
{
    priv_mps3_i2c_t *that = container_of(self, priv_mps3_i2c_t, mps3_i2c.i2c);
    i2c_sbcon_tx_byte(that->device, data);
    return i2c_sbcon_rx_ack(that->device) == 0;
}

static int _read(mdh_i2c_t *self, uint16_t address, uint8_t *buffer, size_t length, bool add_stop)
{
    priv_mps3_i2c_t *that = container_of(self, priv_mps3_i2c_t, mps3_i2c.i2c);
    uint32_t read = 0;
    i2c_sbcon_master_receive(that->device, address, buffer, length, !add_stop, &read);
    return read;
}

static int _write(mdh_i2c_t *self, uint16_t address, const uint8_t *buffer, size_t length, bool add_stop)
{
    priv_mps3_i2c_t *that = container_of(self, priv_mps3_i2c_t, mps3_i2c.i2c);
    uint32_t written = 0;
    i2c_sbcon_master_transmit(that->device, address, buffer, length, !add_stop, &written);
    return written;
}

static priv_mps3_i2c_t *get_priv_mps3_i2c_instance(const struct i2c_sbcon_dev_t *i2c_sbcon_instance)
{
#ifdef I2C0_SBCON_NS
    if (i2c_sbcon_instance == &I2C0_SBCON_DEV) {
        return &gs_priv_i2c_touch;
    }
#endif
#ifdef I2C1_SBCON_NS
    if (i2c_sbcon_instance == &I2C1_SBCON_DEV) {
        return &gs_priv_i2c_audio;
    }
#endif
#ifdef I2C2_SBCON_NS
    if (i2c_sbcon_instance == &I2C2_SBCON_DEV) {
        return &gs_priv_i2c_shield0;
    }
#endif
#ifdef I2C3_SBCON_NS
    if (i2c_sbcon_instance == &I2C3_SBCON_DEV) {
        return &gs_priv_i2c_shield1;
    }
#endif

    return NULL;
}

PinName mps3_i2c_get_sda_pin(const struct i2c_sbcon_dev_t *i2c_sbcon_instance)
{
#ifdef I2C0_SBCON_NS
    if (i2c_sbcon_instance == &I2C0_SBCON_DEV) {
        return gs_priv_i2c_touch.sda;
    }
#endif
#ifdef I2C1_SBCON_NS
    if (i2c_sbcon_instance == &I2C1_SBCON_DEV) {
        return gs_priv_i2c_audio.sda;
    }
#endif
#ifdef I2C2_SBCON_NS
    if (i2c_sbcon_instance == &I2C2_SBCON_DEV) {
        return gs_priv_i2c_shield0.sda;
    }
#endif
#ifdef I2C3_SBCON_NS
    if (i2c_sbcon_instance == &I2C3_SBCON_DEV) {
        return gs_priv_i2c_shield1.sda;
    }
#endif

    return NC;
}

PinName mps3_i2c_get_scl_pin(const struct i2c_sbcon_dev_t *i2c_sbcon_instance)
{
#ifdef I2C0_SBCON_NS
    if (i2c_sbcon_instance == &I2C0_SBCON_DEV) {
        return gs_priv_i2c_touch.scl;
    }
#endif
#ifdef I2C1_SBCON_NS
    if (i2c_sbcon_instance == &I2C1_SBCON_DEV) {
        return gs_priv_i2c_audio.scl;
    }
#endif
#ifdef I2C2_SBCON_NS
    if (i2c_sbcon_instance == &I2C2_SBCON_DEV) {
        return gs_priv_i2c_shield0.scl;
    }
#endif
#ifdef I2C3_SBCON_NS
    if (i2c_sbcon_instance == &I2C3_SBCON_DEV) {
        return gs_priv_i2c_shield1.scl;
    }
#endif

    return NC;
}
