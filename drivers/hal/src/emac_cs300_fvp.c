/* Copyright (c) 2021 - 2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal/emac_api.h"
#include "hal/network_stack_memory_manager.h"
#include "hal/us_ticker_api.h"
#include CMSIS_device_header
#include "emac_cs300.h"
#include "hal-toolbox/us_ticker_util.h"
#include "lan91c111_driver.h"
#include "platform_base_address.h"
#include "ticker.h"

#include <string.h>

#define EMAC_LAN91C111_INTERFACE_NAME   "lan91c111"
#define EMAC_LAN91C111_MAC_ADDRESS_SIZE 6U
#define EMAC_LAN91C111_MEMORY_ALIGNMENT 4U
#define EMAC_LAN91C111_MTU              1518U // Dst(6) + Src(6) + 802.1Q tag(4) + type/length(2) + Data(1500)

static void wait_ms(uint32_t ms);
static uint32_t microchip_crc32_compute(const uint8_t *src, size_t len);
static size_t get_lan91c111_mtu(const mdh_emac_t *const self);
static size_t get_lan91c111_data_alignment_size(const mdh_emac_t *const self);
static size_t get_lan91c111_interface_name(const mdh_emac_t *const self, char *name, size_t name_size);
static size_t get_lan91c111_mac_addr_size(const mdh_emac_t *const self);
static mdh_emac_status_t get_lan91c111_mac_addr(const mdh_emac_t *const self, uint8_t *addr);
static mdh_emac_status_t power_up_lan91c11(const mdh_emac_t *self,
                                           mdh_emac_callbacks_t *cbks,
                                           mdh_network_stack_memory_manager_t *memory_manager,
                                           void *ctx);
static mdh_emac_status_t power_down(mdh_emac_t *const self);
static mdh_emac_status_t transmit_lan91c111(mdh_emac_t *const self, const mdh_network_stack_buffer_t *buf);
static mdh_emac_status_t receive_lan91c111(mdh_emac_t *const self, const mdh_network_stack_buffer_t *buf);
static mdh_emac_status_t add_to_multicast_group_lan91c111(mdh_emac_t *const self, const uint8_t *addr);
static mdh_emac_status_t remove_from_multicast_group_lan91c111(mdh_emac_t *const self, const uint8_t *addr);
static mdh_emac_status_t config_multicast_reception_lan91c111(mdh_emac_t *const self, bool receive_all);

static const mdh_emac_vtable_t gsc_vtable = {.power_up = power_up_lan91c11,
                                             .power_down = power_down,

                                             .get_mtu = get_lan91c111_mtu,
                                             .get_align = get_lan91c111_data_alignment_size,
                                             .get_interface_name = get_lan91c111_interface_name,
                                             .get_mac_addr_size = get_lan91c111_mac_addr_size,
                                             .get_mac_addr = get_lan91c111_mac_addr,
                                             .set_mac_addr = NULL,

                                             .add_to_multicast_group = add_to_multicast_group_lan91c111,
                                             .remove_from_multicast_group = remove_from_multicast_group_lan91c111,
                                             .config_multicast_reception = config_multicast_reception_lan91c111,

                                             .transmit = transmit_lan91c111,
                                             .receive = receive_lan91c111};
static const lan91c111_dev_cfg_t gsc_eth_device_config = {.base = ETHERNET_BASE_NS};
static lan91c111_dev_data_t gsc_eth_device_data = {.wait_ms_cbk = wait_ms, .crc32_cbk = microchip_crc32_compute};
static lan91c111_dev_t gs_eth_device = {.cfg = &gsc_eth_device_config, .data = &gsc_eth_device_data};
static cs300_emac_t gs_emac_instance = {.emac.vtable = &gsc_vtable, .cbks = NULL, .memory_manager = NULL};

// Blocking wait
static void wait_ms(uint32_t ms)
{
    us_ticker_util_wait(bsp_us_ticker_get_default_instance(), ms * 1000U);
}

// This is not a standard way of calculating CRC32 but appears to be what is
// implemented on Microchip Ethernet controller devices
static uint32_t microchip_crc32_compute(const uint8_t *src, size_t len)
{
    uint32_t crc = 0xFFFFFFFFUL;

    for (size_t byte_index = 0; byte_index < len; byte_index++) {
        uint8_t temp = *src++;
        for (size_t bit_index = 0; bit_index < 8; bit_index++) {
            uint8_t carry = ((crc & 0x80000000) ? 1 : 0) ^ (temp & 0x01);
            crc <<= 1;
            if (carry) {
                crc = (crc ^ 0x04c11db6) | carry;
            }

            temp >>= 1;
        }
    }

    return crc;
}

static mdh_emac_status_t power_up_lan91c11(const mdh_emac_t *self,
                                           mdh_emac_callbacks_t *cbks,
                                           mdh_network_stack_memory_manager_t *memory_manager,
                                           void *ctx)
{
    if ((NULL == self) || (NULL == cbks) || (NULL == cbks->rx) || (NULL == cbks->tx) || (NULL == memory_manager)) {
        return MDH_EMAC_STATUS_INVALID_ARGS;
    }

    gs_emac_instance.cbks = cbks;
    gs_emac_instance.memory_manager = memory_manager;
    gs_emac_instance.context = ctx;

    bsp_us_ticker_init(bsp_us_ticker_get_default_instance());
    mdh_emac_status_t status = (LAN91C111_ERROR_NONE == lan91c111_init(&gs_eth_device))
                                   ? MDH_EMAC_STATUS_NO_ERROR
                                   : MDH_EMAC_STATUS_INTERNAL_ERROR;

    if (status == MDH_EMAC_STATUS_NO_ERROR) {
        NVIC_ClearPendingIRQ(ETHERNET_IRQn);
        NVIC_EnableIRQ(ETHERNET_IRQn);

        lan91c111_enable_interrupt(&gs_eth_device, MAC_REG_MSK_RCV);
    }

    return status;
}

static mdh_emac_status_t power_down(mdh_emac_t *const self)
{
    NVIC_DisableIRQ(ETHERNET_IRQn);

    return MDH_EMAC_STATUS_NO_ERROR;
}

static size_t get_lan91c111_mtu(const mdh_emac_t *const self)
{
    return EMAC_LAN91C111_MTU;
}

static size_t get_lan91c111_data_alignment_size(const mdh_emac_t *const self)
{
    return EMAC_LAN91C111_MEMORY_ALIGNMENT;
}

static size_t get_lan91c111_interface_name(const mdh_emac_t *const self, char *name, size_t name_size)
{
    size_t retrieved_size = 0U;

    if (name_size < 2U) {
        return retrieved_size;
    }

    if (sizeof(EMAC_LAN91C111_INTERFACE_NAME) > name_size) {
        // Ensures cropped name is null terminated
        memcpy(name, EMAC_LAN91C111_INTERFACE_NAME, name_size - 2U);
        name[name_size - 1U] = '\0';

        retrieved_size = name_size;
    } else {
        memcpy(name, EMAC_LAN91C111_INTERFACE_NAME, sizeof(EMAC_LAN91C111_INTERFACE_NAME));

        retrieved_size = sizeof(EMAC_LAN91C111_INTERFACE_NAME);
    }

    return retrieved_size;
}

static size_t get_lan91c111_mac_addr_size(const mdh_emac_t *const self)
{
    return EMAC_LAN91C111_MAC_ADDRESS_SIZE;
}

static mdh_emac_status_t get_lan91c111_mac_addr(const mdh_emac_t *const self, uint8_t *addr)
{
    lan91c111_read_mac_address(&gs_eth_device, addr);
    return MDH_EMAC_STATUS_NO_ERROR;
}

static mdh_emac_status_t transmit_lan91c111(mdh_emac_t *const self, const mdh_network_stack_buffer_t *buf)
{
    uint8_t *buffer_data =
        (uint8_t *)mdh_network_stack_memory_manager_get_payload(gs_emac_instance.memory_manager, buf);

    size_t buf_payload_len = mdh_network_stack_memory_manager_get_payload_len(gs_emac_instance.memory_manager, buf);

    mdh_emac_status_t status =
        (LAN91C111_ERROR_NONE == lan91c111_send_frame(&gs_eth_device, buffer_data, buf_payload_len))
            ? MDH_EMAC_STATUS_NO_ERROR
            : MDH_EMAC_STATUS_INTERNAL_ERROR;

    gs_emac_instance.cbks->tx(self,
                              gs_emac_instance.context,
                              (status == MDH_EMAC_STATUS_NO_ERROR) ? MDH_EMAC_TRANSFER_DONE : MDH_EMAC_TRANSFER_ERROR,
                              buf);

    return status;
}

static mdh_emac_status_t receive_lan91c111(mdh_emac_t *const self, const mdh_network_stack_buffer_t *buf)
{
    if (true == lan91c111_is_rx_fifo_empty(&gs_eth_device)) {
        return MDH_EMAC_STATUS_INTERNAL_ERROR;
    }

    uint8_t *buffer_data =
        (uint8_t *)mdh_network_stack_memory_manager_get_payload(gs_emac_instance.memory_manager, buf);

    size_t buf_payload_len = mdh_network_stack_memory_manager_get_payload_len(gs_emac_instance.memory_manager, buf);

    size_t frame_size = lan91c111_receive_frame(&gs_eth_device, buffer_data, buf_payload_len);

    mdh_network_stack_memory_manager_set_payload_len(gs_emac_instance.memory_manager, buf, frame_size);

    mdh_emac_status_t status = frame_size ? MDH_EMAC_STATUS_NO_ERROR : MDH_EMAC_STATUS_INTERNAL_ERROR;

    lan91c111_enable_interrupt(&gs_eth_device, MAC_REG_MSK_RCV);

    return status;
}

static mdh_emac_status_t add_to_multicast_group_lan91c111(mdh_emac_t *const self, const uint8_t *addr)
{
    mdh_emac_status_t status = (LAN91C111_ERROR_NONE == lan91c111_add_multicast_group(&gs_eth_device, addr))
                                   ? MDH_EMAC_STATUS_NO_ERROR
                                   : MDH_EMAC_STATUS_INTERNAL_ERROR;

    return status;
}

static mdh_emac_status_t remove_from_multicast_group_lan91c111(mdh_emac_t *const self, const uint8_t *addr)
{
    mdh_emac_status_t status = (LAN91C111_ERROR_NONE == lan91c111_remove_multicast_group(&gs_eth_device, addr))
                                   ? MDH_EMAC_STATUS_NO_ERROR
                                   : MDH_EMAC_STATUS_INTERNAL_ERROR;

    return status;
}

static mdh_emac_status_t config_multicast_reception_lan91c111(mdh_emac_t *const self, bool receive_all)
{
    if (receive_all) {
        lan91c111_accept_all_multicast_enable(&gs_eth_device);
    } else {
        lan91c111_accept_all_multicast_disable(&gs_eth_device);
    }

    return MDH_EMAC_STATUS_NO_ERROR;
}

mdh_emac_t *cs300_emac_get_default_instance(void)
{
    return &(gs_emac_instance.emac);
}

void ETHERNET_Handler(void)
{
    NVIC_ClearPendingIRQ(ETHERNET_IRQn);

    lan91c111_isr_save_context(&gs_eth_device);

    if (lan91c111_get_interrupt_status(&gs_eth_device, MAC_REG_MSK_RCV)) {
        lan91c111_disable_interrupt(&gs_eth_device, MAC_REG_MSK_RCV);

        gs_emac_instance.cbks->rx(&(gs_emac_instance.emac), gs_emac_instance.context, MDH_EMAC_RECEIVE_DONE);
    }

    lan91c111_isr_restore_context(&gs_eth_device);
}
