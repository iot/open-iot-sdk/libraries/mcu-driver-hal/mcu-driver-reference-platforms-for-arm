/* Copyright (c) 2017-2023, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mps3_uart.h"
#include "cmsdk_uart.h"

#include "hal/serial_api.h"
#include "PeripheralNames.h"
#include "objects.h"
#include "pinmap.h"

#include <stddef.h>
#include <string.h>

typedef struct priv_mps3_uart_s {
    cmsdk_uart_t cmsdk_uart;
    PinName rx;
    PinName tx;
    IRQn_Type rx_irq_number;
    IRQn_Type tx_irq_number;
} priv_mps3_uart_t;

static priv_mps3_uart_t *get_priv_mps3_uart_instance(const struct uart_cmsdk_dev_t *cmsdk_instance);

#ifdef UART0_CMSDK_NS
static priv_mps3_uart_t gs_priv_uart_0 = {.cmsdk_uart.serial.vfptr = &cmsdk_uart_vtable,
                                          .cmsdk_uart.device = &UART0_CMSDK_DEV,
                                          .rx_irq_number = UARTRX0_IRQn,
                                          .tx_irq_number = UARTTX0_IRQn,
                                          .rx = CONSOLE_RX,
                                          .tx = CONSOLE_TX};
#endif
#ifdef UART1_CMSDK_NS
static priv_mps3_uart_t gs_priv_uart_1 = {.cmsdk_uart.serial.vfptr = &cmsdk_uart_vtable,
                                          .cmsdk_uart.device = &UART1_CMSDK_DEV,
                                          .rx_irq_number = UARTRX1_IRQn,
                                          .tx_irq_number = UARTTX1_IRQn,
                                          .rx = U1_RX,
                                          .tx = U1_TX};
#endif
#ifdef UART2_CMSDK_NS
static priv_mps3_uart_t gs_priv_uart_2 = {.cmsdk_uart.serial.vfptr = &cmsdk_uart_vtable,
                                          .cmsdk_uart.device = &UART2_CMSDK_DEV,
                                          .rx_irq_number = UARTRX2_IRQn,
                                          .tx_irq_number = UARTTX2_IRQn,
                                          .rx = U2_RX,
                                          .tx = U2_TX};
#endif
#ifdef UART3_CMSDK_NS
static priv_mps3_uart_t gs_priv_uart_3 = {.cmsdk_uart.serial.vfptr = &cmsdk_uart_vtable,
                                          .cmsdk_uart.device = &UART3_CMSDK_DEV,
                                          .rx_irq_number = UARTRX3_IRQn,
                                          .tx_irq_number = UARTTX3_IRQn,
                                          .rx = D0_0,
                                          .tx = D0_1};
#endif
#ifdef UART4_CMSDK_NS
static priv_mps3_uart_t gs_priv_uart_4 = {.cmsdk_uart.serial.vfptr = &cmsdk_uart_vtable,
                                          .cmsdk_uart.device = &UART4_CMSDK_DEV,
                                          .rx_irq_number = UARTRX4_IRQn,
                                          .tx_irq_number = UARTTX4_IRQn,
                                          .rx = D1_0,
                                          .tx = D1_1};
#endif
#ifdef UART5_CMSDK_NS
static priv_mps3_uart_t gs_priv_uart_5 = {.cmsdk_uart.serial.vfptr = &cmsdk_uart_vtable,
                                          .cmsdk_uart.device = &UART5_CMSDK_DEV,
                                          .rx_irq_number = UARTRX5_IRQn,
                                          .tx_irq_number = UARTTX5_IRQn,
                                          .rx = U5_RX,
                                          .tx = U5_TX};
#endif

static priv_mps3_uart_t *get_priv_mps3_uart_instance(const struct uart_cmsdk_dev_t *cmsdk_instance)
{
#ifdef UART0_CMSDK_NS
    if (cmsdk_instance == &UART0_CMSDK_DEV_NS) {
        return &gs_priv_uart_0;
    }
#endif // UART0_CMSDK_NS

#ifdef UART1_CMSDK_NS
    if (cmsdk_instance == &UART1_CMSDK_DEV_NS) {
        return &gs_priv_uart_1;
    }
#endif // UART1_CMSDK_NS

#ifdef UART2_CMSDK_NS
    if (cmsdk_instance == &UART2_CMSDK_DEV_NS) {
        return &gs_priv_uart_2;
    }
#endif // UART2_CMSDK_NS

#ifdef UART3_CMSDK_NS
    if (cmsdk_instance == &UART3_CMSDK_DEV_NS) {
        return &gs_priv_uart_3;
    }
#endif // UART3_CMSDK_NS

#ifdef UART4_CMSDK_NS
    if (cmsdk_instance == &UART4_CMSDK_DEV_NS) {
        return &gs_priv_uart_4;
    }
#endif // UART4_CMSDK_NS

#ifdef UART5_CMSDK_NS
    if (cmsdk_instance == &UART5_CMSDK_DEV_NS) {
        return &gs_priv_uart_5;
    }
#endif // UART5_CMSDK_NS

    return NULL;
}

#ifdef UART0_CMSDK_NS
void UARTRX0_Handler(void)
{
    uart_cmsdk_clear_interrupt(gs_priv_uart_0.cmsdk_uart.device, UART_CMSDK_IRQ_RX);
    gs_priv_uart_0.cmsdk_uart.on_event(gs_priv_uart_0.cmsdk_uart.context, MDH_SERIAL_IRQ_TYPE_RX);
}

void UARTTX0_Handler(void)
{
    uart_cmsdk_clear_interrupt(gs_priv_uart_0.cmsdk_uart.device, UART_CMSDK_IRQ_TX);
    gs_priv_uart_0.cmsdk_uart.on_event(gs_priv_uart_0.cmsdk_uart.context, MDH_SERIAL_IRQ_TYPE_TX);
}
#endif

#ifdef UART1_CMSDK_NS
void UARTRX1_Handler(void)
{
    uart_cmsdk_clear_interrupt(gs_priv_uart_1.cmsdk_uart.device, UART_CMSDK_IRQ_RX);
    gs_priv_uart_1.cmsdk_uart.on_event(gs_priv_uart_1.cmsdk_uart.context, MDH_SERIAL_IRQ_TYPE_RX);
}

void UARTTX1_Handler(void)
{
    uart_cmsdk_clear_interrupt(gs_priv_uart_1.cmsdk_uart.device, UART_CMSDK_IRQ_TX);
    gs_priv_uart_1.cmsdk_uart.on_event(gs_priv_uart_1.cmsdk_uart.context, MDH_SERIAL_IRQ_TYPE_TX);
}
#endif

#ifdef UART2_CMSDK_NS
void UARTRX2_Handler(void)
{
    uart_cmsdk_clear_interrupt(gs_priv_uart_2.cmsdk_uart.device, UART_CMSDK_IRQ_RX);
    gs_priv_uart_2.cmsdk_uart.on_event(gs_priv_uart_2.cmsdk_uart.context, MDH_SERIAL_IRQ_TYPE_RX);
}

void UARTTX2_Handler(void)
{
    uart_cmsdk_clear_interrupt(gs_priv_uart_2.cmsdk_uart.device, UART_CMSDK_IRQ_TX);
    gs_priv_uart_2.cmsdk_uart.on_event(gs_priv_uart_2.cmsdk_uart.context, MDH_SERIAL_IRQ_TYPE_TX);
}
#endif

#ifdef UART3_CMSDK_NS
void UARTRX3_Handler(void)
{
    uart_cmsdk_clear_interrupt(gs_priv_uart_3.cmsdk_uart.device, UART_CMSDK_IRQ_RX);
    gs_priv_uart_3.cmsdk_uart.on_event(gs_priv_uart_3.cmsdk_uart.context, MDH_SERIAL_IRQ_TYPE_RX);
}

void UARTTX3_Handler(void)
{
    uart_cmsdk_clear_interrupt(gs_priv_uart_3.cmsdk_uart.device, UART_CMSDK_IRQ_TX);
    gs_priv_uart_3.cmsdk_uart.on_event(gs_priv_uart_3.cmsdk_uart.context, MDH_SERIAL_IRQ_TYPE_TX);
}
#endif

#ifdef UART4_CMSDK_NS
void UARTRX4_Handler(void)
{
    uart_cmsdk_clear_interrupt(gs_priv_uart_4.cmsdk_uart.device, UART_CMSDK_IRQ_RX);
    gs_priv_uart_4.cmsdk_uart.on_event(gs_priv_uart_4.cmsdk_uart.context, MDH_SERIAL_IRQ_TYPE_RX);
}

void UARTTX4_Handler(void)
{
    uart_cmsdk_clear_interrupt(gs_priv_uart_4.cmsdk_uart.device, UART_CMSDK_IRQ_TX);
    gs_priv_uart_4.cmsdk_uart.on_event(gs_priv_uart_4.cmsdk_uart.context, MDH_SERIAL_IRQ_TYPE_TX);
}
#endif

#ifdef UART5_CMSDK_NS
void UARTRX5_Handler(void)
{
    uart_cmsdk_clear_interrupt(gs_priv_uart_5.cmsdk_uart.device, UART_CMSDK_IRQ_RX);
    gs_priv_uart_5.cmsdk_uart.on_event(gs_priv_uart_5.cmsdk_uart.context, MDH_SERIAL_IRQ_TYPE_RX);
}

void UARTTX5_Handler(void)
{
    uart_cmsdk_clear_interrupt(gs_priv_uart_5.cmsdk_uart.device, UART_CMSDK_IRQ_TX);
    gs_priv_uart_5.cmsdk_uart.on_event(gs_priv_uart_5.cmsdk_uart.context, MDH_SERIAL_IRQ_TYPE_TX);
}
#endif

bool mps3_uart_init(mdh_serial_t **serial, struct uart_cmsdk_dev_t *cmsdk_instance)
{
    priv_mps3_uart_t *priv_uart = get_priv_mps3_uart_instance(cmsdk_instance);

    if (NULL == priv_uart) {
        return false;
    }

    *serial = &(priv_uart->cmsdk_uart.serial);

    NVIC_DisableIRQ(priv_uart->rx_irq_number);
    NVIC_DisableIRQ(priv_uart->tx_irq_number);

    pin_set_function(priv_uart->tx, ALTERNATE_FUNC);
    pin_set_function(priv_uart->rx, ALTERNATE_FUNC);

    if (UART_CMSDK_ERR_NONE != uart_cmsdk_init(priv_uart->cmsdk_uart.device, PERIPHERAL_CLOCK_HZ)) {
        return false;
    };

    // Clear any byte already in the buffer
    uint8_t dummy = 0;
    (void)uart_cmsdk_read(priv_uart->cmsdk_uart.device, &dummy);

    NVIC_EnableIRQ(priv_uart->rx_irq_number);
    NVIC_EnableIRQ(priv_uart->tx_irq_number);

    return true;
}

bool mps3_uart_deinit(struct uart_cmsdk_dev_t *cmsdk_instance)
{
    priv_mps3_uart_t *priv_uart = get_priv_mps3_uart_instance(cmsdk_instance);

    if (NULL == priv_uart) {
        return false;
    }

    NVIC_DisableIRQ(priv_uart->rx_irq_number);
    NVIC_DisableIRQ(priv_uart->tx_irq_number);
    uart_cmsdk_uninit(priv_uart->cmsdk_uart.device);

    return true;
}

PinName mps3_uart_get_tx_pin(const struct uart_cmsdk_dev_t *cmsdk_instance)
{
#ifdef UART0_CMSDK_NS
    if (cmsdk_instance == &UART0_CMSDK_DEV_NS) {
        return gs_priv_uart_0.tx;
    }
#endif // UART0_CMSDK_NS

#ifdef UART1_CMSDK_NS
    if (cmsdk_instance == &UART1_CMSDK_DEV_NS) {
        return gs_priv_uart_1.tx;
    }
#endif // UART1_CMSDK_NS

#ifdef UART2_CMSDK_NS
    if (cmsdk_instance == &UART2_CMSDK_DEV_NS) {
        return gs_priv_uart_2.tx;
    }
#endif // UART2_CMSDK_NS

#ifdef UART3_CMSDK_NS
    if (cmsdk_instance == &UART3_CMSDK_DEV_NS) {
        return gs_priv_uart_3.tx;
    }
#endif // UART3_CMSDK_NS

#ifdef UART4_CMSDK_NS
    if (cmsdk_instance == &UART4_CMSDK_DEV_NS) {
        return gs_priv_uart_4.tx;
    }
#endif // UART4_CMSDK_NS

#ifdef UART5_CMSDK_NS
    if (cmsdk_instance == &UART5_CMSDK_DEV_NS) {
        return gs_priv_uart_5.tx;
    }
#endif // UART5_CMSDK_NS

    return NC;
}

PinName mps3_uart_get_rx_pin(const struct uart_cmsdk_dev_t *cmsdk_instance)
{
#ifdef UART0_CMSDK_NS
    if (cmsdk_instance == &UART0_CMSDK_DEV_NS) {
        return gs_priv_uart_0.rx;
    }
#endif // UART0_CMSDK_NS

#ifdef UART1_CMSDK_NS
    if (cmsdk_instance == &UART1_CMSDK_DEV_NS) {
        return gs_priv_uart_1.rx;
    }
#endif // UART1_CMSDK_NS

#ifdef UART2_CMSDK_NS
    if (cmsdk_instance == &UART2_CMSDK_DEV_NS) {
        return gs_priv_uart_2.rx;
    }
#endif // UART2_CMSDK_NS

#ifdef UART3_CMSDK_NS
    if (cmsdk_instance == &UART3_CMSDK_DEV_NS) {
        return gs_priv_uart_3.rx;
    }
#endif // UART3_CMSDK_NS

#ifdef UART4_CMSDK_NS
    if (cmsdk_instance == &UART4_CMSDK_DEV_NS) {
        return gs_priv_uart_4.rx;
    }
#endif // UART4_CMSDK_NS

#ifdef UART5_CMSDK_NS
    if (cmsdk_instance == &UART5_CMSDK_DEV_NS) {
        return gs_priv_uart_5.rx;
    }
#endif // UART5_CMSDK_NS

    return NC;
}
