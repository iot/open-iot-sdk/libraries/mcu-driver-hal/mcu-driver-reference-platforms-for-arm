/* Copyright (c) 2021-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal/flash_api.h"

#include "flash_layout.h"

#include <stdbool.h>
#include <string.h>

// The V2M-MPS3 does not have a physical flash memory,
// the implementation emulates flash over SRAM.

#define FLASH_PAGE_SIZE   256
#define FLASH_END_ADDRESS (FLASH_BASE_ADDRESS + FLASH_TOTAL_SIZE)

static int32_t erase_sector(mdh_flash_t *self, uint32_t sector_start_addr);
static int32_t read(mdh_flash_t *self, uint32_t address, uint8_t *data, uint32_t size);
static int32_t program_page(mdh_flash_t *self, uint32_t page_start_addr, const uint8_t *data, uint32_t size);
static uint32_t get_sector_size(const mdh_flash_t *self, uint32_t sector_start_addr);
static uint32_t get_page_size(const mdh_flash_t *self);
static uint32_t get_start_address(const mdh_flash_t *self);
static uint32_t get_size(const mdh_flash_t *self);
static uint8_t get_erase_value(const mdh_flash_t *self);

static const mdh_flash_vtable_t gsc_vtable = {.erase_sector = erase_sector,
                                              .read = read,
                                              .program_page = program_page,
                                              .get_sector_size = get_sector_size,
                                              .get_page_size = get_page_size,
                                              .get_start_address = get_start_address,
                                              .get_size = get_size,
                                              .get_erase_value = get_erase_value};

static mdh_flash_t gs_flash = {.vfptr = &gsc_vtable};

static bool is_address_invalid(uint32_t address)
{
    return !((FLASH_BASE_ADDRESS <= address) && (address < FLASH_END_ADDRESS));
}

static bool is_memory_out_of_bound(uint32_t address, uint32_t size)
{
    return (address + size) > FLASH_END_ADDRESS;
}

static int32_t erase_sector(mdh_flash_t *self, uint32_t sector_start_addr)
{
    (void)self;

    if (is_address_invalid(sector_start_addr)) {
        return -1;
    }

    memset((void *)sector_start_addr, 0xFFU, FLASH_AREA_IMAGE_SECTOR_SIZE);
    return 0;
}

static int32_t read(mdh_flash_t *self, uint32_t address, uint8_t *data, uint32_t size)
{
    (void)self;

    if (is_address_invalid(address) || is_memory_out_of_bound(address, size)) {
        return -1;
    }

    memcpy(data, (void *)address, size);

    return 0;
}

static int32_t program_page(mdh_flash_t *self, uint32_t page_start_addr, const uint8_t *data, uint32_t size)
{
    (void)self;

    if (is_address_invalid(page_start_addr) || is_memory_out_of_bound(page_start_addr, size)) {
        return -1;
    }

    memcpy((void *)page_start_addr, data, size);

    return 0;
}

static uint32_t get_sector_size(const mdh_flash_t *self, uint32_t sector_start_addr)
{
    (void)self;

    if (is_address_invalid(sector_start_addr)) {
        return MDH_FLASH_INVALID_SIZE;
    }

    return FLASH_AREA_IMAGE_SECTOR_SIZE;
}

static uint32_t get_page_size(const mdh_flash_t *self)
{
    (void)self;
    return FLASH_PAGE_SIZE;
}

static uint32_t get_start_address(const mdh_flash_t *self)
{
    (void)self;
    return FLASH_BASE_ADDRESS;
}

static uint32_t get_size(const mdh_flash_t *self)
{
    (void)self;
    return FLASH_TOTAL_SIZE;
}

static uint8_t get_erase_value(const mdh_flash_t *self)
{
    (void)self;
    return 0xFF;
}

mdh_flash_t *get_ram_drive_instance(void)
{
    return &gs_flash;
}
