/* Copyright (c) 2017-2022 Arm Limited
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Supports the High-resolution Ticker for mbed by implementing
 * \ref us_ticker_api.h, using a CMSDK Timer \ref timer_cmsdk_dev_t.
 */

#include "hal/lp_ticker_api.h"
#include "hal/us_ticker_api.h"
#include CMSIS_device_header
#include "device.h"
#include "device_cfg.h"
#include "hal-toolbox/container_of.h"
#include "syscounter_armv8-m_cntrl_drv.h"
#include "systimer_armv8-m_drv.h"
#include "ticker_cs300.h"

/* Generic helper functions. */
static void ticker_init(struct systimer_armv8_m_dev_t *timer, IRQn_Type irq);
static void ticker_free(struct systimer_armv8_m_dev_t *timer, IRQn_Type irq);
static uint32_t ticker_read(struct systimer_armv8_m_dev_t *timer);
static void ticker_set_interrupt(struct systimer_armv8_m_dev_t *timer, timestamp_t timestamp);
static void ticker_clear_interrupt(struct systimer_armv8_m_dev_t *timer);

/* usec ticker functions. */
static uint32_t us_ticker_read(mdh_ticker_t *self);
static void us_ticker_set_interrupt(mdh_ticker_t *self, timestamp_t timestamp);
static void us_ticker_disable_interrupt(mdh_ticker_t *self);
static void us_ticker_clear_interrupt(mdh_ticker_t *self);
static void us_ticker_fire_interrupt(mdh_ticker_t *self);
static ticker_irq_callback_f us_ticker_set_irq_callback(mdh_ticker_t *self, ticker_irq_callback_f callback);
static const ticker_info_t *us_ticker_get_info(mdh_ticker_t *self);

/* lp ticker functions. */
static uint32_t lp_ticker_read(mdh_ticker_t *self);
static void lp_ticker_set_interrupt(mdh_ticker_t *self, timestamp_t timestamp);
static void lp_ticker_disable_interrupt(mdh_ticker_t *self);
static void lp_ticker_clear_interrupt(mdh_ticker_t *self);
static void lp_ticker_fire_interrupt(mdh_ticker_t *self);
static ticker_irq_callback_f lp_ticker_set_irq_callback(mdh_ticker_t *self, ticker_irq_callback_f callback);
static const ticker_info_t *lp_ticker_get_info(mdh_ticker_t *self);

static const mdh_ticker_vtable_t gsc_vtable_us_ticker = {
    .read = us_ticker_read,
    .disable_interrupt = us_ticker_disable_interrupt,
    .clear_interrupt = us_ticker_clear_interrupt,
    .set_interrupt = us_ticker_set_interrupt,
    .fire_interrupt = us_ticker_fire_interrupt,
    .set_irq_handler = us_ticker_set_irq_callback,
    .get_info = us_ticker_get_info,
};

static cs300_us_ticker_t gs_us_ticker_instance = {
    .ticker.vtable = &gsc_vtable_us_ticker,
    .inst = &USEC_TIMER_DEV,
};

static ticker_irq_callback_f us_ticker_callback;

static const mdh_ticker_vtable_t gsc_vtable_lp_ticker = {
    .read = lp_ticker_read,
    .disable_interrupt = lp_ticker_disable_interrupt,
    .clear_interrupt = lp_ticker_clear_interrupt,
    .set_interrupt = lp_ticker_set_interrupt,
    .fire_interrupt = lp_ticker_fire_interrupt,
    .set_irq_handler = lp_ticker_set_irq_callback,
    .get_info = lp_ticker_get_info,
};

static cs300_lp_ticker_t gs_lp_ticker_instance = {
    .ticker.vtable = &gsc_vtable_lp_ticker,
    .inst = &LP_TIMER_DEV,
};

static ticker_irq_callback_f lp_ticker_callback;

static void ticker_init(struct systimer_armv8_m_dev_t *timer, IRQn_Type irq)
{
    NVIC_DisableIRQ(irq);
    systimer_armv8_m_disable_timer(timer);
    systimer_armv8_m_disable_interrupt(timer);

    /* Only initialize the SYSCOUNTER if we are not running in NS mode, as
     * indicated by the DOMAIN_NS preprocessor define (as set by the
     * application or library consuming the HAL). NS mode does not have write
     * access to the SYSCNTR register (SYSCNTR_CNTRL_BASE_S), and only has read
     * access to some of the bits of the SYSCNTR register via the
     * SYSCNTR_READ_BASE_NS alias. This avoids a secure fault when using ticker
     * from NS code. */
#if !defined(DOMAIN_NS) || DOMAIN_NS == 0
    syscounter_armv8_m_cntrl_init(&SYSCOUNTER_CNTRL_ARMV8_M_DEV);
#endif
    systimer_armv8_m_init(timer);
    NVIC_EnableIRQ(irq);
}
static void ticker_free(struct systimer_armv8_m_dev_t *timer, IRQn_Type irq)
{
    NVIC_DisableIRQ(irq);
    systimer_armv8_m_disable_interrupt(timer);
    systimer_armv8_m_uninit(timer);
}

static uint32_t ticker_read(struct systimer_armv8_m_dev_t *timer)
{
    return (uint32_t)systimer_armv8_m_get_counter_value(timer);
}

static void ticker_set_interrupt(struct systimer_armv8_m_dev_t *timer, timestamp_t timestamp)
{
    if (timestamp < ticker_read(timer)) {
        return;
    }
    systimer_armv8_m_disable_timer(timer);
    systimer_armv8_m_set_timer_value(timer, (uint32_t)timestamp - ticker_read(timer));
    systimer_armv8_m_enable_interrupt(timer);
    systimer_armv8_m_enable_timer(timer);
}
static void ticker_clear_interrupt(struct systimer_armv8_m_dev_t *timer)
{
    /* Disabling timer clears interrupt */
    systimer_armv8_m_disable_timer(timer);
    systimer_armv8_m_disable_interrupt(timer);
    systimer_armv8_m_enable_timer(timer);
}

static uint32_t us_ticker_read(mdh_ticker_t *self)
{
    cs300_us_ticker_t *ticker = container_of(self, cs300_us_ticker_t, ticker);

    return ticker_read(ticker->inst);
}

static void us_ticker_set_interrupt(mdh_ticker_t *self, timestamp_t timestamp)
{
    cs300_us_ticker_t *ticker = container_of(self, cs300_us_ticker_t, ticker);

    ticker_set_interrupt(ticker->inst, timestamp);
}

static void us_ticker_disable_interrupt(mdh_ticker_t *self)
{
    cs300_us_ticker_t *ticker = container_of(self, cs300_us_ticker_t, ticker);

    systimer_armv8_m_disable_interrupt(ticker->inst);
}

static void us_ticker_clear_interrupt(mdh_ticker_t *self)
{
    cs300_us_ticker_t *ticker = container_of(self, cs300_us_ticker_t, ticker);

    ticker_clear_interrupt(ticker->inst);
}

static void us_ticker_fire_interrupt(mdh_ticker_t *self)
{
    NVIC_SetPendingIRQ(USEC_INTERVAL_IRQ);
}

static ticker_irq_callback_f us_ticker_set_irq_callback(mdh_ticker_t *self, ticker_irq_callback_f callback)
{
    ticker_irq_callback_f tmp = us_ticker_callback;
    us_ticker_callback = callback;
    return tmp;
}

static const ticker_info_t *us_ticker_get_info(mdh_ticker_t *self)
{
    static const ticker_info_t info = {USEC_TIMER_FREQ_HZ, USEC_REPORTED_BITS};
    return &info;
}

void cs300_us_ticker_init(mdh_ticker_t *self)
{
    cs300_us_ticker_t *ticker = container_of(self, cs300_us_ticker_t, ticker);

    ticker_init(ticker->inst, USEC_INTERVAL_IRQ);
}

void cs300_us_ticker_deinit(mdh_ticker_t *self)
{
    cs300_us_ticker_t *ticker = container_of(self, cs300_us_ticker_t, ticker);

    ticker_free(ticker->inst, USEC_INTERVAL_IRQ);
}

static uint32_t lp_ticker_read(mdh_ticker_t *self)
{
    cs300_lp_ticker_t *ticker = container_of(self, cs300_lp_ticker_t, ticker);

    return ticker_read(ticker->inst);
}

static void lp_ticker_set_interrupt(mdh_ticker_t *self, timestamp_t timestamp)
{
    cs300_lp_ticker_t *ticker = container_of(self, cs300_lp_ticker_t, ticker);

    ticker_set_interrupt(ticker->inst, timestamp);
}

static void lp_ticker_disable_interrupt(mdh_ticker_t *self)
{
    cs300_lp_ticker_t *ticker = container_of(self, cs300_lp_ticker_t, ticker);

    systimer_armv8_m_disable_interrupt(ticker->inst);
}

static void lp_ticker_clear_interrupt(mdh_ticker_t *self)
{
    cs300_lp_ticker_t *ticker = container_of(self, cs300_lp_ticker_t, ticker);

    ticker_clear_interrupt(ticker->inst);
}

static void lp_ticker_fire_interrupt(mdh_ticker_t *self)
{
    NVIC_SetPendingIRQ(LP_INTERVAL_IRQ);
}

static ticker_irq_callback_f lp_ticker_set_irq_callback(mdh_ticker_t *self, ticker_irq_callback_f callback)
{
    ticker_irq_callback_f tmp = lp_ticker_callback;
    lp_ticker_callback = callback;
    return tmp;
}

static const ticker_info_t *lp_ticker_get_info(mdh_ticker_t *self)
{
    static const ticker_info_t info = {LP_REPORTED_FREQ_HZ, LP_REPORTED_BITS};
    return &info;
}

mdh_ticker_t *cs300_us_ticker_get_default_instance(void)
{
    return &(gs_us_ticker_instance.ticker);
}

mdh_ticker_t *cs300_lp_ticker_get_default_instance(void)
{
    return &(gs_lp_ticker_instance.ticker);
}

#ifndef usec_interval_irq_handler
#error "usec_interval_irq_handler should be defined, check device_cfg.h!"
#endif
void usec_interval_irq_handler(void)
{
    us_ticker_clear_interrupt(cs300_us_ticker_get_default_instance());
    us_ticker_disable_interrupt(cs300_us_ticker_get_default_instance());

    if (us_ticker_callback)
        us_ticker_callback(cs300_us_ticker_get_default_instance());
}

#ifndef lp_interval_irq_handler
#error "lp_interval_irq_handler should be defined, check device_cfg.h!"
#endif
void lp_interval_irq_handler(void)
{
    lp_ticker_clear_interrupt(cs300_lp_ticker_get_default_instance());
    lp_ticker_disable_interrupt(cs300_lp_ticker_get_default_instance());

    if (lp_ticker_callback)
        lp_ticker_callback(cs300_lp_ticker_get_default_instance());
}

void cs300_lp_ticker_init(mdh_ticker_t *self)
{
    cs300_lp_ticker_t *ticker = container_of(self, cs300_lp_ticker_t, ticker);

    ticker_init(ticker->inst, LP_INTERVAL_IRQ);
}

void cs300_lp_ticker_deinit(mdh_ticker_t *self)
{
    cs300_lp_ticker_t *ticker = container_of(self, cs300_lp_ticker_t, ticker);

    ticker_free(ticker->inst, LP_INTERVAL_IRQ);
}
