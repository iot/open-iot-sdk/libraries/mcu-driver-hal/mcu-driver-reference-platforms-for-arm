/* Copyright (c) 2021-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mps3_i2s.h"

#include "hal/sai_api.h"
#include "hal/us_ticker_api.h"
#include "audio_i2s_mps3_drv.h"
#include CMSIS_device_header
#include "hal-toolbox/us_ticker_util.h"
#include "platform_base_address.h"
#include "ticker_cs300.h"

#ifndef container_of
// Returns a pointer to the `type` containing the `member` pointed by `ptr`;
#define container_of(ptr, type, member)                    \
    ({                                                     \
        const typeof(((type *)0)->member) *__mptr = (ptr); \
        (type *)((char *)__mptr - offsetof(type, member)); \
    })
#endif

// Private type
struct priv_mps3_i2s_s {
    mps3_i2s_t mps3_i2s;

    mdh_sai_event_f on_event;
    mdh_sai_transfer_complete_f on_complete;
    void *context;

    volatile uint8_t *buffer;
    volatile uint32_t len;
};

// Private function declarations
static mdh_sai_status_t mps3_i2s_sai_set_transfer_complete_callback(mdh_sai_t *self, mdh_sai_transfer_complete_f cbk);
static mdh_sai_status_t mps3_i2s_sai_set_event_callback(mdh_sai_t *self, mdh_sai_event_f cbk);
static mdh_sai_status_t mps3_i2s_sai_transfer(mdh_sai_t *self, uint8_t *samples, uint32_t len, void *ctx);
static mdh_sai_status_t mps3_i2s_sai_cancel_transfer(mdh_sai_t *self);

// Private variables & constants
static const mdh_sai_vtable_t gsc_mps3_sai_vtable = {.set_transfer_complete_callback =
                                                         mps3_i2s_sai_set_transfer_complete_callback,
                                                     .set_event_callback = mps3_i2s_sai_set_event_callback,
                                                     .transfer = mps3_i2s_sai_transfer,
                                                     .cancel_transfer = mps3_i2s_sai_cancel_transfer};

static struct priv_mps3_i2s_s gs_priv_sai_instances[2] = {
    [0] = {.mps3_i2s.sai.vtable = &gsc_mps3_sai_vtable}, [1] = {.mps3_i2s.sai.vtable = &gsc_mps3_sai_vtable}};

static const struct audio_i2s_mps3_dev_cfg_t gsc_audio_device_config = {.base = FPGA_I2S_BASE_NS};

static struct audio_i2s_mps3_dev_t gs_audio_device = {.cfg = &gsc_audio_device_config};

// Private function definitions

// Interface implementation
static mdh_sai_status_t mps3_i2s_sai_set_transfer_complete_callback(mdh_sai_t *self, mdh_sai_transfer_complete_f cbk)
{
    struct priv_mps3_i2s_s *that = container_of(self, struct priv_mps3_i2s_s, mps3_i2s.sai);
    if (that->len != 0) {
        return MDH_SAI_STATUS_BUSY;
    }
    that->on_complete = cbk;
    return MDH_SAI_STATUS_NO_ERROR;
}

static mdh_sai_status_t mps3_i2s_sai_set_event_callback(mdh_sai_t *self, mdh_sai_event_f cbk)
{
    struct priv_mps3_i2s_s *that = container_of(self, struct priv_mps3_i2s_s, mps3_i2s.sai);
    if (that->len != 0) {
        return MDH_SAI_STATUS_BUSY;
    }
    that->on_event = cbk;
    return MDH_SAI_STATUS_NO_ERROR;
}

static mdh_sai_status_t mps3_i2s_sai_transfer(mdh_sai_t *self, uint8_t *samples, uint32_t len, void *ctx)
{
    struct priv_mps3_i2s_s *that = container_of(self, struct priv_mps3_i2s_s, mps3_i2s.sai);

    if (((len % 4) != 0) || (0 != that->len)) {
        return MDH_SAI_STATUS_BUSY;
    }

    that->buffer = samples;
    that->len = len;
    that->context = ctx;

    if (that == gs_priv_sai_instances) {
        audio_i2s_mps3_enable_txinterrupt(&gs_audio_device);
        audio_i2s_mps3_enable_txbuf(&gs_audio_device);
    } else {
        audio_i2s_mps3_enable_rxinterrupt(&gs_audio_device);
        audio_i2s_mps3_enable_rxbuf(&gs_audio_device);
    }

    return MDH_SAI_STATUS_NO_ERROR;
}

static mdh_sai_status_t mps3_i2s_sai_cancel_transfer(mdh_sai_t *self)
{
    struct priv_mps3_i2s_s *that = container_of(self, struct priv_mps3_i2s_s, mps3_i2s.sai);

    if (that == gs_priv_sai_instances) {
        audio_i2s_mps3_disable_txbuf(&gs_audio_device);
        audio_i2s_mps3_disable_txinterrupt(&gs_audio_device);
    } else {
        audio_i2s_mps3_disable_rxbuf(&gs_audio_device);
        audio_i2s_mps3_disable_rxinterrupt(&gs_audio_device);
    }

    if (0 != that->len) {
        that->len = 0;

        if (NULL != that->on_complete) {
            that->on_complete(&that->mps3_i2s.sai, that->context, MDH_SAI_TRANSFER_COMPLETE_CANCELLED);
        }
    }

    return MDH_SAI_STATUS_NO_ERROR;
}

// inherent methods.

// Flush the input buffer
static void mps3_flush_input(void)
{
    audio_i2s_mps3_disable_rxinterrupt(&gs_audio_device);
    audio_i2s_mps3_disable_rxbuf(&gs_audio_device);
    // flush reception
    while (!audio_i2s_mps3_is_rx_buffer_empty(&gs_audio_device)) {
        read_sample(&gs_audio_device);
    }
}

// Public function definitions
// IRQ Handler
void I2S_Handler(void)
{
    NVIC_ClearPendingIRQ(I2S_IRQn);

    struct priv_mps3_i2s_s *tx = &gs_priv_sai_instances[0];
    if (0 == tx->len) {
        audio_i2s_mps3_disable_txinterrupt(&gs_audio_device);
        if (NULL != tx->on_complete) {
            tx->on_complete(&tx->mps3_i2s.sai, tx->context, MDH_SAI_TRANSFER_COMPLETE_DONE);
        }
    }

    if (0 != tx->len) {
        uint16_t *samples = (uint16_t *)tx->buffer;
        while ((tx->len != 0) && !audio_i2s_mps3_is_tx_buffer_full(&gs_audio_device)) {
            struct audio_i2s_mps3_sample_t sample = {0};
            // The CS42L52 expects the signal polarity to be inverted
            sample.left_channel = samples[0] ^ 0x8000;
            sample.right_channel = samples[1] ^ 0x8000;
            samples += 2;
            tx->len -= 4;
            write_sample(&gs_audio_device, sample);
        }
        tx->buffer = (uint8_t *)samples;
    } else {
        // flush the buffer before disabling the peripheral
        while (!audio_i2s_mps3_is_tx_buffer_empty(&gs_audio_device)) {
        }
        audio_i2s_mps3_disable_txbuf(&gs_audio_device);
    }

    struct priv_mps3_i2s_s *rx = &gs_priv_sai_instances[1];
    uint16_t *samples = (uint16_t *)rx->buffer;
    bool has_received_data = (0 != rx->len) && !audio_i2s_mps3_is_rx_buffer_empty(&gs_audio_device);
    while ((0 != rx->len) && !audio_i2s_mps3_is_rx_buffer_empty(&gs_audio_device)) {
        struct audio_i2s_mps3_sample_t sample = read_sample(&gs_audio_device);
        samples[0] = sample.left_channel;
        samples[1] = sample.right_channel;
        samples += 2;
        rx->len -= 4;
    }
    rx->buffer = (uint8_t *)samples;

    if (0 == rx->len) {
        if (has_received_data) {
            mps3_flush_input();
            if (NULL != rx->on_complete) {
                rx->on_complete(&rx->mps3_i2s.sai, rx->context, MDH_SAI_TRANSFER_COMPLETE_DONE);
            }
        } else if (!audio_i2s_mps3_is_rx_buffer_empty(&gs_audio_device)) {
            mps3_flush_input();
            if (NULL != rx->on_event) {
                rx->on_event(&rx->mps3_i2s.sai, MDH_SAI_EVENT_OVERRUN);
            }
        }
    }
}

void mps3_i2s_init(mps3_i2s_t **tx, mps3_i2s_t **rx, uint32_t sample_rate)
{
    audio_i2s_mps3_set_codec_reset(&gs_audio_device);
    audio_i2s_mps3_set_fifo_reset(&gs_audio_device);

    audio_i2s_mps3_disable_txinterrupt(&gs_audio_device);
    audio_i2s_mps3_disable_txbuf(&gs_audio_device);
    audio_i2s_mps3_disable_rxinterrupt(&gs_audio_device);
    audio_i2s_mps3_disable_rxbuf(&gs_audio_device);

    if (NULL != tx) {
        *tx = &gs_priv_sai_instances[0].mps3_i2s;
    }
    if (NULL != rx) {
        *rx = &gs_priv_sai_instances[1].mps3_i2s;
    }

    NVIC_ClearPendingIRQ(I2S_IRQn);
    NVIC_EnableIRQ(I2S_IRQn);

    audio_i2s_mps3_clear_codec_reset(&gs_audio_device);

    us_ticker_util_wait(cs300_us_ticker_get_default_instance(), 100 * 1000);

    uint32_t divide_ratio = 3072000 / (sample_rate * 2);
    audio_i2s_mps3_speed_config(&gs_audio_device, divide_ratio);
}

void mps3_i2s_unlock(void)
{
    audio_i2s_mps3_clear_fifo_reset(&gs_audio_device);
}

void mps3_i2s_uninit(void)
{
    NVIC_DisableIRQ(I2S_IRQn);

    audio_i2s_mps3_set_codec_reset(&gs_audio_device);
    audio_i2s_mps3_set_fifo_reset(&gs_audio_device);

    audio_i2s_mps3_disable_rxbuf(&gs_audio_device);
    audio_i2s_mps3_disable_txbuf(&gs_audio_device);
    audio_i2s_mps3_disable_rxinterrupt(&gs_audio_device);
    audio_i2s_mps3_disable_txinterrupt(&gs_audio_device);
}
