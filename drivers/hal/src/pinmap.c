/* Copyright (c) 2017-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "pinmap.h"

#include "PinNames.h"
#include "gpio_cmsdk_drv.h"
#include "gpio_objects.h"
#include "objects.h"
#include "platform/error_handler.h"

void pin_set_function(PinName pin, PinFunction function)
{
    if (pin == NC) {
        halt_on_error("Cannot assigned a function to an unconnected pin");
    }

    /* The pin has to be a GPIO pin */
    if ((pin < EXP0) || (pin > EXP51)) {
        return;
    }

    struct gpio_cmsdk_dev_t *gpio_dev;
    enum gpio_cmsdk_altfunc_t altfunc_flags;

    // Convert pin function integer to pin function type
    switch (function) {
        case ALTERNATE_FUNC:
            altfunc_flags = GPIO_CMSDK_ALT_FUNC;
            break;
        case GPIO_FUNC:
            altfunc_flags = GPIO_CMSDK_MAIN_FUNC;
            break;
        default:
            return;
    }

    // Get the GPIO port the pin belongs to
    switch (GPIO_DEV_NUMBER(pin)) {
#ifdef GPIO0_CMSDK_NS
        case GPIO0_NUMBER:
            gpio_dev = &GPIO0_CMSDK_DEV;
            break;
#endif
#ifdef GPIO1_CMSDK_NS
        case GPIO1_NUMBER:
            gpio_dev = &GPIO1_CMSDK_DEV;
            break;
#endif
#ifdef GPIO2_CMSDK_NS
        case GPIO2_NUMBER:
            gpio_dev = &GPIO2_CMSDK_DEV;
            break;
#endif
        default: {
            halt_on_error("GPIO %d, associated with expansion pin %d, is disabled", pin, GPIO_DEV_NUMBER(pin));
            return;
        }
    }

    gpio_cmsdk_init(gpio_dev);
    // The direction does not matter when not using the main gpio function
    // Therefore passing `0` as it is the reset value of the register.
    (void)gpio_cmsdk_pin_config(gpio_dev, GPIO_PIN_NUMBER(pin), (enum gpio_cmsdk_direction_t)0, altfunc_flags);
}
