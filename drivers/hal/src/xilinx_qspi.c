/* Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "xilinx_qspi.h"

#include "hal/qspi_api.h"
#include "PeripheralNames.h"
#include "PinNames.h"
#include "objects.h"

#include <stdint.h>
#include <string.h>

#ifndef container_of
// Returns a pointer to the `type` containing the `member` pointed by `ptr`;
#define container_of(ptr, type, member)                    \
    ({                                                     \
        const typeof(((type *)0)->member) *__mptr = (ptr); \
        (type *)((char *)__mptr - offsetof(type, member)); \
    })
#endif

typedef struct priv_xilinx_qspi_t {
    xilinx_qspi_t xilinx_qspi;
    qspi_xilinx_dev_t *device;
} priv_xilinx_qspi_t;

static priv_xilinx_qspi_t *get_priv_xilinx_qspi_instance(const qspi_xilinx_dev_t *qspi_instance);
static mdh_qspi_status_t translate_status_native2hal(qspi_xilinx_error_t native_driver_status);
static mdh_qspi_status_t populate_transaction(qspi_xilinx_transaction_t *const transaction,
                                              const mdh_qspi_command_t *command,
                                              const void *tx_data,
                                              size_t tx_size,
                                              void *rx_data,
                                              size_t rx_size);
static mdh_qspi_status_t set_mode(mdh_qspi_t *self, uint8_t mode);
static mdh_qspi_status_t set_frequency(mdh_qspi_t *self, uint32_t hz);
static mdh_qspi_status_t _write(mdh_qspi_t *self, const mdh_qspi_command_t *command, const void *data, size_t *length);
static mdh_qspi_status_t _read(mdh_qspi_t *self, const mdh_qspi_command_t *command, void *data, size_t *length);
static mdh_qspi_status_t transfer(mdh_qspi_t *self,
                                  const mdh_qspi_command_t *command,
                                  const void *tx_data,
                                  size_t tx_size,
                                  void *rx_data,
                                  size_t rx_size);
static const mdh_qspi_vtable_t gsc_vtable = {
    .set_mode = set_mode, .set_frequency = set_frequency, .write = _write, .read = _read, .transfer = transfer};

static priv_xilinx_qspi_t gs_priv_qspi = {.xilinx_qspi.qspi.vfptr = &gsc_vtable, .device = &QSPI_WRITE_DEV};

mdh_qspi_status_t xilinx_qspi_init(xilinx_qspi_t **qspi, qspi_xilinx_dev_t *qspi_instance)
{
    priv_xilinx_qspi_t *priv_qspi = get_priv_xilinx_qspi_instance(qspi_instance);
    if (NULL == priv_qspi) {
        return MDH_QSPI_STATUS_INVALID_PARAMETER;
    }

    *qspi = &(priv_qspi->xilinx_qspi);

    qspi_xilinx_error_t native_driver_status = qspi_xilinx_init(priv_qspi->device);
    if (native_driver_status != QSPI_XILINX_ERR_NONE) {
        return translate_status_native2hal(native_driver_status);
    }

    return MDH_QSPI_STATUS_OK;
}

mdh_qspi_status_t xilinx_qspi_deinit(qspi_xilinx_dev_t *qspi_instance)
{
    priv_xilinx_qspi_t *priv_qspi = get_priv_xilinx_qspi_instance(qspi_instance);
    if (NULL == priv_qspi) {
        return MDH_QSPI_STATUS_INVALID_PARAMETER;
    }

    return translate_status_native2hal(qspi_xilinx_deinit(priv_qspi->device));
}

static mdh_qspi_status_t set_mode(mdh_qspi_t *self, uint8_t mode)
{
    priv_xilinx_qspi_t *that = container_of(self, priv_xilinx_qspi_t, xilinx_qspi.qspi);

    qspi_xilinx_error_t native_driver_status = qspi_xilinx_set_clock_mode(that->device, (qspi_xilinx_clock_mode_t)mode);
    if (native_driver_status != QSPI_XILINX_ERR_NONE) {
        return translate_status_native2hal(native_driver_status);
    }

    return MDH_QSPI_STATUS_OK;
}

static mdh_qspi_status_t set_frequency(mdh_qspi_t *self, uint32_t hz)
{
    return (hz == QSPI_FIXED_FREQUENCY_HZ) ? MDH_QSPI_STATUS_OK : MDH_QSPI_STATUS_INVALID_PARAMETER;
}

static mdh_qspi_status_t _write(mdh_qspi_t *self, const mdh_qspi_command_t *command, const void *data, size_t *length)
{
    qspi_xilinx_error_t native_driver_status;
    mdh_qspi_status_t hal_status;

    qspi_xilinx_transaction_t transaction = {0};
    hal_status = populate_transaction(&transaction, command, data, *length, NULL, 0U);
    if (hal_status != MDH_QSPI_STATUS_OK) {
        return hal_status;
    }

    priv_xilinx_qspi_t *that = container_of(self, priv_xilinx_qspi_t, xilinx_qspi.qspi);

    native_driver_status = qspi_xilinx_execute_transaction(that->device, &transaction);
    if (native_driver_status != QSPI_XILINX_ERR_NONE) {
        return translate_status_native2hal(native_driver_status);
    }

    return MDH_QSPI_STATUS_OK;
}

static mdh_qspi_status_t _read(mdh_qspi_t *self, const mdh_qspi_command_t *command, void *data, size_t *length)
{
    qspi_xilinx_error_t native_driver_status;
    mdh_qspi_status_t hal_status;

    qspi_xilinx_transaction_t transaction = {0};
    hal_status = populate_transaction(&transaction, command, NULL, 0U, data, *length);
    if (hal_status != MDH_QSPI_STATUS_OK) {
        return hal_status;
    }

    priv_xilinx_qspi_t *that = container_of(self, priv_xilinx_qspi_t, xilinx_qspi.qspi);

    native_driver_status = qspi_xilinx_execute_transaction(that->device, &transaction);
    if (native_driver_status != QSPI_XILINX_ERR_NONE) {
        return translate_status_native2hal(native_driver_status);
    }

    return MDH_QSPI_STATUS_OK;
}

static mdh_qspi_status_t transfer(mdh_qspi_t *self,
                                  const mdh_qspi_command_t *command,
                                  const void *tx_data,
                                  size_t tx_size,
                                  void *rx_data,
                                  size_t rx_size)
{
    qspi_xilinx_error_t native_driver_status;
    mdh_qspi_status_t hal_status;

    qspi_xilinx_transaction_t transaction = {0};
    hal_status = populate_transaction(&transaction, command, tx_data, tx_size, rx_data, rx_size);
    if (hal_status != MDH_QSPI_STATUS_OK) {
        return hal_status;
    }

    priv_xilinx_qspi_t *that = container_of(self, priv_xilinx_qspi_t, xilinx_qspi.qspi);

    native_driver_status = qspi_xilinx_execute_transaction(that->device, &transaction);
    if (native_driver_status != QSPI_XILINX_ERR_NONE) {
        return translate_status_native2hal(native_driver_status);
    }

    return MDH_QSPI_STATUS_OK;
}

/** Translate a native driver status to HAL API status.
 *
 * @param native_driver_status An error code specific to the Xilinx QSPI controller driver.
 * @return A QSPI HAL API error code.
 */
static mdh_qspi_status_t translate_status_native2hal(qspi_xilinx_error_t native_driver_status)
{
    mdh_qspi_status_t hal_status;
    switch (native_driver_status) {
        case QSPI_XILINX_ERR_NONE:
            hal_status = MDH_QSPI_STATUS_OK;
            break;
        case QSPI_XILINX_ERR_INVALID_ARGS:
            hal_status = MDH_QSPI_STATUS_INVALID_PARAMETER;
            break;
        case QSPI_XILINX_ERR_TRANSACTION:
        case QSPI_XILINX_ERR_COMMAND:
        default:
            hal_status = MDH_QSPI_STATUS_ERROR;
            break;
    }
    return hal_status;
}

/** Populate a driver-specific transaction type.
 *
 * @param transaction A driver-specific transaction struct to set.
 * @param command A QSPI command struct.
 * @param tx_data TX buffer.
 * @param tx_size TX buffer length in bytes.
 * @param rx_data RX buffer.
 * @param rx_size RX buffer length in bytes.
 * @return MDH_QSPI_STATUS_OK if the transaction has been correctly set.
           MDH_QSPI_STATUS_INVALID_PARAMETER if invalid parameter found.
 */
static mdh_qspi_status_t populate_transaction(qspi_xilinx_transaction_t *const transaction,
                                              const mdh_qspi_command_t *command,
                                              const void *tx_data,
                                              size_t tx_size,
                                              void *rx_data,
                                              size_t rx_size)
{
    if ((command->instruction.disabled || command->instruction.bus_width != MDH_QSPI_BUS_WIDTH_SINGLE)
        || (!command->address.disabled && command->address.size != MDH_QSPI_ADDRESS_SIZE_24)
        || (!command->alt.disabled)) {
        return MDH_QSPI_STATUS_INVALID_PARAMETER;
    }

    memset(transaction, 0, sizeof(qspi_xilinx_transaction_t));
    transaction->opcode = command->instruction.value;
    if (!command->address.disabled) {
        transaction->num_address_bytes = 3U;
        transaction->address = command->address.value;
    }
    transaction->num_dummy_bytes = command->dummy_count;
    transaction->data.tx_buff = tx_data;
    transaction->data.tx_buff_size = tx_size;
    transaction->data.rx_buff = rx_data;
    transaction->data.rx_buff_size = rx_size;

    return MDH_QSPI_STATUS_OK;
}

static priv_xilinx_qspi_t *get_priv_xilinx_qspi_instance(const qspi_xilinx_dev_t *qspi_instance)
{
#ifdef QSPI_WRITE_NS
    if (qspi_instance == &QSPI_WRITE_DEV_NS) {
        return &gs_priv_qspi;
    }
#endif
#ifdef QSPI_WRITE_S
    if (qspi_instance == &QSPI_WRITE_DEV_S) {
        return &gs_priv_qspi;
    }
#endif

    return NULL;
}
