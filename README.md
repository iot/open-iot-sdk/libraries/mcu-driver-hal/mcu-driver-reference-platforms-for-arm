# Reference MCU-Driver-HAL driver implementation for Arm platforms

This repository contains the driver implementation for Arm hardware platforms based on [MCU-Driver-HAL](components/mcu-driver-hal).

## Building

```sh
# Configure
cmake -S . -B build --toolchain=components/mcu-driver-hal/tools/cmake/toolchain/toolchain-{armclang|arm-none-eabi-gcc}.cmake -DCMAKE_SYSTEM_PROCESSOR=cortex-m55
# Build
cmake --build build -j
```

You may want to add optional arguments to those lines to customize your build.
For example, the configure stage accepts:

- `-DCMAKE_BUILD_TYPE=Release` will build your applications optimised for size. See
  [here](./components/mcu-driver-hal/tools/cmake/profiles) for more details.
- `-DCMAKE_VERBOSE_MAKEFILE=ON` will make your build tool more verbose, typically printing the
  command lines invoking the compiler.
- `-DCMAKE_EXPORT_COMPILE_COMMANDS=1` generates a build database that can be used by IDEs to provide
  better completion.
- `-DVARIANT=FVP` will choose an FVP build variant instead the default MPS3 FPGA.
- `-DHTRUN_ARGUMENTS="--micro=FVP_CS300_U55;--fm=MPS3"` configures `ctest` and `htrun` to work with the FVP via the `mbed-fastmodel-agent` (if installed).

The build stage can be passed a specific target name after a `--` to build only that target or `clean` to remove the build products (objects and executables). E.g.

```sh
cmake --build build -j -- clean
cmake --build build -j -- mdh-arm-hal-example-serial
```

# Directory Structure

The mcu-driver-reference-platforms-for-arm repository is structured as follows:

| Folders                | Contents                                                                           |
| ---------------------- | ---------------------------------------------------------------------------------- |
| `components/`          | Contains all external components that this repository depends on, including CMSIS, MCU-Driver-HAL and Native drivers |
| `docs/`                | Contains Documentation (building, running, debugging, etc.)                        |
| `drivers/`             | Contains glue layer that binds Native drivers (under `components/`) with MCU-Driver-HAL vtable interface |
| `examples`             | Contains examples                                                                  |
| `platforms/`           | Contains support for individual platforms                     |
| `tests/`               | Contains tests (Greentea)                                                         |

## License and contributions

The software is provided under the [Apache-2.0 license](LICENSE-apache-2.0.txt). All contributions to software and documents are licensed by contributors under the same license model as the software/document itself (ie. inbound == outbound licensing). This project may reuse software already licensed under another license, provided the license is permissive in nature and compatible with Apache v2.0.

Folders containing files under different permissive license than Apache 2.0 are listed in the [LICENSE](LICENSE.md) file.

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for more information.

## Security issues reporting

If you find any security vulnerabilities, please do not report it in the GitLab issue tracker. Instead, send an email to the security team at arm-security@arm.com stating that you may have found a security vulnerability in the Reference MCU-Driver-HAL driver implementation for Arm platforms.

More details can be found at [Arm Developer website](https://developer.arm.com/support/arm-security-updates/report-security-vulnerabilities).
