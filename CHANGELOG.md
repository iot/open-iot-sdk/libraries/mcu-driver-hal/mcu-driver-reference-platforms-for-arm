# v2023.04 (2023-04-13)

## Changes

* cmsis: Use CMSIS_device_header macro instead of cmsis.h to improve code readability.
* Drivers: Sync native drivers with those in internal gerrit repositories.
  Merge updates from both. Following drivers are updated:
  - UART CMSDK
  - SPI pl022
  - I2C
  - MPS3 Audio
  - SMSC9220 Ethernet
* mdh: Update mcu-driver-hal SHA to include refactor within mbed-wait
* tests: Add AN552-specific test setup handler to ensure correct operation of the FPGA CI Test Shield.
* ci: Enable possible tpip violation warning messages in Merge Requests.
* Extract platform dependency from mps3 uart driver.
  New cmsdk uart mdh driver is only dependent on cmsdk uart native driver.
  Platform specific parts (pin init, irq handling) are left in mps3_uart.c.
* platforms: Add basic error handler for each platform.
* cmake: Update mcu-driver-hal reference to remove the deprecated mcu-driver-bootstrap CMake target from dependencies.
* cmake: Update mcu-driver-hal reference to pull the recent changes for FPGA CI Test Shield lib. Provide the required platform-specific handlers for AN552.
* platforms: For each platform add busy wait utils based on the mcu-driver-hal/bootstrap lib.
* platforms: Add missing DEVICE_SERIAL_ASYNCH=0 to solve undefined macro warnings.
* emac: Set length of received frames.


# v2023.01 (2023-01-19)

## Changes

* changelog: Add towncrier news fragments and configuration
* ci: Add public sync from public Gitlab to internal GitLab
* license: Add components information

  LICENSE.md does not only list name of components and their
  licenses but also more detailed information like version,
  url-origin.
* CMake: Replace the MBED_TARGET_DEFINITIONS and MBED_CONFIG_DEFINITIONS custom
  config variables with CMake targets. It's no longer required to specify
  MBED_TARGET_DEFINITIONS or MBED_CONFIG_DEFINITIONS, as mcu-driver-hal will not
  read these anymore.
* clang-format: Enable sorting includes
* ci: Enable autobot
* ethernet: Update variable declaration to the new style to remove build warning


This changelog should be read in conjunction with release notes provided
for a specific release version.
