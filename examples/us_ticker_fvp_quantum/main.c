/* Copyright (c) 2021-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_TRACE_MAX_LEVEL TRACE_LEVEL_DEBUG
#define TRACE_GROUP          "main"

#include "hal/serial_api.h"
#include "hal/us_ticker_api.h"
#include "mbed_atomic/mbed_atomic.h"
#include "mbed_trace/mbed_trace.h"
#include "mps3_uart.h"
#include "ticker.h"

#include <inttypes.h>

#define TIMEOUT_IN_TICKS  1000UL // Ticker interrupt delay; ticks.
#define NUM_STATE_SAMPLES 0xff   // Max number of ticker state samples to record.

typedef struct ticker_state_s {
    uint32_t ticker_counter;
    uint8_t irq_counter;
} ticker_state_t;

static mdh_serial_t *gs_serial = NULL;
static volatile uint8_t gsv_irq_count;
static ticker_state_t gs_ticker_states[NUM_STATE_SAMPLES] = {0};
static size_t gs_num_samples = 0;

static void irq_handler(mdh_ticker_t *us_ticker);
static inline void record_ticker_state(uint32_t ticker_counter, uint8_t irq_counter);
static void dump_recorded_ticker_states(void);
static void serial_print(const char *string);

int main(void)
{
    mdh_ticker_t *us_ticker;

    // Set up serial.
    mps3_uart_init(&gs_serial, &UART0_CMSDK_DEV_NS);
    mdh_serial_set_baud(gs_serial, MBED_CONF_PLATFORM_STDIO_BAUD_RATE);

    // Set up tracing.
    mbed_trace_init();
    mbed_trace_print_function_set(serial_print);

    // Set up microsecond ticker.
    us_ticker = bsp_us_ticker_get_default_instance();
    bsp_us_ticker_init(us_ticker);
    mdh_us_ticker_set_irq_handler(us_ticker, irq_handler);

    // Get a reference tick.
    const uint32_t c_ref_tick = mdh_us_ticker_read(us_ticker);
    const uint32_t c_irq_tick = c_ref_tick + TIMEOUT_IN_TICKS;
    const uint32_t c_last_wait_tick = c_ref_tick + 2 * TIMEOUT_IN_TICKS;
    record_ticker_state(c_ref_tick, 0xff);

    // Set interrupt. Reset the interrupt counter to 0.
    core_util_atomic_store_u8(&gsv_irq_count, 0);
    mdh_us_ticker_set_interrupt(us_ticker, c_irq_tick);
    record_ticker_state(mdh_us_ticker_read(us_ticker), 0);

    // Wait, poll ticker & record states.
    uint32_t curr_tick = 0UL;
    while (curr_tick < c_last_wait_tick) {
        curr_tick = mdh_us_ticker_read(us_ticker);
        record_ticker_state(curr_tick, core_util_atomic_load_u8(&gsv_irq_count));
    }

    // Dump all info.
    tr_debug("ref_tick=%10" PRIu32, c_ref_tick);
    tr_debug("irq_tick=%10" PRIu32, c_irq_tick);
    tr_debug("last_wait_tick=%10" PRIu32, c_last_wait_tick);
    dump_recorded_ticker_states();

    while (1) {
    };
}

void irq_handler(mdh_ticker_t *us_ticker)
{
    mdh_us_ticker_clear_interrupt(us_ticker);
    core_util_atomic_incr_u8(&gsv_irq_count, 1);
}

void record_ticker_state(uint32_t ticker_counter, uint8_t irq_counter)
{
    if (gs_num_samples < NUM_STATE_SAMPLES) {
        gs_ticker_states[gs_num_samples].ticker_counter = ticker_counter;
        gs_ticker_states[gs_num_samples++].irq_counter = irq_counter;
    }
}

void dump_recorded_ticker_states(void)
{
    for (size_t i = 0; i < gs_num_samples; i++) {
        tr_info("%10" PRIu32 ", %2" PRIu8, gs_ticker_states[i].ticker_counter, gs_ticker_states[i].irq_counter);
    }
}

void serial_print(const char *const string)
{
    const char *c = string;
    while (*c != '\0') {
        mdh_serial_put_data(gs_serial, *c);
        c++;
    }
    mdh_serial_put_data(gs_serial, '\r');
    mdh_serial_put_data(gs_serial, '\n');
}
