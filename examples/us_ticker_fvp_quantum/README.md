# Microsecond ticker and the FVP quantum param example

Run this example with the Fast Models FVP to see the effects of different `--quantum N` param values on the microsecond ticker performance.

Note the values of the `[INFO][main]` traces.

```log
[INFO][main]: <ticker-counter>, <ticker-interrupt-counter>
```

They show `<ticker-counter>, <ticker-interrupt-counter>` on each line. The interrupt is set to fire at `<irq_tick>` value.

The default quantum value is `10000 ticks`. You should see a step-wise increase of the ticker counter value. Try running the same ELF with a decreased quantum number to see a behavior that resembles execution on the actual hardware.

```bash
./FVP_Corstone_SSE-300_Ethos-U55 --application <mcu-driver-reference-platforms-for-arm-build-dir>/mdh_arm/examples/us_ticker_fvp_quantum/mdh-arm-hal-example-us_ticker_fvp_quantum.elf --quantum 100
```
