/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal/gpio_api.h"
#include "hal/spi_api.h"
#include "example_spi.h"
#include "mps3_io.h"
#include "mps3_spi.h"

int main(void)
{
    mps3_spi_t *spi_interface = NULL;
    mps3_spi_init(&spi_interface, &SPI_SHIELD0_PL022_DEV_NS);

    mps3_io_t n_chip_select;
    mps3_io_init(&n_chip_select, &GPIO0_CMSDK_DEV, mps3_spi_get_cs_pin(&SPI_SHIELD0_PL022_DEV_NS));

    example_spi(&(n_chip_select.gpio), &(spi_interface->spi));

    while (1) {
    }
}
