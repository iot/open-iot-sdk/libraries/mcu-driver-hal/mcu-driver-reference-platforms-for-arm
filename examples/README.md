# MCU-Driver-HAL Examples

This directory contains the build instructions for the examples defined in the MCU-Driver-HAL repository.

Each example build instructions provides access to hardware specific source code and link with the example main algorithm provided as a library that is linked to the executable.

For details of the examples main algorithm see each example [here][mdh_examples].

## Building the example

Each application can be built from the root of the project by doing the following:
1. Clone the repository recursively to get the `mcu-driver-hal` submodule:
    ```
    $ git clone --recursive <HostingLocation>/mcu-driver-hal/mcu-driver-reference-platforms-for-arm
    ```
1. Change the current working directory to `mcu-driver-reference-platforms-for-arm/`
1. Run:
    ```
    $ cmake -S . -B cmake_build -DCMAKE_BUILD_TYPE=debug --toolchain=components/mcu-driver-hal/tools/cmake/toolchain/toolchain-{armclang|arm-none-eabi-gcc}.cmake -DCMAKE_SYSTEM_PROCESSOR=cortex-m55
    ```
1. Run:
    ```
    $ cmake --build cmake_build --target EXAMPLE_CMAKE_EXECUTABLE_NAME
    ```
    where `EXAMPLE_CMAKE_EXECUTABLE_NAME` is the name of the CMake executable for the example you would like to build.

Outcome: The application compiles, links and produces artefacts.

## Program the artefact

Copy the artefact located in `mcu-driver-reference-platforms-for-arm/cmake_build/mdh_arm/examples/<INTERFACE>/` following the steps in [Loading custom application][fvp_cs300_oob_mps3_loading].

[fvp_cs300_oob_mps3_loading]: ../docs/fpga/corstone-300/out_of_box-mps3.md#loading-custom-application
[mdh_examples]: https://gitlab.arm.com/iot/open-iot-sdk/mcu-driver-hal/mcu-driver-hal/-/blob/main/examples/
