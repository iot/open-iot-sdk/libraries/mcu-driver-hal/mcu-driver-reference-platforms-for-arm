/* Copyright (c) 2022-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "example_gpio_input.h"

#include "hal/gpio_api.h"
#include "PinNames.h"
#include "mps3_io.h"
#include "platform/busy_wait.h"

#define NUMBER_OF_LEDS 10U

static void one_second_delay(void);

int main(void)
{
    mps3_io_t led_button;
    mps3_io_init(&led_button, &MPS3_IO_DEV_NS, USERPB1);
    mdh_gpio_set_direction(&(led_button.gpio), MDH_GPIO_DIRECTION_IN);
    mdh_gpio_set_mode(&(led_button.gpio), MDH_GPIO_MODE_PULL_NONE);

    PinName pins[NUMBER_OF_LEDS] = {[0] = LED1,
                                    [1] = LED2,
                                    [2] = LED3,
                                    [3] = LED4,
                                    [4] = LED5,
                                    [5] = LED6,
                                    [6] = LED7,
                                    [7] = LED8,
                                    [8] = USERLED1,
                                    [9] = USERLED2};

    mps3_io_t leds[NUMBER_OF_LEDS];

    for (size_t i = 0; i < NUMBER_OF_LEDS; i++) {
        mps3_io_init(&(leds[i]), &MPS3_IO_DEV_NS, pins[i]);
    }

    mdh_gpio_t *led_gpios[NUMBER_OF_LEDS] = {[0] = &(leds[0].gpio),
                                             [1] = &(leds[1].gpio),
                                             [2] = &(leds[2].gpio),
                                             [3] = &(leds[3].gpio),
                                             [4] = &(leds[4].gpio),
                                             [5] = &(leds[5].gpio),
                                             [6] = &(leds[6].gpio),
                                             [7] = &(leds[7].gpio),
                                             [8] = &(leds[8].gpio),
                                             [9] = &(leds[9].gpio)

    };

    while (1) {
        example_gpio_input(&(led_button.gpio), led_gpios, NUMBER_OF_LEDS, one_second_delay);
    }
}

static void one_second_delay(void)
{
    wait_us(1000000U);
}
