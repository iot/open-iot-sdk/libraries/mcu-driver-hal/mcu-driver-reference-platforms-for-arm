/* Copyright (c) 2020-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_CONF_MBED_TRACE_ENABLE 1
#define TRACE_GROUP                 "main"

#include "hal/serial_api.h"
#include CMSIS_device_header
#include "fvp_sai.h"
#include "mbed_trace/mbed_trace.h"
#include "mps3_uart.h"

#include <stdint.h>

/* TODO: This example requires rather big dependencies for signal processing
 * (CMSIS-DSP) and Neural-network processing (tinyml and associated).
 */

typedef enum demo_state_e { DEMO_STATE_INIT, DEMO_STATE_RUNNING, DEMO_STATE_INTERRUPTED, DEMO_STATE_DONE } demo_state_t;

static volatile demo_state_t gsv_state = DEMO_STATE_INIT;
static mdh_serial_t *gs_serial = NULL;

static void serial_print(const char *string)
{
    while (*string != 0) {
        mdh_serial_put_data(gs_serial, *string);
        string++;
    }
    mdh_serial_put_data(gs_serial, '\r');
    mdh_serial_put_data(gs_serial, '\n');
}

static void sai_handler(mdh_sai_t *self, void *ctx, mdh_sai_transfer_complete_t code)
{
    if (gsv_state == DEMO_STATE_RUNNING) {
        tr_info("Handler triggered: self=%p ctx=%p code=%u", self, ctx, code);
        gsv_state = DEMO_STATE_INTERRUPTED;
    }
}

int main()
{
    static uint16_t samples[1024];

    mps3_uart_init(&gs_serial, &UART0_CMSDK_DEV_NS);
    mdh_serial_set_baud(gs_serial, 115200);
    mbed_trace_init();
    mbed_trace_print_function_set(serial_print);

    fvp_sai_t *pfvpsai = fvp_sai_init(1, 16, 1600, 16);
    if (NULL == pfvpsai) {
        tr_error("Failed to initialize RX VSI");
        while (true) {
            __WFE();
        }
    }

    tr_info("VSI initialized");
    mdh_sai_t *psai = &pfvpsai->sai;
    mdh_sai_set_transfer_complete_callback(psai, sai_handler);

    tr_info("VSI initiate transfer");
    gsv_state = DEMO_STATE_RUNNING;
    mdh_sai_transfer(psai, (uint8_t *)samples, sizeof(samples), NULL);

    while (gsv_state == DEMO_STATE_RUNNING) {
        __NOP();
    }

    tr_info("Done");
    gsv_state = DEMO_STATE_DONE;
    while (1) {
        __NOP();
    }
}
