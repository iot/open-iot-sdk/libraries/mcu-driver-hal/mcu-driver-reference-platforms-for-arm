# Copyright (c) 2021-2022 Arm Limited
# SPDX-License-Identifier: Apache-2.0

#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at

#      http://www.apache.org/licenses/LICENSE-2.0

#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

default:
  # cancel the job if a new pipeline is triggered on the same branch
  interruptible: true
  image: ${OPEN_IOT_SDK_DOCKER_REGISTRY}/open-iot-sdk:${OPEN_IOT_SDK_DOCKER_VERSION}

workflow:
  rules:
    - when: always

include:
  - local: .gitlab/ci/pipeline/.job_templates.yml

stages:
  - build
  - run

Build ARMC6:
  tags:
    - iotmsw-amd64
  stage: build
  variables:
    TOOLCHAIN: toolchains/toolchain-armclang.cmake
  parallel:
    matrix:
      - TARGET: [ARM_AN552_MPS3]
        PROCESSOR: [cortex-m55]
        VARIANT: [FPGA]
        HTRUN_ARGUMENTS: ["--verbose;--port=/serial;--baud-rate=115200;--copy=pyocd;--reset=pyocd;--target-id=LAVA_BOARD_ID"]
        # lava job is prepared in build job. Hence, optional argument such as
        # tests to skip should be specified in build job
        TEST_TO_SKIP: 'mdh-arm-test-emac|fpga_ci_test_shield|mdh-arm-test-sai-loopback'
      - TARGET: [ARM_AN552_MPS3]
        PROCESSOR: [cortex-m55]
        VARIANT: [FVP]
        HTRUN_ARGUMENTS: ["--micro=FVP_CS300_U55;--fm=MPS3"]
      - TARGET: [ARM_AN555_MPS3]
        VARIANT: [FVP]
        PROCESSOR: [cortex-m85]
        HTRUN_ARGUMENTS: ["--micro=FVP_CS310;--fm=MPS3"]
      - TARGET: [ARM_AN386_MPS2]
        VARIANT: [FVP]
        PROCESSOR: [cortex-m4]
        HTRUN_ARGUMENTS: ["--micro=FVP_MPS2_M4;--fm=MPS2"]
  extends: .build

Run ARMC6 FVP AN552:
  tags:
    - iotmsw-amd64
  stage: run
  variables:
    TEST_TO_SKIP: 'mdh-arm-test-emac'
  parallel:
    matrix:
      - TARGET: [ARM_AN552_MPS3]
        VARIANT: [FVP]
  extends: .run_fvp

Run ARMC6 FVP AN555:
  tags:
    - iotmsw-amd64
  stage: run
  variables:
    TEST_TO_SKIP: 'mdh-arm-test-emac'
  parallel:
    matrix:
      - TARGET: [ARM_AN555_MPS3]
        VARIANT: [FVP]
  extends: .run_fvp

Run ARMC6 FVP AN386:
  tags:
    - iotmsw-amd64
  stage: run
  variables:
    TEST_TO_SKIP: 'mdh-arm-test-emac|mdh-arm-test-flash-functional_tests'
  parallel:
    matrix:
      - TARGET: [ARM_AN386_MPS2]
        VARIANT: [FVP]
  extends: .run_fvp

Run ARMC6 FPGA:
  tags:
    - iotmsw-amd64
  stage: run
  parallel:
    matrix:
      - TARGET: [ARM_AN552_MPS3]
        VARIANT: [FPGA]
  extends: .run_fpga
